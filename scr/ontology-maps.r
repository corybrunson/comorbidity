# setup
rm(list = ls())
library(icd)
library(dplyr)
setwd("~/Documents/CQM/comorbidity")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/ontology/"

# initialize ontology maps list
ontology_maps <- list(
  "ICD9-5" = list()
)

# incorporate maps from 'icd' package

# load CCS data
load(paste0(sdir, "ccs.rda"))
ccs_dat <- filter(ccs_dat, icd9_type == "DX")
# level-3 prefixes
icd9_prefix <- ccs_dat %>%
  select(icd9) %>%
  mutate(icd9_3 = substr(icd9, 1, 3))
# append to ontology list
ontology_maps[["ICD9-5"]] <- c(ontology_maps[["ICD9-5"]], list(
  "ICD9-3" = select(icd9_prefix, source = icd9, target = icd9_3),
  "CCS-single" = select(ccs_dat, source = icd9, target = ccs_code_single),
  "CCS-multi" = select(ccs_dat, source = icd9, target = ccs_code_multi)
))

# Rzhetsky phenotype ontology

# read text file copy/pasted from SI
rzhetsky <- readLines("data/ontology/Rzhetsky-phenotypes.txt") %>%
  matrix(nrow = 2) %>% t() %>% as_tibble() %>%
  setNames(c("target", "source")) %>%
  mutate(source = stringr::str_sub(source, start = 2, end = -1)) %>%
  mutate(source = strsplit(source, split = ", ")) %>%
  tidyr::unnest(source) %>%
  mutate(source = gsub(" ", "", source))
# append to ontology list
ontology_maps[["ICD9-5"]] <- c(ontology_maps[["ICD9-5"]], list(
  "Rzhetsky" = select(rzhetsky, source, target)
))

save(ontology_maps, file = paste0(sdir, "ontology-maps.rda"))
