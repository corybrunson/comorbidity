rm(list = ls())
setwd("~/Documents/CQM/nascent/comorbidity/")

# packages
pkgs <- c("rvest")
for (i in which(!(pkgs %in% installed.packages()))) {
  install.packages(pkgs[i])
}
for (pkg in pkgs) library(pkg, character.only = TRUE)

# ICD-6

# 3-digit codes
url3 <- "http://www.wolfbane.com/icd/icd6.htm"
html3 <- url3 %>% read_html() %>% html_nodes("pre") %>% html_text()
txt3 <- strsplit(html3, split = "\n")[[1]]
txt3 <- txt3[txt3 != ""]
# diagnoses and groups
diags3 <- data.frame(
  code = gsub("^([A-Z]{0,1}[0-9]{2,3}) .*$", "\\1", txt3[!grepl("^\\(", txt3)]),
  description = gsub("^[A-Z]{0,1}[0-9]{2,3} (.*)$", "\\1",
                     txt3[!grepl("^\\(", txt3)])
)
grps3 <- data.frame(
  range = gsub("(^\\([^\\)]*\\)) .*$", "\\1", txt3[grep("^\\(", txt3)]),
  group = gsub("^\\([^\\)]*\\) (.*$)", "\\1", txt3[grep("^\\(", txt3)])
)

# 4-digit codes
url4 <- "http://www.wolfbane.com/icd/icd6h.htm"

# save
save(diags3, grps3, file = "data/codes/icd6.RData")

# ICD-9

# 3-digit codes
url3 <- "http://www.wolfbane.com/icd/icd9.htm"
html3 <- url3 %>% read_html() %>% html_nodes("pre") %>% html_text()
txt3 <- strsplit(html3, split = "\n")[[1]]
txt3 <- txt3[txt3 != ""]
# diagnoses and groups
diags3 <- data.frame(
  code = gsub("^([A-Z]{0,1}[0-9]{2,3}) .*$", "\\1", txt3[!grepl("^\\(", txt3)]),
  description = gsub("^[A-Z]{0,1}[0-9]{2,3} (.*)$", "\\1",
                     txt3[!grepl("^\\(", txt3)])
)
grps3 <- data.frame(
  range = gsub("(^\\([^\\)]*\\)) .*$", "\\1", txt3[grep("^\\(", txt3)]),
  group = gsub("^\\([^\\)]*\\) (.*$)", "\\1", txt3[grep("^\\(", txt3)])
)
# add group index column to diagnosis data frame
grps3 <- grps3 %>%
  mutate(codelen = ifelse(nchar(as.character(range)) == 9, 3, 4)) %>%
  mutate(min = substr(range, 2, 1 + codelen),
         max = substr(range, 3 + codelen, 2 + 2 * codelen))
diags3$group <- grps3$group[sapply(diags3$code, function(x) {
  if (grepl("^[0-9]", x)) {
    intersect(intersect(
      which(as.numeric(grps3$min) <= as.numeric(as.character(x))),
      which(as.numeric(grps3$max) >= as.numeric(as.character(x)))
    ), grep("^[0-9]", grps3$min))
  } else {
    intersect(intersect(
      which(as.numeric(substring(grps3$min, 2)) <= as.numeric(substring(x, 2))),
      which(as.numeric(substring(grps3$max, 2)) >= as.numeric(substring(x, 2)))
    ), grep(paste0("^", substr(x, 1, 1)), grps3$min))
  }
})]

# 4-digit codes
url4 <- "http://www.wolfbane.com/icd/icd9h.htm"

# save
save(diags3, grps3, file = "data/codes/icd9.RData")
