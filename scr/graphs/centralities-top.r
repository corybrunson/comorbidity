# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(tidyr)
library(icd)
source("R/utils.r")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

load(paste0(sdir, "node-centrality.RData"))
load(paste0(sdir, "group-centrality.RData"))

# combine node and group centrality data
bind_rows(
  node_centrality %>%
    left_join(
      source_info %>%
        select(source, ontology) %>%
        rename(dataset = source),
      by = "dataset"
    ) %>%
    mutate(type = "node") %>%
    rename(concept = node),
  group_centrality %>%
    mutate(type = "group") %>%
    rename(concept = group, ontology = map)
) %>%
  print() -> centrality

# ensure that all p-value corrections are imposed in all cases
centrality %>%
  select(correction, dataset, type) %>%
  count(correction, dataset, type) %>%
  spread(correction, n) %>%
  print()
# restrict to false discovery rate correction
centrality %>%
  filter(correction == "fdr") %>%
  select(-correction) %>%
  print() -> centrality

# standardize the centrality measures
centrality %>%
  # restrict to appropriate datasets
  filter(!grepl("(namcs)|(vaers)", dataset)) %>%
  mutate(dataset = forcats::fct_inorder(factor(dataset))) %>%
  # prepare centrality statistics
  group_by(dataset) %>%
  mutate(nv = n()) %>%
  ungroup() %>%
  mutate(
    dcent = as.integer(dcent),
    bcent = as.integer(bcent),
    ccent = ccent * (nv - 1)
  ) %>%
  select(-nv) %>%
  print() -> centrality

# augment ICD codes with English terms
centrality %>%
  pull(dataset) %>%
  unique() %>%
  lapply(function(dat) lapply(
    centrality %>%
      filter(dataset == dat) %>% pull(ontology) %>% unique(), function(ont) {
        if (ont == "Rzhetsky") return(NULL)
        centrality %>%
          filter(dataset == dat & ontology == ont) %>%
          pull(concept) %>% as.character() %>%
          cbind(
            concept = .,
            icd_explain_table(.)
          ) %>%
          mutate(valid_icd = valid_icd9 | valid_icd10) %>%
          select(concept, code, valid_icd, short_desc) %>%
          mutate(dataset = dat, ontology = ont)
      }
  )) %>%
  unlist(recursive = FALSE) %>%
  bind_rows() %>%
  as_tibble() %>%
  print() -> concept_explanations

# ...
centrality %>%
  left_join(
    concept_explanations %>% select(-valid_icd),
    by = c("dataset", "concept", "ontology")
  ) %>%
  left_join(
    concept_explanations %>%
      group_by(dataset) %>%
      summarize(valid_icd = all(valid_icd, na.rm = TRUE)) %>%
      ungroup(),
    by = "dataset"
  ) %>%
  mutate(code = ifelse(valid_icd & ontology != "Rzhetsky", concept, "")) %>%
  mutate(description = ifelse(
    valid_icd & ontology != "Rzhetsky",
    short_desc, concept
  )) %>%
  select(-short_desc, -valid_icd) %>%
  rename(degree = dcent, closeness = ccent, betweenness = bcent) %>%
  gather("measure", "centrality", degree, closeness, betweenness) %>%
  print() -> centrality

# print tables
for (cent_meas in unique(centrality$measure)) {
  for (dat_ont in c("Rzhetsky", "ICD9-3")) {
    centrality %>%
      filter(measure == cent_meas & ontology == dat_ont & centrality != 0) %>%
      group_by(dataset) %>%
      top_n(if (dat_ont == "Rzhetsky") 10 else 12, wt = centrality) %>%
      ungroup() %>%
      mutate(description = abbreviate(description, minlength = 72)) %>%
      left_join(
        source_info %>%
          select(dataset = source, dataset_abbr = abbr),
        by = "dataset"
      ) %>%
      mutate(dataset = factor(dataset, levels = source_info$source)) %>%
      arrange(dataset, desc(centrality)) %>%
      select(
        Dataset = dataset_abbr,
        Code = code,
        Description = description,
        Centrality = centrality
      ) %>%
      { if (dat_ont == "Rzhetsky") select(., -Code) else . } %>%
      xtable::xtable(
        caption = paste0("Most ", cent_meas,
                         "-central disorders in comorbidity networks ",
                         "using the ", dat_ont, " ontology, ",
                         "subject to Benjamini-Hochberg-corrected 5\\% FDR."),
        label = paste0("tab:centrality-", cent_meas, "-", dat_ont),
        digits = 3,
        display = c(rep("s", if (dat_ont == "Rzhetsky") 3 else 4), "fg")
      ) %>%
      xtable::print.xtable(
        file = paste0("tab/centrality-", cent_meas, "-", dat_ont, ".tex"),
        size = "\\scriptsize",
        hline.after = c(-1, which(!duplicated(.$Dataset)) - 1, nrow(.)),
        table.placement = "H",
        include.rownames = FALSE,
        caption.placement = "top"
      )
  }
}

for (i in seq_along(unique(centrality$measure))) {
  cent_meas <- as.character(unique(centrality$measure)[i])
  centrality %>%
    filter(measure == cent_meas & type == "node" & centrality != 0) %>%
    group_by(dataset) %>%
    top_n(8, wt = centrality) %>%
    ungroup() %>%
    mutate(description = abbreviate(description, minlength = 72)) %>%
    left_join(
      source_info %>%
        select(dataset = source, dataset_abbr = abbr),
      by = "dataset"
    ) %>%
    mutate(dataset = factor(dataset, levels = source_info$source)) %>%
    arrange(dataset, desc(centrality)) %>%
    select(
      Dataset = dataset_abbr,
      Code = code,
      Description = description,
      Centrality = centrality
    ) %>%
    xtable::xtable(
      caption = paste0("Most ", cent_meas,
                       "-central disorders in each comorbidity network, ",
                       "subject to Benjamini-Hochberg-corrected 5\\% FDR."),
      label = paste0("tab:centrality-", cent_meas),
      digits = 3, display = c("s", "s", "s", "s", "fg")
    ) %>%
    xtable::print.xtable(
      file = paste0("tab/centrality-", cent_meas, ".tex"),
      size = "\\scriptsize",
      hline.after = c(-1, which(!duplicated(.$Dataset)) - 1, nrow(.)),
      table.placement = "H",
      include.rownames = FALSE,
      caption.placement = "top"
    )
}
