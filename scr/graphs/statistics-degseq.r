#' Analyze the degree sequences of the comorbidity networks, in particular
#' assessing whether they evidently belong or do not belong to the power-law
#' family, and select a preferred family whose best-fit parameters will be used
#' in the analysis of global single-value network statistics.

# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(igraph)
library(poweRlaw)
source("R/graph-construction.r")
source("R/utils.r")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/graphs/"

# restrict to representative datasets
source_info <- source_info %>%
  filter(!grepl("namcs|vaers", file))

# binary association measures
measures <- c("oddsratiommle", "phi", "forbes", "rho_psych")
# density quantiles
quantiles <- seq(.1, .9, .1)

# number of cores available for parallelization
n_cores <- parallel::detectCores()
# statistics to pull from `poweRlaw::compare_distributions()`
compare_stats <- c("test_statistic", "p_one_sided", "p_two_sided")
# families of distributions to compare
fams <- c("pois", "pl", "exp", "lnorm")

# function to compare goodness of fit across families for a degree sequence `x`
compare_families <- function(x) {
  # Poisson
  pois_fit <- dispois$new(x)
  pois_fit$setXmin(estimate_xmin(pois_fit))
  #pois_fit$setPars(estimate_pars(pois_fit))
  #pois_boot <- bootstrap(pois_fit, no_of_sims = 100, threads = n_cores)
  # power law
  pl_fit <- displ$new(x)
  pl_fit$setXmin(estimate_xmin(pl_fit))
  #pl_fit$setPars(estimate_pars(pl_fit))
  #pl_boot <- bootstrap(pl_fit, no_of_sims = 100, threads = n_cores)
  # exponential
  exp_fit <- disexp$new(x)
  exp_fit$setXmin(estimate_xmin(exp_fit))
  #exp_fit$setPars(estimate_pars(exp_fit))
  #exp_boot <- bootstrap(exp_fit, no_of_sims = 100, threads = n_cores)
  # log-normal
  lnorm_fit <- dislnorm$new(x)
  lnorm_fit$setXmin(estimate_xmin(lnorm_fit))
  #lnorm_fit$setPars(estimate_pars(lnorm_fit))
  #lnorm_boot <- bootstrap(lnorm_fit, no_of_sims = 100, threads = n_cores)
  # compare families to to each other based on the minimum estimated `xmin`
  xmin_min <- min(c(
    pois_fit$getXmin(),
    pl_fit$getXmin(),
    exp_fit$getXmin(),
    lnorm_fit$getXmin()
  ))
  pois_fit$setXmin(xmin_min)
  pois_fit$setPars(estimate_pars(pois_fit))
  pl_fit$setXmin(xmin_min)
  pl_fit$setPars(estimate_pars(pl_fit))
  exp_fit$setXmin(xmin_min)
  exp_fit$setPars(estimate_pars(exp_fit))
  lnorm_fit$setXmin(xmin_min)
  lnorm_fit$setPars(estimate_pars(lnorm_fit))
  comp_mat <- combn(fams, 2)
  comp_res <- apply(comp_mat, 2, function(col) {
    res <- try(compare_distributions(
      get(paste0(col[1], "_fit")), get(paste0(col[2], "_fit"))
    ))
    if (inherits(res, "try-error")) return(rep(NA, 3))
    unlist(res[compare_stats])
  })
  cbind(
    setNames(as.data.frame(t(comp_mat)), paste0("family_", 1:2)),
    setNames(as.data.frame(t(comp_res)), compare_stats)
  )
}

# network parameters to include in the comparison
alphas <- 10 ^ c(-2, -5) # full study range 10 ^ c(-1, -6)
corrections <- c("none", "fwer", "fdr")

# subfolder for degree distribution files
if (! file.exists(paste0(tdir, "tail-estimation"))) {
  dir.create(paste0(tdir, "tail-estimation"))
}

# compare goodness of fit for each network
compare_dat <- tibble()
for (i in seq_along(source_info$file)) {
  f <- source_info$file[i]
  print(as.character(f))
  # load data
  d <- readRDS(paste0(sdir, f))
  # sample size (number of patients)
  n <- unique(d$n)
  # (number of) distinct diseases
  v <- as.character(unique(unlist(d[, 1:2])))
  m <- length(v)
  # iterate over evidential thresholds
  for (alpha in alphas) {
    for (correction in corrections) {
      print(c(
        as.character(f), format(alpha, scientific = FALSE), correction, "unit"
      ))
      # construct graph
      g <- graph_from_pairs_data(
        data = d,
        pval_cutoff = alpha, pval_correction = correction,
        weighted = FALSE
      )
      # degree sequence
      deg_seq <- degree(g)[degree(g) > 0]
      # degree distribution in matrix form
      deg_dist <- unname(as.matrix(as.data.frame(table(deg_seq))))
      mode(deg_dist) <- "numeric"
      # save to file in subfolder
      write.table(
        deg_dist,
        paste0(
          tdir, "tail-estimation/",
          as.character(source_info$source[i]), "-",
          gsub("\\.", "\\_", format(alpha, scientific = FALSE)), "-",
          correction, ".dat"
        ),
        sep = " ", row.names = FALSE, col.names = FALSE
      )
      # sample if necessary to limit computation time
      if (length(deg_seq) > 1680) {
        deg_seq <- deg_seq[sample(length(deg_seq), size = 1680, replace = FALSE)]
      }
      # test fits of three major families to degree sequence
      compare_dat <- rbind(
        compare_dat,
        transform(
          compare_families(deg_seq),
          source = as.character(source_info$source[i]),
          pval_threshold = alpha,
          pval_correction = correction
        )
      )
    }
  }
}

saveRDS(compare_dat, file = paste0(tdir, "graphs-degseq.rds"))
