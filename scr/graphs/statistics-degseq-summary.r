#' Summarize the results of the analysis of degree sequence shapes in
#' `statistics-degseq.r`.

rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(tibble)
library(dplyr)
library(tidyr)
library(ggplot2)
source("R/utils.r")

sdir <- "~/Documents/CQM/comorbidity/data/graphs/"

Fams <- c(
  "Poisson" = "pois",
  "power law" = "pl",
  "exponential" = "exp",
  "log-normal" = "lnorm"
)
readRDS(paste0(sdir, "graphs-degseq.rds")) %>%
  as_tibble() %>%
  rename(pval = pval_threshold, corr = pval_correction) %>%
  mutate_at(vars(starts_with("fam")), funs(factor(
    names(Fams)[match(as.character(.), unname(Fams))],
    levels = names(Fams)
  ))) %>%
  mutate(corr = ifelse(corr == "none", as.character(corr), toupper(corr))) %>%
  mutate(corr = forcats::fct_inorder(corr)) %>%
  print() -> compare_dat

compare_dat %>%
  left_join(select(source_info, source, abbr), by = "source") %>%
  select(-source) %>%
  select(-p_one_sided) %>%
  mutate(pval = format(pval, scientific = FALSE)) %>%
  select(abbr, pval, everything()) %>%
  gather(key = "var", value = "val", test_statistic, p_two_sided) %>%
  unite("id", pval, corr, var) %>%
  spread(key = id, value = val) %>%
  arrange(abbr, family_1, family_2) %>%
  select(1:3, 15:4) %>%
  rlang::set_names(c(
    "Dataset", "Family 1", "Family 2",
    paste0(
      rep(c("\\(R\\) ", "\\(p\\) "), times = 6),
      rep(paste0("(\\(10^{-", c(2, 5), "}\\)"), each = 6),
      rep(paste0(c("", ", B", ", B-H"), ")"), each = 2, times = 2),
      sep = ""
    )
  )) %>%
  xtable::xtable(
    label = "tab:degseq-compare",
    caption = paste0(
      "Log-likelihood ratios $R$ and two-sided p-values $p$ ",
      "from likelihood-ratio tests between four families of models ",
      "to degree sequence tails. ",
      "LRTs were performed for graphs constructed from each dataset ",
      "using two p-value significance thresholds and corrections ",
      "for family-wise error rate and for false discovery rate. ",
      "Family 1 is preferred when $R>0$, Family 2 when $R<0$."
    ),
    align = c("r", rep("l", 3), rep("r", 12)),
    digits = c(0, rep(0, 3), rep(c(1, 3), times = 6))
  ) %>%
  xtable::print.xtable(
    file = "tab/degseq-compare.tex",
    size = "scriptsize",
    caption.placement = "top",
    hline.after = c(-1, which(!duplicated(.$Dataset)) - 1, nrow(.)),
    include.rownames = FALSE,
    rotate.colnames = TRUE,
    sanitize.colnames.function = identity
  )

# https://stackoverflow.com/a/18530540/4556798
scientific_10 <- function(x) {
  gsub("e", "%*%10^", scales::scientific_format(digits = 2)(x))
}
degseq_compare_plot <- compare_dat %>%
  mutate(lrt = test_statistic / abs(test_statistic)) %>%
  left_join(source_info, by = "source") %>%
  ggplot(aes(x = family_1, y = family_2)) +
  theme_minimal() +
  facet_grid(abbr ~ pval + corr,
             labeller = as_labeller(label_value, multi_line = FALSE)) +
  coord_fixed() +
  geom_tile(aes(fill = test_statistic, color = lrt), size = 1) +
  scale_fill_distiller(name = "R", limits = c(-30, 30), type = "div") +
  scale_color_distiller(type = "div") +
  guides(color = FALSE) +
  geom_text(aes(label = scientific_10(p_two_sided)), parse = TRUE, size = 3) +
  labs(x = "Family 1", y = "Family 2") +
  ggtitle(
    "Likelihood-ratio tests between model families fit to degree sequence tails"
  )
print(degseq_compare_plot)
ggsave(
  degseq_compare_plot,
  height = 18, width = 16,
  filename = "fig/degseq-compare.pdf"
)
