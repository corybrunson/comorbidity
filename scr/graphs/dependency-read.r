# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(igraph)
library(ffbase)
source("R/utils.r")
source("R/graph-construction.r")
source("R/graph-dependency.r")
# a named list of named lists of data frames
load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM0/comorbidity/data/dependency/"
tdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

thetas <- c(".05", "B-.05")

# save tables of group betweenness centralities
group_betweenness <- tibble()
for (i in 1:nrow(source_info)) for (j in 1:length(thetas)) {
  # identify the source ontology
  sub_map_info <- map_info %>%
    filter(ontology == source_info$ontology[i])
  if (nrow(sub_map_info) == 0) next
  # load the file (if it exists)
  dc_file <- paste0(
    sdir,
    "dependencycube-",
    source_info$source[i], "-",
    gsub("\\.|-", "", thetas[j]),
    ".csv"
  )
  if (!file.exists(dc_file)) next
  dc <- read.csv.ffdf(file = dc_file)
  # load the graph for the node indices
  load(file = gsub("\\.csv", "-graph\\.rda", dc_file))
  nodes <- tibble(
    name = gsub("\\.", "", vertex_attr(g, "name")),
    id = as.integer(V(g))
  )
  # for each coarsening map
  for (k in 1:nrow(sub_map_info)) {
    # augment the map with integer codes
    map <- ontology_maps[[sub_map_info$ontology[k]]][[sub_map_info$map[k]]] %>%
      as_tibble() %>%
      mutate_all(as.character) %>%
      inner_join(nodes, by = c("source" = "name")) %>%
      mutate(
        source_int = id,
        target_int = as.integer(as.factor(target))
      ) %>%
      select(-id)
    map_int <- map %>% select(source_int, target_int) %>% as.ffdf()
    # calculate betweenness centrality
    dc$broker_prop <- dc$broker_count / dc$geodesic_count
    dc <- merge(dc, map_int, by.x = "broker", by.y = "source_int")
    btw_fun <- function(x) {
      x %>%
        group_by(target_int) %>%
        summarize(
          source_count = n_distinct(broker),
          betweenness_quot_sum = sum(broker_prop, na.rm = TRUE) / 2
        )
    }
    map_betweenness <- ffdfdply(dc, dc$target_int, btw_fun) %>%
      as_tibble() %>%
      left_join(
        map %>% select(target, target_int) %>% distinct()
      ) %>%
      select(-target_int) %>%
      arrange(desc(betweenness_quot_sum)) %>%
      mutate(
        ontology = sub_map_info$ontology[k],
        map = sub_map_info$map[k]
      )
    # bind to the overall dataset
    group_betweenness <- bind_rows(group_betweenness, map_betweenness)
  }
}
save(group_betweenness, file = "data/sensitivity/group-betweenness.rda")

# save tables of group closeness centralities
group_closeness <- tibble()
for (i in 1:nrow(source_info)) for (j in 1:length(thetas)) {
  # identify the source ontology
  sub_map_info <- map_info %>%
    filter(ontology == source_info$ontology[i])
  if (nrow(sub_map_info) == 0) next
  # load the file (if it exists)
  dc_file <- paste0(
    sdir,
    "dependencycube-",
    source_info$source[i], "-",
    gsub("\\.|-", "", thetas[j]),
    ".csv"
  )
  if (!file.exists(dc_file)) next
  dc <- read.csv.ffdf(file = dc_file)
  # load the graph for the node indices
  load(file = gsub("\\.csv", "-graph\\.rda", dc_file))
  nodes <- tibble(
    name = gsub("\\.", "", vertex_attr(g, "name")),
    id = as.integer(V(g))
  )
  # for each coarsening map
  for (k in 1:nrow(sub_map_info)) {
    # augment the map with integer codes
    map <- ontology_maps[[sub_map_info$ontology[k]]][[sub_map_info$map[k]]] %>%
      as_tibble() %>%
      mutate_all(as.character) %>%
      inner_join(nodes, by = c("source" = "name")) %>%
      mutate(
        source_int = id,
        target_int = as.integer(as.factor(target))
      ) %>%
      select(-id)
    map_int <- map %>% select(source_int, target_int) %>% as.ffdf()
    # calculate closeness centrality
    dc$broker_prop <- dc$broker_count / dc$geodesic_count
    dc <- merge(dc, map_int, by.x = "sender", by.y = "source_int")
    clo_fun <- function(x) {
      x %>%
        group_by(target_int) %>%
        summarize(
          source_count = n_distinct(sender),
          closeness_quot_sum = 1 / sum(broker_prop, na.rm = TRUE)
        )
    }
    map_closeness <- ffdfdply(dc, dc$target_int, clo_fun) %>%
      as_tibble() %>%
      left_join(
        map %>% select(target, target_int) %>% distinct()
      ) %>%
      select(-target_int) %>%
      arrange(desc(closeness_quot_sum)) %>%
      mutate(
        ontology = sub_map_info$ontology[k],
        map = sub_map_info$map[k]
      )
    # bind to the overall dataset
    group_closeness <- bind_rows(group_closeness, map_closeness)
  }
}
save(group_closeness, file = "data/sensitivity/group-closeness.rda")
