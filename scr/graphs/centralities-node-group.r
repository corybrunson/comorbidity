# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(igraph)
source("R/graph-construction.r")
source("R/utils.r")
source("R/params.r")
load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

# restrict to representative datasets
source_info <- source_info %>%
  filter(!grepl("namcs|vaers", file))

# Compare the most-central disorders in each dataset Use unit weights (positive
# associations only) and a variably-corrected p-value cutoff of .05

# data set of node centralities for each source and two evidential cutoffs
node_centrality <- tibble()
for (i in 1:nrow(source_info)) {
  # load data
  d <- readRDS(paste0(sdir, source_info$file[i]))
  n <- n_distinct(unlist(d[, 1:2]))
  for (corr in param_vals$correction) {
    print(c(as.character(source_info$source[i]), corr))
    # construct graph
    g <- graph_from_pairs_data(
      data = d,
      pval_cutoff = .05,
      pval_correction = corr,
      weighted = FALSE
    )
    node_centrality <- bind_rows(node_centrality, tibble(
      dataset = source_info$source[i],
      correction = corr,
      node = V(g)$name,
      dcent = degree(g),
      ccent = closeness(g),
      bcent = betweenness(g)
    ))
  }
}
save(node_centrality, file = paste0(tdir, "node-centrality.RData"))

# Phase III: Group degree and closeness centralities

group_centrality <- tibble()
for (i in 1:nrow(source_info)) {
  # continue only if a suitable mapping exists
  sub_map_info <- map_info %>%
    filter(ontology == source_info$ontology[i])
  if (nrow(sub_map_info) == 0) next
  # load data
  d <- readRDS(paste0(sdir, source_info$file[i]))
  n <- n_distinct(unlist(d[, 1:2]))
  for (corr in param_vals$correction) {
    # construct graph
    g <- graph_from_pairs_data(
      data = d,
      pval_cutoff = .05, # (closeness centrality infinities)
      pval_correction = corr,
      weighted = FALSE
    )
    # adjacency matrix
    A <- as_adj(g, sparse = FALSE)
    # geodesic distance matrix
    L <- distances(g, weights = NA)
    L[is.infinite(L)] <- n
    # group centralities for each ontology mapping
    for (j in 1:nrow(sub_map_info)) {
      print(c(as.character(source_info$source[i]), corr, sub_map_info$map[j]))
      # mapping
      map <- ontology_maps[[sub_map_info$ontology[j]]][[sub_map_info$map[j]]]
      # degree and closeness centralities
      targets <- unique(map$target)
      dcent <- rep_len(NA, length(targets))
      ccent <- rep_len(NA, length(targets))
      for (k in seq_along(targets)) {
        sources <- map %>% filter(target == targets[k]) %>% pull(source)
        source_nodes <- which(vertex_attr(g, "name") %in% sources)
        if (length(source_nodes) == 0) next
        # group degree centrality
        dcent[k] <- A[source_nodes, -source_nodes, drop = FALSE] %>%
          apply(2, any) %>%
          sum()
        # group closeness centrality
        ccent[k] <- L[source_nodes, -source_nodes, drop = FALSE] %>%
          apply(2, mean) %>%
          sum()
      }
      # bind to tibble
      group_centrality <- bind_rows(
        group_centrality,
        tibble(
          dataset = source_info$source[i],
          correction = corr,
          map = sub_map_info$map[j],
          group = targets,
          dcent = dcent,
          ccent = 1 / ccent
        ) %>% filter(!is.na(dcent))
      )
    }
  }
}
save(group_centrality, file = paste0(tdir, "group-centrality.RData"))
