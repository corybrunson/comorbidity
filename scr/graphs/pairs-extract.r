# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(tidyr)
library(dplyr)

# source and target data directories
sdir <- "~/Documents/CQM0/comorbidity/data/"
tdir <- "~/Documents/CQM/comorbidity/data/pairs/"

# notation
# n = Total sample size
# n1 = Subjects with feature 1
# n2 = Subjects with feature 2
# n12 = Subjects with features 1 and 2
# n1_2 = n1 - n12
# n2_1 = n2 - n12
# n_12 = n - n1_2 - n2_1 - n12

# Rzhetsky et al, 2007: Columbia University Medical Center, ????
# http://www.pnas.org/content/suppl/2007/06/21/0704820104.DC1
# 04820Appendix2.pdf
# 04820Appendix3.pdf
# 04820Appendix4.pdf
# 04820Appendix5.pdf
# SI Data Set 1.txt
classes <- c(rep("character", 2), rep("numeric", 6))
data <- read.table(file = paste0(sdir, "rzhetsky2007/SI Data Set 1.txt"),
                   header = FALSE, sep = ",", quote = "",
                   colClasses = classes)
names(data) <- c("disease_1", "disease_2",
                 "llr", "pval",
                 "n1_2", "n2_1", "n12", "unk")
data <- transform(data,
                  disease_1 = gsub("^\\'(.*)\\'$", "\\1", disease_1),
                  disease_2 = gsub("^\\'(.*)\\'$", "\\1", disease_2),
                  n1 = n1_2 + n12,
                  n2 = n2_1 + n12,
                  n = 1478976 - 5e5) # see Bagley et al (2016), Appendix, p.1
data <- data[, c("disease_1", "disease_2",
                 "n", "n1", "n2", "n12",
                 "llr", "pval")]
saveRDS(data, file = paste0(tdir, "columbia.rds"))

# Hidalgo et al, 2009: Medicare, 1990-1993
# http://barabasilab.neu.edu/projects/hudine/resource/data/data.html
# AllNet3.net
# 3-character ICD-9 prefixes
classes <- c(rep("character", 2), rep("numeric", 8))
data <- read.table(file = paste0(sdir, "hidalgo2009/AllNet3.net"),
                   header = FALSE, sep = "\t", colClasses = classes)
names(data) <- c("icd9_1", "icd9_2",
                 "n1", "n2", "n12",
                 "rr", "rr_lower", "rr_upper",
                 "phi", "t_test")
data <- transform(data,
                  n = round(rr * n1 * n2 / n12, 0))
data <- data[, c("icd9_1", "icd9_2",
                 "n", "n1", "n2", "n12")]
saveRDS(data, file = paste0(tdir, "medicare1993-3.rds"))
# AllNet5.net
# 5-character ICD-9 codes
classes <- c(rep("character", 2), rep("numeric", 8))
data <- read.table(file = paste0(sdir, "hidalgo2009/AllNet5.net"),
                   header = FALSE, sep = "\t", colClasses = classes)
names(data) <- c("icd9_1", "icd9_2",
                 "n1", "n2", "n12",
                 "rr", "rr_lower", "rr_upper",
                 "phi", "t_test")
data <- transform(data,
                  n = round(rr * n1 * n2 / n12, 0))
data <- data[, c("icd9_1", "icd9_2",
                 "n", "n1", "n2", "n12")]
saveRDS(data, file = paste0(tdir, "medicare1993-5.rds"))

# Ball and Botsis (2011): VAERS
# https://vaers.hhs.gov/data/data
# 2005VAERSData.zip - 2016VAERSData.zip
# read data
n_yrs <- 5
files <- list.files(paste0(sdir, "vaers/"), pattern = "VAERSData")
files <- files[length(files) - (n_yrs - 1):0]
years <- gsub("^([0-9]+)VAERSData.*$", "\\1", files)
data <- bind_rows(lapply(1:length(files), function(i) {
  read.csv(unz(paste0(sdir, "vaers/", files[i]),
               filename = paste0(years[i], "VAERSSYMPTOMS.csv")),
           header = TRUE, quote = "\"")
}))
names(data) <- tolower(names(data))
n <- dplyr::n_distinct(data$vaers_id)
data <- data %>%
  select(vaers_id, symptom1, symptom2, symptom3, symptom4, symptom5) %>%
  gather(key = "rank", value = "symptom",
         symptom1, symptom2, symptom3, symptom4, symptom5) %>%
  select(vaers_id, symptom) %>%
  filter(symptom != "") %>%
  arrange(vaers_id) %>%
  distinct()
ns <- table(data$symptom)
ns <- data.frame(symptom = names(ns),
                 n = as.vector(ns))
data <- data[data$vaers_id %in% data$vaers_id[duplicated(data$vaers_id)], ]
# http://stackoverflow.com/a/38221736
data <- data %>% group_by(vaers_id) %>%
  do(data.frame(t(combn(.$symptom, 2)))) %>%
  ungroup(data) %>%
  select(X1, X2) %>%
  rename(symptom_1 = X1, symptom_2 = X2) %>%
  as.data.frame()
# aggregate
data[which(data$symptom_1 < data$symptom_2), ] <-
  data[which(data$symptom_1 < data$symptom_2), 2:1]
data$n12 <- 1
data <- aggregate(n12 ~ symptom_1 + symptom_2, data = data, FUN = sum)
# merge in frequencies
data$n <- n
data <- merge(data,
              transform(ns, n1 = n)[, c("symptom", "n1")],
              by.x = "symptom_1", by.y = "symptom",
              all.x = TRUE, all.y = FALSE)
data <- merge(data,
              transform(ns, n2 = n)[, c("symptom", "n2")],
              by.x = "symptom_2", by.y = "symptom",
              all.x = TRUE, all.y = FALSE)
data <- data[do.call(order, data[, c("symptom_1", "symptom_2")]),
             c(2:1, 3, 5:6, 4)]
saveRDS(data, file = paste0(tdir, "vaers",
                            years[1], "-", years[length(years)], ".rds"))

# Roque, Jensen, et al (2011):
# Sct. Hans Mental Health Centre
# http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1002141
# journal.pcbi.1002141.s003.XLS
# http://journals.plos.org/ploscompbiol/article/comment?id=10.1371/
# annotation/421186a3-aa4c-434b-8f00-72f7e33124f3
# 
# read Excel file
data <- gdata::read.xls(
  xls = paste0(sdir, "roquejensen2011/journal.pcbi.1002141.s003.XLS"),
  sheet = 1, header = TRUE, skip = 1,
  stringsAsFactors = FALSE
)
names(data) <- c("icd10_1", "icd10term_1", "icd10_2", "icd10term_2",
                 "n1", "n2", "n12", "ln2ratio", "pval", "fdr")
data <- transform(data,
                  n = round(n1 * n2 / ((n12 + 1) / (2 ^ ln2ratio) - 1), 0))
data <- data[, c("icd10_1", "icd10_2",
                 "n", "n1", "n2", "n12")]
saveRDS(data, file = paste0(tdir, "scthans1998-2008.rds"))

# Blair et al (2013):
# Columbia University (2003)
# Denmark
# New York Presbyterian Hospital and Columbia University (2004-2013)
# Stanford University
# University of Texas at Houston
# University of Chicago
# MarketScan
# Medicare
# http://www.sciencedirect.com/science/article/pii/S0092867413010246
# mmc2.xls
# mmc3.xls
# mmc4.xls
# mmc5.xls
# read Excel files
complex_mendelian <- gdata::read.xls(xls = paste0(sdir, "blair2013/mmc4.xls"),
                                     sheet = 1, header = TRUE, skip = 1,
                                     stringsAsFactors = FALSE)
names(complex_mendelian) <- c("complex", "mendelian",
                              "gender",
                              "rr_relative",
                              "odds_conditional", "odds_conditional_ci",
                              "pval_bonferroni",
                              "rr_global", "odds_global", "rr_lm", "rr_lm_ci")
mendelian_mendelian <- gdata::read.xls(xls = paste0(sdir, "blair2013/mmc5.xls"),
                                       sheet = 1, header = TRUE, skip = 1,
                                       stringsAsFactors = FALSE)
names(mendelian_mendelian) <- c("mendelian_1", "mendelian_2",
                                "rr_relative",
                                "odds_conditional", "odds_conditional_ci",
                                "pval_bonferroni",
                                "rr_global", "odds_global",
                                "odds_adj", "odds_adj_ci",
                                "odds_lm", "odds_lm_ci",
                                "odds_lmadj", "odds_lmadj_ci")
sources <- c("columbia2003", "denmark", "presbyterian2013", "stanford",
             "texashouston", "chicago", "marketscan", "medicare")
ns <- length(sources)
stats <- c("rr_relative", "odds_conditional", "pval_bonferroni")
for (i in 1:ns) {
  data <- as.data.frame(rbind(unname(as.matrix(complex_mendelian[, 1:2])),
                              unname(as.matrix(mendelian_mendelian[, 1:2]))))
  names(data) <- paste0("disease_", 1:2)
  for (stat in stats) {
    data[[stat]] <- as.numeric(gsub(
      paste(c(rep("[^,]+,", i - 1),
              "([^,]+)",
              rep(",[^,]+", ns - i)),
            collapse = ""),
      "\\1",
      c(complex_mendelian[[stat]], mendelian_mendelian[[stat]])
    ))
  }
  saveRDS(data, file = paste0(tdir, sources[i], ".rds"))
}

# Hanauer & Ramakrishnan (2013): University of Michigan Health System, ????
# https://www.dropbox.com/s/q59hrgtp049slft/JAMIA_analysis.zip?dl=0
# how to interpret the data.txt
# outputClinicalConceptsFinal_1_to_50000_days.txt
data <- read.table(
  file = paste0(sdir,
                "hanauer2013/outputClinicalConceptsFinal_1_to_50000_days.txt"),
  header = TRUE, sep = "\t"
)
names(data) <- c("icd9_1", "icd9_2",
                 "n12", "n1_2", "n2_1", "n_12",
                 "odds", "chisq_pval", "chisq",
                 "s1", "s2", "flow", "binom_pval")
data <- transform(data,
                  n1 = n1_2 + n12,
                  n2 = n2_1 + n12,
                  n = n_12 + n1_2 + n2_1 + n12)
data <- data[, c("icd9_1", "icd9_2",
                 "n", "n1", "n2", "n12")]
saveRDS(data, file = paste0(tdir, "michigan.rds"))

# Chen & Xu (2014): FAERS
# https://www.fda.gov/drugs/guidancecomplianceregulatoryinformation/
# surveillance/adversedrugeffects/ucm082193.htm
files <- list.files(path = sdir, pattern = "VAERSData\\.zip", full.names = TRUE)
files <- list.files(path = sdir, pattern = "faers[^/]+\\.zip",
                    full.names = TRUE)
# REQUIRES UMLS METATHESAURUS

# Jensen et al (2014): Danish National Patient Registry, 1996-2010
# http://www.nature.com/articles/ncomms5022#supplementary-information
data <- gdata::read.xls(xls = paste0(sdir, "jensen2014/ncomms5022-s2.xlsx"),
                        sheet = 1, header = TRUE, skip = 3,
                        stringsAsFactors = FALSE, perl = "perl",
                        verbose = TRUE)
names(data) <- c("diagnosis_1_code", "diagnosis_1_text",
                 "diagnosis_2_code", "diagnosis_2_text",
                 "n12", "rr", "pval_est", "pval_samp", "n", "pval_dir")
data <- data[, c("diagnosis_1_code", "diagnosis_1_text",
                 "diagnosis_2_code", "diagnosis_2_text",
                 "n12", "rr", "pval_est", "pval_samp", "n")]
saveRDS(data, file = paste0(tdir, "dnpr1996-2010.rds"))

# Bagley et al (2016):
# Columbia University, 2013
# STRIDE (Stanford University), 2013
# http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004885
# journal.pcbi.1004885.s001.PDF
# journal.pcbi.1004885.s002.CSV
# journal.pcbi.1004885.s003.CSV
# journal.pcbi.1004885.s004.CSV
# journal.pcbi.1004885.s005.CSV
# journal.pcbi.1004885.s006.CSV
# journal.pcbi.1004885.s007.CSV
data <- read.csv(file = paste0(sdir,
                               "bagley2016/journal.pcbi.1004885.s004.CSV"),
                 header = TRUE, stringsAsFactors = TRUE)
names(data) <- c("disease_2", "clusterid_2", "clustersize_2", "clustername_2",
                 "disease_1", "clusterid_1", "clustername_1",
                 "n1", "n2", "n12", "n1_2", "n2_1", "n_12", "n",
                 "exp_d12", "diff_obs_exp", "quot_obs_exp", "pval", "n_")
data <- data[, c("disease_1", "disease_2",
                 "n", "n1", "n2", "n12")]
saveRDS(data, file = paste0(tdir, "columbia2013.rds"))
data <- read.csv(file = paste0(sdir,
                               "bagley2016/journal.pcbi.1004885.s005.CSV"),
                 header = TRUE, stringsAsFactors = TRUE)
names(data) <- c("disease_2", "clusterid_2", "clustersize_2", "clustername_2",
                 "disease_1", "clusterid_1", "clustername_1",
                 "n1", "n2", "n12", "n1_2", "n2_1", "n_12", "n",
                 "exp_d12", "diff_obs_exp", "quot_obs_exp", "pval", "n_")
data <- data[, c("disease_1", "disease_2",
                 "n", "n1", "n2", "n12")]
saveRDS(data, file = paste0(tdir, "stanford2013.rds"))

# NAMCS (National Ambulatory Medical Care Survey)
# https://www.cdc.gov/nchs/ahcd/ahcd_questionnaires.htm
# namcs-scrape-stata.r
n_yrs <- 5
for (len in c(3, 5)) {
  data <- readRDS("data/namcs/namcs-stata.rds")
  data$years <- as.numeric(as.character(data$year))
  yr0 <- max(data$year) - n_yrs
  data <- data[data$year > yr0, ]
  years <- range(data$year)
  n <- nrow(data)
  # three diagnoses per record (all years)
  data <- data[, grep("diag[1-3]", names(data))]
  # shorten ICD-9 codes to len-character prefixes
  for (i in grep("^diag", names(data))) {
    data[[i]] <- substr(data[[i]], 1, len)
  }
  # convert rule-out and empty diagnosis fields to NA
  prdiag_to_na <- function(diag, prdiag) {
    ifelse(!grepl("[^0]", diag) | diag == "-9" |
             prdiag == "No diagnosis entered" | prdiag == "Not applicable",
           NA, diag)
  }
  data <- transform(data,
                    diag1 = prdiag_to_na(diag1, prdiag1),
                    diag2 = prdiag_to_na(diag2, prdiag2),
                    diag3 = prdiag_to_na(diag3, prdiag3))
  # convert duplicate diagnosis fields to NA
  data <- transform(data,
                    diag3 = ifelse(diag3 == diag2, NA, diag3),
                    diag2 = ifelse(diag2 == diag1, NA, diag2))
  data <- transform(data,
                    diag3 = ifelse(diag3 == diag1, NA, diag3))
  ns <- table(do.call(c, data[1:3]))
  ns <- data.frame(icd9 = names(ns), n = as.vector(ns))
  # restrict to entries with multiple diagnoses
  data <- data[!is.na(data$diag2), 1:3]
  # pairs
  data <- do.call(rbind, apply(combn(ncol(data), 2), 2, function(x) {
    d <- data[, x]
    names(d) <- paste0("icd9_", 1:2)
    d
  }))
  data[which(data$icd9_1 > data$icd9_2), ] <-
    data[which(data$icd9_1 > data$icd9_2), 2:1]
  data$n12 <- 1
  data <- aggregate(n12 ~ icd9_1 + icd9_2, data = data, FUN = sum)
  data$n <- n
  data <- merge(data,
                transform(ns, n1 = n)[, c("icd9", "n1")],
                by.x = "icd9_1", by.y = "icd9",
                all.x = TRUE, all.y = FALSE)
  data <- merge(data,
                transform(ns, n2 = n)[, c("icd9", "n2")],
                by.x = "icd9_2", by.y = "icd9",
                all.x = TRUE, all.y = FALSE)
  data <- data[do.call(order, data[, c("icd9_1", "icd9_2")]), c(2:1, 3, 5:6, 4)]
  saveRDS(data, file = paste0(tdir, "namcs",
                              years[1], "-", years[2],
                              "-", len, ".rds"))
}
