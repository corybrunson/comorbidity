# Calculate summary statistics for comorbidity networks constructed from each
# data set using each slate of parameters.

# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(igraph)
source("R/graph-construction.r")
source("R/utils.r")
source("R/params.r")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/graphs/"

# proportions of pairs and ranges of p-values
(pair_props <- sapply(source_info$file, function(f) {
  readRDS(paste0(sdir, f)) %>%
    dplyr::select(1:2) %>% (function(d) {
      nrow(d) / choose(dplyr::n_distinct(as.vector(as.matrix(d[, 1:2]))), 2)
    })
}))
(pval_quants <- sapply(source_info$file, function(f) {
  readRDS(paste0(sdir, f)) %>%
    dplyr::pull(pval_fisher) %>% quantile()
}))

# initialize data frame
data <- data.frame(
  source = character(),
  sample_size = numeric(),
  disease_count = numeric(),
  pval_threshold = numeric(),
  pval_correction = character(),
  measure = character(),
  measure_cutoff = numeric(),
  connected_prop = numeric(),
  density = numeric(),
  degree_mean = numeric(),
  degree_gini = numeric(),
  degree_kspower = numeric(),
  degree_assortativity = numeric(),
  triad_closure = numeric(),
  distance_mean = numeric(),
  modularity_wt = numeric()
  # Tunc-Verma measure of core-periphery structure?
)

choose_family <- function(x) {
  plfit <- poweRlaw::displ$new(x)
  plfit$setXmin(poweRlaw::estimate_xmin(plfit))
  plfit$setPars(poweRlaw::estimate_pars(plfit))
  expfit <- poweRlaw::disexp$new(x)
  expfit$setXmin(poweRlaw::estimate_xmin(expfit))
  expfit$setPars(poweRlaw::estimate_pars(expfit))
  lnormfit <- poweRlaw::dislnorm$new(x)
  lnormfit$setXmin(poweRlaw::estimate_xmin(lnormfit))
  lnormfit$setPars(poweRlaw::estimate_pars(lnormfit))
  cols <- RColorBrewer::brewer.pal(3, "Set2")
  poweRlaw::plot(plfit, xlab = "Number of interaction partners", ylab = "Rank")
  poweRlaw::lines(plfit, col = cols[1], lty = 1, lwd = 4)
  poweRlaw::lines(expfit, col = cols[2], lty = 2, lwd = 4)
  poweRlaw::lines(lnormfit, col = cols[3], lty = 3, lwd = 4)
  legend(x = "bottomleft", box.lty = 0, col = cols, lty = 1:3, lwd = 4,
         legend = c("power-law", "exponential", "log-normal"))
}

largest_component <- function(g) {
  comp <- components(g)
  induced_subgraph(g, which(comp$membership == which.max(comp$csize)))
}

#densities <- exp(seq(log(.1), log(.001), length.out = 5))

pdf(file = "fig/fits.pdf", width = 8, height = 7)

# calculate graph statistics: frequency tables
for (i in seq_along(source_info$file)) {
  f <- source_info$file[i]
  print(f)
  # load data
  d <- readRDS(paste0(sdir, f))
  # sample size (number of patients)
  n <- unique(d$n)
  # (number of) distinct diseases
  # NEED TO CHECK FOR DISEASES NOT INCLUDED IN DATASETS
  v <- as.character(unique(unlist(d[, 1:2])))
  m <- length(v)
  
  # vary p-value threshold and error rate correction
  for (pval in param_vals$pval) for (corr in param_vals$correction) {
    
    print(c(f, format(pval, scientific = FALSE), "unit"))
    # construct graph
    g <- graph_from_pairs_data(
      data = d,
      pval_cutoff = pval, pval_correction = corr,
      weighted = FALSE
    )
    # distance sequence
    if (vcount(g) <= 2520) {
      l <- distances(g)
      l <- l[upper.tri(l)]
    } else {
      v01 <- sample(V(g), size = 5040, replace = FALSE)
      l <- distances(g, v = v01[1:2520], to = v01[2520 + 1:2520])
      l <- c(l[upper.tri(l)], l[lower.tri(l)])
    }
    # degree sequence
    deg_seq <- degree(g)[degree(g) > 0]
    if (length(deg_seq) > 1680) {
      deg_seq <- deg_seq[sample(length(deg_seq), size = 1680, replace = FALSE)]
    }
    # choose families
    deg_fam <- try(choose_family(x = deg_seq))
    if (is.character(deg_fam)) {
      deg_fit_params <- c(NA, NA)
    } else {
      title(main = paste0(
        gsub("\\.rds$", "", f), ", p < ", format(pval, scientific = FALSE)
      ))
      # fit families
      deg_fit <- poweRlaw::dislnorm$new(deg_seq)
      deg_fit$setXmin(poweRlaw::estimate_xmin(deg_fit))
      deg_fit$setPars(poweRlaw::estimate_pars(deg_fit))
      deg_fit_params <- deg_fit$pars
    }
    # augment data frame
    data <- rbind(data, data.frame(
      source = gsub("\\.rds$", "", f),
      sample_size = n,
      disease_count = choose(m, 2),
      pval_threshold = pval,
      pval_correction = corr,
      measure = NA_character_,
      measure_cutoff = NA_real_,
      density = edge_density(g),
      connected_prop = vcount(largest_component(g)) / vcount(g),
      degree_mean = mean(degree(g)),
      degree_gini = ineq::Gini(degree(g)),
      degree_param1 = deg_fit_params[1],
      degree_param2 = deg_fit_params[2],
      degree_assortativity = assortativity_degree(g),
      triad_closure = transitivity(g, type = "global"),
      distance_mean = mean(l[!is.infinite(l) & l != 0]),
      modularity_wt = modularity(cluster_walktrap(largest_component(g)))
    ))
    
    # vary association measure and cutoff
    for (meas in param_vals$measure) {
      # since graphs are unweighted and only positive associations included,
      # exclude the trivial cutoff
      for (th in param_vals$threshold[[meas]][-1]) {
        
        print(c(f, format(pval, scientific = FALSE), meas, th))
        # construct graph
        g <- graph_from_pairs_data(
          data = d,
          measure = meas, measure_cutoff = th,
          pval_cutoff = pval, pval_correction = corr,
          weighted = FALSE
        )
        # distance sequence
        l <- distances(g)
        l <- l[upper.tri(l)]
        # degree sequence
        deg_seq <- degree(g)[degree(g) > 0]
        if (length(deg_seq) > 1680) {
          deg_seq <- deg_seq[sample(length(deg_seq),
                                    size = 1680,
                                    replace = FALSE)]
        }
        # choose families
        deg_fam <- try(choose_family(x = deg_seq))
        if (is.character(deg_fam)) {
          deg_fit_params <- c(NA, NA)
        } else {
          title(main = paste0(gsub("\\.rds$", "", f),
                              ", p < ", format(pval, scientific = FALSE),
                              " & ", meas, " > ", th))
          # fit families
          deg_fit <- poweRlaw::dislnorm$new(deg_seq)
          deg_fit$setXmin(poweRlaw::estimate_xmin(deg_fit))
          deg_fit$setPars(poweRlaw::estimate_pars(deg_fit))
          deg_fit_params <- deg_fit$pars
        }
        # augment data frame
        data <- rbind(data, data.frame(
          source = gsub("\\.rds$", "", f),
          sample_size = n,
          disease_count = choose(m, 2),
          pval_threshold = pval,
          pval_correction = corr,
          measure = meas,
          measure_cutoff = th,
          density = edge_density(g),
          connected_prop = vcount(largest_component(g)) / vcount(g),
          degree_mean = mean(degree(g)),
          degree_gini = ineq::Gini(degree(g)),
          degree_param1 = deg_fit_params[1],
          degree_param2 = deg_fit_params[2],
          degree_assortativity = assortativity_degree(g),
          triad_closure = transitivity(g, type = "global"),
          distance_mean = mean(l[!is.infinite(l) & l != 0]),
          modularity_wt = modularity(cluster_walktrap(largest_component(g)))
        ))
        # save progress
        saveRDS(data, file = paste0(tdir, "graphs-summary.rds"))
        
      }
    }
    
  }
  
}

dev.off()

saveRDS(data, file = paste0(tdir, "graphs-summary.rds"))
