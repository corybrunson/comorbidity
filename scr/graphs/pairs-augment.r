# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(tidyr)
library(dplyr)
source("R/pairs-calculations.r")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"

# available stats
files <- list.files(path = sdir)
sources <- gsub("\\.rds$", "", files)
stats <- lapply(files, function(f) {
  names(readRDS(paste0(sdir, f)))
})
names(stats) <- sources

# availability of frequency tables
incl <- sapply(lapply(stats, match, nomatch = 0L,
                      x = c("n", "n1", "n2", "n12")), all)
# Note: exclude datasets from Blair et al (2013), because their comparisons
# and p-values are specially-tailored to their Mendelian-complex hypotheses
# Note: exclude dataset from Jensen et al (2014),
# because the pairs are directional
files <- files[incl]
sources <- sources[incl]
stats <- stats[incl]
rm(incl)

# augment with hypothesis tests and binary association measures
for (i in seq_along(files)) {
  message("Augmenting ", files[i])
  data <- readRDS(paste0(sdir, files[i]))
  data <- data %>%
    chisquare_from_ft() %>%
    chisq_from_ft() %>%
    fisherexact_from_ft() %>%
    oddsratio_from_ft() %>%
    phi_from_ft() %>%
    forbes_from_ft() %>%
    rho_from_ft(z = 2)
  saveRDS(data, file = paste0(sdir, files[i]))
}
