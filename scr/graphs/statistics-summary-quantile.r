# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
#devtools::install_github("richardjtelford/ggbiplot", ref = "experimental")
library(tidyverse)
pkgs <- c("forcats", "modelr", "broom")
#pkgs <- c("purrr", "ggbiplot")
for (p in pkgs) library(p, character.only = TRUE)
source("R/utils.r")

# note: previously used a more limited selection of p-values
data <- readRDS(data, file = "data/graphs/graphs-summary-quantile.rds") %>%
  as_tibble() %>%
  mutate(measure = fct_explicit_na(measure, "unit")) %>%
  left_join(measure_info %>%
              select(measure, meas_abbr = abbr, meas_tex = abbr_tex)) %>%
  left_join(source_info %>% select(source, source_abbr = abbr)) %>%
  mutate_if(is.character, funs(factor(., levels = unique(.)))) %>%
  mutate_if(is.factor, funs(factor(., levels = unique(.))))
(data %>% select(measure_logit_quantile, measure) %>% table(useNA = "always"))

# linear models
nestdata <- data %>%
  filter(!grepl("namcs|vaers", source)) %>%
  gather(
    key = "statistic", value = "value",
    connected_prop, density,
    #degree_mean,
    degree_gini,
    #degree_param1, degree_param2,
    degree_assortativity, triad_closure, distance_mean#,
    #modularity_wt
  ) %>%
  merge(stat_info, by = "statistic") %>%
  group_by(statistic, stat_abbr) %>% nest() %>%
  # add average values for graphs without evaluative cutoffs
  mutate(
    value_mean = data %>%
      lapply(filter, measure == "unit") %>%
      lapply(summarize, mean_value = mean(value, na.rm = TRUE)) %>%
      sapply(pull, mean_value)
  ) %>%
  mutate(
    data = mapply(cbind, data, value_mean = value_mean, SIMPLIFY = FALSE) %>%
      lapply(as_tibble) %>%
      lapply(mutate, value_diff = value - value_mean)
  )
nestdata %>% pull(data) %>%
  lapply(select, measure_logit_quantile, measure) %>%
  lapply(table, useNA = "always")
nestdata %>% pull(data) %>%
  lapply(dplyr::filter, measure == "unit") %>%
  lapply(select, pval_logit_quantile, source) %>%
  lapply(table, useNA = "always")

baseline_measures <- function(d) {
  d %>%
    mutate_at(vars(ends_with("_quantile")), funs(ifelse(is.na(.), 0, .))) %>%
    #mutate(quantile = ifelse(is.na(quantile), 0, quantile)) %>%
    # duplicate 'unit' data for each measure, since they'd all yield the same
    mutate(
      unit = (measure == "unit"),
      measure = ifelse(unit, NA, as.character(measure))
    ) %>%
    full_join(tibble(
      unit = TRUE,
      measure2 = c("oddsratiommle", "phi", "forbes", "rho_psych")
    )) %>%
    mutate(measure = ifelse(is.na(measure), measure2, measure)) %>%
    select(-unit)
}

classical_model2 <- function(d) {
  lm(formula = value_diff ~
       source + pval_logit_quantile +
       measure:measure_logit_quantile +
       0,
     data = baseline_measures(d))
}
multilevel_model2 <- function(d) lme4::lmer(
  formula = value ~
    pval_logit_quantile +
    (1 | source) +
    measure_logit_quantile + (1 + measure_logit_quantile | measure) +
    1,
  data = filter(d, measure != "unit")
)

lmdata <- nestdata %>%
  mutate(
    model2 = map(data, classical_model2),
    model4 = map(data, multilevel_model2)
  ) %>%
  mutate_at(c(vars(starts_with("model"))), funs(tidy = lapply(., tidy)))

# evidential and evaluative cutoffs, classical regression models
cov_lab_2 <- c(
  as.character(unique(lmdata$data[[1]]$source_abbr)),
  "log(TWER)",
  paste0(
    "Quant. (",
    as.character(setdiff(levels(data$meas_tex), "$1$"))[c(3, 1, 2, 4)],
    ")"
  )
)
lmdata %>% pull(model2) %>% stargazer::stargazer(
  out = "tab/model2-quantile.tex", label = "tab:lm2-quantile",
  title = paste0(
    "LMs of network statistics on ",
    "data source, test-wise error rate with quantile cutoff, ",
    "and binary association measure with quantile cutoff."
  ),
  align = TRUE, digits = 2, star.cutoffs = 10^(-(1:3)),
  dep.var.labels.include = FALSE,
  column.labels = lmdata$stat_abbr,
  column.sep.width = "-5pt",
  covariate.labels = cov_lab_2,
  omit.stat = c("rsq", "ser", "f"),
  no.space = FALSE, font.size = "scriptsize",
  notes = paste0(
    "\\textbf{Remove this note after adding horizontal lines between ",
    "groups of predictors (datasets, evidential cutoff, evaluative cutoffs).}"
  )
)

# evidential and evaluative cutoffs, hierarchical regression models
cov_lab_4 <- c(
  "log(TWER)", "Quant.",
  "Const."
)
lmdata %>% pull(model4) %>% stargazer::stargazer(
  out = "tab/model4-quantile.tex", label = "tab:hlm2-quantile",
  title = paste0(
    "Hierarchical models of network statistics ",
    "on test-wise error rate with quantile cutoffs, ",
    "grouped by data source, and ",
    "on quantile cutoff effects grouped by binary association measure"
  ),
  align = TRUE, digits = 2, star.cutoffs = 10^(-(1:3)),
  dep.var.labels.include = FALSE,
  column.labels = lmdata$stat_abbr,
  column.sep.width = "-5pt",
  covariate.labels = cov_lab_4,
  omit.stat = c("rsq", "ser", "f"),
  no.space = FALSE, font.size = "scriptsize"
)
model4_terms <- c(
  "$\\sigma_{1}(\\text{source})$",
  "$\\sigma_{1}(\\text{measure})$",
  "$\\sigma_{\\text{quant.}}(\\text{measure})$",
  #"$\\rho_{1,\\text{quant.}}(\\text{measure})$",
  "$\\sigma(\\text{resid.})$"
)
lmdata %>% pull(model4_tidy) %>%
  lapply(filter, group != "fixed" & !grepl("^cor\\_", term)) %>%
  lapply(pull, estimate) %>% do.call(what = cbind) %>%
  as.data.frame() %>% setNames(stat_info$stat_abbr) %>%
  {
    rownames(.) <- model4_terms
    .
  } %>%
  xtable::xtable(
    label = "tab:hlm2-quantile-variance",
    caption = paste0(
      "Variance decomposition for ",
      "hierarchical models of network statistics ",
      "on test-wise error rate with quantile cutoffs, ",
      "grouped by data source, and ",
      "on quantile cutoff effects grouped by binary association measure"
    ),
    align = c("l", rep("r", nrow(stat_info))), digits = 3
  ) %>% xtable::print.xtable(
    file = "tab/model4-quantile-variance.tex",
    size = "scriptsize", caption.placement = "top",
    sanitize.rownames.function = identity,
    sanitize.colnames.function = identity
  )
