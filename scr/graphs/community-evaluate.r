# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(tidyr)
library(ggplot2)
library(ggrepel)
source("R/utils.r")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

hc_data <- readRDS(paste0(sdir, "hc-data.rds"))
cd_data <- readRDS(paste0(sdir, "cd-data.rds"))

# plot Dunn indices vs cut height for both sets of partitions
dunn_ht_plot <- ggplot(cd_data, aes(x = avg_height, y = dunn)) +
  theme_bw() +
  facet_wrap(~ source) +
  scale_x_continuous(trans = "log", breaks = 10^(seq(-6,2,2))) +
  scale_y_continuous(trans = "log", breaks = 10^(seq(-8,2,2))) +
  geom_step(data = hc_data, aes(x = height), direction = "vh") +
  geom_line(aes(group = alg_abbr), color = "gray") +
  geom_point(aes(shape = alg_abbr), show.legend = FALSE) +
  geom_text_repel(aes(label = pval_threshold), size = 3) +
  geom_step(data = hc_data, aes(x = height, y = dunn_null_mean),
            direction = "vh")
ggsave(
  dunn_ht_plot,
  height = 6, width = 8,
  filename = "fig/community-dunn-ht.pdf"
)

# plot Dunn indices vs number of parts for both sets of partitions
dunn_n_plot <- ggplot(cd_data, aes(x = n, y = dunn)) +
  theme_bw() +
  facet_wrap(~ source) +
  scale_y_continuous(trans = "log", breaks = 10^(seq(-8,2,2))) +
  geom_step(data = hc_data, direction = "vh") +
  geom_line(aes(group = alg_abbr), color = "gray") +
  geom_point(aes(shape = alg_abbr), show.legend = FALSE) +
  geom_text_repel(aes(label = pval_threshold), size = 3) +
  geom_step(data = hc_data, aes(y = dunn_null_mean), direction = "vh")
ggsave(
  dunn_n_plot,
  height = 6, width = 8,
  filename = "fig/community-dunn-n.pdf"
)

# plot network modularities vs number of parts for both sets of partitions
modularity_n_plot <- ggplot(cd_data, aes(x = n, y = modularity)) +
  theme_bw() +
  facet_wrap(~ source) +
  #scale_y_continuous(trans = "log", breaks = 10^(seq(-8,2,2))) +
  geom_step(data = hc_data, aes(y = mod_.05),
            direction = "vh", linetype = "dashed") +
  geom_step(data = hc_data, aes(y = mod_B.05),
            direction = "vh", linetype = "dotted") +
  geom_line(aes(group = alg_abbr), color = "gray") +
  geom_point(aes(shape = alg_abbr), show.legend = FALSE) +
  geom_text_repel(aes(label = pval_threshold), size = 3) +
  geom_step(data = hc_data, aes(y = mod_.05_mean),
            direction = "vh", linetype = "dashed") +
  geom_step(data = hc_data, aes(y = mod_B.05_mean),
            direction = "vh", linetype = "dotted")
ggsave(
  modularity_n_plot,
  height = 6, width = 8,
  filename = "fig/community-modularity-n.pdf"
)

# adjoin plots
ggsave(
  gridExtra::arrangeGrob(
    grobs = list(dunn_n_plot, modularity_n_plot), nrow = 1, ncol = 2,
    respect = FALSE
  ),
  height = 6, width = 12,
  filename = "fig/community-dunn-modularity-n.pdf"
)
