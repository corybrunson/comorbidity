# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(tidyr)
library(igraph)
source("R/graph-construction.r")
source("R/utils.r")
#load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

# restrict to sources for which all pairs are available
source_incl <- c(1, 8)
# community detection algorithms to use
# (must permit disconnected graphs)
alg_data <- tibble(
  algorithm = c("fast_greedy", "edge_betweenness", "walktrap"),
  alg_abbr = c("FG", "EB", "WT")
)
# number of samples over which to average
n_samp <- 12

# random partition with a given number of parts
random_partition <- function(n, k) {
  stopifnot(k <= n)
  d <- sort(sample(n - 1, size = k - 1))
  m <- rep(1:k, times = c(d, n) - c(0, d))
  m[sample(n)]
}

# initialize hierarchical clustering and community detection datasets
hc_data <- tibble()
cd_data <- tibble()

# loop over included sources
for (i in source_incl) {
  f <- source_info$file[i]
  print(as.character(f))
  # load data
  d <- readRDS(paste0(sdir, f))
  # sample size (number of patients)
  n <- unique(d$n)
  # (number of) distinct diseases
  v <- as.character(unique(unlist(d[, 1:2])))
  m <- length(v)
  
  # reciprocal odds ratio matrix
  r <- d %>%
    select(1:2, oddsratiommle) %>%
    setNames(c("concept_1", "concept_2", "oddsratiommle")) %>%
    {bind_rows(., select(
      .,
      concept_1 = concept_2, concept_2 = concept_1,
      oddsratiommle = oddsratiommle
    ))} %>%
    bind_rows(tibble(
      concept_1 = v,
      concept_2 = v,
      oddsratiommle = Inf
    )) %>%
    spread(key = concept_2, value = oddsratiommle) %>%
    {
      s <- as.matrix(select(., -1))
      rownames(s) <- pull(., 1)
      s
    } %>%
    {1/.} %>%
    as.dist()
  
  # networks at two key TWERs
  g_.05 <- graph_from_pairs_data(data = d, pval_cutoff = .05,
                                 weighted = FALSE)
  g_B.05 <- graph_from_pairs_data(data = d, pval_cutoff = .05 / choose(m, 2),
                                  weighted = FALSE)
  g_.05 <- permute(g_.05, match(vertex_attr(g_.05, "name"), labels(r)))
  g_B.05 <- permute(g_B.05, match(vertex_attr(g_B.05, "name"), labels(r)))
  stopifnot(all(vertex_attr(g_.05, "name") == labels(r)))
  stopifnot(all(vertex_attr(g_B.05, "name") == labels(r)))
  
  # hierarchical clustering
  hc <- hclust(d = r, method = "complete")
  hts <- hc$height
  pb <- dplyr::progress_estimated(n = 158)
  dunns_hc <- sapply(2:159, function(k) {
    pb$tick()$print()
    ct <- cutree(hc, k = k)
    stopifnot(all(labels(r) == names(ct)))
    #fpc::cluster.stats(r, ct)$dunn
    clValid::dunn(r, ct)
  })
  mods_.05_hc <- sapply(2:159, function(k) {
    ct <- cutree(hc, k = k)
    modularity(g_.05, ct)
  })
  mods_B.05_hc <- sapply(2:159, function(k) {
    ct <- cutree(hc, k = k)
    modularity(g_B.05, ct)
  })
  
  # Dunn indices for random partitions (average over `n_samp`)
  mean_dunns <- rep(NA_real_, 159)
  sd_dunns <- rep(NA_real_, 159)
  pb <- dplyr::progress_estimated(n = 158)
  for (k in 2:159) {
    pb$tick()$print()
    dunns_rand <- rep(NA_real_, n_samp)
    for (l in 1:n_samp) {
      #s <- sample(1:k, size = 160, replace = TRUE)
      s <- random_partition(160, k)
      #dunns_rand[l] <- fpc::cluster.stats(r, s)$dunn
      dunns_rand[l] <- clValid::dunn(r, s)
    }
    mean_dunns[k] <- mean(dunns_rand)
    sd_dunns[k] <- sd(dunns_rand)
  }
  
  # modularities for random partitions of .05 and B-.05 networks
  mean_mods_.05 <- rep(NA_real_, 159)
  sd_mods_.05 <- rep(NA_real_, 159)
  mean_mods_B.05 <- rep(NA_real_, 159)
  sd_mods_B.05 <- rep(NA_real_, 159)
  pb <- dplyr::progress_estimated(n = 158)
  for (k in 2:159) {
    pb$tick()$print()
    mods_.05_rand <- rep(NA_real_, n_samp)
    mods_B.05_rand <- rep(NA_real_, n_samp)
    for (l in 1:n_samp) {
      #s <- sample(1:k, size = 160, replace = TRUE)
      s <- random_partition(160, k)
      mods_.05_rand[l] <- modularity(g_.05, s)
      mods_B.05_rand[l] <- modularity(g_B.05, s)
    }
    mean_mods_.05[k] <- mean(mods_.05_rand)
    sd_mods_.05[k] <- sd(mods_.05_rand)
    mean_mods_B.05[k] <- mean(mods_B.05_rand)
    sd_mods_B.05[k] <- sd(mods_B.05_rand)
  }
  
  # hierarchical clustering data
  hc_data_i <- tibble(
    source = as.character(source_info$abbr[i]),
    height = hts,
    n = 1:159,
    dunn = c(NA, dunns_hc),
    mod_.05 = c(NA, mods_.05_hc),
    mod_B.05 = c(NA, mods_B.05_hc),
    dunn_null_mean = mean_dunns, dunn_null_sd = sd_dunns,
    mod_.05_mean = mean_mods_.05, mod_.05_sd = sd_mods_.05,
    mod_B.05_mean = mean_mods_B.05, mod_B.05_sd = sd_mods_B.05
  )
  
  # community partitions
  cd_data_i <- tibble()
  
  # iterate over choices of p-value threshold (.05 and Bonferroni correction)
  ths_data <- tibble(
    th = c(.5 * 10^(-(0:5)), .05 / choose(m, 2)),
    th_str = c(paste0(5, "e-", 1:6), "B-.05")
  )
  
  for (j in 1:nrow(ths_data)) {
    print(Sys.time())
    print(c(as.character(f), signif(ths_data$th[j], 3)))
    
    # construct graph
    g <- graph_from_pairs_data(
      data = d,
      pval_cutoff = ths_data$th[j],
      weighted = FALSE
    )
    stopifnot(all(vertex_attr(g, "name") %in% labels(r)))
    g <- permute(g, match(vertex_attr(g, "name"), labels(r)))
    stopifnot(all(vertex_attr(g, "name") == labels(r)))
    
    # calculate community partitions
    ch_data_ij <- lapply(alg_data$algorithm, function(s) {
      cluster_fun <- get(paste0("cluster_", s))
      partn <- cluster_fun(g)
      #dunn <- fpc::cluster.stats(r, partn$membership)$dunn
      dunn <- clValid::dunn(r, partn$membership)
      mod <- modularity(g, partn$membership)
      # data
      tibble(
        pval_threshold = ths_data$th_str[j], algorithm = s,
        n = length(sizes(partn)), dunn = dunn, modularity = mod
      )
    }) %>%
      bind_rows()
    print(ch_data_ij)
    cd_data_i <- bind_rows(cd_data_i, ch_data_ij)
  }
  
  # average height of cuts surrounding the number of clusters
  cd_data_i <- cd_data_i %>%
    mutate(n1 = n + 1) %>%
    left_join(select(hc_data_i, h = height, n), by = "n") %>%
    left_join(select(hc_data_i, h1 = height, n1 = n), by = "n1") %>%
    mutate(avg_height = sqrt(h * h1)) %>%
    select(-h, -h1, -n1)
  cd_data_i <- cd_data_i %>%
    left_join(alg_data, by = "algorithm") %>%
    unite(pval_threshold, alg_abbr, col = "label", sep = "-", remove = FALSE)
  cd_data_i <- cd_data_i %>%
    mutate(source = as.character(source_info$abbr[i]))
  
  # bind to external datasets
  hc_data <- bind_rows(hc_data, hc_data_i)
  cd_data <- bind_rows(cd_data, cd_data_i)
  
  # save partial datasets
  saveRDS(hc_data, file = paste0(tdir, "hc-data.rds"))
  saveRDS(cd_data, file = paste0(tdir, "cd-data.rds"))
}
