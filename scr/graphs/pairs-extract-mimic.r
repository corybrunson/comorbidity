# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(tidyr)
library(dplyr)

# source and target data directories
sdir <- paste0("~/Documents/CQM/mimic/mimic-demo/physionet.org/works/",
               "MIMICIIIClinicalDatabaseDemo/files/version_1_4/")
sdir <- "~/Documents/CQM0/mimic/mimic-code/buildmimic/postgres/-A/"
sdir <- "~/Documents/CQM0/mimic-iii/"
tdir <- "~/Documents/CQM/comorbidity/data/pairs/"

# notation
# n = Total sample size
# n1 = Subjects with feature 1
# n2 = Subjects with feature 2
# n12 = Subjects with features 1 and 2
# n1_2 = n1 - n12
# n2_1 = n2 - n12
# n_12 = n - n1_2 - n2_1 - n12

# MIMIC-III Demo
# https://mimic.physionet.org/gettingstarted/demo/
#data <- read.csv(paste0(sdir, "DIAGNOSES_ICD.csv"),
#                 colClasses = "character")
# MIMIC-III
# https://mimic.physionet.org/
data <- read.csv(gzfile(paste0(sdir, "DIAGNOSES_ICD.csv.gz")),
                 colClasses = "character")

names(data) <- tolower(names(data))
n <- dplyr::n_distinct(data$subject_id)
data <- data %>%
  select(subject_id, icd9_code) %>%
  filter(icd9_code != "") %>%
  distinct()
ns <- table(data$icd9_code)
ns <- data.frame(icd9 = names(ns),
                 n = as.vector(ns))
data <- data[data$subject_id %in% data$subject_id[duplicated(data$subject_id)],
             ]
data <- data %>% group_by(subject_id) %>%
  do(data.frame(t(combn(.$icd9_code, 2)))) %>%
  ungroup(data) %>%
  select(X1, X2) %>%
  rename(icd9_1 = X1, icd9_2 = X2) %>%
  as.data.frame()
# aggregate
data[which(data$icd9_1 > data$icd9_2), ] <-
  data[which(data$icd9_1 > data$icd9_2), 2:1]
data$n12 <- 1
data <- aggregate(n12 ~ icd9_1 + icd9_2, data = data, FUN = sum)
# merge in frequencies
data$n <- n
data <- merge(data,
              transform(ns, n1 = n)[, c("icd9", "n1")],
              by.x = "icd9_1", by.y = "icd9",
              all.x = TRUE, all.y = FALSE)
data <- merge(data,
              transform(ns, n2 = n)[, c("icd9", "n2")],
              by.x = "icd9_2", by.y = "icd9",
              all.x = TRUE, all.y = FALSE)
data <- data[do.call(order, data[, c("icd9_1", "icd9_2")]),
             c(2:1, 3, 5:6, 4)]
saveRDS(data, file = paste0(tdir, "mimic-iii.rds"))
