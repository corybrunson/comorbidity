# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(tidyverse)
source("R/graph-construction.r")
source("R/utils.r")
# a named list of named lists of data frames
load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"
load(paste0(sdir, "concordance-data.RData"))

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
prevalences <- tibble()
for (i in seq_along(source_info$file)) {
  f <- source_info$file[i]
  print(f)
  # load data
  d <- readRDS(paste0(sdir, f))
  # sample size (number of patients)
  n <- unique(d$n)
  # distinct diseases and prevalences
  dd <-
    bind_rows(
      d %>% select(1, n1) %>% setNames(c("disorder", "prevalence")),
      d %>% select(2, n2) %>% setNames(c("disorder", "prevalence"))
    ) %>%
    distinct() %>%
    mutate(source = source_info$source[i])
  prevalences <- bind_rows(prevalences, dd)
}

centrality_prevalence <- concordance_data %>%
  filter(measure == "unit" & cutoff == "B-.05") %>%
  select(-measure, -cutoff, type = centrality, centrality = value) %>%
  left_join(prevalences, by = c("source" = "source", "node" = "disorder"))
print(centrality_prevalence)

centrality_prevalence_plot <- centrality_prevalence %>%
  ggplot(aes(centrality, prevalence)) +
  facet_wrap(~ abbr + type, ncol = 3, scales = "free",
             labeller = as_labeller(label_value, multi_line = FALSE)) +
  geom_point()
ggsave(
  centrality_prevalence_plot,
  height = 16, width = 8,
  filename = "fig/centrality-prevalence.pdf"
)
