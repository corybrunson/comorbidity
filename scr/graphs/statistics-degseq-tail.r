
# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/graphs/tail-estimation/"
tdir <- "~/Documents/CQM/comorbidity/data/graphs/tail-estimation/"

# all degree distribution data files
files <- list.files(sdir, "\\.dat$", full.names = TRUE)

# function to execute Python script on a file
tail_estimation <- function(file) {
  name <- gsub("\\.dat", "", basename(file))
  cmd <- paste(
    "python",
    "src/tail-estimation.py",
    file,
    paste0(tdir, paste0(name, ".pdf"))
  )
  system(cmd)
}

for (file in files) tail_estimation(file)

list.files(tdir, "\\.pdf", full.names = FALSE)
