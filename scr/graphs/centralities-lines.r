# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(tidyr)
library(ggplot2)
source("R/utils.r")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

load(paste0(sdir, "concordance-data.RData"))

# add ranks and filter to desired p-value cutoffs
concordance_data <- concordance_data %>%
  filter(!stringr::str_detect(cutoff, "^B-")) %>%
  mutate(pval = as.numeric(as.character(cutoff))) %>%
  filter(pval < .5) %>%
  group_by(source, cutoff, measure, centrality) %>%
  mutate(rank = min_rank(desc(value))) %>%
  ungroup()# %>%
  #left_join(select(measure_info, measure, measure_abbr = abbr), by = "measure")

# for each data source and each centrality measure,
# line plots of most central nodes across p-value cutoffs,
# highlighting nodes that changed most in rank

# number of "top" nodes to include
n <- 24
# max and min p-values
p_ran <- concordance_data %>% pull(pval) %>% range()

# loop over binary association measures
for (m in as.character(unique(concordance_data$measure))) {
  print(m)
  
  # list of plots
  l <- list()
  
  # loop over sources and measures
  for (s in as.character(unique(concordance_data$abbr))) {
    print(s)
    for (a in as.character(unique(concordance_data$centrality))) {
      print(a)
      
      # data subset
      d <- concordance_data %>%
        filter(abbr == s & centrality == a & measure == m)
      
      # top `n` nodes at each cutoff 
      d_n <- d %>%
        group_by(cutoff) %>% top_n(n, value) %>% ungroup() %>%
        select(node) %>% distinct()
      # plot
      top_lines <- d %>%
        inner_join(d_n, by = "node") %>%
        ggplot(aes(x = pval, y = value, group = node)) +
        geom_line() +
        scale_x_continuous(breaks = .5*10^(-(0:6)), trans = "log")
      
      # subset of at most 120 nodes
      #d_s <- d %>% select(node) %>% distinct() %>% sample_n(120)
      
      # flag nodes with greatest differences in rank between extremes
      d_r <- d %>%
        filter(pval == p_ran[1] | pval == p_ran[2]) %>%
        select(cutoff, node, rank) %>%
        spread(key = cutoff, value = rank) %>%
        mutate(diff = `5e-06` - `0.05`) %>%
        select(node, diff) %>%
        {bind_rows(
          mutate(top_n(., n/2, diff), direction = "increase"),
          mutate(top_n(., n/2, -diff), direction = "decrease")
        )}
      top_diffs <- d %>%
        inner_join(d_r, by = "node") %>%
        ggplot(aes(x = pval, y = value, group = node)) +
        theme_bw() +
        scale_x_continuous(breaks = .5*10^(-(0:6)), trans = "log") +
        geom_line(data = d, color = "lightgrey") +
        geom_line(aes(linetype = direction), show.legend = FALSE) +
        xlab("p-value cutoff") + ylab("centrality") +
        ggtitle(
          paste0(a, " centralities of disorders in the ",
                 m, "-based ", s, " network"),
          paste0("highlighting the ", n/2,
                 " disorders each with greatest increase and decrease in rank")
        )
      
      l <- c(l, list(top_diffs))
    }
  }
  
  ggsave(
    gridExtra::marrangeGrob(grobs = l, nrow = 1, ncol = 1),
    height = 6, width = 6,
    filename = paste0("fig/centrality-", m, "-lines-pages.pdf")
  )
  
  ggsave(
    gridExtra::marrangeGrob(grobs = l, nrow = 3, ncol = 8),
    height = 12, width = 36,
    filename = paste0("fig/centrality-", m, "-lines-grid.pdf")
  )
  
}
