
# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")

# source data directories
sdir <- "~/Documents/CQM/comorbidity/data/graphs/tail-estimation/"

# PDFs rendered by `tail-estimation.py`
pdfs <- list.files(sdir, "\\.pdf$")
# text file of xi estimates
ests <- read.csv(file.path(sdir, "estimators.csv", fsep = ""), header = TRUE)
# ensure they agree
stopifnot(length(pdfs) == nrow(ests))
stopifnot(all(tools::file_path_sans_ext(pdfs) %in% ests$file))

# source names
source("R/utils.r")

# LaTeX table of estimators
library(dplyr)
library(stringr)
library(tidyr)
ests %>%
  mutate(file = str_replace(file, "-(3|5)", "_\\1")) %>%
  mutate(file = str_replace(file, "mimic-iii", "mimic_iii")) %>%
  separate(file, c("source", "pval", "correction"), sep = "-") %>%
  mutate(source = str_replace(source, "\\_", "-")) %>%
  left_join(select(source_info, source, abbr), by = "source") %>%
  select(-source) %>% select(abbr, everything()) %>%
  mutate(pval = str_replace(pval, "\\_", ".")) %>%
  mutate(correction = ifelse(correction == "none", "--", toupper(correction))) %>%
  mutate_at(vars(hill:kernel), list(gamma = ~ 1 / . + 1)) %>%
  rename_at(vars(hill:kernel), funs(str_c(., "_xi"))) %>%
  print() -> estimates
estimates %>%
  rename(
    Dataset = abbr, `p-value` = pval, Correction = correction,
    `\\(\\xi_\\text{Hill}\\)` = hill_xi,
    `\\(\\xi_\\text{moments}\\)` = moments_xi,
    `\\(\\xi_\\text{kernel}\\)` = kernel_xi,
    `\\(\\gamma_\\text{Hill}\\)` = hill_gamma,
    `\\(\\gamma_\\text{moments}\\)` = moments_gamma,
    `\\(\\gamma_\\text{kernel}\\)` = kernel_gamma
  ) %>%
  print() -> estimates
estimates %>%
  xtable::xtable(
    label = "tab:tail-estimation",
    caption = paste0(
      "Tail index (\\(\\xi\\)) and tail exponent (\\(\\gamma\\)) estimators ",
      "that were successfully estimated on comorbidity networks."
    ),
    align = c(rep("l", 4), rep("r", 6)), digits = 3
  ) %>% xtable::print.xtable(
    file = "tab/tail-estimation.tex",
    size = "scriptsize", caption.placement = "top",
    include.rownames = FALSE,
    sanitize.rownames.function = identity,
    sanitize.colnames.function = identity
  )

# compare xi estimates
pairs(ests[, 2:4])
pairs(log(ests[, 2:4]))
# compare gamma estimates
pairs(1 / ests[, 2:4] + 1)
pairs(log(1 / ests[, 2:4] + 1))
