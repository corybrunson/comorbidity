# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(igraph)
source("R/graph-construction.r")
source("R/utils.r")
source("R/params.r")
load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

# restrict to representative datasets
source_info <- source_info %>%
  filter(!grepl("namcs|vaers", file))

# Compare the centrality rankings for a single dataset, using different 
# statistical cutoffs and binary association measures (including none, i.e. unit
# edge weights)

# grid of graph construction settings
conc_grid <- as_tibble(expand.grid(
  pval = 0.01,
  corr = param_vals$correction,
  measure = c("unit", param_vals$measure)
))
# 0 < x < 1 ~> 1 < y < Inf
reciprocal <- function(x) 1 / x
# 1 < x < Inf ~> 1 < y < Inf
shifted_reciprocal <- function(x) 1 / (x - 1) + 1

# initialize concordance data
concordance_data <- tibble()
# loop over data sources
for (i in 1:nrow(source_info)) {
  # load data
  d <- readRDS(paste0(sdir, source_info$file[i]))
  
  # initialize data frame of concordance measures
  conc_dat <- vector("list", nrow(conc_grid))
  # iterate over the rows of the data frame
  for (j in 1:nrow(conc_grid)) {
    print(cbind(source = source_info$source[i], conc_grid[j, ]))
    
    # graph with both association and distance weights
    g <- graph_from_pairs_data(
      data = d,
      pval_cutoff = conc_grid$pval[j],
      pval_correction = as.character(conc_grid$corr[j]),
      # `NULL` if 'unit', else values from `d`
      measure = d[[as.character(conc_grid$measure[j])]],
      weighted = (as.character(conc_grid$measure[j]) != "unit")
    )
    # want link weights to have minimum value 1
    fun_inv <- switch(
      as.character(conc_grid$measure[j]),
      unit = identity,
      oddsratiommle = shifted_reciprocal,
      phi = reciprocal,
      forbes = shifted_reciprocal,
      rho_psych = reciprocal,
      jaccard = reciprocal
    )
    g <- set_edge_attr(g, "weight_inv", value = fun_inv(edge_attr(g, "weight")))
    
    # data frame of centrality measures
    cent_dat <- tibble(
      name = vertex_attr(g, "name"),
      degree = strength(g, weights = edge_attr(g, "weight")),
      betweenness = betweenness(g, weights = edge_attr(g, "weight_inv")),
      closeness = closeness(g, weights = edge_attr(g, "weight_inv"))
    )
    cent_dat <- tidyr::gather(
      cent_dat, "centrality", "value",
      degree, betweenness, closeness, factor_key = TRUE
    )
    # append to data list
    conc_dat[[j]] <- as_tibble(cbind(
      source_info[i, -1],
      conc_grid[j, ],
      cent_dat
    ))
  }
  
  # bind to concordance data
  concordance_data <- bind_rows(concordance_data, bind_rows(conc_dat))
  save(concordance_data, file = paste0(tdir, "concordance-data.RData"))
}
