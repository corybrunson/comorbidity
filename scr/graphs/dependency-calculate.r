# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(igraph)
#library(keyplayer)
source("R/utils.r")
source("R/graph-construction.r")
source("R/graph-dependency.r")
# a named list of named lists of data frames
load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM0/comorbidity/data/dependency/"

thetas <- c(".05", "B-.05")

for (j in length(thetas):1) {
  for (i in c(8, 5, 3)) {
    # continue only if a suitable mapping exists
    sub_map_info <- map_info %>%
      filter(ontology == source_info$ontology[i])
    if (nrow(sub_map_info) == 0) next
    # check for file
    print(paste0(source_info$source[i], "; ", thetas[j]))
    dc_file <- paste0(
      tdir,
      "dependencycube-",
      source_info$source[i], "-",
      gsub("\\.|-", "", thetas[j]),
      ".rda"
    )
    if (file.exists(dc_file)) next
    # load data
    d <- readRDS(paste0(sdir, source_info$file[i]))
    n <- n_distinct(unlist(d[, 1:2]))
    pval_cutoffs <- data.frame(
      pval = c(.05, .05 / n),
      label = thetas
    )
    # construct graph
    g <- graph_from_pairs_data(
      data = d,
      measure = "oddsratiommle", measure_cutoff = 1,
      pval_cutoff = pval_cutoffs$pval[j],
      weighted = FALSE
    )
    rm(d)
    # estimated dependency cube
    dc <- dependency_cube_matrix(g)
    save(dc, file = dc_file)
  }
}
