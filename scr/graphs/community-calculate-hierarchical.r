# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(stringr)
library(dplyr)
library(tidyr)
library(ggplot2)
library(igraph)
source("R/graph-construction.r")
source("R/utils.r")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

# restrict to sources for which all pairs are available
#source_incl <- c(1, 8)
source_incl <- c(1)
# number of samples over which to average
n_samp <- 12

# reciprocal odds ratio matrix
reciprocal_odds_ratio_matrix <- function(d) {
  d %>%
    select(1:2, oddsratiommle) %>%
    setNames(c("concept_1", "concept_2", "oddsratiommle")) %>%
    {bind_rows(., select(
      .,
      concept_1 = concept_2, concept_2 = concept_1,
      oddsratiommle = oddsratiommle
    ))} %>%
    bind_rows(tibble(
      concept_1 = v,
      concept_2 = v,
      oddsratiommle = Inf
    )) %>%
    spread(key = concept_2, value = oddsratiommle) %>%
    {
      s <- as.matrix(select(., -1))
      rownames(s) <- pull(., 1)
      s
    } %>%
    {1/.} %>%
    as.dist()
}

# initialize tibbles
compare_data <- tibble()
concord_data <- tibble()

# loop over included sources
for (i in source_incl) {
  f <- source_info$file[i]
  print(as.character(f))
  # load data
  d <- readRDS(paste0(sdir, f))
  # sample size (number of patients)
  n <- unique(d$n)
  # (number of) distinct diseases
  v <- as.character(unique(unlist(d[, 1:2])))
  m <- length(v)
  
  # reciprocal odds ratio matrix
  r <- reciprocal_odds_ratio_matrix(d)
  
  # networks at two key TWERs
  g <- graph_from_pairs_data(data = d, pval_cutoff = .05, weighted = FALSE)
  gB <- graph_from_pairs_data(data = d, pval_cutoff = .05 / choose(m, 2),
                              weighted = FALSE)
  g <- igraph::permute(g, match(vertex_attr(g, "name"), labels(r)))
  gB <- igraph::permute(gB, match(vertex_attr(gB, "name"), labels(r)))
  stopifnot(all(vertex_attr(g, "name") == labels(r)))
  stopifnot(all(vertex_attr(gB, "name") == labels(r)))
  
  # hierarchical clusterings
  hc_complete <- hclust(d = r, method = "complete")
  hc_single <- hclust(d = r, method = "single")
  hc_diana <- as.hclust(cluster::diana(x = r))
  
  # community detections
  hc_fastgreedy <- as.hclust(igraph::cluster_fast_greedy(g))
  hc_edgebtw <- as.hclust(igraph::cluster_edge_betweenness(g))
  hc_walktrap <- as.hclust(igraph::cluster_walktrap(g))
  hc_fastgreedyB <- as.hclust(igraph::cluster_fast_greedy(gB))
  hc_edgebtwB <- as.hclust(igraph::cluster_edge_betweenness(gB))
  hc_walktrapB <- as.hclust(igraph::cluster_walktrap(gB))
  
  # all hierarchical partitions
  ps <- c(
    "complete", "single", "diana",
    "fastgreedy", "edgebtw", "walktrap",
    "fastgreedyB", "edgebtwB", "walktrapB"
  )
  
  # compare Dunn and modularity at each cut for each method
  compare_data_i <- tibble(
    source = as.character(source_info$source[i]),
    k = 1:(m-1)
  )
  for (p in ps) {
    # hierarchical partition
    hc <- get(paste0("hc_", p))
    # check validity
    for (k in 2:(m-1)) {
      stopifnot(all(labels(r) == names(cutree(hc, k = k))))
    }
    # Dunn index
    pb <- dplyr::progress_estimated(n = (m - 2))
    compare_data_i[[paste0("dunn_", p)]] <- c(NA, sapply(2:(m-1), function(k) {
      pb$tick()$print()
      #fpc::cluster.stats(r, ct)$dunn
      clValid::dunn(r, cutree(hc, k = k))
    }))
    # modularity Q
    compare_data_i[[paste0("mod_", p)]] <- c(NA, sapply(2:(m-1), function(k) {
      modularity(g, cutree(hc, k = k))
    }))
  }
  compare_data <- bind_rows(compare_data, compare_data_i)
  saveRDS(compare_data, file = paste0(tdir, "hc-compare-data.rds"))
  
  # condordance of methods
  concord_data_i <- tibble()
  for (p in 1:(length(ps)-1)) {
    # hierarchical partition
    hc_p <- get(paste0("hc_", ps[p]))
    for (q in (p+1):length(ps)) {
      # hierarchical partition
      hc_q <- get(paste0("hc_", ps[q]))
      
      # adjusted Rand index
      pb <- dplyr::progress_estimated(n = (m-2)^2)
      rand_pq <- sapply(2:(m-1), function(k) {
        ct_p <- cutree(hc_p, k = k)
        sapply(2:(m-1), function(l) {
          ct_q <- cutree(hc_q, k = l)
          pb$tick()$print()
          mclust::adjustedRandIndex(ct_p, ct_q)
        })
      })
      
      # variation of information metric
      pb <- dplyr::progress_estimated(n = (m-2)^2)
      voi_pq <- sapply(2:(m-1), function(k) {
        ct_p <- cutree(hc_p, k = k)
        sapply(2:(m-1), function(l) {
          ct_q <- cutree(hc_q, k = l)
          pb$tick()$print()
          igraph::compare(ct_p, ct_q)
        })
      })
      
      # contribution to concordance data
      # (`l` and `k` above correspond to rows and columns, respectively)
      # (hence 'k_1' uses 'each' and 'k_2' uses 'times')
      concord_data_ij <- tibble(
        method_1 = ps[p],
        method_2 = ps[q],
        k_1 = rep(2:(m-1), each = m-2),
        k_2 = rep(2:(m-1), times = m-2),
        rand = as.vector(rand_pq),
        voi = as.vector(voi_pq)
      )
      concord_data_i <- bind_rows(concord_data_i, concord_data_ij)
    }
  }
  concord_data_i$source <- as.character(source_info$source[i])
  
  concord_data <- bind_rows(concord_data, concord_data_i)
  saveRDS(concord_data, file = paste0(tdir, "hc-concord-data.rds"))
}

stop()
#compare_data <- readRDS(paste0(tdir, "hc-compare-data.rds"))
#concord_data <- readRDS(paste0(tdir, "hc-concord-data.rds"))

method_info <- tibble(
  method = forcats::fct_inorder(factor(c(
    "complete", "single", "diana",
    "fastgreedy", "edgebtw", "walktrap",
    "fastgreedyB", "edgebtwB", "walktrapB"
  ))),
  technique = c(
    "complete", "single", "diana",
    rep(c("fastgreedy", "edgebtw", "walktrap"), times = 2)
  ),
  type = c(
    rep("hierarchical clustering", 3), rep("community detection", 6)
  ),
  pval = as.factor(c(
    rep("NA", 3), rep(".05", 3), rep("B-.05", 3)
  ))
)

# evaluation plots
readRDS(paste0(tdir, "hc-compare-data.rds")) %>%
  gather("index_method", "value", matches("^(dunn_|mod_)")) %>%
  separate("index_method", c("index", "method"), sep = "_") %>%
  mutate(method = factor(method, levels = method_info$method)) %>%
  spread(index, value) %>%
  left_join(method_info, by = "method") %>%
  print() -> compare_data

size_range <- c(.67, 1)
dunn_plot <- ggplot(compare_data, aes(x = k, y = dunn, group = method)) +
  theme_bw() +
  facet_wrap(~ source) +
  scale_y_continuous(trans = "log", breaks = 10^(seq(-8,2,2))) +
  geom_line(aes(linetype = pval, size = type, color = technique)) +
  scale_size_discrete(range = size_range) +
  scale_color_brewer(type = "qual", palette = 2) +
  labs(x = "Cut (number of groups)", y = "Dunn index")
mod_plot <- ggplot(compare_data, aes(x = k, y = mod, group = method)) +
  theme_bw() +
  facet_wrap(~ source) +
  geom_line(aes(linetype = pval, size = type, color = technique)) +
  scale_size_discrete(range = size_range) +
  scale_color_brewer(type = "qual", palette = 2) +
  labs(x = "Cut (number of groups)", y = "Network modularity")
dunn_mod_plot <- ggplot(compare_data, aes(x = dunn, y = mod, group = method)) +
  theme_bw() +
  facet_wrap(~ source) +
  scale_x_continuous(trans = "log", breaks = 10^(seq(-8,2,2))) +
  geom_line(aes(linetype = pval, size = type, color = technique)) +
  scale_size_discrete(range = size_range) +
  scale_color_brewer(type = "qual", palette = 2) +
  labs(x = "Dunn index", y = "Network modularity")

# arrange evaluation plots
ggsave(
  gridExtra::arrangeGrob(
    grobs = list(
      dunn_plot +
        guides(color = "none") +
        theme(
          legend.position = "bottom",
          legend.box = "vertical",
          legend.spacing = unit(-2, "mm")
        ),
      mod_plot +
        guides(linetype = "none", size = "none") +
        theme(legend.position = "bottom", legend.margin = margin(8,8,8,8))
    ), nrow = 1, ncol = 2,
    respect = FALSE
  ),
  height = 6, width = 10,
  filename = "fig/cluster-dunn-modularity-k.pdf"
)

meas_labeller <- as_labeller(c(
  dunn = "Dunn index",
  log_dunn = "log(Dunn index)",
  mod = "Network modularity"
))
compare_data %>%
  filter(source == "columbia") %>%
  mutate(log_dunn = log(dunn)) %>%
  gather("measure", "value", log_dunn, mod) %>%
  ggplot(aes(x = k, y = value, group = method)) +
  theme_bw() +
  facet_wrap(~ measure, scales = "free", ncol = 1, labeller = meas_labeller) +
  geom_line(aes(linetype = pval, size = type, color = technique)) +
  scale_size_discrete(range = size_range) +
  scale_color_brewer(type = "qual", palette = 2) +
  labs(x = "Cut (number of groups)",  y = "") ->
  dunn_mod_k_plot
ggsave(
  "fig/cluster-dunn-modularity-k.pdf",
  dunn_mod_k_plot + theme(
    legend.position = "bottom",
    legend.box = "vertical",
    legend.spacing = unit(-2, "mm")
  ),
  width = 6, height = 10
)

# comparison plots

readRDS(paste0(tdir, "hc-concord-data.rds")) %>%
  unite("comparison", method_1, method_2, sep = "_", remove = FALSE) %>%
  mutate_at(
    vars(starts_with("method_")),
    funs(factor(., levels = method_info$method))
  ) %>%
  print() -> concord_data
rand_tile <- concord_data %>%
  filter_at(
    vars(starts_with("method_")),
    all_vars(stringr::str_detect(., "complete|single|diana|(B$)"))
  ) %>%
  ggplot(aes(x = k_1, y = k_2, color = rand)) +
  theme_void() +
  coord_fixed() +
  facet_grid(method_2 ~ method_1, switch = "both") +
  geom_tile() +
  scale_color_distiller(type = "div") +
  theme(legend.position = "bottom")
voi_tile <- concord_data %>%
  filter_at(
    vars(starts_with("method_")),
    all_vars(stringr::str_detect(., "complete|single|diana|(B$)"))
  ) %>%
  ggplot(aes(x = k_1, y = k_2, color = voi)) +
  theme_void() +
  coord_fixed() +
  facet_grid(method_2 ~ method_1, switch = "both") +
  geom_tile() +
  scale_color_distiller(type = "div") +
  theme(legend.position = "bottom")

initialism <- function(x) {
  x <- snakecase::to_upper_camel_case(x)
  x <- stringr::str_extract_all(x, "[A-Z]")
  sapply(x, stringr::str_c, collapse = "")
}
concord_data %>%
  filter(k_1 == k_2) %>%
  filter_at(
    vars(starts_with("method_")),
    all_vars(stringr::str_detect(., "complete|single|diana|(B$)"))
  ) %>%
  left_join(
    select(method_info, method, type_1 = type),
    by = c("method_1" = "method")
  ) %>%
  left_join(
    select(method_info, method, type_2 = type),
    by = c("method_2" = "method")
  ) %>%
  mutate_at(vars(starts_with("type_")), initialism) %>%
  unite("comparison_type", type_1, type_2, sep = " vs ") %>%
  print() -> concord_subdata
rand_line <- concord_subdata %>%
  ggplot(aes(x = k_1, y = rand, group = comparison)) +
  theme_bw() +
  geom_line(aes(linetype = comparison_type)) +
  labs(x = "Cut (number of groups)", y = "Adjusted Rand index") +
  theme(legend.position = "bottom")
voi_line <- concord_subdata %>%
  ggplot(aes(x = k_1, y = voi, group = comparison)) +
  theme_bw() +
  geom_line(aes(linetype = comparison_type)) +
  labs(x = "Cut (number of groups)", y = "Variance of information") +
  theme(legend.position = "bottom")

# arrange comparison plots
ggsave(
  "fig/cluster-voi-tile.pdf",
  voi_tile +
    theme(
      legend.position = "top",
      strip.text.y = element_text(angle = 270)
    ) +
    labs(color = "VI"),
  width = 6, height = 6
)
ggsave(
  gridExtra::arrangeGrob(
    grobs = list(rand_tile, voi_tile),
    nrow = 1, ncol = 2,
    respect = FALSE
  ),
  height = 6, width = 10,
  filename = "fig/cluster-rand-voi-tile.pdf"
)
ggsave(
  gridExtra::arrangeGrob(
    grobs = list(rand_line, voi_line),
    nrow = 1, ncol = 2,
    respect = FALSE
  ),
  height = 6, width = 10,
  filename = "fig/cluster-rand-voi-line.pdf"
)
