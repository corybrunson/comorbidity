# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(igraph)
library(dplyr)
source("R/graph-distances.r")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

# available stats
files <- list.files(path = sdir)
sources <- gsub("\\.rds$", "", files)
stats <- lapply(files, function(f) {
  names(readRDS(paste0(sdir, f)))
})
names(stats) <- sources
n_records <- sapply(files, function(f) {
  unique(readRDS(paste0(sdir, f))$n)
})
n_disorders <- sapply(files, function(f) {
  readRDS(paste0(sdir, f)) %>% select(1:2) %>%
    tidyr::gather("incidence", "disorder", 1:2) %>%
    select(disorder) %>% n_distinct()
})

# availability of frequency tables + limited number of nodes
incl_ft <- sapply(lapply(stats, match, nomatch = 0L,
                         x = c("n", "n1", "n2", "n12")), all) &
  n_disorders < 1e4
files <- files[incl_ft]
sources <- sources[incl_ft]
stats <- stats[incl_ft]
rm(incl_ft)

# Phase I: Compare the distances between all pairs of (3- or 5-character 
# resolution) ICD-9 codes using different statistical cutoffs, binary 
# association measures (including none, i.e. unit weights), and notions of 
# distance (p-modulus with p=1,2,infinity)

# p-value cutoffs, binary association measures, and notions of distance
cutoffs <- c(.05, .001, "bonferroni.05")
measures <- c("unit", "phi", "oddsratio", "logoddsratio", "jaccard")
distfuns <- list(
  path = function(graph) igraph::distances(graph),
  resistance = resistance_dist_all#,
  #mincut = inverse_min_cut
)
dist_grid <- expand.grid(cutoff = cutoffs,
                         measure = measures,
                         distfun = names(distfuns))
dist_cat <- apply(dist_grid, 1, paste, collapse = "_")
# initialize nested list of distance matrices
if (file.exists(paste0(tdir, "dist-list.RData"))) {
  load(file = paste0(tdir, "dist-list.RData"))
} else {
  dist_list <- vector("list", length(files))
  names(dist_list) <- sources
}
for (i in seq_along(files)) {
  if (!is.null(dist_list[[i]])) next
  cat("\n", files[i], "\n\n")
  # load data
  d <- readRDS(paste0(sdir, files[i]))
  # (number of) distinct diseases
  v <- unique(unlist(d[, 1:2]))
  # initialize data frame of distance matrices
  # note: reverse order from 'combn' call and use 'mat[lower.tri(mat)]'
  dist_dat1 <- as.data.frame(t(combn(v, 2))[, 2:1])
  dist_dat2 <- vector("list", length(dist_cat))
  names(dist_dat2) <- dist_cat
  # iterate over the rows of the data frame
  for (j in 1:length(dist_cat)) {
    cat(dist_cat[j], "\n")
    
    # impose p-value cutoff
    cutoff <- if (dist_grid$cutoff[j] != "bonferroni.05") {
      as.numeric(as.character(dist_grid$cutoff[j]))
    } else {
      .05 / choose(length(v), 2)
    }
    #d <- d[d$pval_fisher < cutoff, ]
    # use binary association measure as edge weights
    d$weight <- if (dist_grid$measure[j] == "unit") 1 else {
      d[[as.character(dist_grid$measure[j])]]
    }
    # remove entries with negative measure values
    d <- d[d$weight >= 0, ]
    
    # construct graph
    g <- graph_from_data_frame(
      d = d[, c(1:2,
                grep("^pval_fisher$", names(d)),
                grep("^weight$", names(d)))],
      directed = FALSE
    )
    # impose p-value cutoff
    g <- delete_edges(g, which(E(g)$pval_fisher < cutoff))
    
    # calculate distances
    dists <- try((distfuns[[as.character(dist_grid$distfun[j])]])(g))
    dist_dat2[[j]] <- if (class(dists) == "try-error") {
      message(dists)
      rep(NA, choose(vcount(g), 2))
    } else {
      dists[lower.tri(dists)]
    }
  }
  dist_list[[i]] <- cbind(dist_dat1, as.data.frame(dist_dat2))
  save(dist_list, file = paste0(tdir, "dist-list.RData"))
}

save(dist_list, file = paste0(tdir, "dist-list.RData"))
rm(dist_list)
