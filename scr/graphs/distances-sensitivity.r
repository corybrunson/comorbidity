# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(vegan)

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

## Phase I

load(paste0(tdir, "dist-list.RData"))
