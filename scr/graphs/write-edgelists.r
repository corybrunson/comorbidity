# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
#library(igraph)
library(dplyr)

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/edgelists/"

# available stats
files <- list.files(path = sdir)
sources <- gsub("\\.rds$", "", files)
stats <- lapply(files, function(f) {
  names(readRDS(paste0(sdir, f)))
})
names(stats) <- sources

# availability of frequency tables
incl_ft <- sapply(lapply(stats, match, nomatch = 0L,
                         x = c("n", "n1", "n2", "n12")), all)
files <- files[incl_ft]
sources <- sources[incl_ft]
stats <- stats[incl_ft]
rm(incl_ft)

# load datasets and print edgelists with weights
# NEED TO DECIDE AN APPROPRIATE WEIGHT FOR MODULUS PURPOSES
for (i in seq_along(files)) {
  data <- readRDS(paste0(sdir, files[i])) %>%
    filter(pval_fisher < .05) %>%
    select(1:2, or)
  write.table(data, file = paste0(tdir, sources[i], ".txt"),
              sep = " ", row.names = FALSE, col.names = FALSE)
}
