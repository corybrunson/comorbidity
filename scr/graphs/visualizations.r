#' Generate network visualizations for each data set using cutoffs that improve
#' interpretability.

# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(igraph)
library(tidygraph)
library(ggraph)
source("R/graph-construction.r")
source("R/utils.r")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/visualization/"

largest_component <- function(g) {
  comp <- components(g)
  induced_subgraph(g, which(comp$membership == which.max(comp$csize)))
}

if (FALSE) {###

dens <- 1e-6
for (i in 1:nrow(source_info)) {
  print(as.character(source_info$source[i]))
  # load data
  d <- readRDS(paste0(sdir, source_info$file[i]))
  # sample size (number of patients)
  n <- unique(d$n)
  # (number of) distinct diseases
  v <- as.character(unique(unlist(d[, 1:2])))
  m <- length(v)
  p <- nrow(d) / choose(m, 2)
  stopifnot(p > dens)
  # iterate over choices of p-value threshold (.05 and Bonferroni correction)
  #ths <- c(.05, .005, .0005, .05 / choose(m, 2))
  #for (th in ths) {
  d$rev_pval <- 1 - d$pval_fisher
  # construct graph
  g <- graph_from_pairs_data(
    data = d,
    measure = "rev_pval", measure_quantile = 1 - (1 - dens / p),
    #pval_cutoff = .05 / choose(m, 2),
    weighted = FALSE
  ) %>% largest_component()
  print(edge_density(g))
  # Fruchterman-Reingold layouts
  pdf(height = 3.5, width = 4, file = paste0(
    "fig/network-viz-", source_info$source[i], ".pdf"
  ))
  par(mar = c(1, 3, 1, 3) + .1)
  plot(
    g, layout = layout_with_fr,
    vertex.size = 3, vertex.color = "grey", vertex.frame.color = NA,
    vertex.label = NA,
    vertex.label.family = "sans", vertex.label.color = "black",
    edge.color = "#88888844"
  )
  dev.off()
  #}
}

}###

# allow specific cut or tolerance range for cut
partition_walktrap <- function(weights = NULL, steps = 4, cut = NULL) {
  tidygraph:::expect_nodes()
  weights <- enquo(weights)
  weights <- rlang::eval_tidy(weights, .E())
  cluster <- cluster_walktrap(graph = .G(), weights = weights, steps = steps)
  partition <- if (is.null(cut)) {
    as.integer(membership(cluster))
  } else if (length(cut) == 1) {
    p <- as.integer(cut_at(cluster, no = cut))
    names(p) <- names(membership(cluster))
    p
  } else if (length(cut) == 2) {
    p <- if (length(cluster) < cut[1]) {
      as.integer(cut_at(cluster, no = cut[1]))
    } else if (length(cluster) > cut[2]) {
      as.integer(cut_at(cluster, no = cut[2]))
    } else {
      as.integer(membership(cluster))
    }
  } else {
    stop("`cut` must be one or two positive integers, or else `NULL`.")
  }
  tidygraph:::desc_enumeration(partition)
}

source_info <- source_info %>%
  dplyr::filter(!grepl("namcs|vaers", file))

for (i in 1:nrow(source_info)) {
  print(as.character(source_info$source[i]))
  
  # load data
  d <- readRDS(paste0(sdir, source_info$file[i]))
  # sample size (number of patients)
  n <- unique(d$n)
  # (number of) distinct diseases
  v <- as.character(unique(unlist(d[, 1:2])))
  m <- length(v)
  
  # distinct diseases with prevalences
  d %>%
    select(1:2, n1, n2) %>%
    tidyr::unite("df1", 1, n1) %>%
    tidyr::unite("df2", 2, n2) %>%
    tidyr::gather("key", "df", 1:2) %>%
    select(df) %>%
    tidyr::separate(df, c("disease", "prevalence"), sep = "_") %>%
    mutate(prevalence = as.integer(prevalence)) %>%
    distinct() %>%
    print() -> f
  
  if (nrow(d) > 1e6) next
  
  # construct graph
  d %>%
    graph_from_pairs_data(
      measure = "oddsratiommle", measure_cutoff = 6,
      pval_cutoff = .05 / choose(m, 2),
      weighted = TRUE
    ) %>%
    largest_component() %>%
    print() -> g
  print(edge_density(g))
  
  g %>%
    as_tbl_graph() %>%
    #activate(links) %>%
    #filter(weight > 2) %>%
    activate(nodes) %>%
    mutate(group = partition_walktrap(cut = c(3, 8))) %>%
    mutate(group = as.character(group)) %>%
    mutate(abb = abbreviate(name, 12)) %>%
    left_join(f, by = c("name" = "disease")) %>%
    ggraph() +
    theme_graph(base_family = "") +
    theme(plot.background = element_rect(fill = "transparent",colour = NA)) +
    geom_edge_link(aes(edge_width = weight), alpha = .2) +
    scale_edge_width_continuous(range = c(.5, 2)) +
    geom_node_point(aes(color = group, size = prevalence)) +
    scale_color_brewer(type = "qual", palette = i) +
    geom_node_text(aes(label = abb), size = 2, nudge_x = -.1, nudge_y = -.15) +
    theme(legend.position = "bottom") +
    #ggtitle(paste0(
    #  source_info$abbr[i],
    #  " network, pruned at FDR-corrected 5%"
    #)) +
    guides(edge_width = "none", color = "none", size = "none") ->
    p
  ggsave(
    paste0("fig/network-ggplot-", source_info$source[i], ".pdf"),
    p, width = 8, height = 8
  )
  ggsave(
    paste0("fig/network-ggplot-", source_info$source[i], ".png"),
    p, width = 8, height = 8, bg = "transparent"
  )
}

stop()

# comparison using different thresholds

# load data
d <- readRDS(paste0(sdir, source_info$file[6]))
# sample size (number of patients)
n <- unique(d$n)
# (number of) distinct diseases
v <- as.character(unique(unlist(d[, 1:2])))
m <- length(v)

# construct graphs at different thresholds
d %>%
  graph_from_pairs_data(
    measure = "oddsratiommle", measure_cutoff = 1.2,
    pval_cutoff = .05,
    weighted = TRUE
  ) %>%
  as_tbl_graph() %>%
  activate(links) %>%
  mutate(threshold = "weak") %>%
  print() -> g1
d %>%
  graph_from_pairs_data(
    measure = "oddsratiommle", measure_cutoff = 2,
    pval_cutoff = .05 / choose(m, 2),
    weighted = TRUE
  ) %>%
  as_tbl_graph() %>%
  activate(links) %>%
  mutate(threshold = "strong") %>%
  print() -> g2

# join graphs at nodes, keeping links distinct
graph_join(g1, g2) %>%
  largest_component() %>%
  as_tbl_graph() %>%
  activate(links) %>%
  mutate(threshold = factor(threshold, levels = c("weak", "strong"))) %>%
  mutate(threshold = forcats::fct_inorder(threshold)) %>%
  mutate(threshold = forcats::fct_recode(
    threshold,
    `p < .05; OR > 1.2` = "weak",
    `p < .05 (Bonferroni); OR > 2` = "strong"
    #weak = "p < .05; OR > 1.2",
    #strong = "p < .05 (Bonferroni); OR > 2"
  )) %>%
  print() -> g

# plot graphs, faceting by threshold
g %>%
  activate(nodes) %>%
  #mutate(group = partition_walktrap(cut = c(3, 8))) %>%
  #mutate(group = as.character(group)) %>%
  mutate(abb = abbreviate(name, 18)) %>%
  ggraph() +
  coord_equal() +
  theme_graph(base_family = "") +
  facet_edges(~ threshold, dir = "v") +
  geom_edge_link(aes(edge_width = weight), alpha = .2) +
  scale_edge_width_continuous(range = c(.5, 2)) +
  geom_node_point() +
  #geom_node_point(aes(color = group)) +
  scale_color_brewer(type = "qual", palette = 6) +
  geom_node_text(aes(label = abb), size = 2, nudge_x = -.1, nudge_y = -.15) +
  theme(legend.position = "bottom") +
  guides(edge_width = "none", color = "none") +
  expand_limits(x = c(-1.2, 1.2), y = c(-1.2, 1.2)) ->
  p
p

ggsave(
  paste0("fig/network-ggplot-stanford-compare.pdf"), p,
  height = 8, width = 5
)
