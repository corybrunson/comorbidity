# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(tidyverse)
library(broom)
library(ordr)
source("R/utils.r")

# note: previously used a more limited selection of p-values
readRDS(data, file = "data/graphs/graphs-summary.rds") %>%
  as_tibble() %>%
  filter(! grepl("namcs|vaers", source)) %>%
  rename(
    pval = pval_threshold,
    correction = pval_correction,
    cutoff = measure_cutoff
  ) %>%
  mutate(measure = fct_explicit_na(measure, "unit")) %>%
  left_join(
    measure_info %>%
      select(measure, meas_abbr = abbr, meas_tex = abbr_tex),
    by = "measure"
  ) %>%
  left_join(
    source_info %>%
      select(source, source_abbr = abbr),
    by = "source"
  ) %>%
  mutate_if(is.character, list(~factor(., levels = unique(.)))) %>%
  mutate_if(is.factor, list(~factor(., levels = unique(.)))) %>%
  print() -> data
# number of rows for each correction and measure
(data %>% select(correction, measure) %>% table(useNA = "always"))

# export stats table to CSV
data %>%
  select(
    source = source_abbr, pval, correction, measure, cutoff,
    density:modularity_wt
  ) %>%
  mutate_at(
    vars(density:modularity_wt),
    ~ format(., digits = 3, nsmall = 3, scientific = FALSE)
  ) %>%
  write_csv(path = "tab/summ.csv")

# graph statistics notation
graph_stats <- tribble(
  ~statistic, ~statistic_expr,
  "connected_prop_logit", expression(logit(LCP)),
  "degree_assortativity", expression(italic(r)),
  "degree_gini", expression(italic(G)),
  "degree_mean_log", expression(log(bar(italic(k)))),
  "degree_param1", expression(hat(mu)),
  "degree_param2", expression(hat(sigma)),
  "density_log", expression(log(delta)),
  "distance_mean_log", expression(log(bar(italic(l)))),
  "modularity_wt", expression(italic(Q)),
  "triad_closure", expression(italic(C))
)

# graph statistics versus data attributes
vsdata <- data %>%
  filter(correction == "none") %>%
  gather(key = "parameter", value = "param_value",
         sample_size, disease_count, pval) %>%
  mutate(connected_prop_logit = qlogis(connected_prop),
         degree_mean_log = log(degree_mean),
         density_log = log(density),
         distance_mean_log = log(distance_mean)) %>%
  gather(key = "statistic", value = "stat_value",
         connected_prop_logit, density_log,
         degree_mean_log, degree_gini, degree_param1, degree_param2,
         degree_assortativity, triad_closure, distance_mean_log,
         modularity_wt)
pdf(height = 36, width = 12, file = "fig/summ-versus.pdf")
ggplot(vsdata, aes(x = param_value, y = stat_value, color = source_abbr)) +
  theme_bw() +
  theme(
    axis.text.x = element_text(angle = 30, hjust = 1),
    legend.position = "bottom",
    legend.key.width = unit(.05, "npc")
  ) +
  scale_x_continuous(trans = "log") +
  facet_grid(statistic ~ parameter, scales = "free") +
  geom_jitter(aes(shape = meas_abbr), alpha = .5) +
  scale_shape(solid = FALSE) +
  stat_summary(aes(group = source), fun.y = median, geom = "line") +
  guides(shape = guide_legend(nrow = 2), color = guide_legend(nrow = 2))
dev.off()

# duplicate 'unit' data for each measure, since they'd all yield the same
baseline_measures <- function(d) {
  d %>%
    mutate(
      unit = (measure == "unit"),
      measure = ifelse(unit, NA, as.character(measure))
    ) %>%
    full_join(tibble(
      unit = TRUE,
      measure2 = c("oddsratiommle", "phi", "forbes", "rho_psych")
    ), by = "unit") %>%
    mutate(measure = ifelse(is.na(measure), measure2, measure)) %>%
    select(-unit, -measure2)
}

# standard deviation of densities due to specific cutoffs
# evidential cutoffs
data %>%
  filter(correction == "none" & measure == "unit") %>%
  select(source, pval, density) %>%
  group_by(source) %>%
  summarise(sd_density = sd(density)) %>%
  print()
# evaluative cutoffs (average of SDs over each source and evidential cutoff)
data %>%
  filter(correction == "none") %>%
  # replace missing values with "zero" (i.e. no cutoff) values,
  # since networks were constructed without negative links
  baseline_measures() %>%
  left_join(select(measure_info, measure, zero), by = "measure") %>%
  mutate(cutoff = ifelse(is.na(cutoff), zero, cutoff)) %>%
  select(source, pval, measure, cutoff, density) %>%
  group_by(source, pval) %>%
  summarise(sd_density = sd(density)) %>%
  group_by(source) %>%
  summarise(mean_sd_density = mean(sd_density)) %>%
  print()

# linear models applied to each statistic
data %>%
  # simplify models by using only uncorrected p-value cutoffs
  filter(correction == "none") %>% select(-correction) %>%
  gather(
    key = "statistic", value = "value",
    connected_prop,
    degree_mean, degree_gini,
    degree_param1, degree_param2,
    degree_assortativity, triad_closure, distance_mean,
    modularity_wt
  ) %>%
  merge(stat_info, by = "statistic") %>%
  group_by(statistic, stat_abbr) %>% nest() %>%
  # add average values for graphs without evaluative cutoffs
  mutate(
    value_mean = data %>%
      lapply(filter, measure == "unit") %>%
      lapply(summarize, mean_value = mean(value, na.rm = TRUE)) %>%
      sapply(pull, mean_value)
  ) %>%
  # response variable = difference in value from this mean
  mutate(
    data = mapply(cbind, data, value_mean = value_mean, SIMPLIFY = FALSE) %>%
      lapply(as_tibble) %>%
      lapply(mutate, value_diff = value - value_mean)
  ) %>%
  ungroup() %>%
  print() -> nestdata
# count fits by predictor variable values
nestdata %>% pull(data) %>%
  lapply(select, measure, cutoff) %>%
  lapply(table, useNA = "always")
nestdata %>% pull(data) %>%
  lapply(dplyr::filter, measure == "unit") %>%
  lapply(select, pval, source) %>%
  lapply(table, useNA = "always")

# difference vs data source & log-p-value w/o intercept
# (applied only to unweighted networks)
classical_model1 <- function(d) {
  lm(formula = value_diff ~ source + log(pval) + 0,
     data = filter(d, measure == "unit"))
}
# difference vs data source & log-p-value & measure-threshold int'n w/o int.
# (each threshold effect is unique to networks using that weighting scheme)
classical_model2 <- function(d) {
  lm(formula = value_diff ~
       source + log(pval) +
       measure:cutoff +
       0,
     data = baseline_measures(d))
}
classical_model2_oddsratio <- function(d) {
  lm(formula = value ~ source + log(pval) + cutoff,
     data = filter(d, measure == "oddsratiommle"))
}
# difference vs log-p-value w/ random effect of data source
multilevel_model1 <- function(d) lme4::lmer(
  formula = value ~
    log(pval) +
    (1 | source) +
    1,
  data = filter(d, measure == "unit" & pval <= .01 & pval >= .0001)
)
# difference vs log-p-value & measure-threshold int'n
# w/ random effect of data source
multilevel_model2 <- function(d) lme4::lmer(
  formula = value ~
    log(pval) +
    measure:cutoff +
    (1 | source) +
    0,
  data = filter(d, measure != "unit")
)

# apply all models to all statistics and tidy model outputs
lmdata <- nestdata %>%
  mutate(
    model1 = map(data, classical_model1),
    model2 = map(data, classical_model2),
    model3 = map(data, multilevel_model1),
    model4 = map(data, multilevel_model2)
  ) %>%
  mutate_at(c(vars(starts_with("model"))), list(tidy = ~ lapply(., tidy)))

# TeX code for measures used inside stargazer tables
meas_tex_internal <- data$meas_tex %>%
  levels() %>%
  setdiff("$1$") %>%
  {.[c(3,1,2,4)]} %>%
  str_remove_all("(^\\$)|(\\$$)")

# each `cov_lab_*` provides TeX code for the predictors in its stargazer table;
# generate each stargazer table without `covariate.labels` to check

# difference vs data source & log-p-value w/o intercept
cov_lab_1 <- c(
  #as.character(levels(data$source_abbr))[-1],
  as.character(unique(lmdata$data[[1]]$source_abbr)),
  "$\\log(p)$"
)
lmdata %>% pull(model1) %>% stargazer::stargazer(
  out = "tab/model1.tex", label = "tab:lm1",
  title = paste0(
    "LMs of network statistics on ",
    "data source and test-wise error rate."
  ),
  align = TRUE, digits = 2, star.cutoffs = 10^(-(1:3)),
  dep.var.labels.include = FALSE,
  column.labels = lmdata$stat_abbr,
  column.sep.width = "-5pt",
  covariate.labels = cov_lab_1, # comment out to validate
  omit.stat = c("rsq", "ser", "f"),
  no.space = FALSE, font.size = "scriptsize",
  notes = paste0(
    "\\textbf{Remove this note after adding horizontal lines between ",
    "groups of predictors (datasets and evidential cutoffs).}"
  )
)
# difference vs data source & log-p-value & measure-threshold int'n w/o int.
cov_lab_2 <- c(
  #as.character(levels(data$source_abbr))[-1],
  as.character(unique(lmdata$data[[1]]$source_abbr)),
  "$\\log(p)$",
  str_c("$", meas_tex_internal, "\\times\\theta_{", meas_tex_internal, "}$")
)
lmdata %>% pull(model2) %>% stargazer::stargazer(
  out = "tab/model2.tex", label = "tab:lm2",
  title = paste0(
    "LMs of network statistics on ",
    "data source, test-wise error rate, ",
    "and binary association measure."
  ),
  align = TRUE, digits = 2, star.cutoffs = 10^(-(1:3)),
  dep.var.labels.include = FALSE,
  column.labels = lmdata$stat_abbr,
  column.sep.width = "-5pt",
  covariate.labels = cov_lab_2,
  omit.stat = c("rsq", "ser", "f"),
  no.space = FALSE, font.size = "scriptsize",
  notes = paste0(
    "\\textbf{Remove this note after adding horizontal lines between ",
    "groups of predictors (datasets, evidential cutoff, evaluative cutoffs).}"
  )
)
# difference vs log-p-value w/ random effect of data source
cov_lab_3 <- c(
  "$\\log(p)$",
  "Const."
)
lmdata %>% pull(model3) %>% stargazer::stargazer(
  out = "tab/model3-stargazer.tex", label = "tab:hlm1-stargazer",
  title = paste0(
    "Hierarchical models of network statistics on test-wise error rate, ",
    "grouped by data source"
  ),
  align = TRUE, digits = 2, star.cutoffs = 10^(-(1:3)),
  dep.var.labels.include = FALSE,
  column.labels = lmdata$stat_abbr,
  column.sep.width = "-5pt",
  covariate.labels = cov_lab_3,
  omit.stat = c("rsq", "ser", "f"),
  no.space = FALSE, font.size = "scriptsize"
)
model3_terms <- c(
  "$\\sigma_{1}(\\text{source})$",
  #"$\\sigma_{1}(\\text{measure})$",
  "$\\sigma(\\text{resid.})$"
)
lmdata %>%
  select(stat_abbr, model3_tidy) %>%
  mutate(stat_abbr = factor(stat_abbr, stat_info$stat_abbr)) %>%
  unnest(cols = c(model3_tidy)) %>%
  filter(group != "fixed") %>%
  mutate(group = fct_inorder(group)) %>%
  select(stat_abbr, estimate, group) %>%
  spread(stat_abbr, estimate) %T>% print() %>%
  mutate(group = model3_terms) %>%
  column_to_rownames("group") %>%
  xtable::xtable(
    label = "tab:hlm1-xtable",
    caption = paste0(
      "Variance decomposition for ",
      "hierarchical models of network statistics on test-wise error rate, ",
      "grouped by data source"
    ),
    align = c("l", rep("r", nrow(stat_info[-2, ]))), digits = 3
  ) %T>%
  # verify order of SD estimates
  print() %>%
  xtable::print.xtable(
    file = "tab/model3-xtable.tex",
    size = "scriptsize", caption.placement = "top",
    sanitize.rownames.function = identity,
    sanitize.colnames.function = identity
  )
# difference vs log-p-value & measure-threshold int'n
# w/ random effect of data source
cov_lab_4 <- c(
  "$\\log(p)$",
  str_c("$", meas_tex_internal, "\\times\\theta_{", meas_tex_internal, "}$")
)
lmdata %>% pull(model4) %>% stargazer::stargazer(
  out = "tab/model4-stargazer.tex", label = "tab:hlm2-stargazer",
  title = paste0(
    "Hierarchical models of network statistics on ",
    "test-wise error rate and binary association measure, ",
    "grouped by data source"
  ),
  align = TRUE, digits = 2, star.cutoffs = 10^(-(1:3)),
  dep.var.labels.include = FALSE,
  column.labels = lmdata$stat_abbr,
  column.sep.width = "-5pt",
  covariate.labels = cov_lab_4,
  omit.stat = c("rsq", "ser", "f"),
  no.space = FALSE, font.size = "scriptsize"
)
model4_terms <- c(
  "$\\sigma_{1}(\\text{source})$",
  "$\\sigma(\\text{resid.})$"
)
lmdata %>%
  select(stat_abbr, model4_tidy) %>%
  mutate(stat_abbr = factor(stat_abbr, stat_info$stat_abbr)) %>%
  unnest(cols = c(model4_tidy)) %>%
  filter(group != "fixed") %>%
  mutate(group = fct_inorder(group)) %>%
  select(stat_abbr, estimate, group) %>%
  spread(stat_abbr, estimate) %T>% print() %>%
  mutate(group = model4_terms) %>%
  column_to_rownames("group") %>%
  xtable::xtable(
    label = "tab:hlm2-xtable",
    caption = paste0(
      "Variance decomposition for ",
      "hierarchical models of network statistics on ",
      "test-wise error rate and binary association measure, ",
      "grouped by data source"
    ),
    align = c("l", rep("r", nrow(stat_info[-2, ]))), digits = 3
  ) %T>%
  # verify order of SD estimates
  print() %>%
  xtable::print.xtable(
    file = "tab/model4-xtable.tex",
    size = "scriptsize", caption.placement = "top",
    sanitize.rownames.function = identity,
    sanitize.colnames.function = identity
  )

# goodness-of-fit summary
lmdata %>%
  select(stat_abbr, model1:model4) %>%
  mutate(stat_abbr = factor(stat_abbr, stat_info$stat_abbr)) %>%
  arrange(stat_abbr) %>%
  mutate_at(vars(model1:model4), list(aic = ~ sapply(., AIC))) %>%
  select(stat_abbr, model1_aic:model4_aic) %>%
  as.data.frame() %>%
  {
    rownames(.) <- .$stat_abbr
    select(., -stat_abbr)
  } %>%
  setNames(paste0("Model ", 1:4)) %T>%
  # review
  print() %>%
  xtable::xtable(
    label = "tab:aic",
    caption = paste0(
      "Akaike information criteria for each model fitted to ",
      "the values taken by each network statistic"
    ),
    align = c("l", rep("r", 4)), digits = 0
  ) %>%
  xtable::print.xtable(
    file = "tab/aic.tex",
    size = "scriptsize", caption.placement = "top",
    math.style.negative = TRUE,
    sanitize.rownames.function = identity,
    sanitize.colnames.function = identity
  )

# principal components analysis
data %>%
  # simplify models by using only uncorrected p-value cutoffs
  filter(correction == "none") %>% select(-correction) %>%
  gather(
    key = "statistic", value = "value",
    connected_prop,
    degree_mean, degree_gini,
    degree_param1, degree_param2,
    degree_assortativity, triad_closure, distance_mean,
    modularity_wt
  ) %>%
  # same centering as in linear regression models
  left_join(select(nestdata, statistic, value_mean), by = "statistic") %>%
  mutate(response = value - value_mean) %>%
  select(-value, -value_mean) %>%
  #mutate_if(is.numeric, funs(ifelse(is.infinite(.), NA, .))) %>%
  spread(statistic, response) %>%
  drop_na() %>%
  print() -> pca_data
pca <- pca_data %>%
  select(connected_prop, density,
         degree_mean, degree_gini, degree_param1, degree_param2,
         degree_assortativity, triad_closure, distance_mean,
         modularity_wt) %>%
  prcomp(center = FALSE, scale. = TRUE) %>%
  as_tbl_ord() %>%
  augment() %>%
  bind_cols_u(select(pca_data, source_abbr, meas_abbr, disease_count, density))
# distribution of variance/inertia
pca_pvar <- recover_inertia(pca) / sum(recover_inertia(pca))
print(cumsum(pca_pvar))
# biplot
pca_biplot <- ggbiplot(pca) +
  scale_color_brewer(type = "qual", palette = "Dark2") +
  geom_u_point(aes(color = source_abbr,
                 shape = meas_abbr,
                 #size = disease_count,
                 alpha = density)) +
  geom_v_vector(color = "#333333", size = .25) +
  geom_v_text_radiate(aes(label = .name), color = "#333333", size = 3) +
  stat_u_ellipse(
    aes(group = source_abbr, color = source_abbr, size = disease_count),
    alpha = 1/3
  ) +
  scale_shape(solid = FALSE) +
  theme_bw() +
  theme(legend.direction = "horizontal",
        legend.box = "vertical",
        legend.position = "bottom", legend.key.width = unit(.05, "npc"),
        legend.margin = margin(t = -3, r = 0, b = -3, l = 0, unit = "pt")) +
  guides(color = guide_legend(nrow = 1))
pdf(height = 9, width = 10, file = "fig/pca.pdf")
print(pca_biplot)
dev.off()

# ellipse label coordinates
fortify(pca, .matrix = "u") %>%
  select(source_abbr, x = PC1, y = PC2) %>%
  group_by(source_abbr) %>%
  summarize(
    x_mean = mean(x), x_sd = sd(x), y_mean = mean(y), y_sd = sd(y),
    xy_cor = cor(x, y)
  ) %>%
  mutate(pref = ifelse(abs(x_mean) > abs(y_mean), "x", "y")) %>%
  mutate(
    x_sign = ifelse(pref == "x", sign(x_mean), sign(y_mean) * sign(xy_cor)),
    y_sign = ifelse(pref == "y", sign(y_mean), sign(x_mean) * sign(xy_cor))
  ) %>%
  mutate(x = x_mean + 2.5 * x_sign * x_sd, y = y_mean + 2.5 * y_sign * y_sd) %>%
  print() -> label_coords
# biplot
pca_biplot <- ggbiplot(pca) +
  scale_color_brewer(type = "qual", palette = "Dark2") +
  geom_u_point(aes(color = source_abbr,
                   shape = meas_abbr,
                   #size = disease_count,
                   alpha = density)) +
  geom_v_vector(color = "#333333", size = .25) +
  geom_v_text_radiate(aes(label = .name), color = "#333333", size = 3) +
  stat_u_ellipse(
    aes(group = source_abbr, color = source_abbr, size = disease_count),
    alpha = 1/3
  ) +
  geom_u_text(
    aes(x = x, y = y, label = source_abbr, color = source_abbr, .matrix = "u"),
    data = label_coords, size = 3
  ) +
  scale_shape(solid = FALSE) +
  theme_bw() +
  theme(legend.direction = "horizontal",
        legend.box = "vertical",
        legend.position = "bottom", legend.key.width = unit(.05, "npc"),
        legend.margin = margin(t = -3, r = 0, b = -3, l = 0, unit = "pt")) +
  guides(color = "none", alpha = "none", shape = "none", size = "none")
pdf(height = 4.5, width = 6, file = "fig/pca-label.pdf")
print(pca_biplot)
dev.off()
# transposed biplot
pca_biplot <- ggbiplot(pca, aes(x = 2, y = 1)) +
  scale_color_brewer(type = "qual", palette = "Dark2") +
  geom_u_point(aes(color = source_abbr,
                   shape = meas_abbr,
                   #size = disease_count,
                   alpha = density)) +
  geom_v_vector(color = "#333333", size = .25) +
  geom_v_text_radiate(aes(label = .name), color = "#333333", size = 3) +
  stat_u_ellipse(
    aes(group = source_abbr, color = source_abbr, size = disease_count),
    alpha = 1/3
  ) +
  geom_u_text(
    aes(x = y, y = x, label = source_abbr, color = source_abbr, .matrix = "u"),
    data = label_coords, size = 3
  ) +
  scale_shape(solid = FALSE) +
  theme_bw() +
  theme(legend.direction = "horizontal",
        legend.box = "vertical",
        legend.position = "bottom", legend.key.width = unit(.05, "npc"),
        legend.margin = margin(t = -3, r = 0, b = -3, l = 0, unit = "pt")) +
  guides(color = "none", alpha = "none", shape = "none", size = "none")
pdf(height = 7, width = 5, file = "fig/pca-label-flip.pdf")
print(pca_biplot)
dev.off()
