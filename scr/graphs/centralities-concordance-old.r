# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(igraph)
source("R/graph-construction.r")
source("R/utils.r")
source("R/params.r")
load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

# restrict to representative datasets
source_info <- source_info %>%
  filter(!grepl("namcs|vaers", file))

# Compare the centrality rankings for a single dataset, using different 
# statistical cutoffs and binary association measures (including none, i.e. unit
# edge weights)

# initialize list of concordance datasets
concordance_data <- vector("list", nrow(source_info))
names(concordance_data) <- source_info$source
# 0 < x < 1 ~> 1 < y < Inf
reciprocal <- function(x) 1 / x
# 1 < x < Inf ~> 1 < y < Inf
shifted_reciprocal <- function(x) 1 / (x - 1) + 1
centralities <- list(
  degree = igraph::strength,
  betweenness = igraph::betweenness,
  closeness = igraph::closeness
)
conc_grid <- as_tibble(expand.grid(
  pval = 0.01,
  corr = param_vals$correction,
  measure = c("unit", param_vals$measure),
  centrality = names(centralities)
))
#conc_cat <- apply(conc_grid, 1, paste, collapse = "_")

# load partial calculation data if saved
if (file.exists(paste0(tdir, "concordance-data.RData"))) {
  load(paste0(tdir, "concordance-data.RData"))
  if (is.data.frame(concordance_data)) stop("Analysis is done.")
}

# calculate concordances for all (remaining) settings
for (i in seq_along(concordance_data)) {
  if (!is.null(concordance_data[[i]])) next
  # load data
  d <- readRDS(paste0(sdir, source_info$file[i]))
  # (number of) distinct diseases
  v <- as.character(unique(unlist(d[, 1:2])))
  # initialize data frame of concordance measures
  conc_dat <- vector("list", nrow(conc_grid))
  #names(conc_dat) <- conc_cat
  # iterate over the rows of the data frame
  for (j in 1:nrow(conc_grid)) {
    print(cbind(source = source_info$source[i], conc_grid[j, ]))
    # centrality type
    cent <- as.character(conc_grid[j, 3])
    # construct graph
    g <- graph_from_pairs_data(
      data = d,
      pval_cutoff = conc_grid$pval[j],
      pval_correction = as.character(conc_grid$corr[j]),
      # `NULL` if 'unit', else values from `d`
      measure = d[[as.character(conc_grid$measure[j])]],
      weighted = (as.character(conc_grid$measure[j]) != "unit"),
      # want link weights to have minimum value 1
      transformation = switch(
        as.character(conc_grid$measure[j]),
        unit = identity,
        oddsratiommle = shifted_reciprocal,
        phi = reciprocal,
        forbes = shifted_reciprocal,
        rho_psych = reciprocal,
        jaccard = reciprocal
      )
    )
    # calculate centralities
    conc_vec <- (centralities[[as.character(conc_grid$centrality[j])]])(g)
    # restore any missing nodes
    conc_dat[[j]] <- as_tibble(cbind(
      source_info[i, -1],
      conc_grid[j, ],
      tibble(
        node = v,
        value = conc_vec[v]
      )
    ))
  }
  #concordance_data[[i]] <- as.data.frame(conc_dat)
  concordance_data[[i]] <- bind_rows(conc_dat)
  save(concordance_data, file = paste0(tdir, "concordance-data.RData"))
}

concordance_data <- concordance_data %>% bind_rows() %>% as_tibble()
save(concordance_data, file = paste0(tdir, "concordance-data.RData"))
