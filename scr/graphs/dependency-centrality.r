# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(igraph)
source("R/utils.r")
source("R/graph-construction.r")
source("R/graph-dependency.r")
# a named list of named lists of data frames
load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM0/comorbidity/data/dependency/"
tdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

thetas <- c(".05", "B-.05")

# save tables of group betweenness centralities
group_betweenness <- tibble()
for (i in 1:nrow(source_info)) for (j in 1:length(thetas)) {
  # identify the source ontology
  sub_map_info <- map_info %>%
    filter(ontology == source_info$ontology[i])
  if (nrow(sub_map_info) == 0) next
  # load the file (if it exists)
  dc_file <- paste0(
    sdir,
    "dependencycube-",
    source_info$source[i], "-",
    gsub("\\.|-", "", thetas[j]),
    ".rda"
  )
  if (!file.exists(dc_file)) next
  load(file = dc_file)
  # for each coarsening map
  for (k in 1:nrow(sub_map_info)) {
    # calculate betweenness centrality
    map <- ontology_maps[[sub_map_info$ontology[k]]][[sub_map_info$map[k]]]
    map_betweenness <- map %>%
      full_join(dc, by = c("source" = "broker")) %>%
      mutate(broker_prop = broker_count / geodesic_count) %>%
      group_by(target) %>%
      summarize(
        source_count = n_distinct(source),
        betweenness_quot_sum = sum(broker_prop, na.rm = TRUE) / 2
      ) %>%
      arrange(desc(betweenness_quot_sum)) %>%
      mutate(map = sub_map_info$map[k])
    # bind to the overall dataset
    group_betweenness <- bind_rows(group_betweenness, map_betweenness)
  }
}
save(group_betweenness, file = "data/sensitivity/group-betweenness.rda")

# save tables of group closeness centralities
group_closeness <- tibble()
for (i in 1:nrow(source_info)) for (j in 1:length(thetas)) {
  # identify the source ontology
  sub_map_info <- map_info %>%
    filter(ontology == source_info$ontology[i])
  if (nrow(sub_map_info) == 0) next
  # load the file (if it exists)
  dc_file <- paste0(
    tdir,
    "dependencycube-",
    source_info$source[i], "-",
    gsub("\\.|-", "", thetas[j]),
    ".rda"
  )
  if (!file.exists(dc_file)) next
  load(file = dc_file)
  message("Calculating group centralities from", dc_file)
  # for each coarsening map
  for (k in 1:nrow(sub_map_info)) {
    # calculate closeness centrality
    map <- ontology_maps[[sub_map_info$ontology[k]]][[sub_map_info$map[k]]]
    map_closeness <- map %>%
      full_join(dc, by = c("source" = "sender")) %>%
      mutate(broker_prop = broker_count / geodesic_count) %>%
      group_by(target) %>%
      summarize(
        source_count = n_distinct(source),
        closeness_quot_sum = 1 / sum(broker_prop, na.rm = TRUE)
      ) %>%
      arrange(desc(closeness_quot_sum)) %>%
      mutate(map = sub_map_info$map[k])
    # bind to the overall dataset
    group_closeness <- bind_rows(group_closeness, map_closeness)
  }
}
save(group_closeness, file = "data/sensitivity/group-closeness.rda")

stop()

# illustrative example

data(karate, package = "igraphdata")
dc <- dependency_cube_matrix(karate)
map_info <- tibble(
  ontology = vertex_attr(karate, "name"),
  map = sample(
    x = LETTERS[1:4], size = vcount(karate), replace = TRUE, prob = 4:1
  )
)
plot(karate, vertex.color = as.numeric(as.factor(map_info$map)))

map_betweenness <- map_info %>%
  full_join(dc, by = c("ontology" = "broker")) %>%
  mutate(broker_prop = broker_count / geodesic_count) %>%
  group_by(map) %>%
  summarize(
    source_count = n_distinct(ontology),
    betweenness_quot_sum = sum(broker_prop, na.rm = TRUE) / 2#,
    #betweenness_sum_quot = sum(broker_count, na.rm = TRUE) /
    #  sum(geodesic_count, na.rm = TRUE)
  )

map_closeness <- map_info %>%
  full_join(dc, by = c("ontology" = "sender")) %>%
  mutate(broker_prop = broker_count / geodesic_count) %>%
  group_by(map) %>%
  summarize(
    source_count = n_distinct(ontology),
    closeness_quot_sum = 1 / sum(broker_prop, na.rm = TRUE)
  )
