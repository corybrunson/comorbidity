# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(tidyr)
library(dplyr)
library(xtable)
source("R/pairs-calculations.r")
source("R/utils.r")
source("R/params.r")
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"

source_info <- source_info %>%
  dplyr::filter(!grepl("namcs|vaers", file))

# evidential thresholds and corrections
crossing(
  c(1, param_vals$pval[seq(2, length(param_vals$pval), 2)]),
  param_vals$correction
) %>%
  set_names(c("pval", "corr")) %>%
  mutate(corr = ifelse(corr == "none", corr, toupper(corr))) %>%
  mutate(corr = fct_rev(corr)) %>%
  arrange(desc(pval), corr) %>%
  print() -> evidential_thresholds
# density info across sources and thresholds
source_info %>%
  mutate(filepath = paste0(sdir, file)) %>%
  mutate(data = map(filepath, readRDS)) %>%
  mutate(data = map(data, distinct)) %>%
  mutate(n = map_int(data, ~ n_distinct(unlist(.x[, 1:2])))) %>%
  mutate(m = sapply(n, choose, k = 2)) %>%
  mutate(pvals = map(data, pull, pval_fisher)) %>%
  select(source, abbr, m, pvals) %>%
  print() -> source_pvals
source_pvals %>%
  crossing(evidential_thresholds) %>%
  mutate(links = case_when(
    corr == "none" ~ map2_int(pvals, pval, ~ sum(.x < .y)),
    corr == "FWER" ~ pmap_int(list(pvals, pval, m), ~ sum(..1 < (..2 / ..3))),
    corr == "FDR" ~ pmap_int(
      list(pvals, pval, m),
      ~ sum(..1 < (..2 * 1:length(..1) / ..3)[rank(..1, ties.method = "max")])
    ),
    TRUE ~ NA_integer_
  )) %>%
  mutate(density = links / m) %>%
  select(source, abbr, pval, corr, links, density) %>%
  print() -> densities
# threshold-by-source table of densities
densities %>%
  select(source = abbr, pval, corr, density) %>%
  spread(source, density) %>%
  arrange(desc(pval), corr) %>%
  mutate(pval = format(pval, scientific = FALSE, drop0trailing = TRUE)) %>%
  rename(`p-value` = pval, Correction = corr) %>%
  xtable(
    digits = c(NA, 6, NA, rep(3, 8)),
    caption = paste0(
      "Densities (proportion of pairs of nodes that are linked) ",
      "of comorbidity networks constructed ",
      "using different evidential thresholds. ",
      "Family-wise error rate (FWER) correction uses ",
      "the Bonferroni procedure; ",
      "false discovery rate (FDR) correction uses ",
      "the Benjamini--Hochberg procedure."
    ),
    label = "tab:densities",
    align = c("r", "l", "l", rep("r", 8))
  ) %>%
  print.xtable(
    file = "tab/densities.tex",
    size = "\\small",
    sanitize.colnames.function = identity,
    include.rownames = FALSE,
    rotate.colnames = TRUE,
    caption.placement = "top"
  )

stop()

thresholds <- c("1", ".05", ".001", "B-.05")

source_info <- source_info %>%
  dplyr::filter(!grepl("namcs|vaers", file))

# table of graph densities across evidential thresholds
densities <- sapply(source_info$file, function(f) {
  d <- readRDS(paste0(sdir, f))
  n <- n_distinct(unlist(d[, 1:2]))
  c(
    "1" = d %>% nrow(),
    ".05" = d %>% filter(pval_fisher < .05) %>% nrow(),
    ".001" = d %>% filter(pval_fisher < .001) %>% nrow(),
    "Bonferroni.05" = d %>% filter(pval_fisher < .05 / n) %>% nrow()
  ) / choose(n, 2)
})
dimnames(densities) <- list(thresholds, source_info$abbr)
densities %>%
  xtable(digits = 3,
         caption = paste0(
           "Densities (proportion of pairs of nodes that are linked) ",
           "of comorbidity networks constructed ",
           "using different evidential thresholds. ",
           "``B'' indicates Bonferroni correction, ",
           "which depends on the number of disorders in each dataset."
           ),
         label = "tab:densities") %>%
  print.xtable(file = "tab/densities.tex",
               size = "\\small",
               sanitize.colnames.function = identity,
               rotate.colnames = TRUE,
               caption.placement = "top")

save(densities, file = "data/summary/densities.rda")

# table of graph densities across evidential and evaluative thresholds

# table of measure ranges in each dataset, subject to evidential thresholds
weights <- lapply(source_info$file, function(f) {
  d <- readRDS(paste0(sdir, f))
  n <- n_distinct(unlist(d[, 1:2]))
  alphas <- c(Inf, .05, .001, .05 / n)
  lapply(seq_along(alphas), function(i) {
    d %>%
      filter(pval_fisher < alphas[i]) %>%
      select(oddsratiommle, phi, forbes, rho_psych) %>%
      gather(
        key = "measure", value = "value",
        oddsratiommle, phi, forbes, rho_psych
      ) %>%
      filter(!(measure == "oddsratiommle" & value <= 1)) %>%
      filter(!(measure == "phi" & value <= 0)) %>%
      filter(!(measure == "forbes" & value <= 1)) %>%
      filter(!(measure == "rho_psych" & value <= 0)) %>%
      group_by(measure) %>%
      summarize(
        "0\\%" = min(value),
        "20\\%" = quantile(value, prob = .2),
        "40\\%" = quantile(value, prob = .4),
        "60\\%" = quantile(value, prob = .6),
        "80\\%" = quantile(value, prob = .8)#,
        #"100\\%" = max(value)
      ) %>%
      ungroup() %>%
      gather(key = "quantile", value = "value", grep("\\%", names(.))) %>%
      mutate(threshold = thresholds[i])
  }) %>%
    bind_rows() %>%
    mutate(file = f) %>%
    left_join(source_info, by = "file") %>%
    mutate(source = factor(abbr, levels = source_info$abbr)) %>%
    select(source, threshold, measure, quantile, value) %>%
    left_join(select(measure_info, measure, abbr), by = "measure") %>%
    mutate(measure = factor(abbr, levels = measure_info$abbr)) %>%
    select(source, threshold, measure, quantile, value) %>%
    mutate(threshold = factor(threshold, levels = thresholds)) %>%
    mutate(quantile = factor(quantile,
                             levels = paste0(c("0", "20", "40", "60", "80"),
                                             "\\%")))
}) %>%
  bind_rows() %>%
  spread(key = source, value = value) %>%
  arrange(threshold, measure, quantile)

save(weights, file = "data/summary/weights.rda")
load("data/summary/weights.rda")

# TeX tables, in groups
measure_groups <- list(
  c("OR-MMLE", "F"), c("phi", "rho")
)
for (i in seq_along(measure_groups)) {
  weights %>%
    filter(measure %in% measure_groups[[i]]) %>%
    left_join(
      measure_info %>% select(abbr, abbr_tex),
      by = c("measure" = "abbr")
    ) %>%
    mutate(measure = factor(measure, measure_groups[[i]])) %>%
    arrange(measure, threshold, quantile) %>%
    rename(BAM = abbr_tex, TWER = threshold, quant. = quantile) %>%
    select(
      TWER, BAM, quant.,
      Columbia, `MedPAR(3)`, `MedPAR(5)`, SctHans, Michigan,
      Stanford, `Columbia*`, MIMIC
    ) %>%
    xtable(
      digits = 3,
      caption = paste0(
        "Quintiles of binary association measures ",
        "for pairs in each dataset ",
        "for different evidential thresholds. ",
        "``B'' indicates Bonferroni correction."
      ),
      label = paste0("tab:weights-", i),
      align = rep("r", ncol(.) + 1)
    ) %>%
    print.xtable(
      file = paste0("tab/weights-", i, ".tex"),
      size = "\\footnotesize",
      include.rownames = FALSE,
      hline.after = c(-1, seq(0, nrow(.), 5)),
      sanitize.colnames.function = identity,
      sanitize.text.function = identity,
      rotate.colnames = TRUE,
      caption.placement = "top"
    )
}
