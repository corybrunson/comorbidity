# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(tidyverse)
library(icd)
library(igraph)
source("R/graph-construction.r")
source("R/utils.r")
# a named list of named lists of data frames
load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"

source_info <- source_info %>%
  filter(!grepl("namcs|vaers", source))

prevalences <- tibble()
for (i in seq_along(source_info$file)) {
  print(as.character(source_info$source[i]))
  # load data
  f <- source_info$file[i]
  d <- readRDS(paste0(sdir, f))
  # sample size (number of patients)
  n <- unique(d$n)
  # distinct diseases and prevalences
  dd <-
    bind_rows(
      d %>% select(1, n1) %>% setNames(c("disorder", "prevalence")),
      d %>% select(2, n2) %>% setNames(c("disorder", "prevalence"))
    ) %>%
    distinct() %>%
    mutate(dataset = source_info$source[i], ontology = source_info$ontology[i])
  prevalences <- bind_rows(prevalences, dd)
}

# augment ICD codes with English terms
concept_explanations <- lapply(
  prevalences %>% pull(dataset) %>% unique(), function(dat) lapply(
    prevalences %>%
      filter(dataset == dat) %>% pull(ontology) %>% unique(), function(ont) {
        if (ont == "Rzhetsky") return(NULL)
        prevalences %>%
          filter(dataset == dat & ontology == ont) %>%
          pull(disorder) %>% as.character() %>%
          cbind(
            disorder = .,
            icd_explain_table(.)
          ) %>%
          mutate(valid_icd = valid_icd9 | valid_icd10) %>%
          select(disorder, code, valid_icd, short_desc) %>%
          mutate(dataset = dat, ontology = ont)
      }
  )
) %>% unlist(recursive = FALSE) %>% bind_rows()


prevalences <- prevalences %>%
  left_join(
    concept_explanations %>%
      select(-valid_icd)
  ) %>%
  left_join(
    concept_explanations %>%
      group_by(dataset) %>%
      summarize(valid_icd = all(valid_icd, na.rm = TRUE)) %>%
      ungroup()
  ) %>%
  mutate(code = ifelse(valid_icd & ontology != "Rzhetsky", disorder, "")) %>%
  mutate(description = ifelse(valid_icd & ontology != "Rzhetsky",
                              short_desc, disorder)) %>%
  select(-short_desc, -valid_icd)

# write table of most-prevalent disorders in each dataset
prevalences %>%
  group_by(dataset) %>%
  top_n(8, wt = prevalence) %>%
  ungroup() %>%
  mutate(description = abbreviate(description, minlength = 72)) %>%
  left_join(
    source_info %>%
      select(dataset = source, dataset_abbr = abbr)
  ) %>%
  mutate(dataset = factor(dataset, levels = source_info$source)) %>%
  arrange(dataset, desc(prevalence)) %>%
  select(
    Dataset = dataset_abbr,
    Code = code,
    Description = description,
    Prevalence = prevalence
  ) %>%
  xtable::xtable(
    caption = paste0("Most prevalent disorders in each comorbidity network."),
    label = "tab:prevalence",
    digits = 3, display = c("s", "s", "s", "s", "fg")
  ) %>%
  xtable::print.xtable(
    file = "tab/prevalence.tex",
    size = "\\scriptsize",
    hline.after = c(-1, which(!duplicated(.$Dataset)) - 1, nrow(.)),
    table.placement = "H",
    include.rownames = FALSE,
    caption.placement = "top"
  )

# plot some illustrative comparative prevalence rates
for (ont in unique(prevalences$ontology)) {
  ont_prev_plot <- prevalences %>%
    filter(!grepl("namcs|vaers", dataset)) %>%
    filter(ontology == ont) %>%
    select(-ontology) %>%
    {
      full_join(., ., by = "disorder", suffix = c("_1", "_2"))
    } %>%
    #dplyr::filter(as.integer(dataset_1) < as.integer(dataset_2)) %>%
    ggplot(aes(x = prevalence_1, y = prevalence_2, label = disorder)) +
    theme_bw() + coord_equal() +
    facet_grid(dataset_2 ~ dataset_1) +
    scale_x_continuous(trans = "log") +
    scale_y_continuous(trans = "log") +
    labs(x = "Prevalence in column dataset", y = "Prevalence in row dataset") +
    geom_point(shape = 16, alpha = 1/3) +
    ggtitle(paste("Prevalences under the", ont, "ontology"))
  n_datasets <- n_distinct(ont_prev_plot$data$dataset_1)
  if (n_datasets < 2) next
  ggsave(
    ont_prev_plot,
    height = 1 + 2 * n_datasets, width = 1 + 2 * n_datasets,
    filename = paste0("fig/prevalences-", ont, ".pdf")
  )
}

# nest by dataset
prevalences <- nest(prevalences, disorder, prevalence, .key = "prevalences")

# any-to-one mapped prevalences
for (i in 1:nrow(map_info)) {
  mapped_prevalences <- prevalences %>%
    filter(ontology == map_info$ontology[i]) %>%
    mutate(ontology = map_info$map[i]) %>%
    mutate(
      prevalences = prevalences %>%
        # WILL OVERESTIMATE PREVALENCE, SINCE PATIENTS RECEIVE MULTIPLE
        # DIAGNOSES FROM THE SAME PREIMAGE OF A MAPPPED ONTOLOGY
    )
}

# any-to-one ICD9-3 prevalences
prev_icd9_3 <- prevalences %>%
  filter(ontology == "ICD9-5")
# any-to-one Rzhetsky prevalences

