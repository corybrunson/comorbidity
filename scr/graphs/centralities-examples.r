# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(tidyr)
library(igraph)
library(ggraph)
library(tidygraph)
source("R/graph-construction.r")
source("R/utils.r")
load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

# Columbia dataset
i <- 1
d <- readRDS(paste0(sdir, source_info$file[i]))
v <- as.character(unique(unlist(d[, 1:2])))
shifted_reciprocal <- function(x) 1 / (x - 1) + 1
g <- graph_from_pairs_data(
  data = d,
  measure = "forbes",
  pval_cutoff = .05 / choose(length(v), 2),
  weighted = TRUE,
  transformation = identity
)
ggraph(g, layout = "fr") +
  theme_void() +
  geom_node_point() +
  geom_edge_link(aes(edge_width = weight, edge_alpha = pval_fisher)) +
  theme(legend.position = "none")

# total weighted degree example

b <- as_tbl_graph(g) %>%
  activate(nodes) %>%
  mutate(
    id = row_number(),
    degree = centrality_degree(),
    strength = centrality_degree(weights = weight)
  )
b
b %>%
  as_tibble() %>%
  ggplot(aes(x = degree, y = strength)) +
  geom_point() +
  ggrepel::geom_text_repel(
    aes(label = name),
    filter(as_tibble(b), strength <= 600 & strength >= 400)
  )
b %>%
  as_tibble() %>%
  filter(strength <= 600 & strength >= 400) %>%
  arrange(degree)
make_ego_graph(b, 1, c(103, 23)) %>%
  lapply(as_tbl_graph) %>%
  lapply(activate, links) %>%
  lapply(as_tibble) %>%
  lapply(arrange, desc(weight))

# betweenness centrality example

m <- distances(g, weights = NA)
w <- distances(g, weights = 1 / edge_attr(g, "weight"))
q <- tibble(
  from = rep(rownames(m), times = ncol(m)),
  to = rep(colnames(m), each = nrow(m)),
  hops = as.vector(m), geodesic = as.vector(w)
) %>%
  slice(which(upper.tri(m))) %>%
  unite("link", from, to, sep = "--", remove = FALSE)
q %>%
  ggplot(aes(x = hops, y = geodesic, group = hops)) +
  geom_boxplot(outlier.alpha = .2)
q %>%
  ggplot(aes(x = hops, y = geodesic, group = hops)) +
  geom_jitter(alpha = .2) +
  ggrepel::geom_text_repel(
    aes(label = link),
    filter(q, geodesic >= .295 & geodesic <= .305 & hops %in% c(1, 4))
  )
q %>%
  filter(hops %in% c(1, 4)) %>%
  filter(round(geodesic, 2) == .3) %>%
  arrange(geodesic) %>%
  select(link, hops, geodesic) %>%
  as.data.frame()
q %>%
  filter(hops %in% c(1, 4)) %>%
  filter(round(geodesic, 2) == .3) %>%
  arrange(geodesic) %>%
  left_join(
    select(d, disease_1, disease_2, pval_12 = pval_fisher),
    by = c("from" = "disease_1", "to" = "disease_2")
  ) %>%
  left_join(
    select(d, disease_1, disease_2, pval_21 = pval_fisher),
    by = c("from" = "disease_2", "to" = "disease_1")
  ) %>%
  mutate(pval = ifelse(is.na(pval_12), pval_21, pval_12)) %>%
  select(link, hops, geodesic, pval) %>%
  as.data.frame()
shortest_paths(b, from = "Multiple epiphyseal dysplasia", to = "Hepatitis E")
