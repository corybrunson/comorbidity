# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(tibble)
library(dplyr)
library(igraph)
library(ggplot2)
source("R/graph-construction.r")
source("R/utils.r")
source("R/params.r")

# source data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"

# restrict to designated sources
source_info <- inner_join(
  source_info,
  tibble(source = param_vals$source),
  by = "source"
)

# cutoffs
measure_cutoffs <- list(
  oddsratiommle = c(1, 2, 6, 60),
  phi = c(0, .005, .05, .2),
  forbes = c(1, 2, 6, 60),
  rho_psych = c(0, .1, .4, .6)
)

# calculate quantile of each cutoff for each p-value
quantile_dat <- tibble()
for (i in seq_along(source_info$file)) {
  f <- as.character(source_info$file[i])
  print(f)
  # load data
  d <- readRDS(paste0(sdir, f))
  # number of pairs with frequency table data
  n <- nrow(unique(d[, 1:2]))
  # (number of) distinct diseases
  v <- as.character(unique(unlist(d[, 1:2])))
  m <- length(v)
  # number of disease pairs
  p <- choose(length(v), 2)

  rows1 <- d$n12 > d$n1 / d$n * d$n2
  
  # vary p-value threshold and error rate correction
  for (pval in param_vals$pval) for (corr in param_vals$correction) {
    
    if (corr == "fwer") {
      # Bonferroni correction for family-wise error rate
      pval_cutoff <- pval / m
    } else if (corr == "fdr") {
      # Benjamini-Hochberg correction for false discovery rate
      # assume missing pairs have larger p-values than included pairs
      # err on the side of including links; use maximum cutoff to resolve ties
      pval_rank <- rank(d$pval_fisher, ties.method = "max")
      pval_cutoff <- (1:nrow(d) / m * pval)[pval_rank]
    } else {
      pval_cutoff <- pval
    }
    
    rows2 <- rows1 & (d$pval_fisher < pval_cutoff)
    
    # vary association measure and cutoff
    for (meas in param_vals$measure) for (th in measure_cutoffs[[meas]]) {
      
      n_links <- length(which(rows2 & (d[[meas]] > th)))
      
      # append data set
      quantile_dat <- bind_rows(quantile_dat, tibble(
        source = source_info$abbr[i],
        pval = pval, corr = corr, meas = meas, th = th,
        index = match(th, measure_cutoffs[[meas]]),
        diseases = m,
        pairs = p,
        # proportion of possible pairs included
        pair_quantile = 1 - n_links / p,
        # proportion of frequency table pairs included
        freq_quantile = 1 - n_links / n
      ))
    }
  }
}

# examine distributions of quantiles across sources and measures
print(quantile_dat)
quantile_dat %>%
  ggplot(aes(x = meas, y = freq_quantile)) +
  theme_bw() +
  scale_y_continuous(trans = "logit") +
  theme(axis.text.x = element_text(angle = -90, hjust = 0, vjust = .5)) +
  facet_grid(source ~ index, scales = "free") +
  geom_boxplot() +
  ggtitle(
    "Quantiles at which selected evaluative cutoffs prune comorbidity networks"
  ) ->
  quantile_boxplot
ggsave(
  quantile_boxplot,
  height = 18, width = 8,
  filename = here::here("fig/quantile-boxplot.pdf")
)
