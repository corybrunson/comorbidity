# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(tidyr)
library(igraph)
source("R/graph-construction.r")
source("R/utils.r")
#load("data/ontology/ontology-maps.rda")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/pairs/"
tdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

# community detection algorithms to use
# (must permit disconnected graphs)
alg_data <- tibble(
  algorithm = c("fast_greedy", "edge_betweenness", "walktrap"),
  alg_abbr = c("FG", "EB", "WT")
)

f <- source_info$file[1]
print(as.character(f))
# load data
d <- readRDS(paste0(sdir, f))
# sample size (number of patients)
n <- unique(d$n)
# (number of) distinct diseases
v <- as.character(unique(unlist(d[, 1:2])))
m <- length(v)

# reciprocal odds ratio matrix
r <- d %>%
  select(1:2, oddsratiommle) %>%
  setNames(c("concept_1", "concept_2", "oddsratiommle")) %>%
  {bind_rows(., select(
    .,
    concept_1 = concept_2, concept_2 = concept_1,
    oddsratiommle = oddsratiommle
  ))} %>%
  bind_rows(tibble(
    concept_1 = v,
    concept_2 = v,
    oddsratiommle = Inf
  )) %>%
  spread(key = concept_2, value = oddsratiommle) %>%
  {
    s <- as.matrix(select(., -1))
    rownames(s) <- pull(., 1)
    s
  } %>%
  {1/.} %>%
  as.dist()

# network at Bonferroni-corrected .05 TWER
g <- graph_from_pairs_data(data = d, pval_cutoff = .05 / choose(m, 2),
                           weighted = FALSE)
g <- permute(g, match(vertex_attr(g, "name"), labels(r)))
stopifnot(all(vertex_attr(g, "name") == labels(r)))

# hierarchical clustering
hc <- hclust(d = r, method = "complete")
hts <- hc$height

for (s in alg_data$algorithm) {
  # community partitions using network algorithms
  cluster_fun <- get(paste0("cluster_", s))
  x <- cluster_fun(g)
  assign(paste0("comm_", s), x)
  # community partitons using hierarchical clustering tree at same sizes
  ct <- cutree(hc, k = length(x))
  assign(
    paste0("ct_", length(x)),
    igraph:::groups.default(list(membership = ct))
  )
}

boldface_member <- function(x) {
  paste0("{\\bfseries ", x, "}")
}
boldface_shared <- function(x) {
  stopifnot(is.list(x) && length(x) == 2)
  y <- x
  for (i in 1:2) {
    y[[i]] <- ifelse(x[[i]] %in% x[[3 - i]], boldface_member(x[[i]]), x[[i]])
  }
  y
}

# compare partitions of size 17:

# part sizes
sapply(communities(comm_fast_greedy), length)
sapply(ct, length)
# differences between largest parts
setdiff(communities(comm_fast_greedy)$`1`, ct_17$`1`)
setdiff(communities(comm_fast_greedy)$`2`, ct_17$`1`)
setdiff(ct_17$`1`, communities(comm_fast_greedy)$`2`)
setdiff(ct_17$`1`, communities(comm_fast_greedy)$`1`)
# overlap matrix (community detection columns, clustering rows)
overlap <- sapply(communities(comm_fast_greedy), function(y) {
  sapply(ct_17, function(x) {
    length(intersect(x, y))
  })
})
overlap[c(1, 11, 6, 4, 9, 8, 2, 3, 5, 7, 10, 12:17), c(2, 3, 1, 4:17)]
# compare most similar parts
compare_clusters <- list(
  list(communities(comm_fast_greedy)[[2]], ct_17[[1]]),
  list(communities(comm_fast_greedy)[[3]], ct_17[[11]]),
  list(communities(comm_fast_greedy)[[1]], ct_17[[6]]),
  list(communities(comm_fast_greedy)[[1]], ct_17[[4]])
)
# print these to a table
for (i in seq_along(compare_clusters)) {
  for (j in 1:2) {
    cat(
      boldface_shared(compare_clusters[[i]])[[j]],
      "",
      sep = "\\\\\n"
    )
  }
}

# find best match to each community detection partition
# from among hierarchical clustering partitions

# optimal `k`, according to adjusted Rand index
k_opts <- setNames(vector("integer", 3), alg_data$algorithm)
overlaps <- setNames(vector("list", 3), alg_data$algorithm)
for (i in 1:nrow(alg_data)) {
  alg <- alg_data$algorithm[i]
  comm <- get(paste0("comm_", alg))
  k_opts[i] <- 1 + which.max(sapply(2:vcount(g), function(k) {
    mclust::adjustedRandIndex(cutree(hc, k = k), comm$membership)
  }))
  ct <- cutree(hc, k = k_opts[i])
  assign(paste0("clust_opt_", alg), ct)
  ct_mem <- igraph:::groups.default(list(membership = ct))
  overlaps[[i]] <- sapply(communities(comm), function(y) {
    sapply(ct_mem, function(x) {
      length(intersect(x, y))
    })
  })
}
print(sapply(overlaps, dim))

# write overlap matrices to TeX files
for (i in 1:nrow(alg_data)) {
  overlap <- overlaps[[i]][, colSums(overlaps[[i]]) > 1]
  stopifnot(all(rowSums(overlap) > 1))
  overlap %>%
    xtable::xtable(
      caption = paste0(
        "Overlap table for ", alg_data$alg_abbr[1],
        " community partition (columns) and ",
        "the best-matched partition cut from the ",
        "complete-linkage hierarchical clustering tree (rows). ",
        "Communities with only one member are excluded."
      ),
      label = paste0("tab:overlap-matrix-", tolower(alg_data$alg_abbr[1])),
      align = c("r", "|", rep("r", ncol(overlap)), "|")
    ) %>%
    xtable::print.xtable(
      file = paste0("tab/overlap-matrix-", tolower(alg_data$alg_abbr[i]), ".tex")
    )
}
# compare most similar parts
compare_clusters <- list(
  list(communities(comm_fast_greedy)[[1]],
       igraph:::groups.default(list(membership = clust_opt_fast_greedy))[[3]]),
  list(communities(comm_edge_betweenness)[[8]],
       igraph:::groups.default(list(membership = clust_opt_edge_betweenness))[[4]]),
  list(communities(comm_walktrap)[[7]],
       igraph:::groups.default(list(membership = clust_opt_walktrap))[[4]])
)
# print these to a table
for (i in seq_along(compare_clusters)) {
  for (j in 1:2) {
    cat(
      boldface_shared(compare_clusters[[i]])[[j]],
      "",
      sep = "\\\\\n"
    )
  }
}
