# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")
library(dplyr)
library(tidyr)
library(forcats)
library(stringr)
library(ggplot2)
library(ordr)
source("R/utils.r")

# source and target data directories
sdir <- "~/Documents/CQM/comorbidity/data/sensitivity/"

# function to convert a named matrix to a data frame with appropriate factors
square_to_pairs <- function(mat, keys = c("row", "col"), value = "value") {
  stopifnot(! any(sapply(dimnames(mat), is.null)))
  if (length(keys) == 1) keys <- paste(keys, 1:2, sep = "_")
  stopifnot(length(keys) == 2)
  df <- tibble(
    key_1 = rownames(mat)[row(mat)[lower.tri(mat, diag = TRUE)]],
    key_2 = colnames(mat)[col(mat)[lower.tri(mat, diag = TRUE)]],
    value = mat[lower.tri(mat, diag = TRUE)]
  )
  df <- mutate(df, key_1 = factor(key_1, rownames(mat)))
  df <- mutate(df, key_2 = factor(key_2, colnames(mat)))
  df <- rename(
    df,
    !! keys[1] := key_1, !! keys[2] := key_2,
    !! value := "value"
  )
  df
}

## Phase I

load(paste0(sdir, "concordance-data.RData"))

# concordance across constructions within datasets
# (levels must agree with those of `source_info`)
stopifnot(all(levels(source_info$source) == levels(concordance_data$source)))
source_incl <- match(
  c("columbia", "medicare1993-3", "scthans1998-2008", "stanford2013"),
  #c("medicare1993-5", "michigan", "columbia2013", "mimic-iii"),
  levels(concordance_data$source)
)

concordance_data %>%
  rename(node = name) %>%
  mutate(abbr = factor(abbr, levels = source_info$abbr)) %>%
  # rename variables in preparation for plotting (labels)
  mutate(pval = paste0("p = ", pval)) %>%
  mutate(corr = ifelse(corr == "none", as.character(corr), toupper(corr))) %>%
  mutate_at(vars(pval, corr), fct_inorder) %>%
  left_join(select(measure_info, measure, meas = abbr), by = "measure") %>%
  mutate(measure = fct_drop(meas), meas = NULL) %>%
  arrange(source, centrality, abbr, pval, corr, measure) %>%
  unite(col = "construction", pval, corr, measure, sep = ", ") %>%
  mutate(construction = fct_inorder(construction)) %>%
  # reorganize data in preparation for Kendall calculations
  spread(construction, value) %>%
  mutate_at(vars(source:centrality), as.character) %>%
  select(-node) %>%
  nest(-(source:centrality)) %>%
  mutate_at(vars(source, abbr, ontology, centrality), fct_inorder) %>%
  # 15-by-15 Kendall correlation matrices
  mutate(kendall = purrr::map(data, cor, method = "kendall")) %>%
  select(-data) %>%
  # pairwise arrangement of correlation matrices
  mutate(kendall_pairs = purrr::map(
    kendall,
    square_to_pairs,
    keys = "construction", value = "Kendall"
  )) %>%
  print() -> construction_concordance
# heatmaps
construction_concordance_plot <- construction_concordance %>%
  filter(source %in% source_info$source[source_incl]) %>%
  unnest(kendall_pairs) %>%
  # factorize construction parameters in preparation for plotting
  mutate(construction_2 = fct_rev(construction_2)) %>%
  ggplot(aes(x = construction_1, y = construction_2)) +
  theme_minimal() + coord_equal() +
  facet_grid(abbr ~ centrality) +
  labs(x = "", y = "") +
  theme(axis.text.x = element_text(angle = 60, hjust = 1)) +
  scale_x_discrete(position = "bottom") +
  scale_fill_distiller(
    limit = c(-1, 1), guide = "colorbar",
    type = "div", palette = 5, na.value = "transparent"
  ) +
  geom_tile(aes(fill = Kendall), color = "white")
ggplot2::ggsave(
  construction_concordance_plot,
  height = 1 + length(source_incl) * 2.5, width = 10,
  filename = "fig/centrality-heatmap-construction.pdf"
)
# inertia decomposition
#mat <- construction_concordance$kendall[[2]]
corr_decomp <- function(mat, pred, prefix) {
  # matrix decomposition
  proj <- pred %*% solve(t(pred) %*% pred) %*% t(pred)
  mat_proj <- proj %*% mat
  mat_perp <- mat - mat_proj
  # inertia decomposition
  inertia <- sum(scale(mat, scale = FALSE)^2)
  inertia_proj <- sum(scale(mat_proj, scale = FALSE)^2)
  inertia_perp <- sum(scale(mat_perp, scale = FALSE)^2)
  stopifnot(all.equal(inertia, inertia_proj + inertia_perp))
  # return data
  res <- data.frame(
    constrained = inertia_proj / inertia,
    residual = inertia_perp / inertia
  )
  names(res) <- paste0(prefix, "_", names(res))
  res
}
corr_decomps <- function(mat) {
  # predictor matrix
  rows <- rownames(mat)
  pred <- cbind(
    none = grepl(", none, ", rows),
    FWER = grepl(", FWER, ", rows),
    FDR = grepl(", FDR, ", rows),
    unit = grepl(", unit$", rows),
    oddsratio = grepl(", OR-MMLE$", rows),
    phi = grepl(", phi$", rows),
    forbes = grepl(", F$", rows),
    rho_psych = grepl(", r\\[t\\]$", rows)
  )
  pred <- apply(pred, 2, as.integer)
  rownames(pred) <- rows
  cbind(
    corr_decomp(mat, pred[, 1:3], "correction"),
    corr_decomp(mat, pred[, 4:8], "measure"),
    corr_decomp(mat, pred[, c(1:3, 5:8)], "both")
  )
}
construction_concordance %>%
  select(abbr, ontology, centrality, kendall) %>%
  arrange(abbr, ontology, centrality) %>%
  mutate(decomps = purrr::map(kendall, corr_decomps)) %>%
  unnest(decomps) %>%
  select(-kendall) %>%
  gather(calculation, proportion, -abbr, -ontology, -centrality) %>%
  separate("calculation", c("parameter", "component"), sep = "_") %>%
  mutate(parameter = fct_inorder(parameter)) %>%
  spread(component, proportion) %>%
  print() -> concordance_decomps
# table
concordance_decomps %>%
  select(abbr, ontology, centrality, parameter, constrained) %>%
  mutate(parameter = fct_recode(
    parameter,
    Correction = "correction",
    Measure = "measure",
    Both = "both"
  )) %>%
  spread(parameter, constrained) %>%
  rename(Dataset = abbr, Ontology = ontology, Centrality = centrality) %>%
  xtable::xtable(
    label = "tab:centrality-concordance-inertia",
    caption = paste0(
      "Proportion of variance in rank correlations accounted for by ",
      "the choice(s) of p-value correction, of binary association measure, ",
      "and of both. ",
      "Kendall rank correlations were calculated among all combinations of ",
      "network construction parameters ",
      "within each data source and centrality measure."
    ),
    align = c("r", rep("l", 3), rep("r", 3)),
    digits = c(0, rep(0, 3), rep(3, 3))
  ) %>%
  xtable::print.xtable(
    file = "tab/centrality-concordance-inertia.tex",
    size = "small",
    caption.placement = "top",
    hline.after = c(-1, which(! duplicated(.$abbr)) - 1, nrow(.)),
    include.rownames = FALSE,
    rotate.colnames = FALSE,
    sanitize.colnames.function = identity
  )
# bar plots
concordance_bars <- concordance_decomps %>%
  filter(parameter != "both") %>%
  ggplot(aes(x = parameter, y = constrained)) +
  facet_grid(abbr ~ centrality) +
  geom_bar(stat = "identity") +
  ggtitle("Decompositions of inertia for centrality rank correlations")
ggsave(
  concordance_bars,
  height = 2.5 * nrow(distinct(select(concordance_decomps, abbr, ontology))),
  width = 3 * nrow(distinct(select(concordance_decomps, centrality))),
  filename = here::here("fig/centrality-decomps.pdf")
)
# biplots
#mat <- construction_concordance$kendall[[2]]
corr_biplot <- function(mat) {
  eigen_mat <- try(eigen_ord(mat))
  if (inherits(eigen_mat, "try-error")) {
    return(ggplot())
  }
  eigen_mat %>%
    as_tbl_ord() %>%
    confer_inertia(1) %>%
    negate_to_nonneg_orthant("u") %>%
    augment() %>%
    mutate_u(
      pval = fct_inorder(str_remove(.name, ", .+, .+$")),
      correction = fct_inorder(str_remove_all(.name, "(^[^,]+, )|(, [^,]+$)")),
      measure = fct_inorder(str_remove(.name, "^.+, .+, "))
    ) %>%
    ggbiplot(axis.percents = FALSE) +
    theme_bw() +
    scale_x_continuous(limits = c(NA, 1)) +
    geom_unit_circle() +
    geom_u_vector(aes(color = measure, linetype = correction)) +
    geom_u_text_repel(
      aes(label = measure, color = measure),
      min.segment.length = Inf
    ) +
    scale_color_brewer(type = "qual", palette = "Set1")
}
construction_concordance %>%
  select(abbr, ontology, centrality, kendall) %>%
  arrange(abbr, ontology, centrality) %>%
  mutate(biplot = purrr::map(kendall, corr_biplot)) %>%
  mutate(biplot = purrr::pmap(., ~ ..5 + ggtitle(
    paste0("Kendall ", ..3, " rank correlations"),
    paste0(..1, " data (", ..2, " ontology)")
  ))) %>%
  print() -> concordance_biplots
ggsave(
  height = 5 * nrow(distinct(select(concordance_biplots, abbr, ontology))),
  width = 6 * nrow(distinct(select(concordance_biplots, centrality))),
  filename = here::here("fig/centrality-biplots.pdf"),
  gridExtra::arrangeGrob(
    grobs = concordance_biplots$biplot, ncol = 3, respect = FALSE
  )
)
# Michigan only
concordance_biplots %>%
  filter(abbr == "Michigan") %>%
  mutate(biplot = purrr::map(
    biplot,
    ~ . +
      labs(x = "", y = "") +
      guides(color = "none", linetype = "none") +
      ggtitle("", "")
  )) ->
  concordance_source_biplots
ggsave(
  height = 4,
  width = 4 * nrow(distinct(select(concordance_biplots, centrality))),
  filename = here::here("fig/centrality-biplots-Michigan.pdf"),
  gridExtra::arrangeGrob(
    grobs = concordance_source_biplots$biplot,
    ncol = 3, respect = FALSE
  )
)

## Phase II

load(paste0(sdir, "node-centrality.RData"))

# value scatterplots between centralities of disorders
# from different datasets using common ontologies

# organize node centralities with source and ontology
onts <- source_info %>%
  group_by(ontology) %>%
  summarize(count = n()) %>%
  ungroup() %>%
  filter(count > 1) %>%
  pull(ontology) %>% as.character()
node_centrality %>%
  rename(source = dataset) %>%
  mutate(node = gsub("\\.", "", node)) %>%
  mutate(
    log_degree = log(dcent),
    log_betweenness = log(bcent),
    exp_closeness = exp(ccent)
  ) %>%
  select(-dcent, -ccent, -bcent) %>%
  left_join(select(source_info, source, ontology), by = "source") %>%
  gather(centrality, value, log_degree, log_betweenness, exp_closeness) %>%
  print() -> ont_centrality
# scatterplots for each ontology
pdf(height = 5, width = 5, file = "fig/ont-cent.pdf")
for (ont in onts) {
  ont_cent <- ont_centrality %>%
    filter(ontology == ont) %>%
    select(-ontology)
  if (length(unique(ont_cent$source)) < 2) next
  for (corr in unique(ont_cent$correction)) {
    ont_corr_cent <- ont_cent %>%
      filter(correction == corr) %>%
      select(-correction)
    for (cent in unique(ont_corr_cent$centrality)) {
      dat <- ont_corr_cent %>%
        filter(centrality == cent) %>%
        select(-centrality) %>%
        spread(source, value) %>%
        rename_all(gsub, pattern = "-", replacement = "_")
      source_pairs <- combn(setdiff(names(dat), "node"), 2)
      for (i in 1:ncol(source_pairs)) {
        gg <- ggplot(dat, aes_string(
          x = source_pairs[1, i],
          y = source_pairs[2, i]
        )) +
          theme_bw() + coord_fixed() +
          geom_abline(intercept = 0, slope = 1, linetype = "dashed") +
          geom_point(alpha = .5) +
          ggtitle(paste0(
            ont, " ontology; p < 0.05 w/ ", corr, " correction; ",
            gsub("_", "-", cent), " centrality"
          ))
        print(gg)
      }
    }
  }
}
dev.off()

# rank correlations between datasets including group centralities

load(paste0(sdir, "node-centrality.RData"))
load(paste0(sdir, "group-centrality.RData"))

# combine node and group centrality data
bind_rows(
  node_centrality %>%
    left_join(
      source_info %>%
        select(source, ontology) %>%
        rename(dataset = source),
      by = "dataset"
    ) %>%
    mutate(type = "node") %>%
    rename(concept = node),
  group_centrality %>%
    mutate(type = "group") %>%
    rename(concept = group, ontology = map)
) %>%
  print() -> centrality

# standardize the centrality measures
centrality %>%
  # restrict to appropriate datasets
  filter(!grepl("(namcs)|(vaers)", dataset)) %>%
  mutate(dataset = fct_inorder(factor(dataset))) %>%
  # prepare centrality statistics
  group_by(dataset) %>%
  mutate(nv = n()) %>%
  ungroup() %>%
  mutate(
    dcent = as.integer(dcent),
    bcent = as.integer(bcent),
    ccent = ccent * (nv - 1)
  ) %>%
  select(-nv) %>%
  print() -> centrality

# biplot function
#mat <- ont_dat$kendall[[1]]
corr_biplot <- function(mat) {
  eigen_mat <- try(eigen_ord(mat))
  if (inherits(eigen_mat, "try-error")) {
    return(ggplot())
  }
  eigen_mat %>%
    as_tbl_ord() %>%
    confer_inertia(c(1, 0)) %>%
    negate_to_nonneg_orthant("u") %>%
    augment() %>%
    ggbiplot(axis.percents = FALSE) +
    theme_bw() +
    scale_x_continuous(limits = c(NA, 1)) +
    geom_unit_circle() +
    geom_u_vector(aes(color = .name, linetype = .name)) +
    geom_u_text_repel(
      aes(label = .name, color = .name),
      min.segment.length = Inf
    ) +
    scale_color_brewer(type = "qual", palette = "Set1")
}
# concordance across datasets
for (ont in unique(centrality$ontology)) {
  n_datasets <- centrality %>%
    select(dataset, ontology) %>%
    distinct() %>%
    filter(ontology == ont) %>%
    select(dataset) %>%
    n_distinct()
  if (n_datasets <= 1) next
  centrality %>%
    filter(ontology == ont) %>%
    left_join(
      source_info %>%
        select(dataset = source, abbr),
      by = "dataset"
    ) %>%
    mutate(abbr = factor(abbr, levels = source_info$abbr)) %>%
    rename(corr = correction) %>%
    mutate(corr = ifelse(corr == "none", as.character(corr), toupper(corr))) %>%
    mutate(corr = fct_rev(corr)) %>%
    select(-dataset, -ontology, -type) %>%
    rename(degree = dcent, betweenness = bcent, closeness = ccent) %>%
    gather(
      degree, betweenness, closeness,
      key = "centrality", value = "value", factor_key = TRUE
    ) %>%
    spread(key = abbr, value = value, fill = 0) %>%
    select(-concept) %>%
    nest(-corr, -centrality) %>%
    mutate(kendall = purrr::map(data, cor, method = "kendall")) %>%
    select(-data) %>%
    mutate(kendall_pairs = purrr::map(
      kendall,
      square_to_pairs,
      keys = "dataset", value = "Kendall"
    )) -> ont_dat
  # heatmaps
  ont_plot <- ont_dat %>%
    unnest(kendall_pairs) %>%
    mutate(
      dataset_1 = factor(dataset_1, levels = source_info$abbr),
      dataset_2 = factor(dataset_2, levels = rev(source_info$abbr))
    ) %>%
    ggplot(aes(x = dataset_1, y = dataset_2)) +
    theme_minimal() + coord_equal() +
    facet_grid(corr ~ centrality) +
    labs(x = "", y = "") +
    theme(axis.text.x = element_text(angle = 60, hjust = 1)) +
    scale_x_discrete(position = "bottom") +
    scale_fill_distiller(
      limit = c(-1, 1), guide = "colorbar",
      type = "div", palette = 5, na.value = "transparent"
    ) +
    geom_tile(aes(fill = Kendall), color = "white")
  ggplot2::ggsave(
    ont_plot,
    height = 6, width = 7,
    filename = paste0("fig/centrality-heatmap-dataset-", ont, ".pdf")
  )
  # biplots
  ont_dat %>%
    filter(! purrr::map_lgl(kendall, ~ any(is.na(.)))) %>%
    select(corr, centrality, kendall) %>%
    arrange(corr, centrality) %>%
    mutate(corr = fct_inorder(
      ifelse(corr == "none", "no", as.character(corr))
    )) %>%
    mutate(biplot = purrr::map(kendall, corr_biplot)) %>%
    mutate(biplot = purrr::pmap(., ~ ..4 + ggtitle(
      paste0("Kendall ", ..2, " rank correlations"),
      paste0("All sources using ", ..1, " correction")
    ))) %>%
    print() -> ont_biplots
  ggdim <- c(
    nrow(distinct(select(ont_biplots, corr))),
    nrow(distinct(select(ont_biplots, centrality)))
  )
  ggsave(
    height = 5 * ggdim[1],
    width = 6 * ggdim[2],
    filename = paste0("fig/centrality-biplots-dataset-", ont, ".pdf"),
    gridExtra::arrangeGrob(
      grobs = ont_biplots$biplot, ncol = ggdim[2], respect = FALSE
    )
  )
}
