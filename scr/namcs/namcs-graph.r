rm(list = ls())
setwd("~/Documents/CQM/comorbidity/")
source("scr/setup/params.r")

# packages
pkgs <- c("dplyr", "igraph")
for (i in which(!(pkgs %in% installed.packages()))) {
  install.packages(pkgs[i])
}
for (pkg in pkgs) library(pkg, character.only = TRUE)

# code
source("R/graph-construction.r")

# load bound NAMCS datasets
load("data/namcs/namcs-stata.RData")

# replace rule-out and empty diagnoses with NA
#unique(unlist(select(namcs, prdiag1, prdiag2, prdiag3)))
namcs$diag1[!grepl("(^No$)|(Dx is not)", namcs$prdiag1)] <- NA
namcs$diag2[!grepl("(^No$)|(Dx is not)", namcs$prdiag2)] <- NA
namcs$diag3[!grepl("(^No$)|(Dx is not)", namcs$prdiag3)] <- NA
# remove rows with only one diagnosis
namcs <- namcs %>% filter((!is.na(diag1) & !is.na(diag2)) |
                            (!is.na(diag1) & !is.na(diag3)) |
                            (!is.na(diag2) & !is.na(diag3)))
# shorten ICD codes to 3-character prefixes
# shorten ICD codes to 3-character prefixes
for (i in grep("^diag[0-9]+$", names(namcs))) {
  namcs[[i]] <- substr(namcs[[i]], 1, 3)
}

# graph construction parameters
edge_freq <- 10

# construct graph
data <- namcs %>% select(matches("^diag[0-9]+$"))
graph <- cooc_graph(data = data, edge_freq = edge_freq,
                    stat = "rr", thres = alpha, node_ignore = "000")

# save
#save(graph, file = "data/namcs/namcs-graph.RData")
saveRDS(graph, file = "data/namcs/namcs-graph.rds")
