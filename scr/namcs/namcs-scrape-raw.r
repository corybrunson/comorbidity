rm(list = ls())
setwd("~/Documents/CQM/nascent/comorbidity/")

# URLs where data is stored
urls <- c(
  "ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/namcs_public_use_files/",
  "ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/NAMCS/"
)
# list all files at these URLs
lfs <- unlist(lapply(urls, function(url) {
  paste0(url, strsplit(
    RCurl::getURL(url, ftp.us.epsv = FALSE, dirlistonly = TRUE), "\r*\n"
  )[[1]])
}))
# identify data files
dfs <- lfs[grep("(NAMCS|namcs)[0-9]{2,4}.(exe|zip)", lfs)]
# label file name vector by year
years <- gsub("^.*(NAMCS|namcs)([0-9]{2,4}).(exe|zip)", "\\2", dfs)
wh2 <- which(nchar(years) == 2)
years[wh2] <- paste0(
  ifelse(as.numeric(years[wh2]) >= 73, "19", "20"),
  years[wh2]
)
stopifnot(all(table(years) == 1))
names(dfs) <- years

# for each data file...
for (i in 1:length(dfs)) {
  f <- dfs[i]
  # file extension
  ext <- gsub("^.*(\\.[a-z]+$)", "\\1", f)
  # all files in temp directory
  temps <- list.files(path = "data/namcs/temp/")
  # destination
  dest <- paste0("data/namcs/temp/namcs-temp", ext)
  # download file
  download.file(f, destfile = dest)
  # un-archive file
  if (ext == ".exe") {
    system(paste0(
      "src/unar1.10.1/unar", " ",
      "data/namcs/temp/namcs-temp", ext, " ",
      "-o data/namcs/temp/"
    ))
  } else if (ext == ".zip") {
    unzip(paste0("data/namcs/temp/namcs-temp", ext))
  } else stop(paste0("Unknown file extension '", ext, "'"))
  # remove Windows executable file
  rmdest <- file.remove(dest)
  # new file in temp directory
  temps <- setdiff(list.files(path = "data/namcs/temp/"), temps)
  # read it if possible
  if (grepl("\\.dta$", temps)) {
    dat <- foreign::read.dta(temps)
  } else {
    # check for fixed-width format for given year
    formatfile <- paste0("data/namcs/formats/format", names(dfs)[i], ".csv")
    if (!file.exists(formatfile)) next else {
      # load format file
      fwf <- read.csv(formatfile)
      # which rows correspond to distinct items
      itemrows <- "???"
      # import file
      dat <- read.fwf(file = paste0("data/namcs/temp/", temps),
                      widths = fwf$length[itemrows],
                      col.names = fwf$abbr[itemrows])
    }
  }
  # lowercase names (for consistency)
  names(dat) <- tolower(names(dat))
  # desired variables
  dat[, c("year", "age", "sex",
          "phycode", "specr", "spec", "region",
          names(dat)[grep("diag[0-9]+$", names(dat))])]
  # save R data file
  save(dat, file = paste0("data/namcs/namcs", year, ".RData"))
}
