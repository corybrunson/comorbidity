rm(list = ls())
setwd("~/Documents/CQM/comorbidity/")

# desired variables
vars <- c(
  "year", "patwt",
  "age", "sex", "ethun", "raceun", "ager",
  "usetobac", "notobac", "paytype", "paytyper", "msa",
  "vdayr", "vmonth", "vyear",
  "injdet", "senbefor",
  "phycode", "retypoff", "specr", "spec", "region",
  "arthrtis", "asthma", "cancer", "cebvd", "chf", "copd", "deprn",
  "diabetes", "hyplipid", "htn", "ihd", "obesity", "ostprsis", "nochron",
  "bdatefl", "sexfl", "ethnicfl", "racerfl"
)
# desired variable regexes
grepvars <- c("diag[0-9]+$")

# URL where data is stored
url <- paste0("ftp://ftp.cdc.gov/pub/Health_Statistics/",
              "NCHS/dataset_documentation/namcs/stata/")
# list all files at this URL
lf <- paste0(url,
             strsplit(RCurl::getURL(url, dirlistonly = TRUE), "\r*\n")[[1]])
# identify archived stata files
dfs <- lf[grep("(NAMCS|namcs)[0-9]{2,4}\\-stata.(exe|zip)", lf)]
# label file name vector by year
years <- gsub("^.*(NAMCS|namcs)([0-9]{2,4})-stata.(exe|zip)", "\\2", dfs)
wh2 <- which(nchar(years) == 2)
years[wh2] <- paste0(
  ifelse(as.numeric(years[wh2]) >= 73, "19", "20"),
  years[wh2]
)
# in case of duplicates, choose .zip file
yeartab <- table(years)
for (i in 1:length(yeartab)) {
  if (yeartab[i] > 1) {
    y <- names(yeartab)[i]
    incl <- years != y | (years == y & grepl("\\.zip", dfs))
    dfs <- dfs[incl]
    years <- years[incl]
  }
}
stopifnot(all(table(years) == 1))
names(dfs) <- years

library(tidyverse)
# initialize list
namcs <- list()
# for each data file...
for (i in seq_along(dfs)) {
  f <- dfs[i]
  # file extension
  ext <- gsub("^.*(\\.[a-z]+$)", "\\1", f)
  # all files in temp directory
  temps <- list.files(path = "data/namcs/temp/")
  # destination
  dest <- paste0("data/namcs/temp/namcs-temp", ext)
  # download file
  download.file(f, destfile = dest)
  # un-archive file
  if (ext == ".exe") {
    system(paste0(
      "src/unar1.10.1/unar", " ",
      "data/namcs/temp/namcs-temp", ext, " ",
      "-o data/namcs/temp/"
    ))
  } else if (ext == ".zip") {
    setwd("data/namcs/temp/")
    unzip(paste0("namcs-temp", ext))
    setwd("../../../")
  } else stop(paste0("Unknown file extension '", ext, "'"))
  # remove Windows executable file
  rmdest <- file.remove(dest)
  # new file in temp directory
  temps <- setdiff(list.files(path = "data/namcs/temp/"), temps)
  # read it if possible
  if (grepl("\\.dta$", temps)) {
    dat <- foreign::read.dta(paste0("data/namcs/temp/", temps)) %>%
      # convert non-factor numeric fields to characters
      mutate_if(is.factor, dplyr::funs("as.character")) %>%
      as_tibble()
    #dat <- haven::read_dta(paste0("data/namcs/temp/", temps))
  } else {
    stop()
    # check for fixed-width format for given year
    formatfile <- paste0("data/namcs/formats/format", names(dfs)[i], ".csv")
    if (!file.exists(formatfile)) next else {
      # load format file
      fwf <- read.csv(formatfile)
      # which rows correspond to distinct items
      itemrows <- "???"
      # import file
      dat <- read.fwf(file = paste0("data/namcs/temp/", temps),
                      widths = fwf$length[itemrows],
                      col.names = fwf$abbr[itemrows],
                      colClasses = "character")
    }
  }
  # remove temporary file
  rmtemp <- file.remove(paste0("data/namcs/temp/", temps))
  # lowercase names (for consistency)
  names(dat) <- tolower(names(dat))
  # desired variables
  cols <- sort(setdiff(union(
    match(vars, names(dat)),
    do.call(c, lapply(grepvars, grep, x = names(dat)))
  ), NA))
  #dat <- dat[, setdiff(cols, NA)]
  # bind to existing data
  #namcs <- dplyr::mutate_each(dplyr::bind_rows(namcs, dat),
  #                            dplyr::funs("as.factor"))
  namcs <- c(namcs, list(dat %>% select(cols)))
}
namcs <- bind_rows(namcs) %>%
  mutate(paytype = ifelse(is.na(paytype), paytyper, paytype)) %>%
  select(-paytyper)

# save bound datasets
saveRDS(namcs, file = "data/namcs/namcs-stata.rds")
