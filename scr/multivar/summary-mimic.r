library(tidyverse)

# mapping from ICD-9 to CCS
ccs_dat <- readRDS(here::here("data/ccs-dat.rds"))

readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
  print() -> mimic_ccs

# frequency-rank plot faceted by care unit
mimic_ccs %>%
  #sample_n(720) %>%
  select(unit = curr_careunit, code = ccs_code_single) %>%
  group_by(unit, code) %>%
  count(name = "frequency") %>%
  group_by(unit) %>%
  select(-code) %>%
  arrange(desc(frequency)) %>%
  mutate(rank = row_number()) %>%
  ungroup() %>%
  ggplot(aes(rank, frequency)) +
  theme_bw() +
  facet_wrap(~ unit, scales = "free") +
  geom_line() +
  scale_x_continuous(breaks = 10^(0:3), minor_breaks = sqrt(10) * 10^(0:3)) +
  scale_y_continuous(breaks = 10^(0:5), minor_breaks = sqrt(10) * 10^(0:5)) +
  coord_trans(x = "log", y = "log") +
  labs(x = "Rank", y = "Frequency") +
  ggtitle("CCS group frequencies in MIMIC-III by unit") ->
  mimic_freqrank
print(mimic_freqrank)
ggsave(
  height = 7, width = 8,
  filename = here::here("fig/mimic-freqrank.pdf"),
  mimic_freqrank
)

# frequency-rank plot for each care unit
for (careunit in unique(mimic_ccs$curr_careunit)) {
  plotfile <- str_c("mimic-", tolower(careunit), "-freqrank.pdf")
  mimic_ccs %>%
    filter(curr_careunit == careunit) %>%
    select(-curr_careunit) %>%
    mutate(one = 1L) %>%
    spread(key = ccs_code_single, value = one, fill = 0L) %>%
    select(-subject_id, -hadm_id) %>%
    select_at(vars(names(.)[order(as.numeric(names(.)))])) %>%
    as.matrix() %>%
    colSums() %>%
    as.table() %>%
    enframe() %>%
    set_names(c("code", "frequency")) %>%
    arrange(desc(frequency)) %>%
    mutate(rank = row_number()) %>%
    ggplot(aes(rank, frequency)) +
    theme_bw() +
    geom_line() +
    scale_x_continuous(breaks = 10^(0:3), minor_breaks = sqrt(10) * 10^(0:3)) +
    scale_y_continuous(breaks = 10^(0:5), minor_breaks = sqrt(10) * 10^(0:5)) +
    coord_trans(x = "log", y = "log") +
    labs(x = "Rank", y = "Frequency") +
    ggtitle(str_c("CCS group frequencies in MIMIC-III ", careunit)) ->
    mimic_freqrank
  ggsave(
    height = 3, width = 4.5,
    filename = here::here("fig", plotfile),
    mimic_freqrank
  )
}

source(here::here("R/correlation-matrix.r"))
