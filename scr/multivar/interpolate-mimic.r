library(tidyverse)

# mapping from ICD-9 to CCS
ccs_dat <- readRDS(here::here("data/ccs-dat.rds"))

readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
  print() -> mimic

# summary statistics across hierarchical clusterings
mimic_interpolate <- tibble()
for (careunit in unique(mimic$curr_careunit)) {
  print(str_c("Care unit: ", careunit))
  
  f <- str_c("mimic-", tolower(careunit), "-pw.rds")
  d <- readRDS(here::here("data/mimic", f))
  m <- pairwise_matrix(d)
  h <- hclust(as.dist(sqrt(1 - m ^ 2)))
  ks <- seq(1, length(h$height), length.out = 24)
  gs <- lapply(ks, function(k) {
    partial_network(partial_data_blocks(d, cutree(h, k = k)))
  })
  for (i in 1:length(gs)) {
    if (is.null(igraph::edge_attr(gs[[i]], "weight"))) {
      gs[[i]] <- igraph::set_edge_attr(gs[[i]], "weight", value = 1)
    }
  }
  tibble(k = ks, net_w = gs) %>%
    mutate(rho_cut = rev(c(sqrt(1 - h$height ^ 2), 0))[as.integer(ks)]) %>%
    mutate(net_u = lapply(net_w, igraph::delete_edge_attr, "weight")) %>%
    mutate(
      density = sapply(net_u, igraph::edge_density),
      degree_gini = sapply(lapply(net_u, igraph::degree), ineq::Gini),
      degree_assortativity = sapply(net_u, igraph::assortativity_degree),
      geodesic_mean = sapply(net_u, igraph::mean_distance),
      triad_closure = sapply(net_u, igraph::transitivity, type = "global"),
      modularity = sapply(net_u, function(g) {
        igraph::modularity(igraph::cluster_walktrap(g))
      })
    ) %>%
    # restrict to cuts > 0.1
    #filter(rho_cut > 0.1) %>%
    gather(
      key = "stat", value = "value", density:modularity,
      factor_key = TRUE
    ) %>%
    mutate(stat = fct_recode(
      stat,
      `Link density` = "density",
      `Gini index of degree sequence` = "degree_gini",
      `Degree assortativity` = "degree_assortativity",
      `Mean internode geodesic distance` = "geodesic_mean",
      `Global triad closure` = "triad_closure",
      `Modularity of walktrap community partition` = "modularity"
    )) %>%
    print() -> summ_dat
  summ_dat %>%
    ggplot(aes(x = rho_cut, y = value)) +
    theme_bw() +
    ggtitle(str_c("Comorbidity network statistics for MIMIC-III ", careunit)) +
    facet_wrap(~ stat, ncol = 2, scales = "free_y") +
    xlab("Clustering cut (absolute correlation)") +
    ylab("Network statistic") +
    scale_x_reverse() +
    geom_line() -> summ_plot
  ggsave(
    summ_plot,
    height = 5, width = 8,
    filename = here::here(
      "fig",
      str_c("mimic-", tolower(careunit), "-interpolate.pdf")
    )
  )
}
