library(tidyverse)
library(RPostgreSQL)

# mapping from ICD-9 to CCS
ccs_dat <- readRDS(here::here("data/ccs-dat.rds"))

# connect to MIMIC-III
mimic <- dbConnect(
  PostgreSQL(),
  dbname = "mimic",
  host = "localhost",
  port = 5432,
  user = "mimicuser",
  password = "mimic"
)

# function to query a table
tbl_mimic <- function(table) {
  table <- as.character(substitute(table))
  tbl(mimic, dbplyr::in_schema("mimiciii", table))
}

# admissions by first unit
tbl_mimic(transfers) %>%
  select(subject_id, hadm_id, prev_careunit, curr_careunit) %>%
  filter(is.na(prev_careunit) & ! is.na(curr_careunit)) %>%
  select(subject_id, hadm_id, curr_careunit) %>%
  distinct() %>%
  print() -> unit_admissions
# diagnoses for these admissions in CCS ontology
tbl_mimic(diagnoses_icd) %>%
  inner_join(unit_admissions, by = c("subject_id", "hadm_id")) %>%
  select(subject_id, hadm_id, curr_careunit, icd9_code) %>%
  left_join(ccs_dat, by = c("icd9_code" = "icd9"), copy = TRUE) %>%
  filter(icd9_type == "DX") %>%
  select(subject_id, hadm_id, curr_careunit, ccs_code_single) %>%
  distinct() %>%
  print() -> unit_diagnoses
# save data
unit_diagnoses %>%
  collect() %>%
  saveRDS(here::here("data/mimic/mimic-ccs.rds"))
