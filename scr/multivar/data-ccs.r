### Clinical Classifications Software categories for ICD-9 codes

library(tidyverse)

## download

# file names
appendix.files <- c("AppendixASingleDX.txt", "AppendixBSinglePR.txt",
                    "AppendixCMultiDX.txt", "AppendixDMultiPR.txt")
# download if necessary
for (file in appendix.files) {
  if (file.exists(here::here(paste0("data/", file)))) next
  url <- paste0("https://www.hcup-us.ahrq.gov/toolssoftware/ccs/", file)
  download.file(url = url, destfile = here::here(paste0("data/", file)),
                method = "curl")
}

## read

# single-level categories
ccs_dat_single <- bind_rows(
  lapply(appendix.files[grep("Single", appendix.files)], function(file) {
    # read lines and identify CCS categories
    l <- readLines(paste0("data/", file))
    w <- grep("^[0-9]", l)
    # initialize data frame with CCS code and name
    dat <- tibble(
      icd9_type = gsub("^.*(DX|PR)\\.txt$", "\\1", file),
      ccs_code = gsub("(^[0-9]+) +.*$", "\\1", l[w]),
      ccs_category = gsub("^[0-9]+ +([A-Z].*$)", "\\1", l[w])
    )
    # vector of space-separated sets of ICD-9 codes
    w <- c(w, length(l) + 2)
    icd9s <- sapply(1:(length(w) - 1), function(i) {
      paste(l[(w[i] + 1):(w[i + 1] - 2)], collapse = " ")
    })
    icd9s <- gsub(" +", " ", icd9s) # remove contiguous spaces
    icd9s <- gsub("(^ )|( $)", "", icd9s) # remove leading/trailing spaces
    # lengths of sets
    icd9_lens <- nchar(icd9s) - nchar(gsub(" ", "", icd9s)) + 1
    stopifnot(nrow(dat) == length(icd9_lens))
    # append vector of ICD-9 codes to data frame
    dat <- bind_cols(icd9 = unlist(strsplit(icd9s, split = " ")),
                     dat[rep(1:nrow(dat), icd9_lens), ])
    # return data frame
    dat
  })
)

# multi-level categories
ccs_dat_multi <- bind_rows(
  lapply(appendix.files[grep("Multi", appendix.files)], function(file) {
    # read lines and identify CCS categories
    l <- readLines(paste0("data/", file))
    w <- grep("^[0-9]", l)
    # initialize data frame with CCS code and name
    ccs_category <- gsub("^[0-9\\.]+ +([A-Z\\'].+)$", "\\1", l[w])
    ccs_category <- gsub("(^[^\\[]+)\\[.*$", "\\1", ccs_category)
    ccs_category <- gsub("( {2}| $)", "\\|", ccs_category)
    ccs_category <- gsub("(^[^\\|]+)\\|.*$", "\\1", ccs_category)
    ccs_category <- gsub(" -$", "\\.\\.\\.", ccs_category)
    dat <- data.frame(
      icd9_type = gsub("^.*(DX|PR)\\.txt$", "\\1", file),
      ccs_code = gsub("(^[0-9\\.]+) +.*$", "\\1", l[w]),
      ccs_category = ccs_category
    )
    # vector of space-separated sets of ICD-9 codes
    w <- c(w, length(l) + 2)
    icd9s <- sapply(1:(length(w) - 1), function(i) {
      paste(l[(w[i] + 1):(w[i + 1] - 2)], collapse = " ")
    })
    icd9s <- gsub(" +", " ", icd9s) # remove contiguous spaces
    icd9s <- gsub("(^ )|( $)", "", icd9s) # remove leading/trailing spaces
    # lengths of sets
    icd9_lens <- nchar(icd9s) - nchar(gsub(" ", "", icd9s)) + 1
    icd9_lens[icd9s == ""] <- 0
    stopifnot(nrow(dat) == length(icd9_lens))
    # append vector of ICD-9 codes to data frame
    dat <- bind_cols(icd9 = unlist(strsplit(icd9s, split = " ")),
                     dat[rep(1:nrow(dat), icd9_lens), ])
    # return data frame
    dat
  })
)

## merge

ccs_dat <- full_join(ccs_dat_single, ccs_dat_multi,
                     by = c("icd9_type", "icd9"),
                     suffix = c("_single", "_multi"))

## save

saveRDS(ccs_dat, file = here::here("data/ccs-dat.rds"))

for (file in appendix.files) {
  if (file.exists(here::here(paste0("data/", file)))) {
    file.remove(here::here(paste0("data/", file)))
  }
}
