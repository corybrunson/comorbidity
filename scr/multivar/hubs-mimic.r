library(tidyverse)
library(tidygraph)

source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))
source(here::here("R/joint-distribution.r"))

# number of hubs to include for each unit
n_hubs <- 3
n_nbrs <- 4

# care units
readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
  pull(curr_careunit) %>%
  unique() %>%
  setdiff(c("NWARD", "NICU")) %>%
  print() -> careunits

# load CCS definitions
ccs_dat <- readRDS(here::here("data/ccs-dat.rds"))
disorder_ccs <- distinct(select(
  filter(ccs_dat, icd9_type == "DX"),
  code = ccs_code_single, disorder = ccs_category_single
))

# function to calculate centralities and neighborhoods
calc_cent <- function(net, n_hubs = 3, n_nbrs = 4) {
  # links and weights
  net %>%
    activate(links) %>%
    as_tibble() %>%
    # CCS codes
    mutate_at(vars(from, to), ~ igraph::V(net)$name[.]) %>%
    identity() -> link_data
  # centralities and neighborhoods
  net %>%
    activate(links) %>%
    # restrict to positive associations
    filter(weight > 0) %>%
    # introduce reciprocal weights for geodesic-based centrality measures
    mutate(distance = 1 / weight) %>%
    # calculate node centralities (syntax is awful and should be improved)
    activate(nodes) %>%
    mutate(betweenness = centrality_betweenness(weights = .E()$distance)) %>%
    # calculate immediate neighborhoods
    mutate(neighborhood = igraph::neighborhood(
      .G(), 1, mode = "all", mindist = 1
    )) %>%
    mutate(neighborhood = map(neighborhood, names)) %>%
    # extract node info
    as_tibble() %>%
    identity() -> node_data
  # hubs and their top neighbors
  node_data %>%
    # hubs
    arrange(desc(betweenness)) %>%
    top_n(n_hubs, betweenness) %>%
    # closest neighbors
    unnest(neighborhood) %>%
    rename(neighbor = neighborhood) %>%
    left_join(
      bind_rows(link_data, rename(link_data, from = to, to = from)),
      by = c("name" = "from", "neighbor" = "to")
    ) %>%
    group_by(name) %>%
    arrange(desc(weight)) %>%
    top_n(n_nbrs, weight)
}

# -+-
res %>%
  # join CCS categories
  left_join(disorder_ccs, by = c("name" = "code")) %>%
  unite("disorder", name, disorder, sep = " ") %>%
  left_join(
    rename(disorder_ccs, alter = disorder),
    by = c("neighbor" = "code")
  ) %>%
  unite("comorbidity", neighbor, alter, sep = " ") %>%
  group_by(disorder, betweenness) %>%
  summarize(comorbidities = str_c(
    comorbidity,
    " (", format(weight, digits = 2, nsmall = 2, scientific = FALSE), ")",
    collapse = ", "
  ))

# hubs for each unit

mimic_top_hubs <- tibble()
# fixed evidential threshold, three models
for (careunit in careunits) {
  
  print(str_c("Care unit: ", careunit))
  
  # pairwise and partial correlation data
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  unit_pt <- readRDS(here::here("data/mimic", ptfile))
  
  # best available joint distribution model data
  
  # joint distribution model files
  jdfile <- list.files(
    here::here("data/mimic"),
    str_c("mimic-", tolower(careunit), "-.+-jd.rds")
  )
  # best available proportion of sample
  jdprops <- jdfile %>%
    str_replace("^.+-prop([0-9\\_]+)-.+-jd\\.rds", "\\1") %>%
    str_replace("\\_", ".") %>%
    as.numeric()
  jdprop <- max(jdprops)
  print(str_c("Proportion of ", careunit, " population in JIDM: ", jdprop))
  jdfile <- jdfile[which(jdprops == max(jdprops))]
  # among remaining files, best available minimum incidence
  jdfreqs <- jdfile %>%
    str_replace("^.+-freq([0-9]+)-.+-jd\\.rds", "\\1") %>%
    as.numeric()
  jdfreq <- min(jdfreqs)
  print(str_c("Minimum incidence: ", jdfreq))
  jdfile <- jdfile[which(jdfreqs == min(jdfreqs))]
  # read file
  unit_jd <- readRDS(here::here("data/mimic", jdfile[1]))
  
  # construct networks from pairs data (with automatic cutoffs)
  unit_pwn <- pairwise_network(unit_pt)
  unit_ptn <- partial_network(unit_pt)
  unit_jdn <- jdm_network(unit_jd)
  
  # calculate betweenness hubs
  bind_rows(
    unit_pwn %>%
      calc_cent() %>%
      mutate(model = "Pairwise"),
    unit_ptn %>%
      calc_cent() %>%
      mutate(model = "Partial"),
    unit_jdn %>%
      calc_cent() %>%
      mutate(model = "JIDM")
  ) %>%
    mutate(unit = careunit) %>%
    identity() -> unit_top_hubs
  
  mimic_top_hubs <- bind_rows(mimic_top_hubs, unit_top_hubs)
}
mimic_top_hubs %>%
  mutate(unit = factor(unit, careunits)) %>%
  mutate(model = fct_inorder(model)) %>%
  # join CCS categories
  left_join(disorder_ccs, by = c("name" = "code")) %>%
  unite("disorder", name, disorder, sep = " ") %>%
  left_join(
    rename(disorder_ccs, alter = disorder),
    by = c("neighbor" = "code")
  ) %>%
  unite("comorbidity", neighbor, alter, sep = " ") %>%
  mutate(disorder = factor(
    disorder,
    str_c(disorder_ccs$code, disorder_ccs$disorder, sep = " ")
  )) %>%
  # collapse comorbidities into comma-separated strings
  group_by(unit, model, disorder, betweenness) %>%
  summarize(comorbidities = str_c(
    comorbidity,
    " (", format(weight, digits = 2, nsmall = 2, scientific = FALSE), ")",
    collapse = ", "
  )) %>%
  ungroup() %>%
  arrange(unit, model, disorder) %>%
  print() -> mimic_top_hubs

mimic_top_hubs %>%
  mutate_if(
    is.numeric,
    ~ format(., digits = 3, nsmall = 3, scientific = FALSE)
  ) %>%
  rename_all(Hmisc::capitalize) %>%
  print() %>%
  write_csv(here::here("tab/hubs-mimic.csv"))

stop("Revise continuation below to get centralities of elsewhere-hubs.")

# number of distinct hubs
mimic_top_hubs %>%
  select(disorder) %>% distinct() %>%
  print() -> top_hubs

# unit hub centralities in each unit

mimic_hubs <- tibble()
# fixed evidential threshold, three models
for (careunit in careunits) {
  
  print(str_c("Care unit: ", careunit))
  
  # pairwise and partial correlation data
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  unit_pt <- readRDS(here::here("data/mimic", ptfile))
  
  # best available joint distribution model data
  
  # joint distribution model files
  jdfile <- list.files(
    here::here("data/mimic"),
    str_c("mimic-", tolower(careunit), "-.+-jd.rds")
  )
  # best available proportion of sample
  jdprops <- jdfile %>%
    str_replace("^.+-prop([0-9\\_]+)-.+-jd\\.rds", "\\1") %>%
    str_replace("\\_", ".") %>%
    as.numeric()
  jdprop <- max(jdprops)
  print(str_c("Proportion of ", careunit, " population in JIDM: ", jdprop))
  jdfile <- jdfile[which(jdprops == max(jdprops))]
  # among remaining files, best available minimum incidence
  jdfreqs <- jdfile %>%
    str_replace("^.+-freq([0-9]+)-.+-jd\\.rds", "\\1") %>%
    as.numeric()
  jdfreq <- min(jdfreqs)
  print(str_c("Minimum incidence: ", jdfreq))
  jdfile <- jdfile[which(jdfreqs == min(jdfreqs))]
  # read file
  unit_jd <- readRDS(here::here("data/mimic", jdfile[1]))
  
  # construct networks from pairs data (with automatic cutoffs)
  unit_pwn <- pairwise_network(unit_pt)
  unit_ptn <- partial_network(unit_pt)
  unit_jdn <- jdm_network(unit_jd)
  
  # calculate betweenness centralities
  bind_rows(
    mutate(calc_cent(unit_pwn), model = "Pairwise"),
    mutate(calc_cent(unit_ptn), model = "Partial"),
    mutate(calc_cent(unit_jdn), model = "JIDM")
  ) %>%
    mutate(model = fct_inorder(model)) %>%
    semi_join(top_hubs, by = "disorder") %>%
    mutate(unit = careunit) %>%
    identity() -> unit_hubs
  
  mimic_hubs <- bind_rows(mimic_hubs, unit_hubs)
}
mimic_hubs <- mutate(mimic_hubs, unit = factor(unit, careunits))

# add CCS definitions and arrange by disorder pair
mimic_hubs %>%
  unite("disorder", name, disorder, sep = " ", remove = FALSE) %>%
  mutate(disorder = fct_reorder(disorder, as.integer(name))) %>%
  unite("network", unit, model, sep = ", ", remove = FALSE) %>%
  arrange(unit, model) %>%
  mutate(network = fct_inorder(network)) %>%
  select(-name, -unit, -model) %>%
  spread(network, betweenness) %>%
  rename(Disorder = disorder) %>%
  mutate_if(
    is.numeric,
    ~ format(., digits = 3, nsmall = 3, scientific = FALSE)
  ) %>%
  #replace_na()
  print() %>%
  write_csv(here::here("tab/hubs-mimic.csv"))
