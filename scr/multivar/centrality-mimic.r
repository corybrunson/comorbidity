# setup
library(tidyverse)
library(tidygraph)
library(ggraph)
library(ordr)

source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))
source(here::here("R/joint-distribution.r"))

ccs_dat <- readRDS(here::here("data/ccs-dat.rds"))
ccs_dat %>%
  filter(icd9_type == "DX") %>%
  select(code = ccs_code_single, disorder = ccs_category_single) %>%
  distinct() %>%
  arrange(as.integer(code)) %>%
  mutate_all(fct_inorder) %>%
  print() -> disorder_ccs

# care units
readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
  pull(curr_careunit) %>%
  unique() %>%
  print() -> careunits

# function to calculate centralities on each network
calc_cents <- function(net, suffix) {
  net %>%
    activate(nodes) %>%
    # join CCS categories
    left_join(disorder_ccs, by = c("name" = "code")) %>%
    activate(links) %>%
    # restrict to positive associations
    filter(weight > 0) %>%
    # introduce reciprocal weights for geodesic-based centrality measures
    mutate(distance = 1 / weight) %>%
    # calculate node centralities (syntax is awful and should be improved)
    activate(nodes) %>%
    mutate(
      degree = centrality_degree(weights = .E()$weight),
      betweenness = centrality_betweenness(weights = .E()$distance),
      closeness = centrality_closeness(weights = .E()$distance)
    ) %>%
    # extract node info
    as_tibble() %>%
    # suffix names
    rename_at(vars(degree, betweenness, closeness), ~ str_c(., "_", suffix))
}

# function to convert a named matrix to a data frame with appropriate factors
square_to_pairs <- function(mat, keys = c("row", "col"), value = "value") {
  stopifnot(! any(sapply(dimnames(mat), is.null)))
  if (length(keys) == 1) keys <- paste(keys, 1:2, sep = "_")
  stopifnot(length(keys) == 2)
  df <- tibble(
    key_1 = rownames(mat)[row(mat)[lower.tri(mat, diag = TRUE)]],
    key_2 = colnames(mat)[col(mat)[lower.tri(mat, diag = TRUE)]],
    value = mat[lower.tri(mat, diag = TRUE)]
  )
  df <- mutate(df, key_1 = factor(key_1, rownames(mat)))
  df <- mutate(df, key_2 = factor(key_2, colnames(mat)))
  df <- rename(
    df,
    !! keys[1] := key_1, !! keys[2] := key_2,
    !! value := "value"
  )
  df
}

# initiate concordance data
network_concordance <- tibble()
# fixed evidential threshold, three models
for (careunit in careunits) {
  
  print(str_c("Care unit: ", careunit))
  
  # best available joint distribution model data
  
  # joint distribution model files
  jdfile <- list.files(
    here::here("data/mimic"),
    str_c("mimic-", tolower(careunit), "-.+-jd.rds")
  )
  # best available proportion of sample
  jdprops <- jdfile %>%
    str_replace("^.+-prop([0-9\\_]+)-.+-jd\\.rds", "\\1") %>%
    str_replace("\\_", ".") %>%
    as.numeric()
  jdprop <- max(jdprops)
  print(str_c("Proportion of ", careunit, " population in JDM: ", jdprop))
  jdfile <- jdfile[which(jdprops == max(jdprops))]
  # among remaining files, best available minimum incidence
  jdfreqs <- jdfile %>%
    str_replace("^.+-freq([0-9]+)-.+-jd\\.rds", "\\1") %>%
    as.numeric()
  jdfreq <- min(jdfreqs)
  print(str_c("Minimum incidence: ", jdfreq))
  jdfile <- jdfile[which(jdfreqs == min(jdfreqs))]
  # read file
  mimic_jd <- readRDS(here::here("data/mimic", jdfile[1]))
  
  # pairwise and partial correlation data for same parameters
  
  set.seed(0)
  # co-occurrence matrix
  readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
    filter(curr_careunit == careunit) %>%
    select(-curr_careunit) %>%
    #distinct() %>%
    mutate(one = 1L) %>%
    spread(key = ccs_code_single, value = one, fill = 0L) %>%
    select(-subject_id, -hadm_id) %>%
    select_at(vars(names(.)[order(as.numeric(names(.)))])) %>%
    # restrict to variables with minimum prevalence
    select_if(list(~sum(.) >= jdfreq)) %>%
    # remove cases with no occurrences
    filter_all(any_vars(. == 1)) %>%
    # stratified subset
    sample_frac(jdprop) %>%
    print() -> mimic_x
  
  # pairwise correlation data
  pwfile <- str_c("mimic-", tolower(careunit), "-pw.rds")
  mimic_pw <- pairwise_data(mimic_x, z = z_cutoff)
  
  # partial correlation data
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  mimic_pt <- partial_data(mimic_pw, use.shrinkage = TRUE)
  
  rm(mimic_x)
  
  # construct networks from pairs data (with automatic cutoffs)
  mimic_pwn <- pairwise_network(mimic_pw)
  mimic_ptn <- partial_network(mimic_pt)
  mimic_jdn <- jdm_network(mimic_jd)
  
  # tibble of centralities
  calc_cents(mimic_pwn, "pairwise") %>%
    inner_join(calc_cents(mimic_ptn, "partial"), by = c("name", "disorder")) %>%
    inner_join(calc_cents(mimic_jdn, "joint"), by = c("name", "disorder")) %>%
    gather("measure_network", "centrality", -(1:2)) %>%
    separate(measure_network, c("measure", "network"), sep = "_") %>%
    mutate_at(vars(measure, network), fct_inorder) %>%
    spread(network, centrality) %>%
    select(-name, -disorder) %>%
    nest(-measure) %>%
    mutate(kendall = map(data, cor, method = "kendall")) %>%
    select(-data) %>%
    mutate(kendall_pairs = map(
      kendall,
      square_to_pairs,
      keys = "network", value = "Kendall"
    )) %>%
    mutate(unit = careunit) %>%
    print() -> net_con
  
  # bind to concordance data
  network_concordance <- bind_rows(network_concordance, net_con)
  
}
# factorize unit variable
network_concordance <- mutate(
  network_concordance,
  unit = factor(unit, careunits)
)

# function to generate biplots from pseudo-correlation matrices
#mat <- network_concordance$kendall[[2]]
corr_biplot <- function(mat) {
  eigen_mat <- try(eigen_ord(mat))
  if (inherits(eigen_mat, "try-error")) {
    return(ggplot())
  }
  eigen_mat %>%
    as_tbl_ord() %>%
    confer_inertia(c(1, 0)) %>%
    negate_to_nonneg_orthant("u") %>%
    augment() %>%
    mutate_u(
      pval = fct_inorder(str_remove(.name, ", .+, .+$")),
      correction = fct_inorder(str_remove_all(.name, "(^[^,]+, )|(, [^,]+$)")),
      measure = fct_inorder(str_remove(.name, "^.+, .+, "))
    ) %>%
    ggbiplot(axis.percents = FALSE) +
    theme_bw() +
    scale_x_continuous(limits = c(NA, 1)) +
    geom_unit_circle() +
    geom_u_vector(aes(linetype = measure)) +
    geom_u_text(aes(label = measure), hjust = "outward", vjust = "outward") +
    guides(linetype = "none")
}

# biplots
network_concordance %>%
  select(unit, measure, kendall) %>%
  arrange(unit, measure) %>%
  mutate(biplot = map(kendall, corr_biplot)) %>%
  mutate(biplot = pmap(., ~ ..4 + ggtitle(
    paste0("Kendall ", ..2, " rank correlations"),
    paste0("MIMIC data for ", ..1, " admissions")
  ))) %>%
  print() -> concordance_biplots
ggsave(
  height = 5 * nrow(distinct(select(concordance_biplots, unit))),
  width = 6 * nrow(distinct(select(concordance_biplots, measure))),
  filename = here::here("fig/mimic-centrality-biplots.pdf"),
  gridExtra::arrangeGrob(
    grobs = concordance_biplots$biplot, ncol = 3, respect = FALSE
  )
)
