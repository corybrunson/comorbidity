library(tidyverse)

source(here::here("R/namcs-info.r"))
source(here::here("R/cluster-sample.r"))
namcs <- readRDS(here::here("data/namcs/namcs-stata.rds"))

# proportion of physicians to keep
prop_phys <- .5

# relationship between race and ethnicity
namcs %>%
  select(raceun, ethun) %>%
  table() %>%
  as.data.frame() %>%
  spread(ethun, Freq) %>%
  mutate(hispanic_prop = `Hispanic or Latino` / `Not Hispanic or Latino`)

# calculate binary demographic variables
namcs %>%
  mutate(
    ethun = ifelse(ethun == "Blank", NA_character_, ethun),
    raceun = ifelse(raceun == "Blank", NA_character_, raceun)
  ) %>%
  mutate(
    age_0_14 = ager == "Under 15 years",
    age_15_24 = ager == "15-24 years",
    age_25_44 = ager == "25-44 years",
    age_45_64 = ager == "45-64 years",
    age_65_74 = ager == "65-74 years",
    age_75_ = ager == "75 years and over",
    male = sex == "Male",
    asian = raceun == "Asian Only",
    black = raceun == "Black/African American Only",
    native = raceun == "American Indian/Alaska Native Only" |
      raceun == "Native Hawaiian/Oth Pac Isl Only",
    white = raceun == "White Only",
    hispanic = ethun == "Hispanic or Latino",
    private = paytype == "Private insurance",
    medicare = paytype == "Medicare",
    medicaid = str_detect(paytype, "^Medicaid"),
    midwest = region == "Midwest",
    south = region == "South",
    west = region == "West",
    metro = msa == "MSA (Metropolitan Statistical Area)"
  ) %>%
  select(one_of(c(
    "year", "phycode",
    as.character(namcs_demographics$name),
    as.character(namcs_disorders$code)
  ))) %>%
  drop_na() %>%
  print() -> namcs_preproc

# subset data by year and cluster sample on physician code
set.seed(1)
namcs_preproc %>%
  filter(year == 2011) %>%
  mutate_at("phycode", as.character) %>%
  group_by(phycode) %>%
  add_tally() %>% cluster_sample_frac(size = prop_phys, weight = n) %>%
  ungroup() %>%
  print() -> namcs_sample

# binarize and save
namcs_sample %>%
  select(-year, -n) %>%
  mutate_at(vars(as.character(namcs_demographics$name)), as.integer) %>%
  mutate_at(
    vars(as.character(namcs_disorders$code)),
    list(~as.integer(. == "Yes"))
  ) %>%
  select_if(function(x) any(x == 1)) %>%
  print() %>%
  saveRDS(here::here("data/namcs/namcs.rds"))
