library(tidyverse)
library(tidygraph)
library(ggraph)

source(here::here("R/namcs-info.r"))
source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))
#source(here::here("R/joint-distribution.r"))

# NAMCS network data sets
readRDS(here::here("data/namcs/namcs.rds")) %>%
  select_at(vars(as.character(namcs_disorders$code))) %>%
  pairwise_data(z = z_cutoff) %>%
  partial_data() %>%
  print() -> namcs_pt
readRDS(here::here("data/namcs/namcs-jd0-rho.rds")) %>%
  rename_all(~ str_replace(., "rho", "rho_joint")) %>%
  print() -> namcs_jd0

# combined dataset ignoring indiscernible estimates
namcs_pt %>%
  full_join(namcs_jd0, by = c("v1", "v2")) %>%
  select(-n, -pval, -ends_with("_Z"), -rho_joint_sd) %>%
  mutate(rho = ifelse(rho_lower <= 0 & rho_upper >= 0, NA_real_, rho)) %>%
  mutate(rho_partial = ifelse(
    rho_partial_lower <= 0 & rho_partial_upper >= 0,
    NA_real_, rho_partial
  )) %>%
  mutate(rho_joint = ifelse(
    rho_joint_lower <= 0 & rho_joint_upper >= 0,
    NA_real_, rho_joint
  )) %>%
  select(-contains("lower"), -contains("upper")) %>%
  print() -> namcs_data

# largest variability in correlation estimates for each sign combination

# group by sign combination and take top variances
namcs_data %>%
  filter_at(vars(starts_with("rho")), ~ ! is.na(.)) %>%
  mutate_at(vars(starts_with("rho")), list(sign = ~ . / abs(.))) %>%
  rowwise() %>%
  mutate(rho_est_var = var(c(rho, rho_partial, rho_joint))) %>%
  ungroup() %>% group_by_at(vars(ends_with("sign"))) %>%
  arrange(desc(rho_est_var)) %>%
  top_n(1, rho_est_var) %>%
  ungroup() %>%
  arrange(desc(rho_sign), desc(rho_partial_sign), desc(rho_joint_sign)) %>%
  # formatting
  left_join(namcs_disorders, by = c("v1" = "code")) %>%
  rename(Disorder1 = abbr) %>%
  left_join(namcs_disorders, by = c("v2" = "code")) %>%
  rename(Disorder2 = abbr) %>%
  rename(Pairwise = rho, Partial = rho_partial, JIDM = rho_joint) %>%
  rename(BtwVar = rho_est_var) %>%
  mutate_at(
    vars(Pairwise, Partial, JIDM, BtwVar),
    ~ format(., digits = 3, nsmall = 3, scientific = FALSE)
  ) %>%
  select(Disorder1, Disorder2, n1, n2, n12, Pairwise, Partial, JIDM, BtwVar) %>%
  print() %>%
  write_csv(here::here("tab/var-max-pairs-namcs.csv"))

# largest total variability in estimates among triads for each sign combo

# self-join pairs data to form triads data and keep max min variances
namcs_data %>%
  filter_at(vars(starts_with("rho")), ~ ! is.na(.)) %>%
  rename_at(vars(starts_with("rho")), list(~ str_c(., "_12"))) %>%
  identity() -> namcs_12
namcs_12 %>%
  rename_all(list(~ str_replace(., "2", "3"))) %>%
  identity() -> namcs_13
namcs_13 %>%
  rename_all(list(~ str_replace(., "1", "2"))) %>%
  identity() -> namcs_23
namcs_12 %>%
  left_join(namcs_13) %>%
  left_join(namcs_23) %>%
  mutate_at(vars(starts_with("rho")), list(sign = ~ . / abs(.))) %>%
  rowwise() %>%
  mutate(
    rho_var_12 = var(c(rho_12, rho_partial_12, rho_joint_12)),
    rho_var_13 = var(c(rho_13, rho_partial_13, rho_joint_13)),
    rho_var_23 = var(c(rho_23, rho_partial_23, rho_joint_23))
  ) %>%
  mutate(rho_var_summ = min(c(rho_var_12, rho_var_13, rho_var_23))) %>%
  ungroup() %>%
  arrange(desc(rho_var_summ)) %>%
  top_n(6, rho_var_summ) %>%
  select(starts_with("v")) %>%
  print() -> namcs_triads
# appearances of each disorder among top 6 triads
namcs_triads %>%
  unlist() %>% as.character() %>% table()
# pairs among top 6 triads
bind_rows(
  select(namcs_triads, v1, v2),
  select(namcs_triads, v1, v2 = v3),
  select(namcs_triads, v1 = v2, v2 = v3)
) %>%
  distinct() %>%
  print() -> namcs_triads_data
# combined & annotated graph of links from each model
namcs_triads_data %>%
  left_join(namcs_data, by = c("v1", "v2")) %>%
  arrange(v1, v2) %>%
  pivot_longer(starts_with("rho"), names_to = "model", values_to = "weight") %>%
  mutate(sign = factor(
    as.character(as.integer(weight / abs(weight))),
    levels = c("-1", "1")
  )) %>%
  mutate(model = fct_recode(
    model,
    Pairwise = "rho",
    Partial = "rho_partial",
    JIDM = "rho_joint"
  )) %>%
  as_tbl_graph(directed = FALSE) %>%
  activate(links) %>%
  mutate(model = fct_inorder(model)) %>%
  activate(nodes) %>%
  left_join(select(namcs_disorders, name = code, abbr), by = "name") %>%
  left_join(distinct(select(namcs_data, name = v1, n = n1)), by = "name") %>%
  mutate(name = fct_inorder(name), abbr = fct_inorder(as.character(abbr))) %>%
  print() -> namcs_triads_graph
# circular graph layout
namcs_triads_graph %>%
  as_tibble(active = "nodes") %>%
  mutate(t = (1:nrow(.) - 1) / nrow(.)) %>%
  mutate(t = 2 * pi * t) %>%
  mutate(x = cos(t), y = sin(t)) %>%
  select(x, y) %>%
  print() -> layout_circle
# plot networks for comparison
namcs_triads_plot <- namcs_triads_graph %>%
  ggraph("manual", x = layout_circle$x, y = layout_circle$y) +
  theme_graph(base_family = "") +
  theme(plot.margin = margin(0, 0, 0, 0, "pt")) +
  coord_equal() +
  facet_edges(~ model, nrow = 1, ncol = 3) +
  geom_edge_link(aes(width = n12, alpha = weight, linetype = sign)) +
  #geom_edge_link(aes(alpha = weight, linetype = sign)) +
  scale_edge_width_continuous(range = c(.5, 4)) +
  scale_edge_linetype_manual(values = c("dashed", "solid")) +
  geom_node_point(aes(size = n)) +
  geom_node_label(
    aes(label = abbr, hjust = "outward", vjust = "outward"),
    alpha = .5
  ) +
  guides(
    size = "none", edge_width = "none",
    edge_alpha = "none", edge_color = "none", edge_linetype = "none"
  ) +
  scale_x_continuous(expand = expand_scale(mult = c(.3, .3))) +
  scale_y_continuous(expand = expand_scale(mult = c(.1, .1))) +
  ggtitle("High-variance triads", "Associations based on full network models")
print(namcs_triads_plot)
ggsave(
  here::here("fig/var-max-triads-networks.pdf"), namcs_triads_plot,
  width = 12, height = 4
)
