library(tidyverse)
library(igraph)
library(tidygraph)
library(ggraph)

source(here::here("R/namcs-info.r"))
source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))

namcs <- readRDS(here::here("data/namcs.rds"))

## interpolate between pairwise and full partial via hierarchical clustering

d <- pairwise_data(namcs)
m <- pairwise_matrix(d)

h <- hclust(as.dist(sqrt(1 - m ^ 2)))
tibble(code = h$labels) %>%
  left_join(namcs_disorders) %>%
  pull(abbr) ->
  h$labels
print(h$height)
plot(h)
pdf(height = 6, width = 8, file = here::here("fig/namcs-hclust.pdf"))
plot(h, main = "Correlation dendrogram of NAMCS chronic disorders")
dev.off()
# more clusters at left; larger clusters at right
r_branch <- sqrt(1 - h$height ^ 2)
# more clusters at left; larger clusters at right
ks <- nrow(m):1
gs <- lapply(ks, function(k) {
  # k = nrow(m) -> pairwise_network(d)
  # k = 1 -> partial_network(partial_data(d))
  partial_network(partial_data_blocks(d, cutree(h, k = k)))
})
ms <- lapply(ks, function(k) {
  # k = nrow(m) -> pairwise_network(d)
  # k = 1 -> partial_network(partial_data(d))
  partial_matrix(partial_data_blocks(d, cutree(h, k = k)))
})
ds <- lapply(ks, function(k) {
  partial_data_blocks(d, cutree(h, k = k)) %>%
    mutate(k = k)
}) %>%
  bind_rows() %>%
  select(-n, -n1, -n2, -n12) %>%
  mutate(rho_cut = rev(c(r_branch, 0))[as.integer(k)])

# correlation (bi)plots
devtools::load_all("~/Documents/edu/R/tidybiplot/")
ms <- lapply(
  ms, `dimnames<-`,
  replicate(2, namcs_disorders$abbr, simplify = FALSE)
)
vs <- lapply(lapply(ms, eigen), as_tbl_ord)
vs[[1]] <- align_to(vs[[1]], diag(1, nrow = nrow(vs[[1]])), .matrix = "u")
vs[2:length(vs)] <- lapply(
  vs[2:length(vs)],
  align_to,
  y = vs[[1]],
  .matrix = "u"
)
eigen_biplots <- lapply(c(13, 7, 1), function(i) {
  #v <- vs[[i]]
  vs[[i]] %>%
    mutate_u(highlight = .name %in% c("obesity", "cancer")) %>%
    ggbiplot(aes(label = .name)) +
    theme_bw() +
    scale_x_continuous(expand = expand_scale(mult = .15)) +
    scale_y_continuous(expand = expand_scale(mult = .2)) +
    scale_color_manual(values = c("black", "darkred")) +
    geom_u_vector(aes(color = highlight)) +
    geom_u_text_radiate(aes(color = highlight)) +
    guides(color = FALSE) +
    ggtitle(paste0("k = ", ks[i]))
})
ggsave(
  height = 5, width = 12, filename = here::here("fig/namcs-eigen-biplots.pdf"),
  gridExtra::arrangeGrob(
    grobs = eigen_biplots, nrow = 1, respect = FALSE
  )
)

if (FALSE) {
  vs <- lapply(ms, function(mat) {
    eig <- eigen(mat)
    eig$vectors %*% diag(sqrt(eig$values))
  })
  hist(ms[[1]] - vs[[1]] %*% t(vs[[1]]))
  pdf(height = 9, width = 15, file = here::here("fig/namcs-cor-biplots.pdf"))
  par(mfrow = c(3, 5), mar = c(1, 1, 1, 1) + 0.1)
  v0 <- NULL
  for (v in vs[c(1, 13)]) {
    if (!is.null(v0)) {
      inners <- colSums(v0 * v)
      signs <- sign(inners)
      v <- sweep(v, 2, signs, "*")
    }
    v0 <- v
    plot(v[, 1:2], pch = 16, cex = .5, asp = 1,
         xlim = c(-1, 1), ylim = c(-1, 1))
    arrows(0, 0, v[, 1], v[, 2], length = .1)
    text(v[, 1:2], colnames(m), cex = .75)
  }
  dev.off()
}

# link weights
link_weight_plot <- ds %>%
  unite(col = "disorder_pair", v1, v2, sep = "_", remove = FALSE) %>%
  mutate(status = factor(ifelse(
    rho_partial_upper < 0, "negative",
    ifelse(rho_partial_lower <= 0, "indiscernible", "positive")
  ), levels = c("positive", "indiscernible", "negative"))) %>%
  ggplot(aes(x = rho_cut, y = rho_partial, group = disorder_pair)) +
  theme_bw() +
  xlab("Clustering cut (absolute correlation)") +
  ylab("Partial tetrachoric correlation") +
  scale_x_reverse() +
  scale_color_brewer(type = "div", palette = 7) +
  geom_line(aes(color = status))
link_weight_plot
ggsave(
  link_weight_plot,
  height = 6, width = 9,
  filename = here::here("fig/link-weight-plot.pdf")
)

# common layout: pairwise & partial
g <- do.call(bind_edges, c(
  list(.data = tbl_graph(nodes = tibble(name = names(namcs)))),
  gs[c(13,1)] %>%
    lapply(activate, links) %>%
    lapply(as_tibble) %>%
    mapply(FUN = mutate, k = ks[c(13,1)], SIMPLIFY = FALSE)
)) %>%
  activate(nodes) %>%
  left_join(select(namcs_disorders, name = code, abbr), by = c("name"))
edge_width_range <- g %>% activate(links) %>% pull(weight) %>%
  abs() %>% range()
namcs_full_partial_plot <- g %>%
  activate(links) %>%
  ggraph(layout = "circle") +
  facet_edges(~ -k, nrow = 1) +
  theme_graph(base_family = "") +
  #ggtitle("Comorbidity network constructions") +
  scale_x_continuous(expand = expand_scale(mult = .3)) +
  scale_y_continuous(expand = expand_scale(mult = .1)) +
  #scale_edge_color_manual(values = c("black", "grey")) +
  scale_edge_color_manual(values = c("#377eb8", "#e41a1c")) +
  scale_edge_width_continuous(range = edge_width_range * 3) +
  geom_edge_link(aes(edge_width = abs(weight), edge_colour = weight < 0)) +
  geom_node_point(size = 2, shape = 21, color = "black", fill = "white") +
  geom_node_label(aes(label = abbr)) +
  guides(edge_width = FALSE, edge_colour = FALSE) +
  coord_fixed()
namcs_full_partial_plot
ggsave(
  namcs_full_partial_plot,
  height = 3, width = 6,
  filename = here::here("fig/namcs-full-partial-plot.pdf")
)

# common layout: interpolation
g <- do.call(bind_edges, c(
  list(.data = tbl_graph(nodes = tibble(name = names(namcs)))),
  gs %>%
    lapply(activate, links) %>%
    lapply(as_tibble) %>%
    mapply(FUN = mutate, k = ks, SIMPLIFY = FALSE)
)) %>%
  activate(nodes) %>%
  left_join(select(namcs_disorders, name = code, abbr), by = c("name"))
edge_width_range <- g %>% activate(links) %>% pull(weight) %>%
  abs() %>% range()
namcs_partial_plot <- g %>%
  activate(links) %>%
  ggraph(layout = "circle") +
  facet_edges(~ -k, nrow = 3) +
  theme_graph(base_family = "") +
  #ggtitle("Comorbidity network constructions") +
  scale_x_continuous(expand = expand_scale(mult = .2)) +
  #scale_y_continuous(expand = expand_scale(mult = .2)) +
  #scale_edge_color_manual(values = c("black", "grey")) +
  scale_edge_color_manual(values = c("#377eb8", "#e41a1c")) +
  scale_edge_width_continuous(range = edge_width_range * 3) +
  geom_edge_link(aes(edge_width = abs(weight), edge_colour = weight < 0)) +
  geom_node_point(size = 2, shape = 21, color = "black", fill = "white") +
  geom_node_label(aes(label = abbr)) +
  guides(edge_width = FALSE, edge_colour = FALSE) +
  coord_fixed()
namcs_partial_plot
ggsave(
  namcs_partial_plot,
  height = 10, width = 18,
  filename = here::here("fig/namcs-partial-plot.pdf")
)

# centrality
cent <- tibble(
  type = c("degree", "closeness", "betweenness"),
  fun = list(igraph::degree, igraph::closeness, igraph::betweenness)
)
for (i in 1:nrow(cent)) {
  centrality_plot <- gs %>%
    lapply(igraph::delete_edge_attr, name = "weight") %>%
    sapply(cent$fun[[i]]) %>%
    as_tibble(rownames = "disorder") %>%
    gather(key = "k", value = "centrality", V1:V13, factor_key = TRUE) %>%
    mutate(rho_cut = rev(c(r_branch, 0))[as.integer(k)]) %>%
    ggplot(aes(x = rho_cut, y = centrality, group = disorder)) +
    theme_bw() +
    xlab("Clustering cut (absolute correlation)") +
    ylab(paste0("Centrality (", cent$type[i], ")")) +
    scale_x_reverse() +
    geom_line()
  print(centrality_plot)
  ggsave(
    centrality_plot,
    height = 6, width = 9, filename = here::here(paste0(
      "fig/node-centrality-", cent$type[i], "-plot.pdf"
    ))
  )
}

# global statistics
summary_plot <- tibble(k = ks, net_w = gs) %>%
  mutate(rho_cut = rev(c(r_branch, 0))[as.integer(k)]) %>%
  mutate(net_u = lapply(net_w, igraph::delete_edge_attr, "weight")) %>%
  mutate(
    density = sapply(net_u, igraph::edge_density),
    degree_gini = sapply(lapply(net_u, igraph::degree), ineq::Gini),
    degree_assortativity = sapply(net_u, igraph::assortativity_degree),
    geodesic_mean = sapply(net_u, igraph::mean_distance),
    triad_closure = sapply(net_u, igraph::transitivity, type = "global"),
    modularity = sapply(net_u, function(g) {
      igraph::modularity(igraph::cluster_walktrap(g))
    })
  ) %>%
  gather(
    key = "stat", value = "value", density:modularity,
    factor_key = TRUE
  ) %>%
  mutate(stat = fct_recode(
    stat,
    `Link density` = "density",
    `Gini index of degree sequence` = "degree_gini",
    `Degree assortativity` = "degree_assortativity",
    `Mean internode geodesic distance` = "geodesic_mean",
    `Global triad closure` = "triad_closure",
    `Modularity of walktrap community partition` = "modularity"
  )) %>%
  ggplot(aes(x = rho_cut, y = value)) +
  theme_bw() +
  facet_wrap(~ stat, ncol = 2, scales = "free_y") +
  xlab("Clustering cut (absolute correlation)") +
  ylab("Network statistic") +
  scale_x_reverse() +
  geom_line()
summary_plot
ggsave(
  summary_plot,
  height = 5, width = 8,
  filename = here::here("fig/namcs-summary-plot.pdf")
)

## compare to interpolations via random agglomerative partitions

d <- pairwise_data(namcs)
m <- pairwise_matrix(d)

ds_dat <- tibble()
for (i in 0:100) {
  
  # random permutation matrix (permutes columns by right action)
  s <- if (i == 0) 1:nrow(m) else sample(1:nrow(m))
  p <- diag(nrow(m))[, s, drop = FALSE]
  # hierarchical clustering based on permuted distance matrix
  h <- hclust(as.dist(t(p) %*% sqrt(1 - m ^ 2) %*% p))
  # more clusters at left; larger clusters at right
  r_branch <- sqrt(1 - h$height ^ 2)
  
  # more clusters at left; larger clusters at right
  ks <- nrow(m):1
  gs <- lapply(ks, function(k) {
    # k = nrow(m) -> pairwise_network(d)
    # k = 1 -> partial_network(partial_data(d))
    partial_network(partial_data_blocks(d, cutree(h, k = k)))
  })
  ds <- lapply(ks, function(k) {
    partial_data_blocks(d, cutree(h, k = k)) %>%
      mutate(k = k)
  }) %>%
    bind_rows() %>%
    select(-n, -n1, -n2, -n12) %>%
    mutate(cut = rev(c(r_branch, 0))[as.integer(k)])
  
  # variances of stepwise differences in "rho_partial"
  ds %>%
    group_by(v1, v2) %>%
    arrange(desc(k)) %>%
    mutate(diff_rho = rho_partial - lag(rho_partial)) %>%
    drop_na() %>%
    summarize(sd_diff = sd(diff_rho)) %>%
    mutate(sim = i) %>%
    bind_rows(ds_dat) ->
    ds_dat
  
}

# plot actual standard deviations against those from random partitions
ds_dat %>%
  ggplot(aes(y = sd_diff)) +
  facet_grid(
    v1 ~ v2,
    labeller = as_labeller(label_value, multi_line = FALSE)
  ) +
  geom_boxplot() +
  geom_point(data = filter(ds_dat, sim == 0), x = 0, color = "red") ->
  random_partition_plot
ggsave(
  random_partition_plot,
  height = 14, width = 12,
  filename = here::here("fig/random-partition-plot.pdf")
)
# rankings
ds_dat %>%
  filter(sim != 0) %>%
  left_join(select(filter(ds_dat, sim == 0), v1, v2, sd_diff),
            by = c("v1", "v2"),
            suffix = c("_sim", "_orig")) %>%
  group_by(v1, v2) %>%
  summarize(rank_100 = sum(sd_diff_sim > sd_diff_orig)) %>%
  pull(rank_100) %>% hist()
# The actual variation in increments of rho with partition agglomeration is 
# higher than would be expected by chance. That is to say, the decreases in rho 
# as the clusters agglomerate are sharper, when they occur, than they would be
# if the agglomeration were done without reference to the strengths of the
# correlations.

## interpolate between pairwise and full partial via model improvement
