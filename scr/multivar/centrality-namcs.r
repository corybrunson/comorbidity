library(tidyverse)
library(tidygraph)
library(ggraph)
library(GGally)

source(here::here("R/namcs-info.r"))
source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))
source(here::here("R/joint-distribution.r"))

# function to calculate centralities on each network
calc_cents <- function(net) {
  net %>%
    activate(links) %>%
    # restrict to positive associations
    filter(weight > 0) %>%
    # introduce reciprocal weights for geodesic-based centrality measures
    mutate(distance = 1 / weight) %>%
    # calculate node centralities (syntax is awful and should be improved)
    activate(nodes) %>%
    mutate(
      degree = centrality_degree(weights = .E()$weight),
      betweenness = centrality_betweenness(weights = .E()$distance),
      closeness = centrality_closeness(weights = .E()$distance)
    ) %>%
    # extract node info
    as_tibble()
}

# NAMCS pairs data sets
readRDS(here::here("data/namcs/namcs.rds")) %>%
  select_at(vars(as.character(namcs_disorders$code))) %>%
  pairwise_data(z = z_cutoff) %>%
  partial_data() %>%
  print() -> namcs_pt
readRDS(here::here("data/namcs/namcs-jd0-rho.rds")) %>%
  rename_all(~ str_replace(., "rho", "rho_joint")) %>%
  print() -> namcs_jd0
readRDS(here::here("data/namcs/namcs-jd1-rho.rds")) %>%
  rename_all(~ str_replace(., "rho", "rho_joint")) %>%
  print() -> namcs_jd1

# construct networks from pairs data (with automatic cutoffs)
namcs_pwn <- pairwise_network(namcs_pt)
namcs_ptn <- partial_network(namcs_pt)
namcs_jdn0 <- jdm_network(namcs_jd0)
namcs_jdn1 <- jdm_network(namcs_jd1)

# tibble of centralities
bind_rows(
  mutate(calc_cents(namcs_pwn), model = "Pairwise"),
  mutate(calc_cents(namcs_ptn), model = "Partial"),
  mutate(calc_cents(namcs_jdn0), model = "JIDM0"),
  mutate(calc_cents(namcs_jdn1), model = "JIDM1")
) %>%
  mutate(model = fct_inorder(model)) %>%
  left_join(select(namcs_disorders, code, abbr), by = c("name" = "code")) %>%
  print() -> namcs_centralities

# centrality distributions
namcs_centralities %>%
  ggplot(aes(x = betweenness)) +
  theme_bw() +
  geom_histogram(bins = 12) +
  facet_wrap(~ model, nrow = 2, ncol = 2) +
  labs(x = "Betweenness centrality", y = "")

# centrality scatterplots
namcs_centralities %>%
  select(name = abbr, betweenness, model) %>%
  spread(model, betweenness) %>%
  ggpairs(shape = "16", columns = 2:5) +
  theme_bw() ->
  namcs_centralities_pairs
ggsave(
  here::here("fig/namcs-centralities-pairs.pdf"), namcs_centralities_pairs,
  height = 6, width = 6
)
