# setup
library(tidyverse)
library(tidygraph)
library(ggraph)
library(ordr)
library(GGally)
source(here::here("R/namcs-info.r"))
source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))
source(here::here("R/joint-distribution.r"))

# fixed evidential threshold, three models

# NAMCS datasets
namcs_exo <- readRDS(here::here("data/namcs/namcs.rds"))
namcs <- select_at(namcs_exo, vars(as.character(namcs_disorders$code)))
#namcs_jd <- readRDS(here::here("data/namcs/namcs-jd.rds"))
#namcs_jd0 <- readRDS(here::here("data/namcs/namcs-jd0-rho.rds"))
#namcs_jd1 <- readRDS(here::here("data/namcs/namcs-jd1-rho.rds"))
readRDS(here::here("data/namcs/namcs-jd0-rho.rds")) %>%
  rename_all(~ str_replace(., "rho", "rho_joint")) ->
  namcs_jd0
readRDS(here::here("data/namcs/namcs-jd1-rho.rds")) %>%
  rename_all(~ str_replace(., "rho", "rho_joint")) ->
  namcs_jd1
# construct matrices and graphs
namcs_pw <- pairwise_data(namcs, z = z_cutoff)
namcs_pwm <- pairwise_matrix(namcs_pw)
namcs_pwn <- pairwise_network(namcs_pw)
namcs_pt <- partial_data(namcs_pw)
namcs_ptm <- partial_matrix(namcs_pt)
namcs_ptn <- partial_network(namcs_pt)
# from joint distribution data
namcs_jdm0 <- jdm_matrix(namcs_jd0)
namcs_jdm1 <- jdm_matrix(namcs_jd1)
namcs_jdn0 <- jdm_network(namcs_jd0)
namcs_jdn1 <- jdm_network(namcs_jd1)

# combined dataset
namcs_pt %>%
  full_join(namcs_jd0, by = c("v1", "v2")) %>%
  full_join(namcs_jd1, by = c("v1", "v2"), suffix = c("_0", "_1")) %>%
  print() -> namcs_data

# compare association strengths among endogenous models

# scatterplots
scatterplots <- list(
  ggplot(namcs_data, aes(
    x = rho,# xmin = rho_lower, xmax = rho_upper,
    y = rho_partial#, ymin = rho_partial_lower, ymax = rho_partial_upper
  )) +
    xlab(expression(italic(r)[italic(t)])) +
    ylab(expression(italic(r)*minute[italic(t)])),
  ggplot(namcs_data, aes(
    x = rho,# xmin = rho_lower, xmax = rho_upper,
    y = rho_joint_0#, ymin = rho_joint_lower_0, ymax = rho_joint_upper_0
  )) +
    xlab(expression(italic(r)[italic(t)])) +
    ylab(expression(hat(P))),
  ggplot(namcs_data, aes(
    x = rho_partial,# xmin = rho_partial_lower, xmax = rho_partial_upper,
    y = rho_joint_0#, ymin = rho_joint_lower_0, ymax = rho_joint_upper_0
  )) +
    xlab(expression(italic(r)*minute[italic(t)])) +
    ylab(expression(hat(P)))
) %>%
  lapply(function(gg) {
    gg +
      geom_point(aes(size = n1 * n2), alpha = .25, shape = 16) +
      #geom_errorbar(alpha = .5) + geom_errorbarh(alpha = .5) +
      scale_x_continuous(breaks = seq(-1, 1, .2),
                         minor_breaks = seq(-1, 1, .1)) +
      scale_y_continuous(breaks = seq(-1, 1, .2),
                         minor_breaks = seq(-1, 1, .1)) +
      #guides(size = guide_legend(
      #  title = expression(italic(n)[1]*italic(n)[2])
      #)) +
      guides(size = FALSE) +
      #coord_fixed() +
      theme_bw() +
      theme(legend.position = "bottom")
  })
grob_scatterplots <- gridExtra::arrangeGrob(
  grobs = scatterplots[c(2, 3, 1)], nrow = 2, ncol = 2,
  #widths = c(11, 9),
  respect = FALSE
)
plot(grob_scatterplots)
ggsave(
  height = 8, width = 9, filename = here::here("fig/namcs-scatterplots.pdf"),
  grob_scatterplots
)

# pairs plot
namcs_data %>%
  select(rho, rho_partial, rho_joint_0, rho_joint_1, everything()) %>%
  ggpairs(aes(size = n1 * n2, alpha = .25, shape = "16"), columns = 1:4) +
  theme_bw() ->
  grob_pairs
ggsave(
  height = 8, width = 9, filename = here::here("fig/namcs-pairs.pdf"),
  grob_pairs
)

# linear relationships between estimates
(lm_pw_pt <- tidy(lm(rho_partial ~ 0 + rho, namcs_data)))
plot(namcs_data[, c("rho", "rho_partial")], pch = 19, cex = .5)
lines(namcs_data$rho, namcs_data$rho * lm_pw_pt$estimate)
(lm_pw_pt <- tidy(lm(rho_partial ~ 1 + offset(rho), namcs_data)))
plot(namcs_data[, c("rho", "rho_partial")], pch = 19, cex = .5)
lines(namcs_data$rho, namcs_data$rho + lm_pw_pt$estimate)
(lm_pw_jd <- tidy(lm(rho_joint_0 ~ 1 + offset(rho), namcs_data)))
plot(namcs_data[, c("rho", "rho_joint_0")], pch = 19, cex = .5)
lines(namcs_data$rho, namcs_data$rho + lm_pw_jd$estimate)
(lm_pt_jd <- tidy(lm(rho_joint_0 ~ 0 + rho_partial, namcs_data)))
plot(namcs_data[, c("rho_partial", "rho_joint_0")], pch = 19, cex = .5)
lines(namcs_data$rho_partial, namcs_data$rho_partial * lm_pt_jd$estimate)

# eigendecompositions and correlation biplots
corr_biplot <- function(mat, title) {
  mat %>%
    eigen_ord() %>%
    as_tbl_ord() %>%
    confer_inertia(c(1, 0)) %>%
    negate_to_nonneg_orthant("u") %>%
    augment() %>%
    mutate_u(
      disorder = namcs_disorders$abbr[match(.name, namcs_disorders$code)]
    ) %>%
    ggbiplot() +
    theme_bw() +
    scale_x_continuous(limits = c(-1, 1)) +
    scale_y_continuous(limits = c(-1, 1)) +
    geom_unit_circle() +
    geom_u_vector() +
    geom_u_text_radiate(aes(label = disorder)) +
    ggtitle(title)
}
namcs_mats <- list(
  Pairwise = namcs_pwm, Partial = namcs_ptm,
  JIDM0 = namcs_jdm0, JIDM1 = namcs_jdm1
)
grob_biplots <-
  map2(namcs_mats, names(namcs_mats), ~ corr_biplot(.x, .y)) %>%
  gridExtra::arrangeGrob(grobs = ., nrow = 2, ncol = 2, respect = FALSE)
plot(grob_biplots)
ggsave(
  height = 10, width = 10,
  filename = here::here("fig/namcs-biplots.pdf"),
  grob_biplots
)

# compare four networks in circular layout
bind_rows(
  select(namcs_data, code = v1, n = n1),
  select(namcs_data, code = v2, n = n2)
) %>%
  distinct() %>%
  mutate(t = (1:nrow(namcs_disorders) - 1) / nrow(namcs_disorders)) %>%
  mutate(t = 2 * pi * t) %>%
  mutate(x = cos(t), y = sin(t)) %>%
  left_join(namcs_disorders, by = "code") %>%
  select(-name) %>% rename(name = code) %>%
  print() -> node_data
node_data %>%
  select(x, y) %>%
  print() -> layout_circle
bind_rows(
  select(namcs_data, from = v1, to = v2, n = n12, pval),
  select(namcs_data, from = v2, to = v1, n = n12, pval)
) %>%
  mutate_at(vars(from, to), as.integer) %>%
  print() -> link_data
plot_circle <- function(net, title) {
  net %>%
    activate(nodes) %>%
    left_join(node_data, by = c("name")) %>%
    activate(links) %>%
    left_join(link_data, by = c("from", "to")) %>%
    rename(rho = weight) %>%
    mutate(sign = factor(
      as.character(as.integer(rho / abs(rho))),
      levels = c("-1", "1")
    )) %>%
    #ggraph("manual", node.positions = layout_circle) +
    ggraph("manual", x = layout_circle$x, y = layout_circle$y) +
    theme_graph(base_family = "") +
    theme(plot.margin = margin(0, 0, 0, 0, "pt")) +
    coord_equal() +
    geom_edge_link(aes(width = n, alpha = rho, linetype = sign)) +
    #geom_edge_link(aes(width = rho, alpha = log(1 - pval), color = sign)) +
    scale_edge_width_continuous(range = c(.5, 4)) +
    scale_edge_linetype_manual(values = c("dashed", "solid")) +
    geom_node_point(aes(size = n)) +
    geom_node_label(
      aes(label = abbr, hjust = "outward", vjust = "outward"),
      alpha = .5
    ) +
    guides(
      size = "none", edge_width = "none",
      edge_alpha = "none", edge_color = "none", edge_linetype = "none"
    ) +
    scale_x_continuous(expand = expand_scale(mult = c(.3, .3))) +
    scale_y_continuous(expand = expand_scale(mult = c(.1, .1))) +
    ggtitle(title)
}
namcs_nets <- list(
  Pairwise = namcs_pwn, Partial = namcs_ptn,
  JIDM0 = namcs_jdn0, JIDM1 = namcs_jdn1
)
grob_circles <-
  map2(namcs_nets, names(namcs_nets), ~ plot_circle(.x, .y)) %>%
  gridExtra::arrangeGrob(grobs = ., nrow = 2, ncol = 2, respect = FALSE)
plot(grob_circles)
ggsave(
  height = 10, width = 10,
  filename = here::here("fig/namcs-networks.pdf"),
  grob_circles
)
