library(tidyverse)

# JDM specs
source(here::here("R/joint-settings.r"))
source(here::here("R/joint-distribution.r"))

readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
  print() -> mimic

n_iter <- 1000

# computationally feasible settings
# (proportion of sample to include and minimum incidence to include a disorder)
for (prop_samp in c(1, 0.5, 0.2)) for (min_inc in c(1000, 100, 10, 1)) {
  
  for (careunit in unique(mimic$curr_careunit)) {
    
    print(str_c(
      "proportion of cases = ", prop_samp,
      "; minimum occurrences = ", min_inc,
      "; care unit = ", careunit
    ))
    
    # presence-absence data
    mimic %>%
      filter(curr_careunit == careunit) %>%
      select(-curr_careunit) %>%
      #distinct() %>%
      mutate(one = 1L) %>%
      spread(key = ccs_code_single, value = one, fill = 0L) %>%
      select(-subject_id, -hadm_id) %>%
      select_at(vars(names(.)[order(as.numeric(names(.)))])) ->
      x
    
    # check rank
    print(str_c("dimensions: ", str_c(dim(as.matrix(x)), collapse = " * ")))
    print(str_c("rank: ", qr(as.matrix(x))$rank))
    
    set.seed(0)
    # restrict dataset
    x %>%
      # restrict to variables with minimum prevalence
      select_if(list(~sum(.) >= min_inc)) %>%
      # remove cases with no occurrences
      filter_all(any_vars(. == 1)) %>%
      # stratified subset
      sample_frac(prop_samp) %>%
      print() -> x_sub
    
    # fit and save joint distribution model
    x_jd <- try(jdm_data(y = x_sub, z = z_cutoff, n.iter = n_iter, DIC = TRUE))
    
    # save results
    if (! inherits(x_jd, "try-error")) {
      datafile <- str_c(
        "mimic-", tolower(careunit),
        "-freq", min_inc, "-prop", prop_samp, "-iter", n_iter, "-jd"
      ) %>%
        str_replace_all("\\.", "_") %>%
        str_c(".rds")
      saveRDS(x_jd, here::here("data/mimic", datafile))
    }
    
  }
  
}

stop()

# fit JDM manually

# parameter settings
z = z_cutoff
df_extra <- 1
n.chains <- n_chains
n.iter <- n_iter
n.burnin <- floor(n.iter / 2)
n.thin <- max(1, floor((n.iter - n.burnin) / 1000))
DIC = TRUE

# restrict to subjects with occurrences
w <- which(rowSums(x) > 0)
stopifnot(ncol(x) > 1 & length(w) > 1)
Y <- as.matrix(x)[w, ]
n <- nrow(Y); m <- ncol(Y)
# subject-level intercepts (occurrence rates)
X <- matrix(1, nrow = n, ncol = 1)
ID <- diag(m)
df <- m + df_extra

# initialization function
inits <- function() {
  Tau <- MCMCpack::rwish(df, ID)
  Sigma <- solve(Tau)
  Z <- abs(t(replicate(n, MASS::mvrnorm(1, rep(0, m), Sigma))))
  Z <- ifelse(as.matrix(Y), Z, -1 * Z)
  Sigma <- mclust::mvnXXX(Z)$parameters$variance$sigma[, , 1]
  Beta <- as.matrix(sapply(seq_len(ncol(Y)), function(j) {
    unname(coef(glm(
      Y[, j] ~ 1,
      family = binomial(link = probit)
    )))
  }))
  Beta.raw <- Beta * sqrt(diag(Sigma))
  mu.raw <- apply(Beta.raw, 2, mean)
  sigma.raw <- apply(Beta.raw, 2, sd)    
  return(list(
    Tau = solve(Sigma),
    Z = Z,
    Beta.raw = Beta.raw,
    mu.raw = mu.raw,
    sigma.raw = sigma.raw
  ))
}

# call JAGS
fits <- R2jags::jags(
  data = list(Y = Y, X = X, n = n, m = m, ID = ID, df = df),
  inits,
  parameters.to.save = c("Beta.raw", "Tau"),
  model.file = here::here("src", "jdm.txt"),
  n.chains = n.chains, n.iter = n.iter, n.burnin = n.burnin,
  n.thin = n.thin,
  DIC = DIC
)

# attach model fits
R2jags::attach.jags(fits, overwrite = TRUE)
# covariance matrices
Sigma2 <- apply(Tau, 1, solve)
dim(Sigma2) <- rev(dim(Tau))
Sigma2 <- aperm(Sigma2, c(3, 2, 1))
# correlation matrices
Rho <- apply(Sigma2, 1, cov2cor)
dim(Rho) <- rev(dim(Sigma2))
Rho <- aperm(Rho, c(3, 2, 1))
# normalized coefficients
dim(Beta.raw) <- c(dim(Beta.raw)[1], dim(Beta.raw)[2], 1)
Beta <- apply(Beta.raw, 3, function(x) x / t(sqrt(apply(Sigma2, 1, diag))))
dim(Beta) <- dim(Beta.raw)
if (FALSE) {
  # mean occurrence rates
  Mu <- array(dim = c(n.sims, n, m))
  for (sims in seq_len(n.sims)) {
    for (sites in seq_len(n)) {
      for (species in seq_len(m)) {
        Mu[sims, sites, species] <- Beta.raw[sims, species, ] %*% X[sites, ] 
      }
    }
  }
}
# detach model fits
R2jags::detach.jags()

# posterior Rho data
vars <- if (is.null(names(x))) 1:ncol(x) else fct_inorder(names(x))
alpha <- (1 - pnorm(z)) * 2
upper_tri <- upper.tri(ID)
Rho_dat <- tibble(
  v1 = vars[(which(upper_tri) - 1) %% m + 1],
  v2 = vars[rep(2:m, times = 2:m - 1)],
  rho = apply(Rho, 2:3, mean)[upper_tri],
  rho_sd = apply(Rho, 2:3, sd)[upper_tri],
  rho_lower = apply(Rho, 2:3, function(x) {
    quantile(x, probs = alpha / 2)
  })[upper_tri],
  rho_upper = apply(Rho, 2:3, function(x) {
    quantile(x, probs = 1 - alpha / 2)
  })[upper_tri]
)
Beta_dat <- tibble(
  var = vars,
  beta = apply(Beta, 2:3, mean)[, , drop = TRUE],
  beta_sd = apply(Beta, 2:3, sd)[, , drop = TRUE],
  beta_lower = apply(Beta, 2:3, function(x) {
    quantile(x, probs = alpha / 2)
  })[, , drop = TRUE],
  beta_upper = apply(Beta, 2:3, function(x) {
    quantile(x, probs = 1 - alpha / 2)
  })[, , drop = TRUE]
)

Rho_dat %>%
  left_join(Beta_dat, by = c("v1" = "var")) %>%
  left_join(Beta_dat, by = c("v2" = "var"), suffix = c("_1", "_2")) %>%
  rename_at(
    vars(starts_with("rho")),
    list(~gsub("^rho", "rho_joint", .))
  ) ->
  mimic_jd

#mimic_jd <- jdm_data(mimic_submat, z = z_cutoff, n.iter = n_iter, DIC = TRUE)
saveRDS(mimic_jd, here::here(paste0(
  "data/mimic-freq", min_inc, "-prop", prop_samp, "-iter", n_iter, "-jd.rds"
)))
