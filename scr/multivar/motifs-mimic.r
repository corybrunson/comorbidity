library(tidyverse)
library(tidygraph)

source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))
source(here::here("R/joint-distribution.r"))

# care units
readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
  pull(curr_careunit) %>%
  unique() %>%
  print() -> careunits

# load CCS definitions
ccs_dat <- readRDS(here::here("data/ccs-dat.rds"))
disorder_ccs <- distinct(select(
  filter(ccs_dat, icd9_type == "DX"),
  code = ccs_code_single, disorder = ccs_category_single
))

# smallest variability in correlation estimates for each sign combination

mimic_low_pairs <- tibble()
# fixed evidential threshold, three models
for (careunit in careunits) {
  
  print(str_c("Care unit: ", careunit))
  
  # pairwise and partial correlation data
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  unit_pt <- readRDS(here::here("data/mimic", ptfile))
  
  # best available joint distribution model data
  
  # joint distribution model files
  jdfile <- list.files(
    here::here("data/mimic"),
    str_c("mimic-", tolower(careunit), "-.+-jd.rds")
  )
  # best available proportion of sample
  jdprops <- jdfile %>%
    str_replace("^.+-prop([0-9\\_]+)-.+-jd\\.rds", "\\1") %>%
    str_replace("\\_", ".") %>%
    as.numeric()
  jdprop <- max(jdprops)
  print(str_c("Proportion of ", careunit, " population in JIDM: ", jdprop))
  jdfile <- jdfile[which(jdprops == max(jdprops))]
  # among remaining files, best available minimum incidence
  jdfreqs <- jdfile %>%
    str_replace("^.+-freq([0-9]+)-.+-jd\\.rds", "\\1") %>%
    as.numeric()
  jdfreq <- min(jdfreqs)
  print(str_c("Minimum incidence: ", jdfreq))
  jdfile <- jdfile[which(jdfreqs == min(jdfreqs))]
  # read file
  unit_jd <- readRDS(here::here("data/mimic", jdfile[1]))
  
  # combined dataset with rho values
  full_join(unit_pt, unit_jd, by = c("v1", "v2")) %>%
    select(-pval, -ends_with("_Z")) %>%
    select(-rho_joint_sd, -starts_with("beta")) %>%
    print() -> unit_data
  
  # combined dataset ignoring indiscernible estimates
  unit_data %>%
    mutate(rho = ifelse(rho_lower <= 0 & rho_upper >= 0, NA_real_, rho)) %>%
    mutate(rho_partial = ifelse(
      rho_partial_lower <= 0 & rho_partial_upper >= 0,
      NA_real_, rho_partial
    )) %>%
    mutate(rho_joint = ifelse(
      rho_joint_lower <= 0 & rho_joint_upper >= 0,
      NA_real_, rho_joint
    )) %>%
    select(-ends_with("lower"), -ends_with("upper")) %>%
    identity() -> unit_data
  
  # total number of pairs
  n_pairs <- choose(length(unique(unlist(select(unit_data, v1, v2)))), 2)
  if (n_pairs != nrow(unit_data)) {
    warning("Data set for care unit ", careunit, " is missing some pairs.")
  }
  
  # group by sign combination and take low variances
  unit_data %>%
    filter_at(vars(starts_with("rho")), ~ ! is.na(.)) %>%
    mutate_at(vars(starts_with("rho")), list(sign = ~ . / abs(.))) %>%
    rowwise() %>%
    mutate(rho_est_var = var(c(rho, rho_partial, rho_joint))) %>%
    ungroup() %>% group_by_at(vars(ends_with("sign"))) %>%
    arrange(rho_est_var) %>%
    top_n(1, rho_est_var) %>%
    ungroup() %>%
    arrange(desc(rho_sign), desc(rho_partial_sign), desc(rho_joint_sign)) %>%
    select(-ends_with("sign")) %>%
    rename(Pairwise = rho, Partial = rho_partial, JIDM = rho_joint) %>%
    rename(BtwVar = rho_est_var) %>%
    mutate(Unit = careunit) %>% select(Unit, everything()) -> unit_low_pairs
  
  mimic_low_pairs <- bind_rows(mimic_low_pairs, unit_low_pairs)
}
mimic_low_pairs <- mutate(mimic_low_pairs, Unit = factor(Unit, careunits))

# add CCS definitions to high-variance pairs data
mimic_low_pairs %>%
  left_join(disorder_ccs, by = c("v1" = "code")) %>%
  rename(disorder1 = disorder) %>%
  left_join(disorder_ccs, by = c("v2" = "code")) %>%
  rename(disorder2 = disorder) %>%
  arrange(Unit, v1, v2) %>%
  unite("Disorder1", v1, disorder1, sep = " ") %>%
  unite("Disorder2", v2, disorder2, sep = " ") %>%
  select(
    Unit, Disorder1, Disorder2, n1, n2, n12,
    Pairwise, Partial, JIDM, BtwVar
  ) %>%
  mutate_at(
    vars(Pairwise, Partial, JIDM, BtwVar),
    ~ format(., digits = 3, nsmall = 3, scientific = FALSE)
  ) %>%
  print() %>%
  write_csv(here::here("tab/var-min-pairs-mimic.csv"))

# largest variability in correlation estimates for each sign combination

mimic_top_pairs <- tibble()
# fixed evidential threshold, three models
for (careunit in careunits) {
  
  print(str_c("Care unit: ", careunit))
  
  # pairwise and partial correlation data
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  unit_pt <- readRDS(here::here("data/mimic", ptfile))
  
  # best available joint distribution model data
  
  # joint distribution model files
  jdfile <- list.files(
    here::here("data/mimic"),
    str_c("mimic-", tolower(careunit), "-.+-jd.rds")
  )
  # best available proportion of sample
  jdprops <- jdfile %>%
    str_replace("^.+-prop([0-9\\_]+)-.+-jd\\.rds", "\\1") %>%
    str_replace("\\_", ".") %>%
    as.numeric()
  jdprop <- max(jdprops)
  print(str_c("Proportion of ", careunit, " population in JIDM: ", jdprop))
  jdfile <- jdfile[which(jdprops == max(jdprops))]
  # among remaining files, best available minimum incidence
  jdfreqs <- jdfile %>%
    str_replace("^.+-freq([0-9]+)-.+-jd\\.rds", "\\1") %>%
    as.numeric()
  jdfreq <- min(jdfreqs)
  print(str_c("Minimum incidence: ", jdfreq))
  jdfile <- jdfile[which(jdfreqs == min(jdfreqs))]
  # read file
  unit_jd <- readRDS(here::here("data/mimic", jdfile[1]))
  
  # combined dataset with rho values
  full_join(unit_pt, unit_jd, by = c("v1", "v2")) %>%
    select(-pval, -ends_with("_Z")) %>%
    select(-rho_joint_sd, -starts_with("beta")) %>%
    print() -> unit_data
  
  # combined dataset ignoring indiscernible estimates
  unit_data %>%
    mutate(rho = ifelse(rho_lower <= 0 & rho_upper >= 0, NA_real_, rho)) %>%
    mutate(rho_partial = ifelse(
      rho_partial_lower <= 0 & rho_partial_upper >= 0,
      NA_real_, rho_partial
    )) %>%
    mutate(rho_joint = ifelse(
      rho_joint_lower <= 0 & rho_joint_upper >= 0,
      NA_real_, rho_joint
    )) %>%
    select(-ends_with("lower"), -ends_with("upper")) %>%
    identity() -> unit_data
  
  # total number of pairs
  n_pairs <- choose(length(unique(unlist(select(unit_data, v1, v2)))), 2)
  if (n_pairs != nrow(unit_data)) {
    warning("Data set for care unit ", careunit, " is missing some pairs.")
  }
  
  # group by sign combination and take top variances
  unit_data %>%
    filter_at(vars(starts_with("rho")), ~ ! is.na(.)) %>%
    mutate_at(vars(starts_with("rho")), list(sign = ~ . / abs(.))) %>%
    rowwise() %>%
    mutate(rho_est_var = var(c(rho, rho_partial, rho_joint))) %>%
    ungroup() %>% group_by_at(vars(ends_with("sign"))) %>%
    arrange(desc(rho_est_var)) %>%
    top_n(1, rho_est_var) %>%
    ungroup() %>%
    arrange(desc(rho_sign), desc(rho_partial_sign), desc(rho_joint_sign)) %>%
    select(-ends_with("sign")) %>%
    rename(Pairwise = rho, Partial = rho_partial, JIDM = rho_joint) %>%
    rename(BtwVar = rho_est_var) %>%
    mutate(Unit = careunit) %>% select(Unit, everything()) -> unit_top_pairs
  
  mimic_top_pairs <- bind_rows(mimic_top_pairs, unit_top_pairs)
}
mimic_top_pairs <- mutate(mimic_top_pairs, Unit = factor(Unit, careunits))

# add CCS definitions to high-variance pairs data
mimic_top_pairs %>%
  left_join(disorder_ccs, by = c("v1" = "code")) %>%
  rename(Disorder1 = disorder) %>%
  left_join(disorder_ccs, by = c("v2" = "code")) %>%
  rename(Disorder2 = disorder) %>%
  arrange(Unit, v1, v2) %>%
  select(
    Unit, Disorder1, Disorder2, n1, n2, n12,
    Pairwise, Partial, JIDM, BtwVar
  )

# estimates for each pair in any unit across other units

mimic_top_pairs %>%
  select(v1, v2) %>%
  distinct() %>%
  arrange(as.integer(v1), as.integer(v2)) %>%
  print() -> top_pairs

mimic_pairs <- tibble()
# fixed evidential threshold, three models
for (careunit in careunits) {
  
  print(str_c("Care unit: ", careunit))
  
  # pairwise and partial correlation data
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  unit_pt <- readRDS(here::here("data/mimic", ptfile))
  
  # best available joint distribution model data
  
  # joint distribution model files
  jdfile <- list.files(
    here::here("data/mimic"),
    str_c("mimic-", tolower(careunit), "-.+-jd.rds")
  )
  # best available proportion of sample
  jdprops <- jdfile %>%
    str_replace("^.+-prop([0-9\\_]+)-.+-jd\\.rds", "\\1") %>%
    str_replace("\\_", ".") %>%
    as.numeric()
  jdprop <- max(jdprops)
  print(str_c("Proportion of ", careunit, " population in JIDM: ", jdprop))
  jdfile <- jdfile[which(jdprops == max(jdprops))]
  # among remaining files, best available minimum incidence
  jdfreqs <- jdfile %>%
    str_replace("^.+-freq([0-9]+)-.+-jd\\.rds", "\\1") %>%
    as.numeric()
  jdfreq <- min(jdfreqs)
  print(str_c("Minimum incidence: ", jdfreq))
  jdfile <- jdfile[which(jdfreqs == min(jdfreqs))]
  # read file
  unit_jd <- readRDS(here::here("data/mimic", jdfile[1]))
  
  # combined dataset with rho values
  full_join(unit_pt, unit_jd, by = c("v1", "v2")) %>%
    select(-pval, -ends_with("_Z")) %>%
    select(-rho_joint_sd, -starts_with("beta")) %>%
    print() -> unit_data
  
  # combined dataset ignoring indiscernible estimates
  unit_data %>%
    mutate(rho = ifelse(rho_lower <= 0 & rho_upper >= 0, NA_real_, rho)) %>%
    mutate(rho_partial = ifelse(
      rho_partial_lower <= 0 & rho_partial_upper >= 0,
      NA_real_, rho_partial
    )) %>%
    mutate(rho_joint = ifelse(
      rho_joint_lower <= 0 & rho_joint_upper >= 0,
      NA_real_, rho_joint
    )) %>%
    select(-ends_with("lower"), -ends_with("upper")) %>%
    identity() -> unit_data
  
  # total number of pairs
  n_pairs <- choose(length(unique(unlist(select(unit_data, v1, v2)))), 2)
  if (n_pairs != nrow(unit_data)) {
    warning("Data set for care unit ", careunit, " is missing some pairs.")
  }
  
  # group by sign combination and take top variances
  unit_data %>%
    filter_at(vars(starts_with("rho")), ~ ! is.na(.)) %>%
    rowwise() %>%
    mutate(rho_est_var = var(c(rho, rho_partial, rho_joint))) %>%
    ungroup() %>%
    rename(Pairwise = rho, Partial = rho_partial, JIDM = rho_joint) %>%
    rename(BtwVar = rho_est_var) %>%
    mutate(Unit = careunit) %>% select(Unit, everything()) %>%
    semi_join(top_pairs, by = c("v1", "v2")) -> unit_pairs
  
  mimic_pairs <- bind_rows(mimic_pairs, unit_pairs)
}
mimic_pairs <- mutate(mimic_pairs, Unit = factor(Unit, careunits))

# add CCS definitions and arrange by disorder pair
mimic_pairs %>%
  left_join(disorder_ccs, by = c("v1" = "code")) %>%
  rename(disorder1 = disorder) %>%
  left_join(disorder_ccs, by = c("v2" = "code")) %>%
  rename(disorder2 = disorder) %>%
  arrange(v1, v2) %>%
  unite("Disorder1", v1, disorder1, sep = " ") %>%
  unite("Disorder2", v2, disorder2, sep = " ") %>%
  mutate_at(
    vars(Pairwise, Partial, JIDM, BtwVar),
    ~ format(., digits = 3, nsmall = 3, scientific = FALSE)
  ) %>%
  select(
    Unit, Disorder1, Disorder2, n1, n2, n12,
    Pairwise, Partial, JIDM, BtwVar
  ) %>%
  print() %>%
  write_csv(here::here("tab/var-max-pairs-mimic.csv"))
