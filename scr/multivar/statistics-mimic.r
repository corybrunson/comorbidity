library(tidyverse)
library(igraph)
library(tidygraph)
library(ggraph)
library(ordr)

source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))
source(here::here("R/joint-distribution.r"))

# care units
readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
  pull(curr_careunit) %>%
  unique() %>%
  #setdiff(c("NWARD", "NICU")) %>%
  print() -> careunits

# load CCS definitions
ccs_dat <- readRDS(here::here("data/ccs-dat.rds"))
disorder_ccs <- distinct(select(
  filter(ccs_dat, icd9_type == "DX"),
  code = ccs_code_single, disorder = ccs_category_single
))

stop("Networks have many negative links; not comparable to pairwise results.")

# function to calculate network statistics
calc_stats <- function(g) {
  
  # distance sequence
  l <- distances(g)
  # degree sequence
  s <- degree(g)[degree(g) > 0]
  
  # fit log-normal family
  f <- poweRlaw::dislnorm$new(s)
  f$setXmin(poweRlaw::estimate_xmin(f))
  f$setPars(poweRlaw::estimate_pars(f))
  
  # calculate network statistics
  tibble(
    density = edge_density(g),
    connected_prop = vcount(largest_component(g)) / vcount(g),
    degree_mean = mean(degree(g)),
    degree_gini = ineq::Gini(degree(g)),
    degree_param1 = f$pars[1],
    degree_param2 = f$pars[2],
    degree_assortativity = assortativity_degree(g),
    triad_closure = transitivity(g, type = "global"),
    distance_mean = mean(l[!is.infinite(l) & l != 0]),
    modularity_wt = modularity(cluster_walktrap(largest_component(g)))
  )
}

# initialize data frame
mimic_stats <- tibble()
# calculate statistics
for (careunit in careunits) {
  
  print(str_c("Care unit: ", careunit))
  
  # pairwise and partial correlation data
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  unit_pt <- readRDS(here::here("data/mimic", ptfile))
  
  # best available joint distribution model data
  
  # joint distribution model files
  jdfile <- list.files(
    here::here("data/mimic"),
    str_c("mimic-", tolower(careunit), "-.+-jd.rds")
  )
  # best available proportion of sample
  jdprops <- jdfile %>%
    str_replace("^.+-prop([0-9\\_]+)-.+-jd\\.rds", "\\1") %>%
    str_replace("\\_", ".") %>%
    as.numeric()
  jdprop <- max(jdprops)
  print(str_c("Proportion of ", careunit, " population in JDM: ", jdprop))
  jdfile <- jdfile[which(jdprops == max(jdprops))]
  # among remaining files, best available minimum incidence
  jdfreqs <- jdfile %>%
    str_replace("^.+-freq([0-9]+)-.+-jd\\.rds", "\\1") %>%
    as.numeric()
  jdfreq <- min(jdfreqs)
  print(str_c("Minimum incidence: ", jdfreq))
  jdfile <- jdfile[which(jdfreqs == min(jdfreqs))]
  # read file
  unit_jd <- readRDS(here::here("data/mimic", jdfile[1]))
  
  # construct networks from pairs data (with automatic cutoffs)
  unit_pwn <- pairwise_network(unit_pt)
  unit_ptn <- partial_network(unit_pt)
  unit_jdn <- jdm_network(unit_jd)
  
  
  
  # augment data frame
  mimic_stats <- rbind(mimic_stats, unit_stats)
}
