library(tidyverse)

source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))

readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
  print() -> mimic

for (careunit in unique(mimic$curr_careunit)) {
  print(str_c("Care unit: ", careunit))
  
  # co-occurrence matrix
  mimic %>%
    filter(curr_careunit == careunit) %>%
    select(-curr_careunit) %>%
    #distinct() %>%
    mutate(one = 1L) %>%
    spread(key = ccs_code_single, value = one, fill = 0L) %>%
    select(-subject_id, -hadm_id) %>%
    select_at(vars(names(.)[order(as.numeric(names(.)))])) %>%
    print() -> mimic_x
  
  # pairwise correlation data
  pwfile <- str_c("mimic-", tolower(careunit), "-pw.rds")
  mimic_pw <- pairwise_data(mimic_x, z = z_cutoff)
  saveRDS(mimic_pw, file = here::here("data/mimic", pwfile))
  
  # partial correlation data
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  mimic_pt <- partial_data(mimic_pw, use.shrinkage = TRUE)
  saveRDS(mimic_pt, file = here::here("data/mimic", ptfile))
  
}
