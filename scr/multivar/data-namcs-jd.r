# setup
rm(list = ls())
library(tidyverse)
stopifnot(all(c("R2jags", "MCMCpack") %in% rownames(installed.packages())))
source(here::here("R/namcs-info.r"))
source(here::here("R/joint-settings.r"))
source(here::here("R/joint-distribution.r"))

namcs <- readRDS(here::here("data/namcs/namcs.rds"))
namcs %>%
  select_at(vars(as.character(namcs_disorders$code))) %>%
  print() -> namcs_endo
namcs %>%
  select_at(vars(as.character(namcs_demographics$name))) %>%
  print() -> namcs_exo

# 1% sample
s <- sample(nrow(namcs), nrow(namcs) * .01)
# only endogenous variables
namcs_jd0 <- jdm_data(
  y = namcs_endo[s, ],
  z = z_cutoff,
  n.chains = n_chains, n.iter = n_iter, n.burnin = n_burnin,
  DIC = TRUE
)
write_rds(namcs_jd0$rho, here::here("data/namcs/namcs-jd0-1pct-rho.rds"))
write_rds(namcs_jd0$beta, here::here("data/namcs/namcs-jd0-1pct-beta.rds"))
write_rds(namcs_jd0$mu, here::here("data/namcs/namcs-jd0-1pct-mu.rds"))
# with exogenous variables
namcs_jd1 <- jdm_data(
  y = namcs_endo[s, ], x = namcs_exo[s, ],
  z = z_cutoff,
  n.chains = n_chains, n.iter = n_iter, n.burnin = n_burnin,
  DIC = TRUE
)
write_rds(namcs_jd1$rho, here::here("data/namcs/namcs-jd1-1pct-rho.rds"))
write_rds(namcs_jd1$beta, here::here("data/namcs/namcs-jd1-1pct-beta.rds"))
write_rds(namcs_jd1$mu, here::here("data/namcs/namcs-jd1-1pct-mu.rds"))
write_rds(namcs_jd1$rho1, here::here("data/namcs/namcs-jd1-1pct-rho1.rds"))

# only endogenous variables
namcs_jd0 <- jdm_data(
  y = namcs_endo,
  z = z_cutoff,
  n.chains = n_chains, n.iter = n_iter, n.burnin = n_burnin,
  DIC = TRUE
)
write_rds(namcs_jd0$rho, here::here("data/namcs/namcs-jd0-rho.rds"))
write_rds(namcs_jd0$beta, here::here("data/namcs/namcs-jd0-beta.rds"))
write_rds(namcs_jd0$mu, here::here("data/namcs/namcs-jd0-mu.rds"))
# with exogenous variables
namcs_jd1 <- jdm_data(
  y = namcs_endo, x = namcs_exo,
  z = z_cutoff,
  n.chains = n_chains, n.iter = n_iter, n.burnin = n_burnin,
  DIC = TRUE
)
write_rds(namcs_jd1$rho, here::here("data/namcs/namcs-jd1-rho.rds"))
write_rds(namcs_jd1$beta, here::here("data/namcs/namcs-jd1-beta.rds"))
write_rds(namcs_jd1$mu, here::here("data/namcs/namcs-jd1-mu.rds"))
write_rds(namcs_jd1$rho1, here::here("data/namcs/namcs-jd1-rho1.rds"))
