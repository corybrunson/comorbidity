library(tidyverse)

source(here::here("R/namcs-info.r"))
source(here::here("R/correlation-matrix.r"))
source(here::here("R/joint-settings.r"))
# NAMCS dataset
readRDS(here::here("data/namcs/namcs.rds")) %>%
  select_at(vars(as.character(namcs_disorders$code))) %>%
  print() -> namcs

if (FALSE) {
  # impose new order on disorder factors based on coefficient of variation
  
  # calculate coefficient-of-variation centralities
  m <- pairwise_matrix(pairwise_data(namcs))
  (linf_centrality <- apply(sqrt(1 - m ^ 2), 2, max))
  (cov_centrality <- apply(m^2, 2, sum))
  # reorder variables in each object
  namcs <- select(namcs, names(sort(-cov_centrality)))
  namcs_disorders <- namcs_disorders %>%
    left_join(tibble(
      code = names(cov_centrality),
      cov_cent = cov_centrality
    )) %>%
    mutate_at(vars(code, name, abbr), funs(fct_reorder(., -cov_cent))) %>%
    arrange(code) %>%
    select(-cov_cent)
}

ptm <- namcs %>%
  pairwise_data(z = z_cutoff) %>%
  partial_data() %>%
  partial_matrix()

# fixed evidential threshold, pairwise and full-partial tetrachoric correlations

# link sensitivity to node deletion: full partial correlation network

# link sensitivity tibble
linksens <- tibble()
for (i in seq_along(levels(namcs_disorders$code))) {
  d <- namcs_disorders$code[i]
  ptm_ <- namcs %>%
    select(-one_of(as.character(d))) %>%
    pairwise_data(z = z_cutoff) %>%
    partial_data() %>%
    partial_matrix()
  linksens_ <- ptm_ %>%
    as_tibble(rownames = "v1") %>%
    gather(key = "v2", value = "rho_partial_drop", -v1) %>%
    mutate_at(
      vars(v1, v2),
      list(~factor(., levels = levels(namcs_disorders$code)))
    ) %>%
    mutate(x = d)
  linksens <- bind_rows(linksens, linksens_)
}
ptm %>%
  as_tibble(rownames = "v1") %>%
  gather(key = "v2", value = "rho_partial", -v1) %>%
  mutate_at(
    vars(v1, v2),
    funs(factor(., levels = levels(namcs_disorders$code)))
  ) %>%
  right_join(linksens, by = c("v1", "v2")) %>%
  mutate(diff = rho_partial_drop - rho_partial) %>%
  print() -> linksens
# bind confidence interval data
linksens %>%
  filter(as.integer(v1) < as.integer(v2)) %>%
  left_join(
    namcs %>%
      pairwise_data(z = z_cutoff) %>%
      partial_data() %>%
      select(v1, v2, pval, starts_with("rho_partial")),
    by = c("v1", "v2", "rho_partial")
  ) %>%
  left_join(select(namcs_disorders, x = code, abbr = abbr)) %>%
  select(-x, x = abbr) %>%
  left_join(select(namcs_disorders, v1 = code, abbr = abbr)) %>%
  select(-v1, v1 = abbr) %>%
  left_join(select(namcs_disorders, v2 = code, abbr = abbr)) %>%
  select(-v2, v2 = abbr) %>%
  mutate(
    rho_partial_se = (rho_partial_upper - rho_partial_lower) / (2 * z_cutoff),
    std_diff = diff / rho_partial_se
  ) %>%
  print -> linksens

# difference plots
std_diff_top_plot <- linksens %>%
  unite("pair", v1, v2, sep = "-") %>%
  unite("sensitivity", pair, x, sep = " \\ ") %>%
  mutate(sensitivity = fct_reorder(sensitivity, abs(std_diff))) %>%
  #arrange(desc(abs(std_diff))) %>%
  top_n(12, wt = abs(std_diff)) %>%
  ggplot(aes(x = rho_partial, xend = rho_partial_drop,
             y = sensitivity, yend = sensitivity)) +
  theme_bw() +
  geom_segment(size = 1) +
  geom_point(shape = 16, size = 2) +
  geom_point(aes(x = rho_partial_drop), shape = 21, size = 2, fill = "white") +
  xlab(expression(Delta*italic(r)[italic(t)])) +
  ylab(expression(Delta*italic(r)[italic(t)]/se[italic(r)[italic(t)]]*" rank"))
std_diff_top_plot
ggsave(
  height = 4, width = 6, filename = here::here("fig/namcs-std-diff-top.pdf"),
  std_diff_top_plot
)

std_diff_all_plot <- linksens %>%
  unite("pair", v1, v2, sep = "--") %>%
  unite("sensitivity", pair, x, sep = " \\ ") %>%
  mutate(sensitivity = fct_reorder(sensitivity, abs(std_diff))) %>%
  ggplot(aes(x = rho_partial, xend = rho_partial_drop,
             y = abs(std_diff), yend = abs(std_diff))) +
  theme_bw() +
  geom_segment() +
  xlab(expression(Delta*italic(r)[italic(t)])) +
  ylab(expression(Delta*italic(r)[italic(t)]/se[italic(r)[italic(t)]]))
std_diff_all_plot
ggsave(
  height = 4, width = 6, filename = here::here("fig/namcs-std-diff-all.pdf"),
  std_diff_all_plot
)

# sign and magnitude of difference: heatmap
diff_ran <- range(linksens$diff)
diff_heatmap <- linksens %>%
  select(v1, v2, x, diff) %>%
  unite(col = "y", v1, v2, sep = "-", remove = FALSE) %>%
  mutate(y = fct_reorder2(
    as.factor(y),
    as.integer(v1), as.integer(v2),
    function(x, y) - (median(x) * 14 + median(y))
  )) %>%
  ggplot() +
  theme_bw() +
  scale_fill_distiller(
    type = "div", palette = 4,
    limits = c(-1, 1) * max(abs(diff_ran))
  ) +
  geom_tile(aes(x = x, y = y, fill = diff))
diff_heatmap

# sign and normalized magnitude of difference: heatmap
std_diff_ran <- range(linksens$std_diff)
std_diff_heatmap <- linksens %>%
  select(v1, v2, x, std_diff) %>%
  unite(col = "y", v1, v2, sep = "-", remove = FALSE) %>%
  mutate(y = fct_reorder2(
    as.factor(y),
    as.integer(v1), as.integer(v2),
    function(x, y) - (median(x) * 14 + median(y))
  )) %>%
  mutate(y = fct_rev(y)) %>%
  ggplot() + theme_bw() +
  theme(
    axis.text.y = element_text(angle = 30, hjust = 1),
    axis.text.x = element_text(angle = 60, hjust = 1),
    legend.position = "top"
  ) +
  scale_fill_distiller(
    type = "div", palette = 4,
    limits = c(-1, 1) * max(abs(std_diff_ran)),
    name = expression(Delta*r[t]/se[r[t]])
  ) +
  geom_tile(aes(x = y, y = x, fill = std_diff)) +
  xlab("Disorder-disorder pair") +
  ylab("Excluded disorder") +
  ggtitle("Sensitivity of tetrachoric to disorder remeval")
std_diff_heatmap

# compare sensitivities to disorder prevalences
diff_boxplots <- linksens %>%
  select(v1, v2, x, diff) %>%
  left_join(
    namcs %>%
      pairwise_data(z = z_cutoff) %>%
      select(x = v1, n = n1) %>%
      distinct() %>%
      left_join(select(namcs_disorders, x = code, abbr = abbr)) %>%
      select(-x, x = abbr),
    by = "x"
  ) %>%
  mutate(x = fct_reorder(x, n)) %>%
  ggplot() + theme_bw() +
  theme(axis.text.x = element_text(angle = -30, hjust = 0)) +
  geom_boxplot(aes(x = x, y = diff)) +
  xlab("Excluded disorder") +
  ylab(expression(Delta*r[t])) +
  ggtitle("Disease influence on correlations versus prevalence")
diff_boxplots
ggsave(
  height = 4, width = 6, filename = here::here("fig/namcs-diff-boxplot.pdf"),
  diff_boxplots
)
std_diff_boxplots <- linksens %>%
  select(v1, v2, x, std_diff) %>%
  left_join(
    namcs %>%
      pairwise_data(z = z_cutoff) %>%
      select(x = v1, n = n1) %>%
      distinct() %>%
      left_join(select(namcs_disorders, x = code, abbr = abbr)) %>%
      select(-x, x = abbr),
    by = "x"
  ) %>%
  mutate(x = fct_reorder(x, n)) %>%
  ggplot() + theme_bw() +
  theme(axis.text.x = element_text(angle = -30, hjust = 0)) +
  geom_boxplot(aes(x = x, y = std_diff)) +
  xlab("Excluded disorder") +
  ylab(expression(Delta*r[t]/se[r[t]])) +
  ggtitle("Disease influence on correlations versus prevalence")
std_diff_boxplots
ggsave(
  height = 4, width = 6, filename = here::here("fig/namcs-sens-boxplot.pdf"),
  std_diff_boxplots
)

# compare sensitivities to disorder-disorder association strengths
linksens_scatter <- linksens %>%
  select(v1, v2, x, std_diff, rho_partial) %>%
  unite(col = "y", v1, v2, sep = "-")
std_diff_scatter <- linksens_scatter %>%
  ggplot() + theme_bw() +
  geom_point(aes(x = rho_partial, y = std_diff), alpha = .5) +
  ggrepel::geom_text_repel(
    data = linksens_scatter %>%
      filter(abs(std_diff) > 10) %>%
      group_by(y) %>% top_n(n = 1, wt = std_diff) %>% ungroup(),
    aes(x = rho_partial, y = std_diff, label = y)
  ) +
  xlab(expression(hat(r[t]))) +
  ylab(expression(Delta*r[t]/se[r[t]])) +
  ggtitle("Sensitivity versus value of correlation coefficients")
std_diff_scatter
ggsave(
  height = 6, width = 6, filename = here::here("fig/namcs-sens-scatter.pdf"),
  std_diff_scatter
)

# sorted table of link-node sensitivities
linksens_tab <- linksens %>%
  select(x, v1, v2, rho_partial, rho_partial_se, diff, std_diff) %>%
  unite(col = "y", v1, v2, sep = "--") %>%
  arrange(desc(abs(std_diff)))
linksens_tab
linksens_tab %>%
  #filter(abs(std_diff) > 10) %>%
  top_n(n = 6, wt = diff) %>%
  select(
    `Disorder` = x,
    `Comorbidity` = y,
    `$r_{ij}$` = rho_partial,
    #`$se_{r_{ij}}$` = rho_partial_se,
    `$\\Delta r_{ij\\mid k}$` = diff
    #`$\\Delta r_{ij\\mid k}/se_{r_{ij}}$` = std_diff
  ) %>%
  xtable::xtable(
    caption = paste(
      "\\small",
      "Greatest sensitivities of estimated comorbid associations",
      "to removals of single disorders from the network."
    ),
    label = "tab:sensitivity",
    digits = 3
  ) %>%
  xtable::print.xtable(
    file = here::here("tab/namcs-comorbidity-sensitivity.tex"),
    include.rownames = FALSE,
    size = "\\footnotesize",
    sanitize.text.function = identity,
    math.style.negative = TRUE
  )

# proportion of positive and negative sensitivities
linksens %>%
  transmute(sign = diff / abs(diff)) %>%
  table()
