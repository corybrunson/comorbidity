library(tidyverse)
library(tidygraph)
library(ggraph)
library(ggalluvial)

source(here::here("R/namcs-info.r"))
source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))
#source(here::here("R/joint-distribution.r"))

# NAMCS network data sets
readRDS(here::here("data/namcs/namcs.rds")) %>%
  select_at(vars(as.character(namcs_disorders$code))) %>%
  pairwise_data(z = z_cutoff) %>%
  partial_data() %>%
  print() -> namcs_pt
readRDS(here::here("data/namcs/namcs-jd0-rho.rds")) %>%
  rename_all(~ str_replace(., "rho", "rho_joint")) %>%
  print() -> namcs_jd0
readRDS(here::here("data/namcs/namcs-jd1-rho.rds")) %>%
  rename_all(~ str_replace(., "rho", "rho_joint")) %>%
  print() -> namcs_jd1

# combined dataset ignoring indiscernible estimates
namcs_pt %>%
  full_join(namcs_jd0, by = c("v1", "v2")) %>%
  full_join(namcs_jd1, by = c("v1", "v2"), suffix = c("_0", "_1")) %>%
  select(-starts_with("n"), -pval, -ends_with("_Z")) %>%
  select(-contains("rho_joint_sd")) %>%
  mutate(rho = ifelse(rho_lower <= 0 & rho_upper >= 0, NA_real_, rho)) %>%
  mutate(rho_partial = ifelse(
    rho_partial_lower <= 0 & rho_partial_upper >= 0,
    NA_real_, rho_partial
  )) %>%
  mutate(rho_joint_0 = ifelse(
    rho_joint_lower_0 <= 0 & rho_joint_upper_0 >= 0,
    NA_real_, rho_joint_0
  )) %>%
  mutate(rho_joint_1 = ifelse(
    rho_joint_lower_1 <= 0 & rho_joint_upper_1 >= 0,
    NA_real_, rho_joint_1
  )) %>%
  select(-contains("lower"), -contains("upper")) %>%
  print() -> namcs_data

# total number of pairs
n_pairs <- choose(length(unique(unlist(select(namcs_data, v1, v2)))), 2)
if (n_pairs != nrow(namcs_data)) {
  warning("NAMCS data set is missing some pairs.")
}

# classifications of pairs
namcs_data %>%
  unite(pair, v1, v2, sep = "-") %>%
  select(
    pair,
    Pairwise = rho, Partial = rho_partial,
    JIDM0 = rho_joint_0, JIDM1 = rho_joint_1
  ) %>%
  mutate_if(is.numeric, list(~ . > 0)) %>%
  print() -> namcs_pairs

# alluvial diagrams

alluvial_colors <- RColorBrewer::brewer.pal(3, "Set1")[2:1]
alluvial_colors <- c(alluvial_colors[1], "#777777", alluvial_colors[2])

plot_alluvial <- namcs_pairs %>%
  group_by(Pairwise, Partial, JIDM0, JIDM1) %>%
  summarise(n = n(), id = first(pair)) %>%
  ungroup() %>%
  gather(Model, Link, Pairwise, Partial, JIDM0, JIDM1) %>%
  mutate(Model = fct_inorder(Model)) %>%
  mutate(Link = case_when(
    Link ~ "+", ! Link ~ "–", TRUE ~ "nd"
  )) %>%
  mutate(Link = factor(Link, c("+", "nd", "–"))) %>%
  ggplot(aes(x = Model, stratum = Link, alluvium = id, y = n)) +
  theme_bw() + ylab(NULL)
lode_custom <- function(n, i) {
  stopifnot(n == 4)
  switch(
    i,
    `1` = 1:4,
    `2` = c(2,3,1,4),
    `3` = c(3,2,4,1),
    `4` = 4:1
  )
}

plot_alluvium <- plot_alluvial +
  geom_flow(stat = "alluvium", aes(fill = Link), na.rm = FALSE,
            lode.guidance = "custom") +
  geom_stratum(aes(fill = Link), na.rm = FALSE) +
  scale_fill_manual(values = alluvial_colors) +
  #scale_fill_brewer(palette = "Set1", na.value = "lightgrey") +
  ggtitle("Link determinations for each network model, with discernment at 95%")
ggsave(
  filename = here::here("fig", "namcs-pairs-alluvial-alluvium.pdf"),
  plot = plot_alluvium,
  height = 4, width = 6,
  # better handling of en dash (negative sign)
  device = cairo_pdf
)

plot_flow <- plot_alluvial +
  geom_flow(aes(fill = Link), na.rm = FALSE) +
  geom_stratum(aes(fill = Link), na.rm = FALSE) +
  scale_fill_manual(values = alluvial_colors) +
  #scale_fill_brewer(palette = "Set1", na.value = "lightgrey") +
  ggtitle("Link determinations for each network model, with discernment at 95%")
ggsave(
  filename = here::here("fig", "namcs-pairs-alluvial-flow.pdf"),
  plot = plot_flow,
  height = 4, width = 6,
  # better handling of en dash (negative sign)
  device = cairo_pdf
)
