library(tidyverse)
library(tidygraph)
library(ggraph)
library(GGally)

source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))
source(here::here("R/joint-distribution.r"))

# whether to restrict network diagrams to discernible links
two.sigmas <- TRUE

ccs_dat <- readRDS(here::here("data/ccs-dat.rds"))
disorder_ccs <- distinct(select(
  filter(ccs_dat, icd9_type == "DX"),
  code = ccs_code_single, disorder = ccs_category_single
))

# care units
readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
  pull(curr_careunit) %>%
  unique() %>%
  print() -> careunits

# fixed evidential threshold, three models
for (careunit in careunits) {
  
  print(str_c("Care unit: ", careunit))
  
  # best available joint distribution model data
  
  # joint distribution model files
  jdfile <- list.files(
    here::here("data/mimic"),
    str_c("mimic-", tolower(careunit), "-.+-jd.rds")
  )
  # best available proportion of sample
  jdprops <- jdfile %>%
    str_replace("^.+-prop([0-9\\_]+)-.+-jd\\.rds", "\\1") %>%
    str_replace("\\_", ".") %>%
    as.numeric()
  jdprop <- max(jdprops)
  print(str_c("Proportion of ", careunit, " population in JIDM: ", jdprop))
  jdfile <- jdfile[which(jdprops == max(jdprops))]
  # among remaining files, best available minimum incidence
  jdfreqs <- jdfile %>%
    str_replace("^.+-freq([0-9]+)-.+-jd\\.rds", "\\1") %>%
    as.numeric()
  jdfreq <- min(jdfreqs)
  print(str_c("Minimum incidence: ", jdfreq))
  jdfile <- jdfile[which(jdfreqs == min(jdfreqs))]
  # read file
  mimic_jd <- readRDS(here::here("data/mimic", jdfile[1]))
  
  # pairwise and partial correlation data for same parameters
  
  set.seed(0)
  # co-occurrence matrix
  readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
    filter(curr_careunit == careunit) %>%
    select(-curr_careunit) %>%
    #distinct() %>%
    mutate(one = 1L) %>%
    spread(key = ccs_code_single, value = one, fill = 0L) %>%
    select(-subject_id, -hadm_id) %>%
    select_at(vars(names(.)[order(as.numeric(names(.)))])) %>%
    # restrict to variables with minimum prevalence
    select_if(list(~sum(.) >= jdfreq)) %>%
    # remove cases with no occurrences
    filter_all(any_vars(. == 1)) %>%
    # stratified subset
    sample_frac(jdprop) %>%
    print() -> mimic_x
  
  # pairwise correlation data
  pwfile <- str_c("mimic-", tolower(careunit), "-pw.rds")
  mimic_pw <- pairwise_data(mimic_x, z = z_cutoff)
  
  # partial correlation data
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  mimic_pt <- partial_data(mimic_pw, use.shrinkage = TRUE)
  
  rm(mimic_x)
  
  # combined dataset
  mimic_data <- full_join(mimic_pt, mimic_jd, by = c("v1", "v2"))
  
  # pairs plot
  mimic_data %>%
    select(rho, rho_partial, rho_joint, everything()) %>%
    ggpairs(aes(size = n1 * n2, alpha = .25, shape = "16"), columns = 1:3) +
    theme_bw() ->
    grob_pairs
  pairsfile <- str_c("mimic-", tolower(careunit), "-pairs.pdf")
  ggsave(
    height = 8, width = 9, filename = here::here("fig", pairsfile),
    grob_pairs
  )
  
  next
  
  # scatterplots on three-way common links
  mimic_scatterplots <- list(
    ggplot(mimic_data, aes(
      x = rho,# xmin = rho_lower, xmax = rho_upper,
      y = rho_partial#, ymin = rho_partial_lower, ymax = rho_partial_upper
    )) +
      xlab(expression(italic(r)[italic(t)])) +
      ylab(expression(italic(r)*minute[italic(t)])),
    ggplot(mimic_data, aes(
      x = rho,# xmin = rho_lower, xmax = rho_upper,
      y = rho_joint#, ymin = rho_joint_lower, ymax = rho_joint_upper
    )) +
      xlab(expression(italic(r)[italic(t)])) +
      ylab(expression(hat(P))),
    ggplot(mimic_data, aes(
      x = rho_partial,# xmin = rho_partial_lower, xmax = rho_partial_upper,
      y = rho_joint#, ymin = rho_joint_lower, ymax = rho_joint_upper
    )) +
      xlab(expression(italic(r)*minute[italic(t)])) +
      ylab(expression(hat(P)))
  ) %>%
    lapply(function(gg) {
      gg +
        theme_bw() +
        geom_point(aes(size = n1 * n2), alpha = .25, shape = 16) +
        #geom_errorbar(alpha = .5) + geom_errorbarh(alpha = .5) +
        scale_x_continuous(breaks = seq(-1, 1, .2),
                           minor_breaks = seq(-1, 1, .1)) +
        scale_y_continuous(breaks = seq(-1, 1, .2),
                           minor_breaks = seq(-1, 1, .1)) +
        theme(legend.position = "none")
    })
  grob_scatterplots <- gridExtra::arrangeGrob(
    grobs = mimic_scatterplots[c(2, 3, 1)], nrow = 2, ncol = 2,
    respect = FALSE
  )
  scatterplotsfile <- str_c("mimic-", tolower(careunit), "-scatters-jdm.pdf")
  ggsave(
    filename = here::here("fig", scatterplotsfile),
    plot = grob_scatterplots,
    height = 8, width = 9
  )
  
  # full pairwise and partial correlation data
  pwfile <- str_c("mimic-", tolower(careunit), "-pw.rds")
  mimic_pw <- readRDS(here::here("data/mimic", pwfile))
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  mimic_pt <- readRDS(here::here("data/mimic", ptfile))
  
  # combined dataset
  full_join(mimic_pt, mimic_jd, by = c("v1", "v2")) %>%
    # indicator for whether a link appears in the JIDM
    mutate(jdm = ! is.na(rho_joint)) %>%
    print() -> mimic_data
  
  # scatterplots on two-way common links
  mimic_scatterplots <- list(
    ggplot(mimic_data, aes(
      x = rho,# xmin = rho_lower, xmax = rho_upper,
      y = rho_partial#, ymin = rho_partial_lower, ymax = rho_partial_upper
    )) +
      xlab(expression(italic(r)[italic(t)])) +
      ylab(expression(italic(r)*minute[italic(t)])),
    ggplot(mimic_data, aes(
      x = rho,# xmin = rho_lower, xmax = rho_upper,
      y = rho_joint#, ymin = rho_joint_lower, ymax = rho_joint_upper
    )) +
      xlab(expression(italic(r)[italic(t)])) +
      ylab(expression(hat(P))),
    ggplot(mimic_data, aes(
      x = rho_partial,# xmin = rho_partial_lower, xmax = rho_partial_upper,
      y = rho_joint#, ymin = rho_joint_lower, ymax = rho_joint_upper
    )) +
      xlab(expression(italic(r)*minute[italic(t)])) +
      ylab(expression(hat(P)))
  ) %>%
    lapply(function(gg) {
      gg +
        theme_bw() +
        geom_point(aes(size = n1 * n2, color = jdm, shape = jdm), alpha = .25) +
        #geom_errorbar(alpha = .5) + geom_errorbarh(alpha = .5) +
        scale_x_continuous(breaks = seq(-1, 1, .2),
                           minor_breaks = seq(-1, 1, .1)) +
        scale_y_continuous(breaks = seq(-1, 1, .2),
                           minor_breaks = seq(-1, 1, .1)) +
        scale_color_brewer(type = "qual", palette = "Set1") +
        scale_shape_manual(values = c(1, 16)) +
        #guides(size = FALSE, color = FALSE, shape = FALSE) +
        theme(legend.position = "none")
    })
  grob_scatterplots <- gridExtra::arrangeGrob(
    grobs = mimic_scatterplots[c(2, 3, 1)], nrow = 2, ncol = 2,
    respect = FALSE
  )
  scatterplotsfile <- str_c("mimic-", tolower(careunit), "-scatters.pdf")
  ggsave(
    filename = here::here("fig", scatterplotsfile),
    plot = grob_scatterplots,
    height = 8, width = 9
  )
  
  # network for different data parameters
  
  if (two.sigmas) {
    # restrict to discernible links
    mimic_data %>%
      mutate(rho = ifelse(rho_lower <= 0 & rho_upper >= 0, NA_real_, rho)) %>%
      mutate(rho_partial = ifelse(
        rho_partial_lower <= 0 & rho_partial_upper >= 0,
        NA_real_, rho_partial
      )) %>%
      mutate(rho_joint = ifelse(
        rho_joint_lower <= 0 & rho_joint_upper >= 0,
        NA_real_, rho_joint
      )) %>%
      print() -> mimic_data
  }
  
  # network diagrams with common layout
  bind_rows(
    mimic_data %>%
      mutate(p1 = n1 / n) %>%
      select(name = v1, prevalence = p1),
    mimic_data %>%
      mutate(p2 = n2 / n) %>%
      select(name = v2, prevalence = p2)
  ) %>%
    distinct() %>%
    print() -> mimic_nodes
  mimic_data %>%
    select(
      v1, v2,
      rho,
      rho_partial,
      rho_joint
    ) %>%
    gather(model, rho, starts_with("rho")) %>%
    mutate(weight = abs(rho), sign = sign(rho)) %>%
    as_tbl_graph() %>%
    activate(nodes) %>%
    left_join(mimic_nodes, by = "name") %>%
    activate(links) %>%
    mutate(model = fct_inorder(model)) %>%
    mutate(model = fct_recode(
      model,
      Pairwise = "rho", Partial = "rho_partial", JIDM = "rho_joint"
    )) %>%
    mutate(sign = fct_inseq(as.factor(sign))) %>%
    print() -> mimic_nets
  ggnets <- mimic_nets %>%
    activate(links) %>%
    # restrict to stronger correlations (consistently for each network)
    filter(weight >= .15) %>%
    ggraph(layout = "kk") +
    theme_graph(base_family = "") +
    facet_edges(~ model) +
    geom_edge_link(aes(width = weight, linetype = sign), alpha = .2) +
    scale_edge_width_continuous(range = c(.2, 1.5), guide = "none") +
    scale_edge_linetype_manual(values = c("dotted", "solid"), guide = "none") +
    geom_node_point(aes(size = prevalence)) +
    scale_size_continuous(range = c(.5, 2.5), guide = "none")
  netsfile <- str_c("mimic-", tolower(careunit), "-networks.pdf")
  ggsave(
    filename = here::here("fig", netsfile),
    plot = ggnets,
    height = 6, width = 18
  )
  
}
