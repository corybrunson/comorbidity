library(tidyverse)
library(tidygraph)
library(ggraph)
library(ggalluvial)

source(here::here("R/joint-settings.r"))
source(here::here("R/correlation-matrix.r"))
source(here::here("R/joint-distribution.r"))

# care units
readRDS(here::here("data/mimic/mimic-ccs.rds")) %>%
  pull(curr_careunit) %>%
  unique() %>%
  print() -> careunits

mimic_pairs <- tibble()
# fixed evidential threshold, three models
for (careunit in careunits) {
  
  print(str_c("Care unit: ", careunit))
  
  # pairwise and partial correlation data
  ptfile <- str_c("mimic-", tolower(careunit), "-pt.rds")
  unit_pt <- readRDS(here::here("data/mimic", ptfile))
  
  # best available joint distribution model data
  
  # joint distribution model files
  jdfile <- list.files(
    here::here("data/mimic"),
    str_c("mimic-", tolower(careunit), "-.+-jd.rds")
  )
  # best available proportion of sample
  jdprops <- jdfile %>%
    str_replace("^.+-prop([0-9\\_]+)-.+-jd\\.rds", "\\1") %>%
    str_replace("\\_", ".") %>%
    as.numeric()
  jdprop <- max(jdprops)
  print(str_c("Proportion of ", careunit, " population in JIDM: ", jdprop))
  jdfile <- jdfile[which(jdprops == max(jdprops))]
  # among remaining files, best available minimum incidence
  jdfreqs <- jdfile %>%
    str_replace("^.+-freq([0-9]+)-.+-jd\\.rds", "\\1") %>%
    as.numeric()
  jdfreq <- min(jdfreqs)
  print(str_c("Minimum incidence: ", jdfreq))
  jdfile <- jdfile[which(jdfreqs == min(jdfreqs))]
  # read file
  unit_jd <- readRDS(here::here("data/mimic", jdfile[1]))
  
  # combined dataset with rho values
  full_join(unit_pt, unit_jd, by = c("v1", "v2")) %>%
    select(-pval, -ends_with("_Z")) %>%
    select(-rho_joint_sd, -starts_with("beta")) %>%
    print() -> unit_data
  
  # combined dataset ignoring indiscernible estimates
  unit_data %>%
    mutate(rho = ifelse(rho_lower <= 0 & rho_upper >= 0, NA_real_, rho)) %>%
    mutate(rho_partial = ifelse(
      rho_partial_lower <= 0 & rho_partial_upper >= 0,
      NA_real_, rho_partial
    )) %>%
    mutate(rho_joint = ifelse(
      rho_joint_lower <= 0 & rho_joint_upper >= 0,
      NA_real_, rho_joint
    )) %>%
    select(-ends_with("lower"), -ends_with("upper")) %>%
    print() -> unit_data
  
  # total number of pairs
  n_pairs <- choose(length(unique(unlist(select(unit_data, v1, v2)))), 2)
  if (n_pairs != nrow(unit_data)) {
    warning("Data set for care unit ", careunit, " is missing some pairs.")
  }
  
  # classifications of pairs
  unit_data %>%
    unite(pair, v1, v2, sep = "-") %>%
    select(pair, Pairwise = rho, Partial = rho_partial, JIDM = rho_joint) %>%
    mutate_if(is.numeric, list(~ . > 0)) %>%
    mutate(Unit = careunit) %>% select(Unit, everything()) %>%
    print() -> unit_pairs
  
  mimic_pairs <- bind_rows(mimic_pairs, unit_pairs)
}

# counts and proportions of pairs
mimic_pairs %>%
  select(-pair) %>%
  mutate(Unit = fct_inorder(Unit)) %>%
  group_by(Unit) %>%
  summarize_all(list(
    Positive = ~ sum(., na.rm = TRUE),
    Negative = ~ sum(! ., na.rm = TRUE),
    Indiscernible = ~ sum(is.na(.))
  )) %>%
  ungroup() %>%
  gather(Model, count, everything(), -Unit) %>%
  separate(Model, c("Model", "Determination"), sep = "\\_") %>%
  mutate(
    Model = fct_inorder(Model),
    Determination = fct_inorder(Determination)
  ) %>%
  add_count(Unit, Model, wt = count) %>%
  mutate(prop = count / n) %>%
  select(Unit, everything()) %>%
  arrange(Unit, Model, Determination) %>%
  print() -> unit_pairs_summary

# summary table
unit_pairs_summary %>%
  mutate(prop = scales::percent(prop)) %>%
  select(-count, -n) %>% spread(Unit, prop) %>%
  xtable::xtable(
    caption = str_c(
      "Link determinations for each MIMIC-III care unit using each model, ",
      "with a consistent evidential threshold of 2 standard deviations."
    ),
    label = "tab:mimic-links",
    align = c(rep("l", 3), rep("r", 7))
  ) %>%
  xtable::print.xtable(
    file = "tab/mimic-links.tex",
    size = "\\small",
    include.rownames = FALSE,
    sanitize.colnames.function = identity,
    #rotate.colnames = TRUE,
    caption.placement = "top"
  )

stop("Adapt updates from `pairs-namcs.r")

# alluvial diagrams

alluvial_colors <- RColorBrewer::brewer.pal(3, "Set1")[2:1]
alluvial_colors <- c(alluvial_colors[1], "#777777", alluvial_colors[2])

plot_alluvial <- mimic_pairs %>%
  #filter(! Unit %in% c("NWARD", "NICU")) %>%
  group_by(Unit, Pairwise, Partial, JIDM) %>%
  summarise(n = n(), id = first(pair)) %>%
  ungroup() %>%
  gather(Model, Link, Pairwise, Partial, JIDM) %>%
  mutate(Model = fct_inorder(Model)) %>%
  mutate(Unit = fct_infreq(Unit)) %>%
  mutate(Link = case_when(
    Link ~ "+", ! Link ~ "–", TRUE ~ "nd"
  )) %>%
  mutate(Link = factor(Link, c("+", "nd", "–"))) %>%
  ggplot(aes(x = Model, stratum = Link, alluvium = id, y = n)) +
  theme_bw() + ylab(NULL) +
  facet_wrap(~ Unit, scales = "free_y")

plot_alluvium <- plot_alluvial +
  geom_flow(stat = "alluvium", aes(fill = Link), na.rm = FALSE) +
  geom_stratum(aes(fill = Link), na.rm = FALSE) +
  scale_fill_manual(values = alluvial_colors) +
  ggtitle("Link determinations for each network model, with discernment at 95%")
ggsave(
  filename = here::here("fig/mimic-pairs-alluvial-alluvium.pdf"),
  plot = plot_alluvium,
  height = 8, width = 9,
  # better handling of en dash (negative sign)
  device = cairo_pdf
)

plot_flow <- plot_alluvial +
  geom_flow(stat = "flow", aes(fill = Link), na.rm = FALSE) +
  geom_stratum(aes(fill = Link), na.rm = FALSE) +
  scale_fill_manual(values = alluvial_colors) +
  ggtitle("Link determinations for each network model, with discernment at 95%")
ggsave(
  filename = here::here("fig/mimic-pairs-alluvial-flow.pdf"),
  plot = plot_flow,
  height = 8, width = 9,
  # better handling of en dash (negative sign)
  device = cairo_pdf
)
