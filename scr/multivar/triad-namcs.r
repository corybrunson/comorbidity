library(tidyverse)
library(tidygraph)
library(ggraph)

source(here::here("R/namcs-info.r"))
source(here::here("R/correlation-matrix.r"))
source(here::here("R/joint-settings.r"))
#source(here::here("R/joint-distribution.r"))

namcs_exo <- readRDS(here::here("data/namcs/namcs.rds"))
namcs <- select_at(namcs_exo, vars(as.character(namcs_disorders$code)))
namcs_pw <- pairwise_data(namcs, z = z_cutoff)
namcs_pt <- partial_data(namcs_pw)
readRDS(here::here("data/namcs/namcs-jd0-rho.rds")) %>%
  rename_all(~ str_replace(., "rho", "rho_joint")) ->
  namcs_jd0
readRDS(here::here("data/namcs/namcs-jd1-rho.rds")) %>%
  rename_all(~ str_replace(., "rho", "rho_joint")) ->
  namcs_jd1

# combined dataset
namcs_pt %>%
  full_join(namcs_jd0, by = c("v1", "v2")) %>%
  full_join(namcs_jd1, by = c("v1", "v2"), suffix = c("_0", "_1")) %>%
  print() -> namcs_data

# HT-DM-arthritis triad

triad <- namcs_disorders$code[c(1,8,10)]
namcs_data %>%
  filter(v1 %in% triad & v2 %in% triad) %>%
  select(
    v1, v2, n1, n2, n12,
    rho, rho_partial, rho_joint_0, rho_joint_1,
    contains("lower"), contains("upper")
  ) %>%
  rename_all(~ str_replace(., "joint([\\_a-z]*)\\_([01])", "joint_\\2\\1")) %>%
  rename_at(
    vars(rho, rho_partial, rho_joint_0, rho_joint_1),
    ~ str_c(., "_estimate")
  ) %>%
  pivot_longer(
    starts_with("rho"),
    names_to = c("model", ".value"),
    names_pattern = "(.*)\\_([^\\_]*)"
  ) %>%
  print() -> triad_stats

triad_stats %>%
  left_join(select(namcs_disorders, code, abbr), by = c("v1" = "code")) %>%
  left_join(select(namcs_disorders, code, abbr), by = c("v2" = "code")) %>%
  mutate(model = fct_inorder(model)) %>%
  mutate(model = fct_recode(
    model,
    Pairwise = "rho", Partial = "rho_partial",
    JIDM0 = "rho_joint_0", JIDM1 = "rho_joint_1"
  )) %>%
  select(
    disorder1 = abbr.x, disorder2 = abbr.y,
    model, lower, estimate, upper
  ) %>%
  rename_all(Hmisc::capitalize) %>%
  mutate_if(
    is.numeric,
    ~ format(., digits = 3, nsmall = 3, scientific = FALSE)
  ) %>%
  print() %>%
  write_csv(here::here("tab/namcs-triad.csv"))

triad_stats %>%
  left_join(select(namcs_disorders, code, abbr), by = c("v1" = "code")) %>%
  left_join(select(namcs_disorders, code, abbr), by = c("v2" = "code")) %>%
  mutate(model = fct_inorder(model)) %>%
  mutate(model = fct_recode(
    model,
    Pairwise = "rho", Partial = "rho_partial",
    JIDM0 = "rho_joint_0", JIDM1 = "rho_joint_1"
  )) %>%
  select(
    disorder1 = abbr.x, disorder2 = abbr.y,
    model, lower, estimate, upper
  ) %>%
  rename_all(Hmisc::capitalize) %>%
  print() %>%
  xtable::xtable(
    label = "tab:namcs-triad",
    caption = paste0(
      "Point estimates and their upper and lower bounds on ",
      "95\\% confidence or credible intervals for the HT--DM--arthritis triad ",
      "in network models of the NAMCS data."
    ),
    align = c(rep("l", 4), rep("r", 3)), digits = 3, nsmall = 3
  ) %>%
  xtable::print.xtable(
    file = "tab/namcs-triad.tex",
    size = "small", caption.placement = "top",
    math.style.negative = TRUE,
    sanitize.rownames.function = identity,
    sanitize.colnames.function = identity
  )
