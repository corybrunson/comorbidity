# setup
rm(list = ls())
library(tidyverse)
source(here::here("R/namcs-info.r"))

# frequency-rank plot
here::here("data/namcs/namcs.rds") %>%
  readRDS() %>%
  select_at(vars(as.character(namcs_disorders$code))) %>%
  as.matrix() %>%
  colSums() %>%
  as.table() %>%
  enframe() %>%
  set_names(c("code", "frequency")) %>%
  left_join(select(namcs_disorders, code, disorder = abbr), by = "code") %>%
  select(-code) %>%
  arrange(desc(frequency)) %>%
  mutate(rank = row_number()) %>%
  ggplot(aes(rank, frequency)) +
  theme_bw() +
  geom_line() +
  scale_x_continuous(breaks = 10^(0:2), minor_breaks = sqrt(10) * 10^(0:2)) +
  scale_y_continuous(breaks = 10^(0:6), minor_breaks = sqrt(10) * 10^(0:6)) +
  coord_trans(x = "log", y = "log") +
  labs(x = "Rank", y = "Frequency") +
  ggtitle("Chronic disorder frequencies in NAMCS") ->
  namcs_freqrank
ggsave(
  height = 3, width = 4.5,
  filename = here::here("fig/namcs-freqrank.pdf"),
  namcs_freqrank
)

# estimated exogenous effects on prevalence
read_rds(here::here("data/namcs/namcs-jd1-beta.rds")) %>%
  #select(-matches("\\_(sd)\\_")) %>%
  gather(key, value, -var) %>%
  mutate(predictor = str_remove(key, "^beta\\_")) %>%
  mutate(predictor = str_remove(predictor, "^(sd|lower|upper)\\_")) %>%
  mutate(predictor = factor(
    predictor,
    c("constant", levels(namcs_demographics$name))
  )) %>%
  mutate(stat = case_when(
    str_detect(key, "\\_sd\\_") ~ "sd",
    str_detect(key, "\\_lower\\_") ~ "lower",
    str_detect(key, "\\_upper\\_") ~ "upper",
    TRUE ~ "mean"
  )) %>%
  mutate(stat = factor(stat, c("mean", "sd", "lower", "upper"))) %>%
  arrange(predictor, stat) %>%
  select(-key) %>%
  spread(stat, value) %>%
  print() -> beta1

# table of exogenous prevalence effect estimates with intervals
beta1 %>%
  left_join(namcs_demographics, by = c("predictor" = "name")) %>%
  mutate(abbr = ifelse(is.na(abbr), "Const.", as.character(abbr))) %>%
  mutate(abbr = factor(abbr, c("Const.", levels(namcs_demographics$abbr)))) %>%
  select(-predictor, predictor = abbr) %>%
  left_join(namcs_disorders, by = c("var" = "code")) %>%
  rename(disorder = abbr) %>%
  filter(lower > 0 | upper < 0) %>%
  select(predictor, disorder, mean) %>%
  spread(disorder, mean) %>%
  mutate_if(
    is.numeric,
    ~ ifelse(is.na(.), "--", format(., digits = 1, nsmall = 2, scientific = F))
  ) %>%
  xtable::xtable(
    caption = str_c(
      "Estimated effects of demographic predictors ",
      "on the incidence of chronic disorders in Model 1. ",
      "Each value indicates the effect of the predictor on the mean of the ",
      "normal distribution from which the latent variable is sampled ",
      "(see the text). ",
      "Estimates whose 95\\% credible intervals contain zero are excluded."
    ),
    label = "tab:jdm-coefs",
    align = c("r", "l", rep("c", nrow(namcs_disorders)))
  ) %>%
  xtable::print.xtable(
    file = str_c("tab/jdm-coefs.tex"),
    size = "\\scriptsize",
    include.rownames = FALSE,
    rotate.colnames = TRUE
  )

# estimated epidemiological comorbidity due to patient-level factors
readRDS(here::here("data/namcs/namcs-jd1-rho1.rds")) %>%
  rename_all(~ str_replace(., "rho", "rho_env_joint")) %>%
  print() -> rho1

# summaries of effect distributions
beta1 %>%
  filter(predictor != "constant") %>%
  summarize(sd = sd(mean))
rho1 %>%
  mutate(sign = rho_env_joint / abs(rho_env_joint)) %>%
  group_by(sign) %>%
  summarize(n = length(rho_env_joint), median = median(rho_env_joint))

# histogram of estimated exogenous effects on prevalence
beta1 %>%
  filter(predictor != "constant") %>%
  ggplot(aes(x = mean)) +
  theme_bw() +
  geom_histogram(binwidth = .2) +
  labs(x = "Effect estimate", y = "Number of effects") ->
  exo_prevalence_hist

# histogram of estimated patient-level component of comorbidity
exo_cooccurrence_hist <- ggplot(rho1, aes(x = rho_env_joint)) +
  theme_bw() +
  geom_histogram(binwidth = .1) +
  labs(x = "Comorbidity component estimate", y = "Number of components")

# concatenate histograms
grob_hists <- gridExtra::arrangeGrob(
  grobs = list(exo_prevalence_hist, exo_cooccurrence_hist),
  nrow = 1, ncol = 2,
  #widths = c(11, 9),
  respect = FALSE
)
plot(grob_hists)
ggsave(
  height = 3, width = 9, filename = here::here("fig/namcs-hists.pdf"),
  grob_hists
)
