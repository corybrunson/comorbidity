# scp data/ontology/ontology-maps.rda jbrunson@sig2-glx.cam.uchc.edu:dependency/
# scp data/pairs/mimic-iii.rds jbrunson@sig2-glx.cam.uchc.edu:dependency/
# scp data/pairs/michigan.rds jbrunson@sig2-glx.cam.uchc.edu:dependency/
# scp data/pairs/medicare1993-5.rds jbrunson@sig2-glx.cam.uchc.edu:dependency/
# scp scr/cluster/dependency/* jbrunson@sig2-glx.cam.uchc.edu:dependency/

print(R.Version())
pkg_dir <- "R/library/"
pkgs <- c("dplyr", "igraph")
for (pkg in pkgs) {
  install.packages(pkg, lib = pkg_dir)
}
library(dplyr, lib.loc = pkg_dir)
library(igraph, lib.loc = pkg_dir)

# names for data sources
source_info <- data.frame(
  file = c(
    "columbia.rds",
    "medicare1993-3.rds",
    "medicare1993-5.rds",
    "scthans1998-2008.rds",
    "michigan.rds",
    "stanford2013.rds",
    "columbia2013.rds",
    "mimic-iii.rds",
    "namcs2011-2015-3.rds",
    "namcs2011-2015-5.rds",
    "vaers2012-2016.rds"
  ),
  source = c(
    "columbia",
    "medicare1993-3",
    "medicare1993-5", 
    "scthans1998-2008",
    "michigan",
    "stanford2013",
    "columbia2013",
    "mimic-iii",
    "namcs2005-2014-3",
    "namcs2005-2014-5", 
    "vaers2005-2016"
  ),
  abbr = c(
    "Columbia", "MedPAR(3)", "MedPAR(5)", "SctHans",
    "Michigan", "Stanford", "Columbia*",
    "MIMIC", "NAMCS(3)", "NAMCS(5)", "VAERS"
  ),
  ontology = c(
    "Rzhetsky", "ICD9-3", "ICD9-5", "ICD10-3", "ICD9-5", "Rzhetsky", "Rzhetsky",
    "ICD9-5", "ICD9-3", "ICD9-5", "VAERS"
  )
)

# names and features of binary association measures
measure_info <- data.frame(
  measure = c(
    "oddsratio",
    "oddsratiommle",
    "phi",
    "forbes",
    "rho_psych",
    "rho_bpe"
  ),
  abbr = c("OR", "OR-MMLE", "phi", "F", "r[t]", "r[t]-BP"),
  abbr_tex = c(
    "$OR$", "$\\widehat{OR}$", "$\\phi$", "$F$", "$r_{t}$", "$\\widehat{r_{t}}$"
  ),
  zero = c(1, 1, 0, 1, 0, 0)
)

graph_from_pairs_data <- function(
  data,
  measure = NULL, measure_cutoff = NULL, pval_cutoff = NULL,
  weighted = FALSE
) {
  
  nodes <- as.character(unique(unlist(data[, 1:2])))
  if (is.null(measure_cutoff)) measure_cutoff <- -Inf
  if (is.null(pval_cutoff)) pval_cutoff <- 1
  if (is.character(measure)) measure <- data[[measure]]
  if (is.character(measure_cutoff)) measure_cutoff <- data[[measure_cutoff]]
  if (is.character(pval_cutoff)) pval_cutoff <- data[[pval_cutoff]]
  if (weighted) data$weight <- measure
  
  if (!is.null(measure)) {
    data <- data[measure > measure_cutoff & data$pval_fisher < pval_cutoff, ]
  }
  inds <- c(1:2, if (weighted) grep("^weight$", names(data)) else NULL)
  
  graph_from_data_frame(
    d = data[, inds],
    directed = FALSE,
    vertices = nodes
  )
}


adjacency_to_pairs <- function(x) {
  wh <- which(as.logical(x))
  cbind(
    (wh - 1) %% nrow(x) + 1,
    ceiling(wh / nrow(x)),
    x[wh]
  )
}

dependency_cube_matrix <- function(graph, verbose = TRUE) {
  # shortest path length matrix
  L <- distances(graph, weights = NA)
  suppressWarnings(storage.mode(L) <- "integer")
  d <- max(setdiff(as.vector(L), NA))
  # shortest path count matrix
  if (verbose) message("Calculating path count matrix...")
  A <- as_adj(graph)
  M <- matrix(NA_integer_, vcount(graph), vcount(graph))
  p <- 0
  P <- diag(vcount(graph))
  peb <- dplyr::progress_estimated(d + 1, 2)
  while (p <= d) {
    M[which(L == p)] <- as.integer(P[which(L == p)])
    p <- p + 1
    P <- P %*% A
    peb$tick()$print()
  }
  diag(M) <- 0L
  # brokerage data (increase efficiency by using 'apply' on two dimensions?)
  if (verbose) message("Generating brokerage dataset...")
  bs <- which(apply(A, 1, sum) > 1)
  peb <- dplyr::progress_estimated(length(bs), 2)
  B <- list()
  for (b in bs) {
    S <- (log(exp(L[, b, drop = FALSE]) %*% exp(L[b, , drop = FALSE])) == L)
    if (!any(S[-b, -b], na.rm = TRUE)) next
    N <- (M[, b, drop = FALSE] %*% M[b, , drop = FALSE])
    B <- c(B, list(cbind(b, adjacency_to_pairs(N * S))))
    peb$tick()$print()
  }
  B <- do.call(rbind, B)
  B %>% as_tibble() %>%
    setNames(c("broker", "sender", "receiver", "broker_count")) %>%
    left_join(
      L %>% adjacency_to_pairs() %>% as_tibble() %>%
        setNames(c("sender", "receiver", "geodesic_length"))
    ) %>%
    left_join(
      M %>% adjacency_to_pairs() %>% as_tibble() %>%
        setNames(c("sender", "receiver", "geodesic_count"))
    ) %>%
    mutate(
      broker_prop = broker_count / geodesic_count,
      sender = vertex_attr(graph, "name", sender),
      receiver = vertex_attr(graph, "name", receiver),
      broker = vertex_attr(graph, "name", broker),
      geodesic_length = as.integer(geodesic_length),
      geodesic_count = as.integer(geodesic_count),
      broker_count = as.integer(broker_count)
    )
}
