# setup
source("dependency/dependency-utils.r")
# a named list of named lists of data frames
load("dependency/ontology-maps.rda")

# source and target data directories
sdir <- "dependency/"
tdir <- "dependency/"

# list of node subsets under each ontology
map_info <- tribble(
  ~ontology, ~map
  , "ICD9-5", "ICD9-3"
  , "ICD9-5", "Rzhetsky"
  #, "ICD9-5", "CCS-single"
  #, "ICD9-5", "CCS-multi"
  #, "ICD9-5", "Elixhauser"
  #, "ICD9-5", "Quan-Elixhauser"
)

# MIMIC-III
i <- 8
# continue only if a suitable mapping exists
sub_map_info <- map_info %>%
  filter(ontology == source_info$ontology[i])
if (nrow(sub_map_info) == 0) next
thetas <- c(".05", "B-.05")
for (j in length(thetas):1) {
  # load data
  d <- readRDS(paste0(sdir, source_info$file[i]))
  # TESTING
  d <- dplyr::sample_frac(d, size = .01)
  n <- n_distinct(unlist(d[, 1:2]))
  pval_cutoffs <- data.frame(
    pval = c(.05, .05 / n),
    label = thetas
  )
  # construct graph
  g <- graph_from_pairs_data(
    data = d,
    measure = "oddsratiommle", measure_cutoff = 1,
    pval_cutoff = pval_cutoffs$pval[j],
    weighted = FALSE
  )
  rm(d)
  print(paste0(source_info$source[i], "; ", thetas[j]))
  # estimated dependency cube
  dc_file <- paste0(
    tdir,
    "dependencycube-",
    source_info$source[i], "-",
    gsub("\\.|-", "", thetas[j]),
    ".rda"
  )
  if (!file.exists(dc_file)) {
    save(NULL, file = dc_file)
  } else {
    warning("File already exists!")
  }
  dc <- dependency_cube_matrix(g)
  save(dc, file = dc_file)
}
