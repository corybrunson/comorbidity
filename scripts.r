# setup
rm(list = ls())
setwd("~/Documents/CQM/comorbidity")

# generate a pairs dataset for each source
#source("scr/namcs/namcs-scrape-raw.r")
source("scr/namcs/namcs-scrape-stata.r")
#source("scr/namcs/namcs-graph.r")
source("scr/graphs/pairs-extract.r")
source("scr/graphs/pairs-extract-mimic.r")
source("scr/graphs/pairs-augment.r")
source("scr/graphs/pairs-summary.r")
source("scr/graphs/quantiles.r")

# visualizations of sparse constructions
source("scr/graphs/visualizations.r")

# analysis of the shapes of degree sequences (alleged power laws)
source("scr/graphs/statistics-degseq.r")
source("scr/graphs/statistics-degseq-summary.r")
source("scr/graphs/statistics-degseq-tail.r")
source("scr/graphs/statistics-degseq-tailstats.r")

# exploratory analysis using a battery of graph statistics
source("scr/graphs/statistics-calculate.r")
source("scr/graphs/statistics-summary.r")
#source("scr/graphs/statistics-summary-quantile.r")

# centrality calculations and sensitivity analysis
source("scr/graphs/centralities-concordance.r")
source("scr/graphs/centralities-prevalences.r")
source("scr/graphs/centralities-node-group.r")
source("scr/graphs/centralities-kendall.r")
#source("scr/graphs/centralities-lines.r")
source("scr/graphs/centralities-top.r")
#source("scr/graphs/centralities-examples.r")

# distance calculations and sensitivity analysis
#source("scr/graphs/distances-calculate.r")
#source("scr/graphs/distances-sensitivity.r")

# clustering calculations and sensitivity analysis
#source("scr/graphs/community-calculate.r")
#source("scr/graphs/community-calculate-hierarchical.r")
#source("scr/graphs/community-evaluate.r")
#source("scr/graphs/community-examples.r")

# multiple and multivariate approaches to population comorbidity
source("scr/multivar/data-css.r")

# multiple and multivariate analysis of NAMCS data
source("scr/multivar/data-namcs.r")
source("scr/multivar/data-namcs-jd.r")
source("scr/multivar/summary-namcs.r")
source("scr/multivar/visualize-namcs.r")
source("scr/multivar/pairs-namcs.r")
source("scr/multivar/motifs-namcs.r")

# multiple and multivariate analysis of MIMIC data
source("scr/multivar/data-mimic.r")
source("scr/multivar/data-mimic-pw-pt.r")
source("scr/multivar/data-mimic-jd.r")
source("scr/multivar/summary-mimic.r")
source("scr/multivar/visualize-mimic.r")
source("scr/multivar/pairs-mimic.r")
source("scr/multivar/motifs-mimic.r")
source("scr/multivar/triad-mimic.r")
source("scr/multivar/hubs-mimic.r")
#source("scr/multivar/interpolate-mimic.r")
