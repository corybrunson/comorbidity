<h1 id="a-reproducibility-and-sensitivity-analysis-of-comorbidity-network-analyses">A reproducibility and sensitivity analysis of comorbidity network analyses</h1>
<p>In this project we conduct sensitivity and reproducibility analyses of prototypical results obtained from the study of comorbidity networks.</p>
<h2 id="introduction">Introduction</h2>
<p>Since around 2007 several research teams at the intersection of data mining and healthcare have proposed network models of population comorbidity constructed by mining statistical evidence for two- or few-disorder dependencies from large patient-level datasets and aggregating these relations into a mathematical graph. These researchers drew upon their models as aides to manual exploration, for hypothesis generation, and, from a network perspective, to test for associations between the structural positions of disorders and other important features such as genetic overlap, lethality, sequentiality, and ontological position.</p>
<p>Many domains of scientific research, including healthcare, are in the midst of a methodological renaissance impelled by a series of alarming reproducibility studies. These have implicated, among other culprits, the failure to report the ways in which early data explorations induced formal methodological choices. The numerous forks encountered in the construction of comorbidity networks, coupled with known biases in the collection of healthcare data and population heterogeneity, amplifies these concerns. However, while the use of comorbidity networks continues to expand, very few studies have reported on the sensitivity of their network analytic results to adjustments in the many parameters governing model construction, and fewer still have evaluated the reproducibility of results across different datasets.</p>
<h2 id="functionality">Functionality</h2>
<ul>
<li><code>utils.r</code>: Summary information for several sets of objects or choices used in the analysis, including the data source, the ontology, and the association measures.</li>
<li><code>pairs-calculations.r</code>: Functions used to append comorbidity data frames with values of statistical tests and association measures.</li>
<li><code>graph-construction.r</code>: A function that takes a comorbidity data frame, which first two columns must contain the disease pairs and which must contain columns for each of the partial sums <code>n</code>, <code>n1</code>, <code>n2</code>, <code>n12</code> of a frequency table, and several construction parameters and returns the comorbidity network constructed using those data and parameters.</li>
<li><code>graph-distances.r</code>: Helper functions for calculating graph distances.</li>
</ul>
<h2 id="analysis-sequence">Analysis sequence</h2>
<p>Several analyses scripted in this repository were not ultimately included in the manuscript, though we discuss them in the Supporting Information. The following sequence of scripts, located in the <code>scr</code> folder, perform the analyses reported in the paper:</p>
<ul>
<li><code>namcs/namcs-scrape-stata.r</code>: Download the STATA files for the NAMCS data set and pre-process them into a data frame.</li>
<li><code>ccs.r</code>: Download Clinical Classifications Software ontology data and pre-process it into a data frame.</li>
<li><code>ontology-maps.r</code>: Assemble several ontologies used by the data sets into a set of crosswalks between them.</li>
</ul>
<p>The following scripts constitute the sensitivity analysis of pairwise comorbidity network analysis, using different evidential and evaluative thresholds to construct networks:</p>
<ul>
<li><code>graphs/pairs-extract.r</code>: Pre-process each provided comorbidity data set (except MIMIC-III) into a standardized comorbidity data frame.</li>
<li><code>graphs/pairs-extract-mimic.r</code>: Pre-process the MIMIC-III diagnosis data into a standardized comorbidity data frame.</li>
<li><code>graphs/pairs-augment.r</code>: Augment the comorbidity data frames with values of statistical tests and association measures.</li>
<li><code>graphs/pairs-summary.r</code>: Calculate density and weight distribution summaries, which don’t require graph construction but only comorbidity (pairs) data.
<ul>
<li><code>tab/densities.tex</code> (manuscript)</li>
<li><code>tab/weights-*.tex</code></li>
</ul></li>
<li><code>graphs/quantiles.r</code>: Compare the ranges of quantiles obtained using different evaluative cutoffs for different association measures. (Experiment by changing the values in <code>measure_cutoff</code>.) We used the values obtained here in <code>R/params.r</code>.</li>
<li><code>graphs/visualizations.r</code>: Render hairball visualizations of each comorbidity network using a consistent, strict cutoff that produces more interpretable images.
<ul>
<li><code>fig/network-viz-*.pdf</code></li>
<li><code>fig/network-ggplot-*.pdf</code> (SI)</li>
</ul></li>
<li><code>graphs/statistics-degseq.r</code>: Fit several distribution families to the degree sequence data of each comorbidity network and perform likelihood-ratio tests.
<ul>
<li><code>data/graphs/graphs-degseq.rds</code></li>
</ul></li>
<li><code>graphs/statistics-degseq-summary.r</code>: Render heatmap and write LaTeX table summarizing likelihood-ratio tests.
<ul>
<li><code>tab/degseq-compare.tex</code> (SI text)</li>
<li><code>fig/degseq-compare.pdf</code> (SI)</li>
</ul></li>
<li><code>graphs/statistics-degseq-tail.r</code>: Fit three regularly varying (power-law) models to the degree sequence data of each comorbidity network.</li>
<li><code>graphs/statistics-degseq-tailstats.r</code>: Summarize the results of the tail estimation analysis.
<ul>
<li><code>tab/tail-estimation.tex</code> (SI text)</li>
</ul></li>
<li><code>graphs/statistics-calculate.r</code>: Calculate summary statistics for comorbidity networks constructed using each data set and parameter choices.
<ul>
<li><code>fig/fits.pdf</code></li>
<li><code>data/graphs/graphs-summary.rds</code></li>
</ul></li>
<li><code>graphs/statistics-summary.r</code>: Analyze the summary statistics calculated above using visualization, multiple (hierarchical) regression, and principal components analysis.
<ul>
<li><code>tab/summ.csv</code> (SI)</li>
<li><code>fig/summ-versus.pdf</code> (SI)</li>
<li><code>fig/summ-pval.pdf</code></li>
<li><code>tab/model1.tex</code> (SI text)</li>
<li><code>tab/model2.tex</code> (SI text)</li>
<li><code>tab/model3-stargazer.tex</code> (manuscript)</li>
<li><code>tab/model3-xtable.tex</code></li>
<li><code>tab/model4-stargazer.tex</code> (SI text)</li>
<li><code>tab/model4-xtable.tex</code></li>
<li><code>tab/aic.tex</code> (SI text)</li>
<li><code>fig/pca.pdf</code></li>
<li><code>fig/pca-label.pdf</code></li>
<li><code>fig/pca-label-flip.pdf</code> (manuscript)</li>
</ul></li>
<li><code>graphs/centralities-concordance.r</code>: Calculate concordances between three centrality rankings on the comorbidity networks constructed using each data set and parameter choices.
<ul>
<li><code>data/sensitivity/concordance-data.RData</code></li>
</ul></li>
<li><code>graphs/centralities-prevalences.r</code>: Plot centrality versus prevalence for the disorders in the ontology of each data set.
<ul>
<li><code>fig/centrality-prevalence.pdf</code></li>
</ul></li>
<li><code>graphs/centralities-node-group.r</code>: Compare the most-central disorders in unweighted comorbidity networks constructed using a fixed data set but varying evidential cutoffs.
<ul>
<li><code>data/sensitivity/node-centrality.RData</code></li>
<li><code>data/sensitivity/group-centrality.RData</code></li>
</ul></li>
<li><code>graphs/centralities-kendall.r</code>: Render heatmaps of Kendall correlations between node centrality rankings for constructions within a dataset and for datasets within an ontology.
<ul>
<li><code>fig/centrality-heatmap-construction.pdf</code></li>
<li><code>tab/centrality-concordance-inertia.tex</code> (manuscript)</li>
<li><code>fig/centrality-decomps.pdf</code></li>
<li><code>fig/centrality-biplots.pdf</code> (SI)</li>
<li><code>fig/ont-cent.pdf</code></li>
<li><code>fig/centrality-heatmap-dataset-*.pdf</code></li>
<li><code>fig/centrality-biplots-dataset-*.pdf</code> (SI)</li>
</ul></li>
<li><code>graphs/centralities-top.r</code>: Write LaTeX tables of most-central disorders.
<ul>
<li><code>tab/centrality-&lt;centrality&gt;-&lt;ontology&gt;.tex</code> (SI text)</li>
<li><code>tab/centrality-&lt;centrality&gt;.tex</code> (manuscript + SI)</li>
</ul></li>
</ul>
<p>The following scripts constitute the comparative analysis between pairwise and more systems-style network constructions using partial correlations and joint distribution models:</p>
<ul>
<li><code>multivar/data-css.r</code>: Download Clinical Classifications Software ontology data and pre-process it into a tibble.
<ul>
<li><code>data/ccs-dat.rds</code></li>
</ul></li>
<li><code>multivar/data-namcs.r</code>: Format the NAMCS data as a tibble and summarize it.
<ul>
<li><code>data/namcs/namcs.rds</code></li>
</ul></li>
<li><code>multivar/data-namcs-jd.r</code>: Fit the JDM to the NAMCS data and save the result.
<ul>
<li><code>data/namcs/namcs-jd0.rds</code></li>
<li><code>data/namcs/namcs-jd1.rds</code></li>
</ul></li>
<li><code>multivar/summary-namcs.r</code>: Provide a basic summary of the NAMCS data and network.
<ul>
<li><code>fig/namcs-freqrank.pdf</code> (SI text)</li>
</ul></li>
<li><code>multivar/pairs-namcs.r</code>:
<ul>
<li><code>fig/namcs-pairs-alluvial-alluvium.pdf</code></li>
<li><code>fig/namcs-pairs-alluvial-flow.pdf</code> (SI text)</li>
</ul></li>
<li><code>multivar/visualize-namcs.r</code>: Generate association strength scatterplots and hairball diagrams to compare the pairwise correlation, partial correlation, and joint distribution network models for the NAMCS data.
<ul>
<li><code>fig/namcs-scatterplots.pdf</code> (manuscript)</li>
<li><code>fig/namcs-mds-biplots.pdf</code> (SI text)</li>
<li><code>fig/&lt;circular layout plot&gt;.pdf</code> (manuscript)</li>
</ul></li>
<li><code>multivar/data-mimic.r</code>: Read the <code>DIAGNOSES</code> tables from the demo and full MIMIC-III databases, partitioned by care unit, and write presence-absence tables for CCS diagnosis groups.
<ul>
<li><code>data/mimic/mimic-ccs.rds</code></li>
</ul></li>
<li><code>multivar/data-mimic-pw-pt.r</code>: Summarize and analyze the positive, negative, and indiscernible associations produced by the pairwise correlation, partial correlation, and joint distribution network models for the NAMCS data.
<ul>
<li><code>data/mimic/mimic-*-pw.rds</code></li>
<li><code>data/mimic/mimic-*-pt.rds</code></li>
</ul></li>
<li><code>multivar/data-mimic-jd.r</code>: Construct joint distribution networks for each care unit.
<ul>
<li><code>data/mimic/mimic-*-&lt;JDM parameters&gt;-jd.rds</code></li>
</ul></li>
<li><code>multivar/summary-mimic.r</code>: Summarize the comorbidity network for each care unit, across the hierarchical interpolation between the pairwise and partial constructions.
<ul>
<li><code>fig/mimic-*-freqrank.pdf</code> (SI text)</li>
</ul></li>
<li><code>multivar/pairs-mimic.r</code>: Summarize and analyze the positive, negative, and indiscernible associations produced by the pairwise correlation, partial correlation, and joint distribution network models for each unit.
<ul>
<li><code>tab/mimic-links.tex</code> (manuscript)</li>
<li><code>fig/mimic-pairs-alluvial-flow.pdf</code> (SI text)</li>
<li><code>fig/mimic-pairs-alluvial-alluvium.pdf</code></li>
</ul></li>
<li><code>multivar/visualize-mimic.r</code>: Generate association strength scatterplots and hairball diagrams to compare the pairwise correlation, partial correlation, and joint distribution network models for each unit.
<ul>
<li><code>fig/mimic-*-scatters.pdf</code> (manuscript)</li>
<li><code>fig/mimic-*-scatters-jdm.pdf</code></li>
<li><code>fig/mimic-*-networks.pdf</code> (SI)</li>
</ul></li>
</ul>
<p>This bash script copies figures and tables generated directly from the above scripts to their numbered names (e.g. “Figure 1”) in the manuscript:</p>
<ul>
<li><code>docs/comorbidity/comorbidity-figs-tabs.sh</code>: Copy figures and tables from the <code>fig</code> and <code>tab</code> folders in the root directory into <code>docs/comorbidity</code> for inclusion in the manuscript.</li>
</ul>
