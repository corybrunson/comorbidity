---
title: |
  | Supporting information for the paper:
  | Sensitivity and robustness of comorbidity network analysis
author:
  - Jason Cory Brunson
  - Thomas P. Agresta
  - Reinhard C. Laubenbacher
output:
  pdf_document:
    fig_caption: yes
    keep_tex: yes
    number_sections: no
header-includes:
  - \usepackage{dcolumn}
  - \usepackage{geometry}
  - \usepackage{pdflscape}
  - \usepackage{bm}
  - \usepackage{rotating}
  - \usepackage{float}
  - \usepackage{setspace}
  - \usepackage{lineno}
  - \usepackage{natbib}
---

<!--
header-includes:
  - \linenumbers
  - \doublespacing
-->

\newcommand{\Beta}{{\mathrm{B}}}
\newcommand{\Kappa}{{\mathrm{K}}}
\newcommand{\Rho}{{\mathrm{P}}}

\renewcommand\thefigure{S\arabic{figure}}
\setcounter{figure}{0}
\renewcommand\thetable{S\arabic{table}}
\setcounter{table}{0}

# Methods

## Detailed discussion of the pairwise models

### Data sets

As noted in the main text, the data sets from which we obtained pairwise contingency tables vary widely:

- @Rzhetsky2007 used a coarse custom ontology mapped from level-5 ICD9 codes.[^Rzhetsky-mapping] Their pairwise data are unique among this collection in including contingency tables for *every* pair of disorders.
- The MedPAR data used by @Hidalgo2009 concern the specific subpopulation of Medicare patients over age 65, and were made available both according to full ICD9 and to the level-3 prefixes of these codes. Both data sets are limited to a subset of pairs, though the authors do not indicate how this subsetting was done.
- Sct. Hans is a psychiatric hospital, so this dataset concerns that specific subpopulation. @Roque2011 restricted their attention (and their public dataset[^Roque-dataset]) to 802 pairs of codes with Benjamini--Hochberg p-values $p<.01$ and odds ratios $OR>2$.
- @Hanauer2013 restricted the study dataset to pairs of codes that appeared in at least 30 patient records each and in at least 10 records together. The study focused on temporal relationships between diagnoses but the dataset was not trimmed to exclude pairs that were not temporally related (c.f. @Jensen2014).
- @Bagley2016 adopted the custom ontology of @Rzhetsky2007. They collected records in 2013 and excluded records before 2008, internally inconsistent records, patients older than 90, conditions with prevalence below 50, and pairs of conditions with co-occurrence below 5. They also included the Columbia data from @Rzhetsky2007, re-processed in this way, which we designate with an asterisk (e.g. "Columbia\*" in the densities table in the main text.
- The data in MIMIC-III is pooled only from hospital admissions that included a stay at any of five intensive care units; like the others mentioned, this subpopulation may exhibit peculiar traits.

In most cases, $\chi^2$ tests on the contingency tables yielded p-values ranging from $0$ to $1-\varepsilon$, $\varepsilon<10^{-3}$. The exception was the data from Sct. Hans, which included only highly discernible comorbid pairs ($p<10^{-4}$).

[^Rzhetsky-mapping]: See SI (<http://www.pnas.org/content/suppl/2007/06/21/0704820104.DC1>), Appendix 3.
[^Roque-dataset]: See SI (<http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1002141#s5>), Dataset S1.


### Binary association measures

We used four binary association measures (BAMs) to evaluate the strength of association between disorders in the pairwise models:

- The _odds ratio_ $OR=\frac{a/b}{c/d}$ is widely used in clinical research for its interpretability, comes recommended as a standard measure of epidemiological comorbidity [@Kraemer1995], and was used in some comorbidity network studies [@Hanauer2009;@Hanauer2013;@Kim2016]. When  the binary variables are independent, it has expected value $1$.
In most analyses, in order to avoid infinite link weights, we substitute the modified maximum likelihood estimate of the odds ratio, $\widehat{OR}=\frac{(a+.5)(d+.5)}{(b+.5)(c+.5)}$ [@Parzen2002].
- _Pearson's binary correlation coefficient_ $\phi=\frac{ad-bc}{\sqrt{(a+b)(a+c)(b+d)(c+d)}}$, designated A~30~ (and misidentified as the tetrachoric) in Hubalek's [-@Hubalek1982] review of BAMs, was used in several exploratory comorbidity network studies [@Hidalgo2009;@Folino2012;@Chen2014a;@Chmiel2014;@Lai2015]. Being a correlation coefficient, its values range from $-1$ to $1$, with independent variables having expected value $0$.
- _Forbes' coefficient of association_ $F=\frac{a/(a+b)}{(a+c)/(a+b+c+d)}$, designated A~40~ by @Hubalek1982, was often used together with $\phi$ and sometimes called "relative risk" [@Hidalgo2009;@Chmiel2014;@Chen2014a]---though it is distinct from that term's two usual referents, the risk ratio formula and $OR$ [@Zhang1998]. Like $OR$, $F$ has expected value $1$ when the variables are independent.
<!--
- the _Jaccard index_ $J=\frac{a}{a+b+c}$, designated A~4~ by @Hubalek1982, was used in several link prediction studies, usually to measure associations between patients rather than diseases [@Steinhaeuser2009;@Davis2011;@Folino2012;Bauer-Mehren2013]. While it was not often used to construct comorbidity networks _per se_, we include it here to represent an option with potentially strong theoretical justification: In ecological settings where many species may be rare, so that their absence is not indicative of the inhabitability of a site and their co-occurrences are more often due to chance, a measure that excludes $d$ from its calculation will be more faithful to this theory and more robust to the rates at which rare species are detected [@Podani2000]. Similarly, in healthcare settings many underlying disorders may not be recorded at any specific encounter, and patients with higher incidence rates, who receive more care, will be overrepresented in the population for which data has been recorded.
-->

We additionally use the _tetrachoric correlation coefficient_ $r_t$.
To motivate this statistic, consider that one can generate a $2\times 2$ contingency table by sampling ordered pairs $(Z_1,Z_2)\sim N(\bm{\mu},\Sigma)$ from a bivariate standard normal distribution with means $\mu=(\mu_1,\mu_2)^\mathsf{T}$ and covariance matrix
$$\arraycolsep=1pt
\Sigma=\left(\begin{array}{cc}
{\sigma_1}^2 & \rho\sigma_1\sigma_2 \\
\rho\sigma_1\sigma_2 & {\sigma_2}^2
\end{array}\right),$$
and then dichotomize the samples $(z_1,z_2)^\mathsf{T}$ according to sign:
$$y_i=
\begin{cases}
0 & z_i \leq 0 \\
1 & z_i > 0
\end{cases}$$
The marginal probabilities $P(D_i)$ for the table are then determined by the scaled means $\mu_1/\sigma_1$ and $\mu_2/\sigma_2$, while the joint probabilities deviate from the products of the marginals according to the linear correlation $\rho$ between the latent variables. Under this model, $r_t$ is the value of $\rho$ for which the joint probabilities are in proportion to the empirical data [@Drasgow2006].
While disease diagnoses are discrete events, the underlying normal distributions can be interpreted as distributions of health and risk, so that a diagnosis occurs when the value exceeds a certain threshold.


## Detailed discussion of the multivariate models

### Partial correlation networks

The statistical tests conventionally used to identify comorbidities fail to account for an important source of variation actually contained within the underlying incidence data: associations with disorders beyond the index pair.
One well-known mechanism is common causation, when, for instance, two disorders are clinically unrelated but have a common risk factor that results in a strong association between them in the population.
Comorbidity network researchers are aware that such "transitive" correlations are likely and have suggested that clustering patterns should be examined with such explanations in mind [@Hanauer2009]; though such confounding can also result in real, possibly causal comorbidities being obscured.

Partial correlations provide a way to control for these confounding effects from the correlation matrix calculated from pairwise data. In the case of normally-distributed data $\mathbf{y}\sim N(\mathbf{0},\Sigma)$ with $\operatorname{diag}\Sigma=({\sigma_1}^2,\ldots,{\sigma_n}^2)^\mathsf{T}$, the _partial correlations_
$$r(y_i,y_j\mid\mathbf{y}\smallsetminus\{y_i,y_j\})=-\frac{\kappa_{ij}}{\sqrt{\kappa_{ii}\kappa_{jj}}}$$
between $y_i$ and $y_j$, obtained using the precision matrix $\Sigma^{-1}=\Kappa=(\kappa_{ij})$, are the standardized coefficients $\beta_{ij}{\sigma_j}/{\sigma_i}=\beta_{ji}{\sigma_i}/{\sigma_j}$ obtained by maximum-likelihood estimation for multiple regression models predicting $y_i$ from $\mathbf{y}\smallsetminus\{y_i\}$ and vice-versa. @Epskamp2017 recommend modeling continuous multivariate data as a network with links weighted by their partial correlations.
When multivariate binary data are modeled using latent multivariate normal distributions, this technique can be applied to pairwise tetrachoric correlations, yielding _partial tetrachoric correlations_ ${r_t}'$. We take this approach to our data sets to construct comorbidity networks based on partial latent correlations.

The multiple regression framework controls for confounding of the effect on $y_i$ of $y_j$ by treating the remaining $y_k$ as independent covariates. In reality, the correlation between two disorders may depend on interaction effects between other disorders as well as on their raw incidence.
Thus the partial correlation network is only a first-order correction to confounding among the response variables. We next consider a multivariate model that encodes the complete set of interactions as a covariance matrix and takes a Bayesian approach to estimate them simultaneously.


### Joint distribution networks

In addition to endogenous confounding by other disorders, spurious effects can also arise when exogenous factors, i.e. patient-level covariates, are ignored: If specific demographic subgroups (e.g. elderly or infirm patients) are at heightened risk of many otherwise physiologically unrelated disorders, failure to stratify the analysis by these demographics will result in artificially inflated correlations among these disorders.
Several comorbidity network studies incorporated demographic predictors into their binary association tests [@Rzhetsky2007;@Feldman2016;@Glicksberg2016], though this information was unavailable for our sensitivity analysis.

Comorbidity network researchers have also acknowledged the problem of multiple comparisons, which becomes acute in this setting due to the combinatorial explosion in the number of subsets of an already large number of variables. Even the coarse ontology of @Rzhetsky2007 presents 12,880 pairings among its 161 concepts, which would require a $p=4\times 10^{-6}$ threshold on each pairwise test to obtain a family-wise error rate of $.05$. Comorbidity network researchers have differed between adopting a classical [@Hanauer2013] versus a corrected cutoff [@Bagley2016].
Even the corrections considered in the previous phase do not resolve the problem of dependency: the tests rely on the same underlying population and the same response variables, as do the partial correlation networks described previously.

The multiple comparisons problem can be obviated by adopting a hierarchical modeling framework, in which grouping effects are treated not as independent but as sampled from some higher-order distribution with its own central tendency and spread [@Gelman2012]. Variation between groups is then partially attributed to this spread, so that estimated group averages "shrink" toward the population average. Meanwhile, the appropriate framework for modeling multiple potentially correlated (or anti-correlated) response variables is multivariate regression, which encodes the interactions between the responses as a covariance matrix $\Sigma$. Once such a model is fitted to incidence data, a network model can be inferred from the estimated covariance matrix $\hat\Sigma$ by normalizing $\hat\Sigma$ to an estimated correlation matrix $\hat\Rho$ (Rho), dropping statistically weak entries, and interpreting the result as a weighted adjacency matrix.
In addition to incorporating endogenous effects among these responses, this framework admits exogenous predictors in the same manner as classical regression.

We adapted for these purposes the _joint distribution model_ (JDM) proposed by @Pollock2014 to allow ecologists to incorporate both species--environment relationships (exogenous effects) and species--species interactions (endogenous effects).
The model includes a matrix $B$ of exogenous effects interpretable in the traditional manner, a matrix $\bm{\Rho}$ of contributions from the exogenous factors to the observed co-occurrence rates, and a matrix $\Rho$ of latent correlations interpretable as comorbidities in the same way as tetrachoric correlations. Indeed, this model generalizes the latent bivariate normal model underlying the tetrachoric correlation coefficient, so that the off-diagonal entries of the residual correlation matrix $\hat{\Rho}$ can be directly compared with the $r_t$.


# Results

## Additional results from the pairwise analysis

### Link determination

In addition to network densities, we also calculated the quintiles of each BAM for each dataset, restricted to associations remaining at each TWER, in order to compare the distributions of the BAMs over the comorbid associations. The quintiles are reported in Tables \ref{tab:weights-1} and \ref{tab:weights-2}.
The BAM distributions are not highly sensitive to the TWER, and the direction of change (whether quintiles increase or decrease as the TWER is tightened) is inconsistent from dataset to dataset. This indicates that, in  general, the amount of evidence for a comorbid association is not predictive of its strength. Some network properties may therefore be more sensitive to an evaluative cutoff than to an evidential cutoff.
Once a fixed TWER has been imposed, BAM quintiles typically differ by no more than a factor of 10.

<!--graphs/pairs-summary.r-->

\input{../../../tab/weights-1.tex}
\input{../../../tab/weights-2.tex}

<!--graphs/visualizations.r-->

\begin{figure}[h!]
\caption{For each data source (row), evaluative cutoff (column), and BAM (abscissa), a box-and-whisker plot of the quantiles at which the links of each comorbidity network were trimmed by the cutoff. The networks were constructed over the range of evidential (p-value) cutoffs $10^{-i},\ i=1,\ldots,6$ and for each p-value correction (none, FWER, FDR).}
\label{fig:quantile-boxplot}
\end{figure}

\begin{figure}[h!]
\caption{Network diagram (hairball plot) of the comorbidity network constructed from the Columbia data, using the evidential cutoff $\alpha<5\%$ with Bonferroni correction and the evaluative cutoff $OR\geq 6$. Clusters identified using the Walktrap algorithm are color-coded.}
\label{fig:network-ggplot-columbia}
\end{figure}

\begin{figure}[h!]
\caption{Network diagram (hairball plot) of the comorbidity network constructed from the MedPAR data on level-3 ICD9 codes, using the evidential cutoff $\alpha<5\%$ with Bonferroni correction and the evaluative cutoff $OR\geq 6$. Clusters identified using the Walktrap algorithm are color-coded.}
\label{fig:network-ggplot-medicare1993-3}
\end{figure}

\begin{figure}[h!]
\caption{Network diagram (hairball plot) of the comorbidity network constructed from the Sct. Hans data, using the evidential cutoff $\alpha<5\%$ with Bonferroni correction and the evaluative cutoff $OR\geq 6$. Clusters identified using the Walktrap algorithm are color-coded.}
\label{fig:network-ggplot-scthans1998-2008}
\end{figure}

\begin{figure}[h!]
\caption{Network diagram (hairball plot) of the comorbidity network constructed from the Stanford data, using the evidential cutoff $\alpha<5\%$ with Bonferroni correction and the evaluative cutoff $OR\geq 6$. Clusters identified using the Walktrap algorithm are color-coded.}
\label{fig:network-ggplot-stanford2013}
\end{figure}

\begin{figure}[h!]
\caption{Network diagram (hairball plot) of the comorbidity network constructed from the Columbia\* data, using the evidential cutoff $\alpha<5\%$ with Bonferroni correction and the evaluative cutoff $OR\geq 6$. Clusters identified using the Walktrap algorithm are color-coded.}
\label{fig:network-ggplot-columbia2013}
\end{figure}

### Degree sequence distributions

Table \ref{tab:degseq-compare} reports the results of the LRTs performed between different families of distributions on the tails of the comorbidity network degree sequences. (A diverging-color tiling visualization is provided in Figure \ref{fig:degseq-compare}.) In general, the distributions are preferred in the following order:
$$\text{log-normal}\ >\ \text{exponential}\ >\ \text{power law}\ >\ \text{Poisson}$$
Comorbidity networks have veritably heavy tails that are poorly modeled by Poisson distributions.
Only in one case (MedPAR(3)) was the power-law fit clearly preferred over the exponential fit, and in no cases was it preferred over the log-normal fit. As has been observed of many other real-world networks using this approach [@Broido2018], comorbidity networks tend to be better-modeled by log-normal distributions.

There is no consistent relationship, controlling for the dataset, between the choice of evidential threshold and the strength of evidence for one family over another. For example, the Bonferroni correction to the MedPAR(3) network construction reversed the observations that node degrees were better modeled by a power law or a log-normal than an exponential, whereas the conclusions for the MedPAR(5) network essentially did not change. In general, though, tightening the evidential threshold had the effect of weakening the preferred order described in the previous paragraph.

Table \ref{tab:tail-estimation} reports the estimators obtained under the hypothesis of regular variation. The method often failed to produce estimators; in cases where more than one estimator was calculated, usually at least one was negative (hence uninterpretable). In the 14 cases (out of 48 attempts) in which at least two estimators were positive, which were based on degree sequences obtained for MedPAR(5), Michigan, and MIMIC, they took dissimilar values and were fitted to the degree sequence tail at different scales.
Moreover, there was no evident relationship between the estimators obtained using the three implemented methods (Adjusted Hill, moments, and kernel-type) across the networks.

<!--graphs/statistics-degseq-summary.r-->

\begin{landscape}
\input{../../../tab/degseq-compare.tex}
\end{landscape}

\begin{figure}[h!]
\caption{Diverging-color tilings of log-likelihood ratios $R$ from likelihood-ratio tests (LRT) between fits of different families of models to degree sequence tails. LRTs were performed for graphs constructed from each dataset using evidential cutoff $\alpha<0.05$ and both corrections. Family 1 is preferred when $R>0$, Family 2 when $R<0$. The boundary color of each tile indicates whether $R$ is positive or negative. The p-value from the LRT is printed on each tile.}
\label{fig:degseq-compare}
\end{figure}

\input{../../../tab/tail-estimation.tex}

### Single-valued summary statistics

<!--graphs/statistics-summary.r-->

A priori of regression modeling, several global network statistics are clearly dependent on the choice of TWER ($\alpha$). After some basic transformations to increase linearity, their values at each graph we constructed are plotted against $\alpha$ in Figure \ref{fig:summ-pval}.
These plots suggest that the effect of $\alpha$ on many global statistics varies by dataset. For example, the density increases more dramatically in the MedPAR(5) and MIMIC-III data sets as the evidential threshold is weakened, with corresponding changes in other statistics including the proportion of nodes in the largest component and the skewness of the degree sequence as measured by the Gini index and the $\mu$ parameter of the log-normal fit. However, this difference may be an artifact of the limited data provided for the other networks; co-occurrence data for most pairs was only available for MedPAR(3) and MedPAR(5) and for all pairs for MIMIC and Columbia, and indeed the patterns for Columbia and MedPAR(3) are less pronounced but similar to those for MedPAR(5) and MIMIC.
Different populations seem to exhibit different comorbidity network structure in terms of the raw values of several global statistics, but our observations are consistent with the choice of TWER having a broadly similar effect on this structure regardless of the source.

\begin{table}[h!]
\caption{Values of several global statistics calculated on comorbidity networks constructed over a range of data sets and parameter settings. See the section on global properties in the main text.}
\label{tab:summ}
\end{table}

\begin{figure}[h!]
\caption{Mean values of several global statistics calculated on comorbidity networks constructed over a range of data sets and parameter settings, plotted against several measures of network size. See the section on global properties in the main text.}
\label{fig:summ-pval}
\end{figure}

\begin{landscape}
\input{../../../tab/model1.tex}
\end{landscape}

A plausible alternative modeling approach would have been to treat the data source as a random effect, sampled from an underlying distribution, on the values of the network statistics. Though only few data sets were used, we fit hierarchical regression models to the same data as a robustness check.
The results were consistent with those for the classical models (Tables \ref{tab:hlm1-stargazer}, \ref{tab:hlm1-xtable}, \ref{tab:hlm2-stargazer}, and \ref{tab:hlm2-xtable}).
The Akaike Information Criterion [@Burnham2004], which penalizes increased model complexity as well as un-accounted for variance as a corrective to overfitting, favored the classical models with respect to every network statistic (Table \ref{tab:aic}).

\begin{landscape}
\input{../../../tab/model3-stargazer.tex}
\input{../../../tab/model4-stargazer.tex}
\end{landscape}

\input{../../../tab/model3-xtable.tex}
\input{../../../tab/model4-xtable.tex}
\input{../../../tab/aic.tex}

<!--
\begin{landscape}
\input{../../../tab/model2-quantile.tex}
\end{landscape}

\begin{landscape}
\input{../../../tab/model4-quantile.tex}
\end{landscape}

\input{../../../tab/model4-quantile-variance.tex}
-->

<!--<community detection>-->

<!--
\begin{figure}
\footnotesize
\noindent\fbox{
  \parbox{.47\textwidth}{
    Community detection (edge betweenness)
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    Hierarchical clustering (complete-linkage)
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    AA metabolism (branched)\\
    AA metabolism (straight-chain)\\
    AA metabolism (sulfur-bearing)\\
    {\bfseries AA transport}\\
    Acidosis\\
    Aciduria\\
    {\bfseries Acute leukemia}\\
    {\bfseries Acute promyelocytic leukemia}\\
    Alcoholism\\
    Alkalosis\\
    Alzheimer's\\
    Anaerobes\\
    Aortic aneurysm\\
    {\bfseries Aplastic anemia}\\
    Breast cancer F\\
    {\bfseries Breast cancer M}\\
    {\bfseries Budd-Chiari s.}\\
    Bundle branch block\\
    {\bfseries Burkitt's lymphoma}\\
    Carcinoma in situ\\
    Cardiomyopathy\\
    {\bfseries Celiac sprue}\\
    Cholelithiasis\\
    {\bfseries Cystic fibrosis}\\
    Diabetes type 1\\
    Diabetes type 2\\
    Goodpasture's s.\\
    {\bfseries Gout}\\
    {\bfseries Helicobacter pilori}\\
    Hepatitis C\\
    Hepatitis E\\
    {\bfseries Hodgkin's disease}\\
    Hyperosmolality\\
    Hypertrophic cardiomyopathy\\
    Hypoglycemia\\
    Hypoosmolality\\
    Lown-Ganong-Levine s.\\
    {\bfseries Lymphosarcoma}\\
    {\bfseries Mineral met. (Ca)}\\
    Mineral met. (Cu)\\
    {\bfseries Mineral met. (Fe)}\\
    {\bfseries Mineral met. (Mg)}\\
    Pick's d.\\
    {\bfseries Plague}\\
    Pneumonia\\
    Poliomyelitis\\
    {\bfseries Reticulosarcoma}\\
    Staphilococcus\\
    Urea cycle met.\\
    {\bfseries Vitamin deficiency}\\
    WPW s.
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    {\bfseries AA transport}\\
    {\bfseries Acute leukemia}\\
    {\bfseries Acute promyelocytic leukemia}\\
    Ankylosing spondylitis\\
    {\bfseries Aplastic anemia}\\
    {\bfseries Breast cancer M}\\
    {\bfseries Budd-Chiari s.}\\
    Buphthalmos\\
    {\bfseries Burkitt's lymphoma}\\
    Carbohydrate transport and metabolism\\
    {\bfseries Celiac sprue}\\
    {\bfseries Cystic fibrosis}\\
    Dermatomyositis-polymyositis\\
    Diphtheria\\
    Giant cell arteritis\\
    {\bfseries Gout}\\
    {\bfseries Helicobacter pilori}\\
    Hepatitis A\\
    {\bfseries Hodgkin's disease}\\
    Hypersensitivity angiitis\\
    Ichthyosis congenita\\
    Kawasaki's d.\\
    Keratoderma\\
    Lethal midline granuloma\\
    {\bfseries Lymphosarcoma}\\
    {\bfseries Mineral met. (Ca)}\\
    {\bfseries Mineral met. (Fe)}\\
    {\bfseries Mineral met. (Mg)}\\
    {\bfseries Plague}\\
    Polyarteritis nodosa\\
    {\bfseries Reticulosarcoma}\\
    Rheumatoid arthritis\\
    Salmonella\\
    Scleroderma\\
    Sjogren's s.\\
    Systemic lupus erythematosus\\
    Takayasu's arteritis\\
    Thrombotic thrombocytopenic purpura\\
    Tuberculosis\\
    {\bfseries Vitamin deficiency}\\
    Wegener's granulomatosis\\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
  }
}
\caption{
Overlapping clusters obtained from the Columbia dataset, using complete-linkage hierarchical clustering on the $1/OR$ distance matrix (right) and fast--greedy community detection on the comorbidity network constructed using a Bonferroni-corrected $0.05$ TWER (left).
The hierarchical clustering tree was cut so as to maximize the adjusted Rand index of the two partitions of the concepts.
Concepts shared by both clusters are rendered in boldface.
\label{fig:overlap-clusters-fg}
}
\end{figure}

\begin{figure}
\footnotesize
\noindent\fbox{
  \parbox{.47\textwidth}{
    Community detection (fast--greedy)
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    Hierarchical clustering (complete-linkage)
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    {\bfseries Acidosis}\\
    Actinomycosis\\
    {\bfseries Alcoholism}\\
    {\bfseries Alkalosis}\\
    Allergic rhinitis\\
    Alopecia\\
    {\bfseries Alzheimer's}\\
    {\bfseries Anaerobes}\\
    Ankylosing spondylitis\\
    {\bfseries Aortic aneurysm}\\
    Aplastic anemia\\
    Benign neoplasms\\
    {\bfseries Bipolar disorder}\\
    Breast cancer F\\
    {\bfseries Bundle branch block}\\
    Carbohydrate transport and metabolism\\
    Carcinoma in situ\\
    {\bfseries Cardiomyopathy}\\
    {\bfseries Cholelithiasis}\\
    {\bfseries Depression}\\
    {\bfseries Diabetes type 1}\\
    {\bfseries Diabetes type 2}\\
    Epilepsy\\
    Erythematosquamous dermatosis\\
    Giant cell arteritis\\
    Goiter\\
    Gout\\
    {\bfseries Gram-negative bacteria}\\
    Helicobacter pilori\\
    Hepatitis A\\
    {\bfseries Hepatitis B}\\
    {\bfseries Hepatitis C}\\
    {\bfseries HIV}\\
    {\bfseries Hyperosmolality}\\
    Hypersensitivity angiitis\\
    Hypoglycemia\\
    {\bfseries Hypoosmolality}\\
    Keratoderma\\
    Lipid metabolism d.\\
    {\bfseries Meningococcus}\\
    Migraine\\
    Mineral met. (Ca)\\
    Mineral met. (Fe)\\
    Mineral met. (Mg)\\
    Multiple sclerosis\\
    Mumps\\
    {\bfseries Parkinson's d.}\\
    {\bfseries Pneumonia}\\
    {\bfseries Poliomyelitis}\\
    Psoriasis\\
    Reticulosarcoma\\
    Rheumatoid arthritis\\
    {\bfseries Schizophrenia}\\
    Scleroderma\\
    {\bfseries Shigella}\\
    Sjogren's s.\\
    {\bfseries Staphilococcus}\\
    Streptococcus\\
    Systemic lupus erythematosus\\
    Tuberculosis\\
    Virus\\
    Vitamin deficiency
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    {\bfseries Acidosis}\\
    {\bfseries Alcoholism}\\
    {\bfseries Alkalosis}\\
    {\bfseries Alzheimer's}\\
    {\bfseries Anaerobes}\\
    {\bfseries Aortic aneurysm}\\
    {\bfseries Bipolar disorder}\\
    {\bfseries Bundle branch block}\\
    {\bfseries Cardiomyopathy}\\
    {\bfseries Cholelithiasis}\\
    {\bfseries Depression}\\
    {\bfseries Diabetes type 1}\\
    {\bfseries Diabetes type 2}\\
    {\bfseries Gram-negative bacteria}\\
    {\bfseries Hepatitis B}\\
    {\bfseries Hepatitis C}\\
    {\bfseries HIV}\\
    {\bfseries Hyperosmolality}\\
    Hypertrophic cardiomyopathy\\
    {\bfseries Hypoosmolality}\\
    Leprosy\\
    {\bfseries Meningococcus}\\
    {\bfseries Parkinson's d.}\\
    {\bfseries Pneumonia}\\
    {\bfseries Poliomyelitis}\\
    {\bfseries Schizophrenia}\\
    {\bfseries Shigella}\\
    {\bfseries Staphilococcus}\\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
  }
}
\caption{
Overlapping clusters obtained from the Columbia dataset, using complete-linkage hierarchical clustering on the $1/OR$ distance matrix (right) and edge betweenness community detection on the comorbidity network constructed using a Bonferroni-corrected $0.05$ TWER (left).
The hierarchical clustering tree was cut so as to maximize the adjusted Rand index of the two partitions of the concepts.
Concepts shared by both clusters are rendered in boldface.
\label{fig:overlap-clusters-eb}
}
\end{figure}
-->

### Centrality rankings of disorders

<!--
See @Mallik2018 for one illustration of how centrality measures can differ in value on correlation networks.
-->

\begin{figure}[h!]
\caption{For each data source and centrality measure, a correlation biplot of Kendall correlations between centrality rankings of disorders based on comorbidity networks constructed using the evidential cutoff $\alpha<0.05$, each of three corrections (none, FWER, FDR), and each of five BAMs (unit, odds ratio, Pearson correlation, Forbes coefficient, and tetrachoric). In this and other biplots, first and second eigenvectors are reversed if necessary so that each centroid lies in the first quadrant.}
\label{fig:centrality-biplots}
\end{figure}

A closer inspection of the effect of the TWER and of its corrections on node centralities (not shown) reveals that the largest inconsistencies in rank, at least in terms of degree and betweenness, tend to occur among the least central disorders. Thus, the highly central "hub" disorders are robust to the TWER.
In contrast, the rankings suggest very different central cores in networks constructed from different data sources.
The main text provides lists of the 8 most central disorders in each dataset, under betweenness centrality.
Tables \ref{tab:centrality-degree} and \ref{tab:centrality-closeness} provide analogous information based on degree and closeness centrality.
Distance-based measures are more discriminating when graphs are more modular; because the stricter evidential cutoffs produced more modular graphs, we calculated centralities for these tables from the comorbidity networks based on 5% TWERs with FDR correction.

<!--graphs/centralities-top.r-->

\input{../../../tab/centrality-degree.tex}
\input{../../../tab/centrality-closeness.tex}

A legitimate concern with centrality analysis is that high centrality is only possible via many links, which may only be discernible due to high prevalence. That is, if two disorders have the same centrality in a "ground truth" network of all population-level comorbidities, the evidential cutoff makes the one with higher prevalence more likely to be centrally located in the comorbidity network (Table \ref{tab:prevalence}).
While we found a clear positive relationship between prevalence and centrality, and a certain minimum prevalence is required for a certain centrality, neither is strongly predictive of the other (not shown).

<!--graphs/prevalences.r-->

\input{../../../tab/prevalence.tex}

\begin{figure}[h!]
\caption{Scatterplots of prevalences of disorders in the ICD9 ontology in different data sets using this ontology. Note the effect of the restriction, in the Michigan dataset, to disorders that appeared on at least 30 patient records.}
\label{fig:prevalences-ICD9-5}
\end{figure}

\begin{figure}[h!]
\caption{Scatterplots of prevalences of disorders in the Rzhetsky ontology in different data sets using this ontology. Note that the disorders included in the Columbia\* dataset exactly match their prevalences in the Columbia dataset.}
\label{fig:prevalences-Rzhetsky}
\end{figure}

Among networks constructed from the three data sets that used the full level-5 ICD9 ontology, centrality rankings were discordant (Figures \ref{fig:centrality-biplots-dataset-Rzhetsky}, \ref{fig:centrality-biplots-dataset-ICD9-3}, and \ref{fig:centrality-biplots-dataset-ICD9-5}): While those of MedPAR(5) and Michigan were moderately, though positively, correlated, that of MIMIC-III was anti-correlated with both, and highly so with MedPAR(5). The underlying patient populations were quite different in these cases, though (Medicare beneficiaries, a regional adult population, and ICU patients, respectively), which may entirely account for these differences.
The node centrality rankings from the three data sets that used the Rzhetsky ontology (Columbia, Stanford, and Columbia\*) were also moderately concordant by each of the three centrality measures.

Based on the mappings to level-3 ICD9 codes and to the Rzhetsky ontology, the group centrality rankings for these data sets become more concordant: more correlated between MedPAR(5) and Michigan and less anti-correlated between both and MIMIC.
We see only very weak concordance between any of the group centrality rankings and the node centrality rankings, under either mapping.
We include tables of most-central disorders for each ontology in Tables \ref{tab:centrality-degree-ICD9-3} through \ref{tab:centrality-closeness-Rzhetsky}.

<!--graphs/centralities-kendall.r-->

<!--
\begin{figure}
\includegraphics[width=\textwidth]{../../../fig/centrality-heatmap-dataset-ICD9-5.pdf}
\includegraphics[width=\textwidth]{../../../fig/centrality-heatmap-dataset-ICD9-3.pdf}
\includegraphics[width=\textwidth]{../../../fig/centrality-heatmap-dataset-Rzhetsky.pdf}
\caption{
Heatmaps of correlations between node centrality vectors for comorbidity networks constructed from different datasets by imposing a fixed evidential cutoff (the Bonferroni correction to $p<.05$) and without weighting the links, using each centrality measure (degree, betweenness, and closeness).
In each comparison, only datasets with the same index ontology, or else a finer ontology with a many-to-one mapping to the index ontology, were included. In the latter cases, the centrality measures are in fact group centralities, not node centralities. The ontologies proceed from finest to coarsest: level-5 ICD9 (top), level-3 ICD9 (middle), Rzhetsky (bottom). Group betweenness centralities were not computed, so only node betweenness centrality rankings are compared. See the text for details.
\label{fig:centrality-heatmap-dataset}
}
\end{figure}
-->

\begin{figure}[h!]
\caption{For each p-value correction and centrality measure, a correlation biplot of Kendall correlations between centrality rankings of disorders based on comorbidity networks constructed from each data set that uses or could be crosswalked to the Rzhetsky ontology. Centrality measures are node-based for data encoded using this ontology and group-based for data crosswalked to this ontology.}
\label{fig:centrality-biplots-dataset-Rzhetsky}
\end{figure}

\begin{figure}[h!]
\caption{For each p-value correction and centrality measure, a correlation biplot of Kendall correlations between centrality rankings of disorders based on comorbidity networks constructed from each data set that uses or could be crosswalked to the level-3 ICD9 ontology. Centrality measures are node-based for data encoded using this ontology and group-based for data crosswalked to this ontology.}
\label{fig:centrality-biplots-dataset-ICD9-3}
\end{figure}

\begin{figure}[h!]
\caption{For each p-value correction and centrality measure, a correlation biplot of Kendall correlations between centrality rankings of disorders based on comorbidity networks constructed from each data set that uses the level-5 ICD9 ontology.}
\label{fig:centrality-biplots-dataset-ICD9-5}
\end{figure}

<!--graphs/centralities-top.r-->

<!--
Tables \ref{tab:centrality-degree-ICD9-3} through \ref{tab:centrality-closeness-Rzhetsky} present the few most central disorders in each dataset using consistent ontologies, based on the same comparisons used to generate Figure \ref{fig:centrality-heatmap-dataset}.
-->

\input{../../../tab/centrality-degree-Rzhetsky.tex}
\input{../../../tab/centrality-degree-ICD9-3.tex}

\input{../../../tab/centrality-closeness-Rzhetsky.tex}
\input{../../../tab/centrality-closeness-ICD9-3.tex}

## Additional results from the multivariate analysis

### Correlation structure and link determination for NAMCS encounters

<!--multivar/summary-namcs.r-->

\begin{figure}
\centering
\includegraphics[width=.5\textwidth]{../../../fig/namcs-freqrank.pdf}
\caption{Frequency--rank plot for the 13 chronic disorders recorded in the NAMCS sample.}
\label{fig:namcs-freqrank}
\end{figure}

Figure \ref{fig:namcs-pairs} compares the estimated disease correlations under the four models.
The switch from the pairwise tetrachoric scheme to JDM0 shifted the correlation estimates negatively by a consistent magnitude, with no evident scaling. A linear model with fixed slope $1$ estimated the shift at $-0.174$.
The relationship between the pairwise and partial correlations is noisier, and is not clearly captured by a linear model with fixed intercept $0$ or by one with fixed slope $1$---that is, by shifting or by scaling.
The correlation estimates in JDM1 are offset according as the exogenous effects were positive or negative and possibly scaled by a non-unit factor. A linear model with a binary term for the sign of the exogenous effect estimated the slope at $.89$.

<!--multivar/???.r-->

\begin{figure}[h!]
\includegraphics[width=\textwidth]{../../../fig/namcs-pairs.pdf}
\caption{
Scatterplots between the pairwise tetrachoric correlations $r_t$, the full partial correlations ${r_t}'$, and the correlation point estimates $\hat\rho_0$ and $\hat\rho_1$ from the JDMs for the NAMCS population.
The area of each point is in proportion to the product $n_1n_2$ of the prevalences of the two disorders---equivalently, to their expected co-occurrence under the null hypothesis of no comorbidity.
\label{fig:namcs-pairs}
}
\end{figure}

The $18\times 13=234$ effect estimates of the exogenous covariates $\hat\Beta$ on the prevalence of each chronic disease are reported in Table \ref{tab:jdm-coefs}, and Figure \ref{fig:namcs-hists} summarizes these estimates, together with the component of each of the $78$ pairwise correlations accounted for by these exogenous effects.
The effects on prevalence were distributed unimodally around zero with a standard deviation near $\frac{3}{5}$.
The effects on co-occurrence $\hat{\bm{\Rho}}$ were substantial, relative to the residual correlations, cleanly split between positive ($49$) and negative ($29$). The positive and negative effects were centered about the median values $.86$ and $-.55$, and the positive effects were themselves bimodally distributed.

\input{../../../tab/jdm-coefs.tex}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{../../../fig/namcs-hists.pdf}
\caption{
Histograms of estimates from the joint distribution model with exogenous (patient-level) predictors.
Left: Exogenous effects on disorder prevalence.
Right: Correlation (epidemiological comorbidity) accounted for by exogenous effects.
\label{fig:namcs-hists}
}
\end{figure}

Differences between the pairwise network on one hand, and the partial correlation and endogenous-only JDM networks on the other, were due to the loss of information on how the co-occurrences of disorder pairs were concentrated in certain subsets of patients when moving from the patient--disorder occurrence data to the disorder--disorder contingency tables (endogenous covariance).
Meanwhile, the discrepancy between the residual networks of the two JDMs, primarily the weakening or disappearance of both positive and negative correlations, indicates that these correlations were due in part to differences in co-occurrence across specific demographic groups (exogenous covariance).
Each systems model (partial correlation or JDM) tended to reduce comorbidity estimates, relative to the conventional model, but there were rich disagreements among these three, especially between the partial correlation network and the JDMs on whether several disease pairs were positively versus negatively associated (Figure \ref{fig:namcs-pairs-alluvial-alluvium}).

<!--multivar/pairs-namcs.r-->

\begin{figure}
\centering
\includegraphics[width=.667\textwidth]{../../../fig/namcs-pairs-alluvial-alluvium.pdf}
\caption{
Alluvial diagram of discernible signs of the population-level associations between NAMCS chronic disorders, using each of 4 network models: pairwise correlation, full partial correlation, endogenous joint distribution, and joint distribution controlling for exogenous predictors.
\label{fig:namcs-pairs-alluvial-alluvium}
}
\end{figure}

<!--multivar/visualize-namcs.r-->

\begin{figure}[h!]
\caption{For each of four models, a correlation biplot of estimated latent correlations between 13 chronic disorders in the NAMCS sample.}
\label{fig:namcs-biplots}
\end{figure}

### Correlation structure and link determination for MIMIC-III units

<!--multivar/summary-mimic.r-->

\begin{figure}[h!]
\caption{For each critical care unit in the MIMIC-III database, a frequency--rank plot for the recorded diagnoses, crosswalked to CCS codes.}
\label{fig:mimic-freqrank}
\end{figure}

The scatterplots in Figure \ref{fig:mimic-csru-pairs} compare correlation estimates for each pair of disorders in the CSRU data, which are representative of the others (excluding the neonatal units).
As with NAMCS, and consistent with expectations, the correlation estimates on average weakened (or turned negative) from the pairwise model to the other models. However, this trend was much more modest than with the chronic disorders recorded in NAMCS.

<!--multivar/pairs-mimic.r-->

\begin{figure}[h!]
\includegraphics[width=\textwidth]{../../../fig/mimic-csru-pairs.pdf}
\caption{
Scatterplots between the pairwise tetrachoric correlations $r_t$, the full partial correlations ${r_t}'$, and the correlation estimates $\hat\rho_0$ from the endogenous JDM for the CSRU population.
\label{fig:mimic-csru-pairs}
}
\end{figure}

\begin{figure}[h!]
\includegraphics[width=\textwidth]{../../../fig/mimic-pairs-alluvial-alluvium.pdf}
\caption{
Alluvial diagrams of discernible signs of the population-level associations between MIMIC diagnoses within each admission unit cohort, using each of 3 network models: pairwise correlation, full partial correlation, and endogenous joint distribution.
\label{fig:mimic-pairs-alluvial-alluvium}
}
\end{figure}

<!--multivar/visualize-mimic.r-->

\begin{figure}[h!]
\caption{Three comorbidity networks constructed from the CCU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.}
\label{fig:mimic-ccu-networks}
\end{figure}

\begin{figure}[h!]
\caption{Three comorbidity networks constructed from the CSRU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.}
\label{fig:mimic-csru-networks}
\end{figure}

\begin{figure}[h!]
\caption{Three comorbidity networks constructed from the MICU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.}
\label{fig:mimic-micu-networks}
\end{figure}

\begin{figure}[h!]
\caption{Three comorbidity networks constructed from the NICU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.}
\label{fig:mimic-nicu-networks}
\end{figure}

\begin{figure}[h!]
\caption{Three comorbidity networks constructed from the NWARD population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.}
\label{fig:mimic-nward-networks}
\end{figure}

\begin{figure}[h!]
\caption{Three comorbidity networks constructed from the SICU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.}
\label{fig:mimic-sicu-networks}
\end{figure}

\begin{figure}[h!]
\caption{Three comorbidity networks constructed from the TSICU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.}
\label{fig:mimic-tsicu-networks}
\end{figure}

<!--multivar/centrality-mimic.r-->

\begin{figure}[h!]
\caption{For each care unit and centrality measure, a correlation biplot of Kendall correlations between centrality rankings of disorders based on each of four comorbidity network models: pairwise correlations, full partial correlations, and a joint distribution model without exogenous effects.}
\label{fig:mimic-centrality-biplots}
\end{figure}

<!--
# To generate the PDF, execute the following on the command line:
pandoc comorbidity-si.md \
-o comorbidity-si.pdf \
-s \
--bibliography=../comorbidity.bib \
--csl=../network-modeling-analysis-in-health-informatics-and-bioinformatics.csl
-->
