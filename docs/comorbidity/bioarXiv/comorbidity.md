---
title: "Sensitivity and robustness of comorbidity network analysis"
author:
  - Jason Cory Brunson
  - Thomas P. Agresta
  - Reinhard C. Laubenbacher
output:
  pdf_document:
    fig_caption: yes
    keep_tex: yes
    number_sections: no
header-includes:
  - \usepackage{dcolumn}
  - \usepackage{geometry}
  - \usepackage{pdflscape}
  - \usepackage{bm}
  - \usepackage{rotating}
  - \usepackage{float}
  - \usepackage{morefloats}
  - \usepackage{setspace}
  - \usepackage{lineno}
  - \usepackage{natbib}
  - \setcitestyle{numbers}
---

<!--
header-includes:
  - \linenumbers
  - \doublespacing
-->

<!--\newcommand{\pkg}[1]{{\fontseries{b}\selectfont #1}}-->
\newcommand{\pkg}[1]{#1}

\newcommand{\Beta}{{\mathrm{B}}}
\newcommand{\Kappa}{{\mathrm{K}}}
\newcommand{\Rho}{{\mathrm{P}}}

\pagebreak


# Summary and Keywords

**Background:**
Comorbidity network analysis (CNA) is an increasingly popular approach in systems medicine, in which mathematical graphs encode epidemiological correlations (links) between diseases (nodes) inferred from their occurrence in an underlying patient population. A variety of methods have been used to infer properties of the constituent diseases or underlying populations from the network structure, but few have been validated or reproduced.

**Objectives:**
To test the robustness and sensitivity of several common CNA techniques to the source of population health data and the method of link determination.

**Methods:**
We obtained six sources of aggregated disease co-occurrence data, coded using varied ontologies, most of which were provided by the authors of CNAs. We constructed families of comorbidity networks from these data sets, in which links were determined using a range of statistical thresholds and measures of association. We calculated degree distributions, single-value statistics, and centrality rankings for these networks and evaluated their sensitivity to the source of data and link determination parameters.
From two open-access sources of patient-level data, we constructed comorbidity networks using several multivariate models in addition to comparable pairwise models and evaluated differences between correlation estimates and network structure.

**Results:**
Global network statistics vary widely depending on the underlying population. Much of this variation is due to network density, which for our six data sets ranged over three orders of magnitude. The statistical threshold for link determination also had strong effects on global statistics, though at any fixed threshold the same patterns distinguished our six populations. The association measure used to quantify comorbid relations had smaller but discernible effects on global structure.
<!--
Partitions of comorbidity networks using community detection algorithms were consistent on networks constructed using different statistical thresholds on the same dataset but difficult to compare between data sets based on different disease ontologies. These partitions also contrasted starkly with partitions obtained using conventional clustering methods on the sample correlation matrix.
-->
Co-occurrence rates estimated using multivariate models were increasingly negative-shifted as models accounted for more effects. However, only associations between the most prevalent disorders were consistent from model to model.
Centrality rankings were likewise similar when based on the same dataset using different constructions; but they were difficult to compare, and very different when comparable, between data sets, especially those using different ontologies. The most central disease codes were particular to the underlying populations and were often broad categories, injuries, or non-specific symptoms.

**Conclusions:**
CNAs can improve robustness and comparability by accounting for known limitations.
In particular, we urge comorbidity network analysts
(a) to include, where permissible, disaggregated disease occurrence data to allow more targeted reproduction and comparison of results;
(b) to report differences in results obtained using different association measures, including both one of relative risk and one of correlation;
<!--
(c) when partitioning a network, to report the results of distinct methods;
-->
(c) when identifying centrally located disorders, to carefully decide the most suitable ontology for this purpose;
and,
(d) when relevant to the interpretation of results, to compare them to those obtained using a multivariate model.

**Keywords:**

<!--MeSH terms-->
Humans,
Odds Ratio,
Systems Biology,
Comorbidity,
Systems Analysis,
Medical Informatics,
Epidemiologic Measurements,
Epidemiologic Methods,
<!--other keywords-->
network analysis,
comorbidity network,
sensitivity analysis,
covariance estimation

<!--
**Abbreviations:**

Test-wise error rate (TWER);
binary association measure (BAM);
International Classification of Diseases, Ninth Revision (ICD9);
principal components analysis (PCA);
largest connected component (LCP);
acute posthemorrhagic anemia (APHA);
supporting information (SI).
See also Table \ref{tab:datasets}.
-->


# Introduction \label{sec:introduction}


## Background

The importance of comorbidity to medical research and practice is fundamental. Usually, the term refers to morbidity that complicates an index condition for an individual patient, which produces population-level associations in epidemiological studies and clinically relevant differences in prognosis or response to treatment and which analyses must take into account [@VandenAkker1996;@Valderas2009]. Recently, several research teams have taken a systems approach to population comorbidity by analyzing "comorbidity networks" (also "disease networks", "disease graphs", and maps of "disease space" or of the "diseasome") aggregated from measures of co-occurrence between pairs or among small groups of disorders [@Emmert-Streib2013;@Capobianco2015]. Their investigations seek to uncover novel clinical associations, to stratify patient populations, and to predict disease progression, among other aims [@Brunson2017].

<!--
Graph-theoretic approaches to association data have proved fruitful in a range of disciplines [@Brandes2013], including systems biology [@Pavlopoulos2011]. Converting subject-level expression or incidence data to (often unweighted) network models results in the loss of covariance, sign, and magnitude information. This conversion makes the analysis of large data sets more tractable, but network construction requires researchers to make multiple choices that have compounding effects. Few and limited attempts have been made to reproduce earlier results, as detailed for some cases below. There is also a conceptual mismatch between the tools designed to investigate actor-oriented networks and the questions motivating variable-oriented association studies.
-->

In this section we review the recent comorbidity network analysis literature and articulate some concerns with the methods used. In subsequent sections, we test how sensitive the network statistics produced, and by extension the kinds of inferences drawn, in these studies are to the source of data and the method of network construction. We conclude with a set of recommendations that we think will strengthen the practical value of future comorbidity network analyses.


## Conventional comorbidity network analysis

_Systems medicine_ consists in the adoption into medical research of principles and techniques from systems biology, which in turn are described as global, integrative, and holistic [@Hood2013;@Ayers2015;@Kirschner2016].
Networks are a prime illustration, having become a staple of systems biology [@Pavlopoulos2011] and seen extensive use in systems medicine [@Gietzelt2016].
Since 2009, alongside an expansion of the scope and scale of social network analysis in medicine, network analysis has accelerated the systems approach to human health [@Brunson2017].

Any complex system can be conceptualized as a network, and a range of structures are called network models, but _network science_ rests predominantly on the theory of mathematical graphs, and therefore on the discretization of underlying relations.
Converting subject-level expression or incidence data to (often unweighted) network models results in the loss of covariance, sign, and magnitude information.
This conversion makes the analysis of large data sets more tractable, as network models occupy far less memory than incidence data and can be analyzed and simulated using highly efficient algorithms.
In most applications, however, network analysis relies on theoretical assumptions that do not necessarily hold for association data [@Brandes2013].
As a consequence, systems analyses of high-dimensional data may be presented as network analyses despite making little or no use of graph theory, an observation made by @Williams2014 of systems biology and later by @Brunson2017 of systems medicine. For example, comorbidity network studies are often strictly dyadic---focused on pairwise relations but not on dependencies among these relations---as in the search for new or poorly understood associations between clinical concepts such as diagnosed disorders [e.g. @Hanauer2009;@Hanauer2013]. Meanwhile, classical techniques such as hierarchical clustering [e.g. @Roque2011] and ordination [e.g. @Lyalina2013] preserve more information from association data than graph-tailored alternatives like community detection and force-directed layouts.

There is also a conceptual mismatch between the tools designed to investigate actor-oriented networks and the questions motivating variable-oriented association studies.
Much of contemporary network science appropriates graph-theoretic operationalizations of such sociological concepts as community, centrality, and brokerage, which have natural and important applications in population health, health economics, and other socio-structural research using relational data. However, these concepts have also been applied to network models of association data, without the motivations and interpretations that would attend a theoretical foundation independent of these social scientific roots, and health informatics is no exception.
This begins with the very concept of a relation, or link, which is usually defined for pairs of clinical concepts by imposing an evidential or evaluative cutoff on a measure of association between binary variables, e.g. rejecting a null hypothesis of no association at $p<.05$ based on a chi-squared test [@Davis2011] or having an odds ratio greater than 3 [@Hanauer2009].
The observations of @Hidalgo2009, for example---that the lethality of a disease correlates with its connectivity and that health states tend to progress from less to more connected diseases---may depend on disease prevalences and other features of the population [c.f. @Chmiel2014] and in part be artifacts of sample size [c.f. @Blair2013] or of patient-level covariates [c.f. @Rzhetsky2007].

We do not suggest that applications of graph theory to population comorbidity data are inherently problematic. The aforementioned observations of @Hidalgo2009 have been reproduced [@Glicksberg2016], the strengths of association between disorders are consistent across a range of data sets [@Blair2013], and several studies using graph-theoretic methods have yielded interesting results.
For example, @Chen2014a used a random walk--based notion of distance to rank the comorbidities of different cancers, in an effort to identify targets for fruitful follow-up laboratory research [@Chen2015].
Other studies have illustrated ways in which network analysis complements geometric techniques: @Lyalina2013 visualized co-occurrence data using both principal component analysis biplots and co-occurrence network layouts, from which they gained complementary insights to the problem of differentiating between distinct disorders with shared symptoms.
Nevertheless, the irrigidity with which links---the observational units of network analysis---are determined and interpreted in this setting call for additional caution.


## Objectives \label{sec:objectives}

We articulate four concerns with the conventional approach:[^collection]

1. Sources of disease incidence data differ in their conventions, completeness, and representativeness, and the consequent differences in comorbidity network structure have not been explored.
2. The method of link determination, meaning the determination of whether to link two disorder codes in the network model based on a statistical signal or strength measure on the co-occurrence data, varies across studies. What effect does imposing an error rate correction on the p-values obtained from pairwise $\chi^2$ tests, or using a binary correlation coefficient to measure the strength of association instead of the odds ratio, have on the properties of the resulting network?
3. A network model aggregated from pairwise correlations discards information about interactions among larger subsets of variables, though these potential effects are important both to the network perspective and to the systems paradigm.
4. The use of network statistics like connectivity, distance, and centrality to characterize networks of disorders depends on correspondences between the definitions and values of these statistics and the theoretical constructs they are being used to measure. Little theoretical guidance is available for the study of networks representing statistical associations rather than directly observed relations.

We address concerns 1 and 2 in a sensitivity analysis.
In the following sections, we describe an analysis pipeline to characterize a pairwise co-occurrence dataset using several single-valued network summary statistics and node centrality. We evaluate the sensitivity of the results to the source of data, the choice of association measure, and the strength of evidence and of association used to determine links.
We address concern 3 in a comparison of a typical pairwise construction to three alternative constructions that control for progressively more factors when estimating co-occurrence rates. For this analysis we focus on the effects of the modeling choice on these estimates.
Finally, we touch upon concern 4 in the Discussion section after reviewing the results of these analyses.
Our goal in the present study is to better understand any systematic biases built in to the comorbidity network approach and how they might be addressed.

[^collection]: These concerns apply to the analysis of disease co-occurrence data, not to their collection, with which a different body of work continues to raise and address concerns.


# Summary of Analyses \label{sec:summary}

We conducted robustness and sensitivity analyses of several network-analytic results of the kind that have been reported in the CNA literature. See the full Methods and Results sections below for detailed discussions and citations.

Six of our data sets, all used for pairwise analysis, were provided by the authors of previous CNAs; one, the intensive care unit database MIMIC-III, is freely available for research use; and one, results of the 2011 National Ambulatory Medical Care Survey (NAMCS), was obtained from the website of the Centers for Disease Control and Prevention.
Our pairwise study examined the effects of the data source; the test-wise error rate (TWER) at which links were determined (evidential threshold); how, if at all, TWERs were corrected for multiple comparisons; the binary association measure (BAM) used to weight links; and the value of the BAM, if any, at which to prune links (evaluative threshold).
A second study compared the pairwise approach to multivariate network constructions on the available incidence-level data sets. These comparisons held other parameters fixed and examined correlation structures directly as well as their network models.

Comorbidity networks have been described as "scale-free" based on the power-law appearance of their degree distributions. However, rigorous tests of this hypothesis, based on competing interpretations of power-law fitting, agree that comorbidity network degree distributions do _not_ follow power laws. This held true across the range of pairwise constructions.
(The degree distributions tended to more closely follow log-normal distributions.)

CNAs often summarize networks in terms of global statistics such as degree assortativity, clustering coefficient, and modularity, but the choices of summary statistics are evidently arbitrary.
Regressions of several global network statistics on the construction parameters revealed that different data sources yield highly distinct networks, though specific cases suggest that pre-processing choices can be equally important.
Most global statistics change as expected with graph density, whether due to stricter evidential or evaluative cutoffs; the exception was the clustering coefficient, which may depend on the interaction between these cutoffs due to indirect comorbidities such as common risk factors.
A principal components analysis demonstrated that variability was explained primarily by graph density, followed by data source, and that networks from different sources were better distinguished under stricter cutoffs.

Studies often identify the disorders that occupy highly central positions ("hubs") in comorbidity networks, with the expectation that they are likely to be more fruitful targets of epidemiological, biological, or clinical follow-up.
We quantitatively compared full rankings of three common centrality measures between networks constructed from data sets using the same ontology, and we described and compared the several most central disorders within and between ontologies.
Full rankings were highly sensitive to the evidential cutoff (TWERs) and/or the BAM, and which parameter was more determinative varied by data source.
Networks constructed from regional EHR data had assorted and often non-specific symptoms and disorders at their centers, including epilepsy, limb pain, respiratory problems, vitamin deficiency, benign neoplasms, and tuberculosis. Other data sources produced their own distinctive hubs. These were usually explained by sheer numbers of epidemiological comorbidities, in part due to high prevalence consistent with their specific subpopulations.
In both settings the same disorders usually topped the rankings by different centrality measures.

Though comorbidity networks are aggregated from pairwise associations in incidence data, rigorous tests of epidemiological comorbidity account for patient-level covariates as well as associations with other population-level disorders. We compared pairwise constructions to two multivariate constructions using partial correlations and joint distribution models (JDMs) on the NAMCS and MIMIC-III data, separated into subpopulations by care unit.
As expected, comorbidities between less prevalent disorders were often indiscernible in the multivariate models, which controlled for the effects of disorders outside each pair. Compared to pairwise correlations, which in all networks were frequently strong and overwhelmingly positive, partial and JDM correlations were on average negative-shifted, including many more negative associations.
Consequently, the resulting network models were much sparser and included many more negative links.
At least for highly prevalent disorders, JDM correlation estimates were better predicted by pairwise correlations than by partial correlations.
A centrality analysis yielded similar results to that of the pairwise constructions: Hubs were robust to the choice of model, while overall centrality rankings were only weakly concordant and no group two models produced similar rankings across all units.


# Discussion \label{sec:discussion}

We summarize the results of our analyses in Section \ref{sec:results}, with references to supporting figures and tables. In this section, we discuss the implications of these results for the practice of comorbidity network analysis and recommend an analytic workflow informed by these implications. CNA ranges widely in motivations and methodologies, but we believe that a study that follows our recommendations will arrive at more robust and interpretable results.


## Sensitivity of pairwise network structure \label{sec:sensitivity}

We explored the dependence of certain network properties on certain parameters of comorbidity network construction. These parameters include the data source (usually an EHR or claims database), the evidential threshold (p-value) used to discern links, the method of correcting for multiple comparisons (if any), and the binary association measure (BAM) used to prune weak links and/or to weight remaining links. The properties considered are network density; shape of the degree sequence; several single-valued global statistics having to do with the degree sequence, local link dependencies, and geodesic distances; and several measures of node centrality. These are a fraction of the possible constructions and properties and may represent only a minority of those that have so far been published. Nevertheless, our observations allow us to draw several provisional but clear conclusions about the methodological robustness of CNA.

One observation, about the shapes of their degree sequences or distributions, is unequivocal.
While the proper way to define and detect power laws from empirical data is contested [@Broido2018;@Voitalov2018a], proposed standards from both camps [@Gillespie2015;@Voitalov2018] lead to the same conclusion, that the degree sequences of comorbidity networks do _not_ follow power laws.
This runs counter to a frequent claim within the literature. While most CNA researchers have inferred from supposed scale-freeness only the existence of highly-connected "hubs" [@Steinhaeuser2009;@Ball2011;@Divo2018], the assumption underpins some more advanced analytic techniques used to prune weak associations prior to analysis [@Jiang2018] or to model network growth based on the accumulation of database records [@Scott2014].
Such techniques may yet be useful, but the weaknesses at their foundations should not be overlooked.

A second observation concerns the structure of comorbidity networks more generally.
While the network architecture is sensitive to every parameter of construction, the underlying population is far more determinative than the choices of link determination or of weighting scheme. This holds true for all of the properties we evaluated. This is a favorable result for the genre, since the goal of many recent comorbidity network applications has been to assess differences in the structures of comorbidity networks derived from different populations [@Warner2015;@Glicksberg2016;@Feldman2016].
For example, @Divo2018 introduced a case--control study design to CNA to compare network hubs between COPD and non-COPD patient populations, in order to better identify potential targets for follow-up research or clinical intervention specific to COPD.
While our secondary use of processed data prevents us from separating the effects of population, practice, and pre-processing, our analysis supports the premiss that it can be meaningful to identify comorbidity network properties that are characteristic of certain populations.

While comorbidity networks constructed from different data sets are clearly discriminable by their global properties, these differences are largely orthogonal to those due to changes in network density: decreased density (increased sparsity), due to tightening of evidential or evaluative cutoffs, results in more skewed degree distributions and less modular structure. Meanwhile the underlying populations are better distinguished by rates of overall connectedness, assortative linking, and triad closure. To characterize comorbidity networks from a moderate number of populations, then, it may be more useful to report such statistics as connected component size distribution, assortativity, and clustering coefficient than the average geodesic distance, Gini index, or modularity.

When comorbidity networks are weighted, on the other hand, the choice of weight may strongly impact the results. This is based on three observations: First, the shape of the weight distribution differs substantially between relative risk--like BAMs (the Forbes coefficient and the odds ratio) and correlation coefficients (the Pearson binary and the tetrachoric).
Second, BAM cutoffs appear to be more important determinants than p-value cutoffs.
Third, node centrality can depend dramatically and idiosyncratically on which BAM is used to measure and prune network links.
Sensitivity to the choice of weights has long been observed in the literature [@Hidalgo2009], but the motivation for selecting one measure over others for CNA often goes unstated. Comparisons are only possible between some published comorbidity network studies because certain exploratory conventions, in particular the combined use of $F$ and $\phi$, have been adopted by later investigators (see Section \ref{sec:linkdetermination}).

<!--
Our observations about clustering and community structure are more problematic. Complete-linkage hierarchical clustering failed to produce a reasonably coarse "natural" partition of the disorders in the Columbia network, based on the popular Dunn index. If a partition is needed in this case, the number of clusters must be manually determined. Modularity minimization also yielded ambiguous results, with three community detection methods producing partitions in three different domains of coarseness: $\approx 10$, $\approx 50$, and $\approx 80$ clusters. Worse still, criteria commonly used to evaluate these partitions were in disagreement: modularity was maximized at very coarse cuts to the hierarchical clustering tree, while Dunn's index for the community detection--based partitions was closer to its values for random partitions than to those along the clustering tree.
These inconsistencies may be due to the comorbidity network simply having little natural modular structure. However, it is highly intuitive for diseases to form clusters of co-occurrence in populations, several studies have described such clusters, and even our results suggest strong overlap between the clusters obtained using different methods. Future studies should carefully validate any clusters of diseases or other clinical concepts observed from correlational data in order to clarify what can be inferred about a population based on such clusters.
-->

Finally, as mentioned above, node centralities are quite robust to the choice of evidential cutoff, but less robust to the choice of BAM used to impose a evaluative cutoff. This is especially true for "hubs", the nodes in each network with the highest centrality. Different disorders may be identified as hubs by different centrality measures, but the same small subset of hubs tends to appear in networks constructed from any specific population.
<!--ref. Matsumoto 2012-->


## Multivariate network models

We compared two multivariate network modeling approaches, partial correlation networks and joint distribution networks, to the conventional pairwise correlation network approach. In this comparison, all models were based on latent, normally-distributed risk factors and produced estimates of the correlations among them. Both multivariate approaches had the intended and expected effects of reducing both the estimated values of and the nominal statistical evidence for most pairwise associations, which are interpreted as comorbidities.
Models deeper into the systems paradigm represented comorbidities as being less aligned with each other or with a single dimension of poor health. As a result, more negative associations, particularly involving codes for depression and cancer versus those for metabolic and cardiac disorders, emerged.
Also as expected, associations between more prevalent disorders were more robust to the choice of model and would therefore lead to more consistent interpretations.

In addition to having fewer links, the multivariate models estimated consistently more negative associations than the pairwise, at least among more prevalent disorders. Indeed, the overall reduction in association estimates included some positive pairwise associations shaking out as negative in the multivariate models. In this way the multivariate models appear to be preventing errors both of magnitude and of sign [@Gelman2014].
Based on these patterns, we expect in general that models that account for more sources of variation will discern strong evidence for fewer epidemiological comorbidities but for more population-level disassociations.

The partial correlation and joint distribution models yielded radically different alternatives to the pairwise model, the former large and sparse, the latter small and dense.
The pairwise correlation estimates among more prevalent disorders were roughly linearly related both to the partial correlation estimates and to the joint distribution estimates.
However, this relationship broke down among the less prevalent disorders.
Taking the joint distribution models to provide more realistic summaries, this suggests that estimating pairwise disease associations from a larger diseaseome introduces a uniform positive bias in the measured associations, which increases the apparent strengths of the comorbid relations and mistakes some null relationships, and possibly some relationships of mutual exclusion, as comorbidities.
Moreover, introducing exogenous covariates into a multivariate model yielded bimodally-distributed effect estimates, which indicate that much of the perceived interaction among disorders, in addition to their prevalence, can be accounted for by demography and environment.

The linearity of some of these relationships stands in stark contrast to the non-linear relationships observed between estimates obtained using joint distribution models and classical co-occurrence indices not based on the same latent structure [@Pollock2014].
This reaffirms the importance of comparing like with like, insofar as this is possible, even amidst fundamentally different modeling approaches.

We emphasize that these multivariate models no more reveal "true" relationships than pairwise models. Indeed, the inclusion or exclusion of certain patient-level predictors or other diseases has the potential to again upend the results. Rather, as the authors of several comorbidity networks have stressed, these "phenome-wide" analyses must be understood as exploratory; the interplay between specific diseases is best understood through more narrowly-targeted research.
Certainly far more clinically relevant epidemiological comorbidities exist in critical care populations than were captured by the partial correlation and joint distribution networks. Nevertheless, these networks serve as valuable checks on conventional constructions, which are certain to contain many indirect and spurious associations.

## Interpretability of network statistics \label{sec:interpretation}

Though we focus on the robustness of the calculations---the reliability of the numerical results---equally important is the soundness of the interpretations---their validity.
Networks are an increasingly popular analysis tool for high-dimensional data sets. Though the ways in which their properties are interpreted draws heavily from analyses of social networks and networks based on other relational data (e.g. electrical signals, protein interactions), their construction is fundamentally different.
The conclusions we draw about complex systems from network models of their constituent interactions must be informed by the process that converts the raw data to the network model.

The use of centrality measures is a case in point.
The degree of a disorder, calculated as the number of disorders it is comorbid with in a patient population, is sensible enough a measure of its "total comorbidity" [@Hidalgo2009] and a useful concept both epidemiologically and clinically. The weights (using BAMs) associated with these comorbid disorders are also clearly useful for discriminating between stronger and weaker co-occurrence rates, hence higher or lower risk factors for patients with an index disorder. However, we found that the choice of disease ontology has a significant impact on comorbidity rankings, so much so that the centralities of disorders before crosswalking to a coarser ontology are not predictive of the centralities of their counterparts after crosswalking. (See the Results section for more detail.)

Meanwhile, none of the weights commonly used to quantify the strengths of comorbidities are _additive_: For example, using one typical network construction,[^typical1] amebiasis, a gastrointestinal infection rare in the United States, and rheumatoid arthritis, a common chronic autoimmune disorder, have 5 and 67 comorbid relations, respectively. Though having very different etiologies and afflicting very different patient populations, these disorders have approximately the same weighted degree (529 and 561). This does not translate to their being similarly severe in any recognized sense, or to their belonging at a similar ranking amidst the other disorders in the ontology.

[^typical1]: Construct a comorbidity network using the Rzhetsky data and ontology with a Bonferroni-corrected $.05$ evidential cutoff and weighting the links by the Forbes coefficient $F$ (and restricting to positive links).

Betweenness and closeness centrality rely on a different version of additivity that is equally problematic. Take the same network, except invert the link weights[^typical2] to indicate distance rather than strength of association. The geodesic distance between type 1 diabetes and breast cancer (in female patients) is then the same as that between multiple epiphyseal dysplasia (MED) and hepatitis E (HepE), approximately $.30$, though the former two disorders are significantly correlated (i.e. directly linked, $p<10^{-27}$) while the latter two can only be reached from each other via three intermediate disorders: multiple epiphyseal dysplasia $\leftrightarrow$ Albright--Sternberg syndrome $\leftrightarrow$ cerebral palsy $\leftrightarrow$ hepatitis C $\leftrightarrow$ HepE.
Yet this indirect sequence of associations leading to HepE is does not have an established clinical interpretation, nor does it imply a natural comparison to the relative risk of MED encoded by the direct link. Indeed, controlling for covariates and subsetting populations may significantly alter the magnitudes and signs of the associations in the sequence, with unpredictable effects on the resulting indirect distance measure.

[^typical2]: To avoid infinities, replace each weight $F$ with $\frac{1}{F-1}+1$.

These limitations are better revealed by weighting schemes, but they arise from the network model itself, which is premised on a principle of "guilt by association" that implicates one node in the effects of another according to their proximity in the network. As @Hou2014 point out in the related field of genomics, this principle "does not reflect the dynamic nature of biological networks". The same may be said of epidemiological networks. As in genomics, comorbidity network centrality analysis is demonstrably effective at prioritization, but without underlying theory or validation it will be difficult to know what critical diagnoses it may fail to identify.

Such concerns are not specific to comorbidity network analysis. Inconsistency and uncertainty over the interpretations of centrality measures in the study of human communication networks led @Freeman1978 to propose the concise set of conceptualizations and measures discussed above: _degree_, based on the idea of communication activity with other nodes; _betweenness_, based on the control of communication among other actors, and _closeness_, based on either independence from the control of others or efficiency of dissemination.
These interpretations can be naturally extended to other kinds of resource exchange, but they do not have straightforward interpretations on correlation networks. Such interpretations are needed if disorders are to be characterized with respect to specific types of centrality---for example, the betweenness centrality of acute posthemorrhagic anemia in the MIMIC-III intensive care population---rather than merely being observed to be centrally located in the network in a non-specific sense.


## Recommendations \label{sec:recommendations}

We enumerate here some recommendations for future comorbidity network studies.
To pre-empt and mitigate the aforeraised concerns, in light of our observations, we urge CNA researchers to include in their analysis, according to their aims and methods, several steps:

- Make patient–diagnosis incidence data available for secondary use. This enables future researchers to perform multivariate analyses that are essential for our understanding of epidemiological comorbidity, as will be increasingly recognized as comorbidity network analysts leverage the gains already made by ecologists and psychologists. The release of such data will require additional processing work, some of it entailed by privacy requirements, e.g. the removal of low-frequency disorders or unique patient profiles; but, the sooner these tasks are pioneered, the sooner they can be more widely adopted. If patient–diagnosis data cannot be released, then at least pairwise frequency tables, which enabled our sensitivity analysis, should be.
- Provide theoretical justification for the choice of disorder ontology. The clinical ontologies used by CNA data sets were designed for different uses, e.g. treatment versus billing versus research, and differences between them due to geography and time as well as purpose are unavoidable. Global and local properties may vary dramatically between networks constructed from the same incidence data before versus after crosswalking to an alternative ontology. If a coarser ontology more accurately identifies patient cohorts that have a well-defined index disorder, then the comorbidity network should be constructed after conversion from the original ontology.
- Provide theoretical justifications for weighted CNAs. Indirect network relations such as those underpinning most centrality measures are highly sensitive to the choice of association measure, and the conventional interpretations of these measures at the pairwise level do not translate naturally to the network setting. If weighting is important but no specific weight is entailed, then obtain and report results obtained using multiple weight types---e.g. unit, relative risk--like, or correlation.
- To summarize the global topology of a comorbidity network, report the size of its largest component, its degree assortativity, and its global clustering coefficient, in addition to any other desired summary statistics. These statistics are commonly calculated and effectually discriminate between comorbidity networks. They are therefore especially appropriate to include in comparisons of networks for different populations or disorders of interest.
- Provide theoretical justification for taking a pairwise versus a partial correlation approach. Controlling for confounding effects among all disorders in an ontology can radically change the structure of a comorbidity network, in particular revealing a large number of negative associations. Since free software allows correlation matrices to be efficiently converted to partial correlation matrices, this approach is not prohibitively more expensive.
- Use joint distribution or other multivariate models to validate associations among the most common disorders or within small subsets of disorders. This modeling approach may be too costly for whole-diseaseome analysis, and it may fail to recover comparatively weak but highly discernible comorbidities outside a core set of more prevalent disorders, but it may help distinguish and prioritize the associations of greatest importance to the interactions among a subset of interest, for example the known risk factors and complications of an index disorder.

In addition to these methodological recommendations, we stress the importance of involving clinicians in CNA, to assist both in formulating network-analytic goals or hypotheses that are meaningful and relevant and in understanding the (usually administrative) data on which the analysis is performed.
Many CNA studies to date have been (co-)authored by clinicians, other physicians, or medical researchers, and medical background knowledge is evident and crucial in the discussions of several seminal analyses.
For example, in one of the earliest CNAs, @Hanauer2009 parse the medical plausibility of different explanations for the novel associations they uncovered and provide an essential discussion of likely sources of inconsistency. In a different type of example altogether, @Schafer2014 develop a novel network construction based on triads, motivated by a three-disease conception of multimorbidity, that eliminated much of the low-relevance periphery of association networks but that has not to our knowledge been used elsewhere.
As applications proliferate, it will be increasingly important to ensure that clinical authors bring their training and experience to bear on these projects.


## Future work \label{sec:futurework}

While previous work has compared different false discovery rate corrections on network data at the pairwise level [@Koo2014], we are not aware of any research, theoretical or empirical, into whether FWER- versus FDR-based corrections more faithfully or usefully recover network properties such as centrality and clustering.
Therefore, while we observe some dependence of these properties on the choice of correction (if any), we cannot recommend any method over any other. To inform this choice, future work should evaluate such corrections on the recovery of network properties based on incidence data generated from realistic covariance matrices and on the ability to draw useful inferences from network models of real-world incidence data.

The network analytic approaches used here represent only a subset of the broader range of systems analytic techniques, which includes structural equation models and (discrete or continuous) dynamical models. Few if any studies of disease incidence and co-occurrence have tested the agreement of inferences drawn from such methods with respect to a common topic or question, whether applied to the same or independently collected data.
These more computationally intensive methods benefit from the target-pruning of network analysis, and it is important to ensure that potentially important targets are not missed.


# Methods

## Pairwise constructions

To address the first two concerns from Section \ref{sec:objectives}, we test the dependence of degree distributions, global network statistics, and node centrality rankings to the source of data, the method of link determination, and the choice of network tools. Our results speak to the generalizability of such properties.


### Data sets \label{sec:datasets-pairwise}

The data sets in Table \ref{tab:datasets} were pre-processed and made available by the authors of previous comorbidity network studies [@Rzhetsky2007;@Hidalgo2009;@Roque2011;@Hanauer2013;@Bagley2016], except for MIMIC-III (which is freely available for research use) [@Johnson2016].
The data sets vary widely in the underlying patient population, in the collection of their data by healthcare institutions, _and_ in the researchers' pre-processing protocols; variation along each of these dimensions contributes to overall variation due to the data source.
Disentangling these factors would require a more thorough study using several sources of patient-level data, and we make no attempt to do so here.

<!--
\begin{tabular}{p{.4\linewidth}p{.11\linewidth}p{.11\linewidth}p{.15\linewidth}p{.8\linewidth}}
-->
\begin{table}[h]
\centering\footnotesize
\caption{Sources of pairwise disorder co-occurrence data used in this study, originally aggregated from patient-level data for previous studies and made available by their authors (except MIMIC-III).}
\begin{tabular}{ l c r l r }
\hline
Source & Time period & Patients & Ontology & Terms \\
\hline
Columbia University Medical Center & unreported & 1.5 million & custom ICD9 mapping & 161 \\
MedPAR & 1990--1993 & 32 million & ICD9 (level 3) & 16,459 \\
 &  &  & ICD9 (level 5) & 657 \\
Sct. Hans Hospital & 1998--2008 & 5,543 & ICD10 (level 3) & 351 \\
University of Michigan Health System & unreported & 1.62 million & ICD9 (level 5) & 14,489 \\
STRIDE (Stanford University) & 2008--2013 & 277,290 & custom ICD9 mapping & 161 \\
MIMIC-III (Beth Israel Deaconess) & 2001--2012 & 38,645 & ICD9 (level 5) & 113--273 \\
\hline
\end{tabular}
\label{tab:datasets}
\end{table}


### Link determination \label{sec:linkdetermination}

The majority of comorbidity networks in our surveyed literature [@Brunson2017] have been aggregated from pairwise co-occurrence data, meaning that they can be constructed from the entries $a,b,c,d$ in the $2\times 2$ contingency tables for disorder pairs $(D_1,D_2)$:
$$
\begin{array}{r|cc|}
& D_2 & \neg D_2 \\
\hline
D_1 & a & b \\
\neg D_1 & c & d \\
\hline
\end{array}
$$
We calculated pairwise p-values via Fisher's exact test,[^chisq] optionally adjusting for multiple comparisons using the family-wise error rate (FWER) Bonferroni correction or the false discovery rate (FDR) Benjamini--Hochberg correction, both of which have been used in the CNA literature [@Roque2011;@Roitmann2014;@Bhavnani2015;@Bagley2016;@Kim2018].

[^chisq]: The $\chi^2$ test commonly used in CNAs is an approximation to Fisher's exact test.

In addition to the option of leaving links unweighted (the "unit" measure) we adopted four measures of binary association: two risk ratios and two correlation coefficients
The _odds ratio_ $OR=\frac{a/b}{c/d}$ [@Kraemer1995;@Parzen2002]; _Pearson's binary correlation coefficient_ $\phi=\frac{ad-bc}{\sqrt{(a+b)(a+c)(b+d)(c+d)}}$ [@Hubalek1982], _Forbes' coefficient of association_ $F=\frac{a/(a+b)}{(a+c)/(a+b+c+d)}$ [@Hubalek1982], and the _tetrachoric correlation coefficient_ $r_t$ calculated using a latent bivariate normal model [@Drasgow2006].

$OR$ is recommended as a standard measure of epidemiological comorbidity [@Kraemer1995] and has been used in several CNA studies [@Hanauer2009;@Hanauer2013;@Kim2016].
Several other studies have used $\phi$ and $F$ together, in part to check the robustness of their results [@Hidalgo2009;@Folino2012;@Chen2014a;@Chmiel2014;@Lai2015], though these misleadingly refer to $F$ as "relative risk" [@Zhang1998].
$OR$, $\phi$, and $F$ have been used in previous CNA studies. $r_t$ complements the mix as a second correlation coefficient and provides methodological continuity with the later multivariate models.
Each measure is symmetric, so that the roles of $b$ and $c$ in the above table are interchangeable.

Each comorbidity network was thus constructed based on five parameters: the source of data $D$; the evidential cutoff (TWER) $\alpha$; the choice of error rate correction $C$, if any; the BAM $m$; and an optional evaluative (BAM) cutoff $\theta$.
We use the notation $N(D,\alpha,C,m,\theta)$ to denote specific networks and substitute bullets $\bullet$ for values to indicate families of networks taken over all values in the following ranges:

- $D$: Columbia, MedPAR(3), MedPAR(5), Sct. Hans, Michigan, Stanford, Columbia\*, MIMIC
- $\alpha$: $10^{-p},\ p=1,\ldots,6$
- $C$: none ($\varnothing$), Bonferroni ($B$), Benjamini--Hochberg ($BH$)
- $m$: $1$ (unit), $OR$, $\phi$, $F$, $r_t$
- $\theta$: each of four values specific to each measure:[^theta] $\theta_{OR}=1,2,6,60$, $\theta_\phi=0,0.005,0.05,0.2$, $\theta_F=1,2,6,60$, $\theta_{r_t}=0,0.1,0.4,0.6$

[^theta]: The threshold ranges of $\theta_m$ for each BAM $m$ were chosen so that the corresponding quantiles of pairs in each data set are roughly equal (Figure \ref{fig:quantile-boxplot}).
It should be noted, though, that the Pearson correlation thresholds are more restrictive than those of the other correlation measure $r_t$.


### Power laws and scale-freeness \label{sec:powerlaw}

Like many other empirical networks, comorbidity networks have been described as following a power law, and thereby being scale-free, usually based on visual inspections of their degree distributions [@Steinhaeuser2009;@Ball2011;@Divo2018]. Other studies have taken scale-freeness as the basis for using pre-processing steps designed for scale-free networks [@Jiang2018] or testing whether such networks are consistent with the preferential attachment growth model [@Scott2014].
However, rigorous definitions of power-law behavior in empirical data are contested [@Broido2018;@Voitalov2018a], and neither the visual methods often used to determine scale-freeness [@Li2005;@Clauset2009] nor the implications that power-law degree sequences are often thought to have for the structural and generative properties of a network [@Mitzenmacher2004;@Willinger2004;@Li2005] are reliable.
(One CNA that applied formal statistical methods to test for power-law degree sequences drew negative conclusions [@Davis2011].)

We began by testing whether comorbidity networks have power-law degree sequences, using two methodologies that have come to represent the terms of the debate in the network literature.
Following the methodology of @Clauset2009 and @Gillespie2015, we fit four pure models---Poisson (expected of Bernoulli random graphs), power law, exponential, and log-normal---to the degree sequence tails of the unweighted networks $N(\bullet,0.05,\bullet)$ and compare the goodnesses of fit of the model families, statistically and visually. Then, following the methodology of @Voitalov2018a and @Voitalov2018, we assumed the degree sequences were drawn from regularly varying distributions and compare several estimators of the power-law exponent for well-definedness and consistency.


### Global structure of comorbidity \label{sec:global}

To assess the effects of the construction parameters on the resulting network structure, we calculated a battery of single-valued global statistics on the unweighted networks $N(\bullet,\bullet,\bullet)$: the proportion of nodes in the largest connected component "LCP", the graph density $\delta$, the mean node degree $\overline{k}$, the Gini index $G$ of the degree sequence [@Badham2013], the degree assortativity $r$ [@Newman2003], global triad closure $C$, the average internode distance $\overline{\ell}$, the modularity $Q$ based on a Walktrap node partition [@Newman2004;@Pons2006], and the maximum-likelihood estimates of the parameters of the model family found to best fit the most degree sequence tails, which turned out to be the parameter estimates $\hat\mu$ and $\hat\sigma$ of the log-normal family.

For each statistic $s$, we fit two families of multiple linear regression models to its values $s_i$ at the networks $N_i=N(D_i,\alpha_i,\varnothing,m_i,\theta_i)$. In each case we take as the response variable the difference $y_i=s_i-\overline{s}$ between $s_i$ and the average value $\overline{s}$ on the networks $N(\bullet,\bullet,\varnothing)$. In this way, the effects of both kinds of cutoff indicate the direction and magnitude of change in the statistic as more strict evaluative cutoffs are imposed.
The first model (Equation \ref{eqn:lm1}) regresses $y_i$ on the data source $d_i$, treated as categorical, and $\log(\alpha_i)$, treated as continuous, using only data from networks $N(D_i,\alpha_i,\varnothing)$. We omit an intercept term, so that each dataset has an associated coefficient that indicates the direction in which $s$ deviates, on this population, from its values on the others.
The second model (Equation \ref{eqn:lm2}) additionally includes interaction terms between the BAMs and evaluative cutoffs and is fit to the values on the networks $N(\bullet,\bullet,\varnothing,\bullet,\bullet)$. These terms encode the fact that restricting cutoffs moves each BAM-specific family of networks away from a common origin.[^hierarchical]

\begin{equation}\label{eqn:lm1}
y_i =
 \sum_{d}\beta_d I(d_i=d) +
 \beta_\alpha\log(\alpha_i) +
 \varepsilon_i,
 \ \ \ \ \varepsilon_i\sim N(0,\sigma^2)
\end{equation}

\begin{equation}\label{eqn:lm2}
y_i =
 \sum_{d}\beta_d I(d_i=d) +
 \beta_\alpha\log(\alpha_i) +
 \sum_{m}\beta_{m,\theta} I(m_i=m)\theta_i +
 \varepsilon_i,
 \ \ \ \ \varepsilon_i\sim N(0,\sigma^2)
\end{equation}

[^hierarchical]: We also fit two families of hierarchical regression models to the same data, grouping by data source and by BAM [@Gelman2012]. In terms of the Akaike information criterion [@Burnham2004], these models were consistently worse, with one exception, than the corresponding multiple regression models, so we do not report the results here.

We complemented these regression models with a principal components analysis (PCA) on the values of all 10 centered and scaled statistics measured across $N(\bullet,\bullet,\varnothing,\bullet,\bullet)$.
A row-principal biplot of the data points and variable axes along the first two principal components (PCs) characterizes the dimensions of greatest variance in terms of the loadings of the global statistics.


<!--
### Community structure \label{sec:community}

Clustering is an essential task in the exploration of high-dimensional data, and is routinely performed on healthcare data sets in order to discern meaningful patient cohorts or meta-concepts. The typical clustering algorithm seeks a partition of the data that maximizes distances (or minimizes similarities) between subjects in different clusters while doing the opposite for those between subjects in the same cluster. Clustering has a natural analog in network analysis, known as _community detection_ due to its origins in social science. Community detection algorithms recognize community structure in a network by maximizing geodesic distances between nodes in different communities and minimizing those between nodes in the same community.[^geodesic]

[^geodesic]: In unweighted networks, the geodesic distance between two nodes is the number of links in a shortest path between them. If links are assigned values corresponding to distances or costs, for example, the length of a path may be defined as the sum of the values of its links.

While some comorbidity network studies partitioned the clinical concepts by applying clustering algorithms to the original dataset (BAMs) from which network structures were derived [@Roque2011;@Schafer2014], others used community detection algorithms to partition the network of concepts [@Davis2011;@Chen2014b;@Divo2015]. Theoretically, these two approaches accomplish the same task, but they have not been shown to produce consistent results. It is also not obvious whether, if both approaches are used to partition the concepts, the partitions should be compared on the original dataset, using techniques for evaluating clustering algorithms, or on the graph, using techniques for evaluating community detection algorithms.

We compare the methods in both ways, as described in the next paragraph. We used the Columbia dataset, the only one collected from a general setting for which frequency information is available for all pairs of concepts. We performed complete-linkage hierarchical clustering on the distance matrix of reciprocal odds ratios $1/\widehat{OR}$. Separately, we constructed an unweighted comorbidity network from the co-occurrence data for each of the TWERs $0.5\times 10^{-p},\ p=0,\ldots,6$, as well as for the Bonferroni-corrected $.05$ TWER. We used three popular community detection algorithms to maximize the modularity of a partition of each comorbidity network: one (edge betweenness; EB) that iteratively removes links with high betweenness scores [@Newman2004], one (Walktrap; WT) based on maximizing the probability that random walks stay within communities [@Pons2006], and one (fast--greedy; FG) designed to efficiently partition large networks [@Clauset2004].

We evaluated the partitions obtained at all cuts of the hierarchical clustering tree, and by applying each community detection algorithm to each network, using two statistics: the Dunn index [@Dunn1974;@Handl2005;@Brock2008], treating them as partitions of the complete odds-ratio distance dataset, and the graph modularity [@Newman2004;@Csardi2006], treating them as partitions of the comorbidity network. Like all statistics used to evaluate partitions, they are based on the principle that an effective partition groups proximate observations together and distant observations apart.
The Dunn index is, for a specified partition into clusters, the ratio of the smallest distance between observations in different clusters to the largest distance between observations in the same cluster. Larger values indicate better partitions, and the index is used in practice to determine the best partition from a hierarchical clustering.
Graph modularity is defined using the same principle. It is defined, for a specified partition into communities, as the difference between the fraction of all links that tie nodes within communities and the expected value of this fraction in case links were distributed uniformly at random. Its values are not directly comparable to those of the Dunn index, but higher values likewise indicate better partitions.

After evaluating the partitions on their own merits, we compare them to each other. Community detection algorithms are conventionally used to produce unique partitions, whereas hierarchical clustering trees may be cut at any height to produce a partition of any size. For each community detection--based partition, we found the cut partition that maximizes the adjusted Rand index [@Hubert1985;@Scrucca2016] therewith. We used these examples to gauge the concordance between the groupings obtained using the two methods.

These comparisons will not show whether clustering based on odds ratios or community detection based on network structure is superior for a specific task. Instead, they will reveal the extent to which the approaches obtain partitions with different qualities. Such differences make the choice between approaches, which ideally is rooted in theory, an important one for the researcher.
-->


### Centrality rankings of disorders \label{sec:centrality}

Network models enable researchers to characterize the structural positions of individual nodes [@Brandes2016a].
Analyses of comorbidity networks have usually invoked any of three standard measures of centrality developed for the analysis of social networks [@Freeman1978]: _degree_, the number of actors linked to an index actor; _betweenness_, the proportion of geodesics (shortest paths) connecting other actors that pass through an index actor; and _closeness_, the reciprocal sum of the geodesic distances (lengths of shortest paths) to an index actor.

Several analyses have characterized disorders by their centrality in a comorbidity network:
@Hidalgo2009 and @Schafer2014 used (weighted and unweighted, respectively) degree centrality to measure the connectedness of disorders, which we can think of as their "total" epidemiological comorbidity; @Schafer2014 also used betweenness centrality to measure the potential influence of an index disorder on a patient's comorbidities.
Several other teams invoked degree, betweenness, and closeness centrality as general indicators of a disorder's importance to the larger diseaseome [@Ball2011;@Chen2014a;@Liu2016].
In terms of specific results, three studies corroborated the exceptionally high centrality of hypertension in comorbidity networks [@Schafer2014;@Feldman2016;@Chen2014a], while another examined the centralities of disorders comorbid with hypertension [@Liu2016].
An increasingly popular approach is to examine differences in disease centrality across study populations: @Feldman2016 compared the betweenness centralities of central diagnoses between demographic subgroups such as low- and high-income populations, and @Divo2018 compared the degree centralities of disorders between COPD and non-COPD populations in a case--control design.

Our sensitivity analysis concerns the effects of link determination on disease centralities and centrality rankings. These effects may be qualitatively different from the effects on global statistics described in the previous section.
If changes in the thresholds have pronounced effects on global network statistics, this might be understood primarily as an effect of changes in network density. Certainly, changes in density---the loss or gain of a significant proportion of links---will affect node centralities, but _relative_ centralities could nevertheless remain stable.
This has been tested in a few instances, with significant changes in rankings being observed:
@Feldman2016 found that the betweenness rankings of disorders were sensitive to their link pruning procedure, and @Ball2011 noted that the centralities of adverse events in their VAERS networks changed noticeably from month to month.

To measure this affect systematically, we calculated degree, betweenness, and closeness centralities in the networks $N(\bullet,0.05,\bullet,\bullet,-\infty)$.
For the strength-based measure (degree), we weighted each link by its BAM. For the path-based measures (betweenness and closeness), we weighted each link by the reciprocal of its BAM, so that stronger associations yield shorter internode distances. We imposed no evaluative threshold.
For each fixed data source and centrality measure, we compared the disorder rankings obtained using each p-value correction and each BAM using Kendall rank correlations [@Kendall1938;@Kendall1945] as a test of the robustness of the approach. We analyzed the rankings geometrically via eigendecompositions of the pseudo-correlation matrices of Kendall values. We report the proportions of inertia explained by each construction parameter and summarize the correlations among the rankings using biplots.[^semidef]

[^semidef]: While a matrix of rank correlations may not be positive-semidefinite, it still admits an eigendecomposition. The correlations between the rankings will equal the cosines between the unit vectors associated with the rankings in the space of the eigenvectors. In most practical settings the first two eigenvalues will be positive, so correlations will be well-represented visually by cosines between the vectors projected to a two-dimensional biplot. So it was in our cases.

It may be that overall centrality rankings are sensitive to network construction while the identification of exceptionally central "hubs", the focus of most applications, is robust: nodes on the periphery tend to be structurally similar and of lower degree, so that the addition or removal of fewer links would be needed to permute their ranks.
It is also important, for the generalizability of CNA, to know whether the high centrality of certain disorders is an artifact of the network construction, a robust property of a specific patient population, or a generalizable epidemiological fact.
To address these "hubs" specifically, we inspected the several most central disorders from each network.

Any specific ontology was only used by two to four data sets. In order to test the robustness of centrality rankings across data sets with different ontologies, we used many-to-one maps from finer to coarser ontologies to identify groups of nodes in finer comorbidity networks with single nodes from coarser networks.
Where such mappings were available (level-5 to level-3 ICD9 and level-5 ICD9 to the ontology of @Rzhetsky2007), we compared group centrality measures for concepts in the finer ontology with node centralities for concepts in the coarser ontology [@Everett1999].
Because group betweenness centrality was computationally prohibitive, we only considered degree and closeness centrality.

<!--
It may be expected and important that centrality rankings vary with differences in the patient population and data collection, so the differences we observed are not necessarily cause for concern. However, only the simple measure of degree centrality was robust to the method of link determination _and_ to the more general choice of network model, while betweenness and closeness centrality produced rankings that varied noticeably with network model and in some cases dramatically with link determination parameters.

The ranking comparisons do hint at some general patterns:
First, the centrality of a disorder, like the global network structure, depends heavily on the underlying dataset---though we note that the clusters of datasets with similar rankings do not correspond exactly to the clusters obtained using t-SNE on the point cloud of global network statistics.
Second, centrality rankings are more concordant when ontologies are coarser, even when obtained using group rather than node centrality scores.
Despite this, group centrality scores tend to correlate poorly with node centrality scores under the many-to-one mappings between ontologies. This may be because the group scores under a mapping measure different properties than the original node centrality scores, or because the populations underlying the datasets we examined were different in important ways from each other.

This last question would be resolved by comparisons like the following: From a patient-level dataset with full ICD9 diagnoses, construct one comorbidity network on the full codes and compute group centralities based on the mapping to level-3 ICD9 codes. Separately, generate the level-3 ICD9 codes for each patient in the original dataset, construct a comorbidity network on these, and and compute node centralities. High concordance between the two rankings of the level-3 ICD9 codes will indicate that the node and group centrality scores detect the same underlying properties, and that the disagreements above were likely due to differences in populations or data collection; low concordance will indicate that the node and group centrality scores capture different properties and do not provide meaningful comparisons between datasets using different ontologies.
-->

## Multiple and multivariate constructions \label{sec:multivariate}

Our sensitivity analysis of pairwise comorbidity networks was enabled by the several research teams who shared their pairwise data. With access to patient-level incidence data, advanced statistical software, and adequate computational resources, a wider family of network models becomes available. Multivariate models that incorporate all disease associations simultaneously can address concerns of multiple comparisons and confounding without resort to post hoc corrections. In this phase, we compare comorbidity relationships obtained from two publicly-available comorbidity datasets using the conventional pairwise construction, a partial correlation construction that corrects for confounding, and a recently-proposed multivariate model that also incorporates patient-level covariates. The comparisons will reveal the effects of systems-level interactions on observed pairwise relations.


### Datasets \label{sec:datasets-multivar}

We draw from two data sets, one low-dimensional but high-volume, in the sense of covering the incidence of a small number of disorders for a large number of patients, and one comparatively high-dimensional and low-volume.

As a low-dimensional use case, we use data for the year 2011 from the National Ambulatory Medical Care Survey (NAMCS), coordinated by the CDC and conducted by hundreds of physicians and community health centers each year (<https://www.cdc.gov/nchs/ahcd/index.htm>). Each entry describes a single encounter, and each physician collects data for two weeks out of the year, _without_ linking records for encounters with the same patient. Thus, the data does not contain long-term data for any patient, and any multiple diagnoses were recorded at a single encounter.
The survey data include a range of patient demographics, provider characteristics, and clinical information such as diagnoses and procedures pertaining to the encounter.
While these diagnosis data are suitable for certain study designs, e.g. cross-sectional descriptions of specialty- or location-specific patient populations, they are inappropriate for comorbidity network analysis, which relies on the recovery of comprehensive health profiles for all patients in a sample.

NAMCS asks providers to indicate whether each patient has each of several chronic disorders, including thirteen that are reported in the Public Use Dataset: arthritis, asthma, cancer, cerebrovascular disease, chronic obstructive pulmonary disease, congestive heart failure, ischemic heart disease, depression, diabetes mellitus, hyperlipidemia, hypertension, obesity, and osteoporosis.
These are widely-recognized conditions that do not require specialized training to diagnose and that will often already appear on patients' records. Therefore, despite the limitations of NAMCS, we expect that the data much more reliably reflect the actual distribution and co-occurrence of these chronic disorders in the clinic-going population.
To make fitting joint distribution models (see Section \ref{sec:jdm}) more computationally feasible, we took a 50% cluster sample from this year of NAMCS encounter data, after restricting to cases for which all variables were recorded, clustered by participating practices and weighted by number of encounters within the practice.

As a contrast to NAMCS, we draw a second data sample from MIMIC-III, described previously. MIMIC-III is similar to NAMCS in containing only one encounter for most patients, but dissimilar in covering a narrow range of practice and therefore a limited population.
To increase the clinical homogeneity of our sample, we restrict our attention to patients admitted to specific units, which include medical intensive care (MICU), surgical intensive care (SICU), the neonatal ward (NWARD), neonatal intensive care (NICU), coronary care (CCU), cardiac surgery recovery (CSRU), and trauma/surgical intensive care (TSICU).
For computational feasibility, we grouped the ICD-9 diagnosis codes recorded in MIMIC-III according to the Clinical Classification Software (CCS) ontology [@AgencyforHealthcareResearchandQuality2012]. Care unit populations ranged in the number of distinct recorded CCS categories from 113 (NICU) to 273 (MICU).


### Partial correlation networks \label{sec:partial}

Conventional measures of comorbidity fail to account for an important source of variation in patient-level incidence data: incidence rates of other disorders. Two disorders that are clinically unrelated may be epidemiologically comorbid due to having risk factors or complications in common, and CNA researchers have pointed out that these are important potential explanations for clustering patterns observed in comorbidity networks [@Hanauer2009].
Partial correlations account for these confounding effects by generalizing the calculation of correlations from regression coefficients: the full partial correlation $r'_{ij}=\hat\beta_{ij}\hat\sigma_j/\hat\sigma_i$ between response variables $y_i$ and $y_j$ is a standardized effect estimate from the regression model of $y_i$ on all other responses, including $y_j$ [@Epskamp2017]. This concept relies on the normality assumptions of classical regression; we use a matrix formulation to obtain a matrix ${R_t}'$ of _partial tetrachoric correlations_ ${r_t}'$ from the matrix $R_t$ of tetrachoric correlations.


### Joint distribution networks \label{sec:jdm}

Epidemiological comorbidities can also arise from patient-level covariates, as when clinically relevant subpopulations (e.g. elderly or infirm patients) are at heightened risk of multiple, otherwise etiologically unrelated disorders.
Several comorbidity network studies incorporated demographic predictors into their binary association tests [@Rzhetsky2007;@Feldman2016;@Glicksberg2016], though this information was unavailable for our sensitivity analysis.
To simultaneously account for the _endogenous_ effects of other disorders and the _exogenous_ effects of patient-level covariates, we adapted the _joint distribution model_ (JDM) designed to incorporate both species interactions and environmental factors into ecological models [@Pollock2014].

In making this choice, we propose and appeal to an _ecological--epidemiological analogy_: Disorders afflicting persons and communities are similar in many respects to species occupying geographical sites. In the case of viral, bacterial, and fungal infections, the former is in fact a special case of the latter. Insofar as the assumptions underlying an ecological analytic technique are met by epidemiological data, the technique is an appropriate one. The analogy posits that this will frequently be the case, or at least that ecological techniques will be no less appropriate than competing ones. Indeed, association network analysis itself is rooted in ecology, which produced many if not most of the measures commonly used to weight association networks [@Johnston1976;@Podani2000]. More recently, ecologists have honed several other methods to account for the same limitations of pairwise network construction discussed here [@Ulrich2007;@Ovaskainen2010;Cazelles2016;@Morueta-Holme2016]; see @Elith2009 and @Kissling2012 for useful reviews.

In the JDM, each patient's distribution has the same covariance matrix, from which a correlation matrix $\Rho=(\rho_{ij})$ is obtained, and is centered at a linear transformation $\Beta=(\beta_{ki})$ of their $q$ exogenous variables ($1\leq k\leq q$), which can be interpreted in the same way as classical regression coefficients.
The latent normality allows us to directly compare the estimated endogenous correlations $\hat\Rho$ to $R_t$ and ${R_t}'$, and the combined estimated effects of the patient-level covariates can be organized into a matrix $\hat{\bm{\Rho}}$ of exogenous components of correlation.
We fit the JDM to the $n\times m$ patient--disorder incidence matrices, using either of two matrices $X$ of exogenous patient-level covariates: an $n\times 1$ intercept matrix to encode only the baseline prevalence of each disorder, and an $n\times q$ matrix augmented with demographic variables. The respective models are designated JDM0 and JDM1 and estimates indexed with $0$ or $1$ accordingly.
See the SI Text for more details.


### Correlation structure

We first examined the differences in correlation structure between models produced using these three approaches to comorbidity, independently of link determination.
From the NAMCS data, we generated four correlation matrices for the 13 chronic disorders described in Section ref{sec:datasets-multivar}: pairwise ($R_t$), full partial (${R_t}'$), and joint distribution with prevalence-only and with full demographic exogenous covariates ($\hat\Rho_0$ and $\hat\Rho_1$). In the last model, the exogenous variables included decadal age groupings (0--14, 15--24, etc., to 65--74, with 75+ the baseline), gender (male, with female the baseline), race/ethnicity (Asian, Black/African-American, American Indian/Alaska Native/Pacific Islander, white, and Hispanic/Latinx, with unknown/other the baseline), insurance status (Private, Medicare, and Medicaid, with others or none the baseline) region (Midwest, South, West, with Northeast the baseline), and metropolitan status (Metropolitan Statistical Area, with non-MSA the baseline).
From the MIMIC data, we only considered the three endogenous models (pairwise, full partial, and joint distribution without exogenous covariates), in part to reduce computational cost and in part to limit the scope of the analysis. Each model included every CCS code.

To assess the effect of moving from a pairwise to a systemic approach to comorbidity, we focused on differences among $R_t$, ${R_t}'$, and $\hat\Rho_0$. The additional effect of controlling for patient-level covariates manifested in differences between $\hat\Rho_0$ and $\hat\Rho_1$.
We compared the correlation matrices $R_t,{R_t}',\hat\Rho_0,\hat\Rho_1$ among the 13 chronic disorders in NAMCS using correlation biplots.
We visualized the relationships among the point estimates of association strength $r_t,{r_t}',\hat\rho_0,\hat\rho_1$, across all ${{m}\choose{2}}$ pairs disorders from each data source, using scatterplots.


### Link determination

Differences in correlation structure translate into differences in network structure through the link determination process.
To evaluate these differences, we adopted a uniform evidential threshold of $\alpha=5\%$ by which to categorize the pairwise associations in each model as discernibly positive, discernibly negative, or indiscernible. We sought patterns of differences between the pairwise determinations using alluvial diagrams [@Brunson2018] and visualized the resulting networks using a circular layout.


### Centrality rankings of disorders

To evaluate differences in the resulting network structures, we compared centrality rankings as in Section \ref{sec:centrality}, using Kendall rank correlations between the different network models of a common data set and visualizing them using pseudo-correlation biplots.


## Software

We performed our analyses using the R statistical programming language [@RCoreTeam2016], unless stated otherwise.
We processed and analyzed data using packages from the \pkg{tidyverse} collection [@Grolemund2016] and manipulated and visualized networks using the \pkg{igraph} [@Csardi2006], \pkg{tidygraph} [@Pedersen2018], and \pkg{ggraph} [@Pedersen2018a] packages.

<!--
We use the \pkg{polycor} package [@Fox2016] to calculate $r_t$ from contingency tables.
We use the \pkg{psych} package [@Revelle2017] to calculate $r_t$ from contingency tables.
-->
Depending on the application, we used the implementation of $r_t$ in the \pkg{psych} package [@Revelle2017] or implemented the approximation method of @Bonett2005 in order to avoid infinities and to calculate standard errors.
We also used different implementations to compute partial correlations $R'$ from pairwise correlations $R$:
For high-volume, low-dimension data, we used \pkg{psych} package implementation `partial.r()`, which uses squared multiple correlations. For low-volume, high-dimension data, this became infeasible, and we first calculated shrinkage estimates [@Schafer2005] for $R'$ using `cor.shrink()`, together with the partial correlation function `cor2pcor()`, from the \pkg{corpcor} package [@Schafer2017].

We organized effect estimates from regression models using the \pkg{stargazer} package [@Hlavac2015].
Biplots were rendered using the \pkg{ordr} package [@Brunson2019].
English descriptions of codes from the International Classification of Diseases, 9th and 10th Revisions, Clinical Modifications (ICD9 and ICD10), were obtained from and formatted using the \pkg{icd} package [@Wasey2017].

We adapted our JDM workflow from the tutorial provided by @Pollock2014, using JAGS [@Plummer2003] to perform Bayesian model fitting through the \pkg{R2jags} package [@Su2015].

Full code to reproduce our analyses will be made available on Bitbucket upon acceptance for publication.

<!--[^code]: https://bitbucket.org/corybrunson/comorbidity-->

<!--[^code]: [blinded]-->


## Ethical considerations

This study did not involve human or other animal subjects.
We conducted secondary analysis on data sets collected and aggregated by other researchers, which are available either publicly or upon request. Of these, patient-level data were only available in the MIMIC-III database, but our analysis relied exclusively on aggregated data.


# Results \label{sec:results}


## Pairwise analysis


### Link determination

Table \ref{tab:densities} presents network density for each dataset and each of several common TWERs.
In most cases the original authors provided co-occurrence data only for a subset of pairs, according to their own study designs; the network density at the 100% TWER is the proportion of pairs they included. Often among these were data on negative associations.
In each case our 5% TWER cutoff excluded many additional pairs, including all negative associations.
The Bonferroni correction excludes the vast majority of the remaining links from networks for which more pairs were originally available (MedPAR(5), MIMIC) but fewer than one third from networks that had already been pruned of weak associations (Sct. Hans, Stanford, Columbia\*).
For any fixed evidential cutoff, the networks range in density over 1--3 orders of magnitude.
Quintiles calculated for each BAM and data source indicated that the evidence for a comorbid association may not be predictive of its strength, so that some network properties may be more sensitive to an evaluative cutoff than to an evidential one (see the SI Text).

\input{../../../tab/densities.tex}


### Degree sequence distributions

Almost all comorbidity networks were better-modeled by log-normal distributions, and then by exponential distributions, than by power-law distributions. Based on our sample, this pattern was not dependent on the construction parameters, except that the preference for log-normal versus exponential versus power-law distributions were less clear for networks constructed using stricter evidential thresholds.
Most regular variation–based power-law models failed to converge or else were inconsistent, reaffirming that power-law models were inappropriate for these networks' degree sequences.
See the SI Text for details.

### Single-valued summary statistics

Table \ref{tab:lm2} reports the effect estimates obtained by fitting Equation \ref{eqn:lm2} to the various global network statistics.
The coefficients within each model (column) associated with the data sources can be directly compared using differences, though the scale is interval, not ratio (i.e. the relative position of $0$ is arbitrary).
The evaluative cutoffs $\theta_m$ are included only as interaction effects with a categorical variable encoding the measure $m$, because the range of values of $\theta_m$ varied across $m$. Roughly, the effect estimates can be compared after scaling by the ranges of the $\theta_m$ (Section \ref{sec:linkdetermination}). For example, the relative effect estimates of $\theta_{\widehat{OR}}$ and $\theta_{r_t}$ on average degree $\overline{k}$ were in proportion to $-1.14\times 59\,:\,-125.11\times 0.6$, or $9\,:\,10$.
The effect estimates obtained using Equation \ref{eqn:lm1} were qualitatively similar, for those predictors included in both models (Table \ref{tab:lm1}).

\begin{landscape}
\input{../../../tab/model2.tex}
\end{landscape}

The vast majority of effect estimates in both tables are discernible with $p<.001$. This is expected from variation in the evidential and evaluative thresholds ($\widehat{\beta}_\alpha$ and $\widehat{\beta}_{m,q}$), which correspond to dramatic changes in the density of the graph. The data source effects $\widehat{\beta}_d$ are likewise discernible, except in some cases with respect to degree distribution ($\delta$, $\overline{k}$, $\sigma$), indicating that none of the data sets produces in any sense an "average" comorbidity network. Some of the data sets produce similar networks, for example MedPAR(3) and Michigan, or Stanford and Columbia\*. The latter use the same ontology and were processed in the same way, though the former have no such similarities.
The MedPAR(3) and MedPAR(5) networks differ only in the resolution of their ontologies, and their global properties are broadly similar; but the Columbia and Columbia\* networks share these background similarities (and the same ontology) yet exhibiting very different structure.

Each statistic varies widely (given the range of its possible values) across these comorbidity networks, though much of this variation can be explained by network density, which is largely a product of population or sample size.
Increasing the number of links in a graph, whether by increasing $\alpha$ or by decreasing $\theta_m$, has the expected effect on LCP (more nodes in the largest component); on $\delta$, $\overline{k}$, and $\mu$ (greater density); and on $\overline{\ell}$ (shorter geodesics). It also tends to decrease the correlation between the total comorbidities of comorbid disorders ($r$) and the modular structure of the network ($Q$). Since these networks tend to be denser than other empirical networks to begin with, this suggests that graphs obtained via more relaxed cutoffs are saturated with connections in a way that obscures these hierarchical properties. Triad closure increases with stricter evidential cutoffs but decreases with stricter evaluative cutoffs. This suggests that cliques of three or more associated disorders often have strong evidential support while their pairwise associations vary widely in strength.
This may be understood as some comorbid relations being artifacts of others, i.e. "transitive correlations" [@Tao2014].

\begin{figure}[h!]
\includegraphics[width=\textwidth]{../../../fig/pca-label.pdf}
\caption{
Row-principal PCA biplot for the summary statistics with networks (cases) in principal coordinates and statistics (variables) in standard coordinates.
The values for graphs constructed from a common dataset are summarized by 95\% confidence ellipses. Symbol corresponds to BAM, color indicates data source, and opacity is proportional to network density. Ellipse thicknesses are proportional to the number of clinical concepts (nodes) in the ontology (graph).
\label{fig:pca}
}
\end{figure}

Several patterns emerge from the PCA biplot (Figure \ref{fig:pca}):
Judging from the variable loadings, PC1 captures a spectrum between highly connected and dense graphs with shorter internode distances, in which communities are difficult to detect, and sparser, more modular graphs. This spectrum aligns with the differences in density, encoded as the opacity of the plotting symbol, produced from a single dataset by varying the evaluative cutoff.
PC2 discriminates between more homogeneous graphs, in terms of degree distribution (low scores), and those high in degree assortativity and triad closure (high scores).
Graphs on finer ontologies, such as the level-5 ICD9 codes used by @Hidalgo2009 and @Hanauer2013, varied more widely across different choices of BAM; while graphs on coarser ontologies, in particular the level-3 ICD9 codes used @Hidalgo2009 and the custom ICD9 mapping of @Rzhetsky2007, varied less by BAM. Their values were more distant from the average than those of the graphs on coarser ontologies, which indicates that their stuctural properties were more idiosyncratic.

The networks constructed from a common dataset form subsets that emanate outward in clearly different directions, indicating that comorbidity network structure depends crucially on the source of data. These clusters have relatively consistent scores on PC2, which separates the clusters by ontology size. In contrast, they vary widely along PC1, which is in most cases clearly correlated with changes due to the evaluative cutoff. These observations are nearly exhaustive: together PC1 and PC2 account for more than 60% of the variance in the point cloud, with PC3 accounting for less than 15% more.[^unsuitable]

[^unsuitable]: We repeated the PCA after including the comorbidity network constructed from VAERS. This network does not stand out from the rest, and indeed situates itself among those other networks constructed from larger data sets.


<!--
### Community structure

Figure \ref{fig:community} compares the values of the Dunn index (left) and the graph modularity (right) on the partitions obtained using hierarchical clustering on a distance matrix and using three community detection algorithms on two comorbidity networks constructed from the distance data.

\includegraphics[width=\textwidth]{../../../fig/community-dunn-modularity-n.pdf}
\begin{figure}
\includegraphics[width=\textwidth]{Figure3.pdf}
\caption{
Left: Step plots of Dunn indices versus partition size for a hierarchical clustering tree on the Columbia dataset using the reciprocal odds ratio $1/\widehat{OR}$ as distance (black segments, top) and averaged over 12 random partitions of the same dataset for each number of parts (black segments, bottom), overlaid with points corresponding to partitions of the same distance matrix obtained by applying three community detection algorithms---edge betweenness (circles), fast--greedy modularity maximization (triangles), and Walktrap (squares)---to unweighted comorbidity networks constructed using different TWERs (solid points).
Grey lines connect the values for partitions by the same algorithm of networks constructed using progressively strict TWERs.
Right: Network modularity versus partition size. The symbols and grey lines trace the same network models as in the left plot. The step plots with larger modularity values trace the partitions obtained from the hierarchical clustering tree for the $1/\widehat{OR}$ distance matrix, applied to the networks constructed using TWER $.05$ (dashed) and Bonferroni-corrected $.05$ (dotted). The lower-modularity step plots trace modularities for the same networks using randomly-generated partitions as in the left plot.
Note the log scale on the vertical axis.
\label{fig:community}
}
\end{figure}

The left frame shows the upward progression, from left to right, of Dunn indices as the clustering tree is pruned at progressively greater heights, resulting in more granular partitions. This progression indicates that the dataset may not be naturally clustered; under ideal settings with far fewer natural clusters than observations, this step plot would peak. Nevertheless, the Dunn indices of the clustering tree are far higher than those of randomly-generated partitions using the same numbers of clusters.
The community detection algorithms produce partitions only moderately better than random, and, in contrast to improving modularity, increasing the TWER does not consistently improve or worsen the partitions in distance terms.

The right frame shows that the hierarchical clustering tree does, in fact, produce peak modularity at a low number of clusters (between 5 and 15). However, when comorbidity networks are produced by excluding statistically weak links, the community detection algorithms produce still more modular partitions. Moreover, as the statistical threshold is tightened, these algorithms partition the comorbidity networks into more and smaller parts that result in greater modularity scores. In contrast, after the early peak, cutting the hierarchical clustering tree at greater heights to produce finer-grained partitions consistently results in lower modularity.

The two approaches---distance-based clustering and graph-based community detection---seek partitions that optimize fundamentally different criteria. The comparison reveals that this can result in substantially different partitions of a common set of variables.
For example, Figure \ref{fig:overlap-clusters-2} presents one comparison, of a cluster obtained using community detection and an overlapping cluster obtained using complete-linkage, cut to produce the same number of clusters.
The two clusters presented have an exceptionally high overlap of 13 concepts; two larger clusters with 15 shared concepts are presented in Figures \ref{fig:overlap-clusters-fg} and \ref{fig:overlap-clusters-eb}.

\begin{figure}
\footnotesize
\noindent\fbox{
  \parbox{.47\textwidth}{
    Community detection (Walktrap)
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    Hierarchical clustering (complete-linkage)
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    AA metabolism (aromatic)\\
    {\bfseries Ankylosing spondylitis}\\
    Anthrax\\
    Behcet's s.\\
    Benign neoplasms\\
    Cervical rib\\
    Dejerine-Sottas d.\\
    {\bfseries Giant cell arteritis}\\
    Goiter\\
    {\bfseries Hypersensitivity angiitis}\\
    {\bfseries Kawasaki's d.}\\
    {\bfseries Lethal midline granuloma}\\
    Lipid metabolism d.\\
    Lumbosacral spondylolysis\\
    Mumps\\
    {\bfseries Polyarteritis nodosa}\\
    Progeria\\
    Renal glycosuria\\
    {\bfseries Rheumatoid arthritis}\\
    {\bfseries Scleroderma}\\
    {\bfseries Sjogren's s.}\\
    Spondylolisthesis\\
    {\bfseries Systemic lupus erythematosus}\\
    {\bfseries Takayasu's arteritis}\\
    {\bfseries Thrombotic thrombocytopenic purpura}\\
    Tularemia\\
    {\bfseries Wegener's granulomatosis}\\
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    {\bfseries Ankylosing spondylitis}\\
    Budd-Chiari s.\\
    Buphthalmos\\
    Dermatomyositis-polymyositis\\
    {\bfseries Giant cell arteritis}\\
    Gout\\
    {\bfseries Hypersensitivity angiitis}\\
    {\bfseries Kawasaki's d.}\\
    {\bfseries Lethal midline granuloma}\\
    Mineral met. (Fe)\\
    {\bfseries Polyarteritis nodosa}\\
    {\bfseries Rheumatoid arthritis}\\
    {\bfseries Scleroderma}\\
    {\bfseries Sjogren's s.}\\
    {\bfseries Systemic lupus erythematosus}\\
    {\bfseries Takayasu's arteritis}\\
    {\bfseries Thrombotic thrombocytopenic purpura}\\
    {\bfseries Wegener's granulomatosis}\\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
  }
}
\caption{
  Overlapping clusters obtained from the Columbia dataset, using complete-linkage hierarchical clustering on the $1/OR$ distance matrix (right) and fast--greedy community detection on the comorbidity network constructed using a Bonferroni-corrected $0.05$ TWER (left). The community detection method maximized the modularity at 17 clusters, so the hierarchical clustering tree was cut so as to produce the same number. Concepts shared by both clusters are rendered in boldface.
  \label{fig:overlap-clusters-2}
}
\end{figure}

Figure \ref{fig:overlap-clusters-wt} presents one comparison, of a cluster obtained using Walktrap community detection and an overlapping cluster obtained using complete-linkage. The complete-linkage partition was cut so as to maximize the congruence of the two partitions according to the adjusted Rand index, and the selected clusters had the largest overlap. Corresponding clusters obtained using the other community detection algorithms are presented in the SI.

\begin{figure}
\footnotesize
\noindent\fbox{
  \parbox{.47\textwidth}{
    Community detection (Walktrap)
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    Hierarchical clustering (complete-linkage)
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    AA metabolism (sulfur-bearing)\\
    Acute promyelocytic leukemia\\
    Alcoholism\\
    {\bfseries Alkalosis}\\
    {\bfseries Alzheimer's}\\
    {\bfseries Anaerobes}\\
    {\bfseries Aortic aneurysm}\\
    Aplastic anemia\\
    Breast cancer F\\
    Breast cancer M\\
    Brucellosis\\
    Budd-Chiari s.\\
    {\bfseries Bundle branch block}\\
    {\bfseries Cardiomyopathy}\\
    Gout\\
    {\bfseries Gram-negative bacteria}\\
    {\bfseries Hepatitis C}\\
    {\bfseries Hyperosmolality}\\
    {\bfseries Hypertrophic cardiomyopathy}\\
    Hypoglycemia\\
    {\bfseries Hypoosmolality}\\
    Mineral met. (Ca)\\
    Mineral met. (Fe)\\
    Mineral met. (Mg)\\
    {\bfseries Pneumonia}\\
    {\bfseries Poliomyelitis}\\
    Reticulosarcoma\\
    {\bfseries Staphilococcus}\\
  }
}
\noindent\fbox{
  \parbox{.47\textwidth}{
    Acidosis\\
    {\bfseries Alkalosis}\\
    {\bfseries Alzheimer's}\\
    {\bfseries Anaerobes}\\
    {\bfseries Aortic aneurysm}\\
    {\bfseries Bundle branch block}\\
    {\bfseries Cardiomyopathy}\\
    Cholelithiasis\\
    Diabetes type 1\\
    Diabetes type 2\\
    {\bfseries Gram-negative bacteria}\\
    {\bfseries Hepatitis C}\\
    {\bfseries Hyperosmolality}\\
    {\bfseries Hypertrophic cardiomyopathy}\\
    {\bfseries Hypoosmolality}\\
    Parkinson's d.\\
    {\bfseries Pneumonia}\\
    {\bfseries Poliomyelitis}\\
    {\bfseries Staphilococcus}\\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
    \\
  }
}
\caption{
Overlapping clusters obtained from the Columbia dataset, using complete-linkage hierarchical clustering on the $1/OR$ distance matrix (right) and Walktrap community detection on the comorbidity network constructed using a Bonferroni-corrected $0.05$ TWER (left).
The hierarchical clustering tree was cut so as to maximize the adjusted Rand index of the two partitions of the concepts.
Concepts shared by both clusters are rendered in boldface.
\label{fig:overlap-clusters-wt}
}
\end{figure}

**Describe.**

It is also worth noting that the large overlap observed here is atypical. Table \ref{tab:overlap-matrix-wt} presents the number of shared concepts in each pair of clusters in both ontologies, excluding clusters consisting of only one concept. The vast majority of clusters contain very few concepts, especially those that comprise community detection partitions. The results may be more interesting for networks with more concepts.

\input{../../../tab/overlap-matrix-wt.tex}

The terms shared by both clusters comprise a natural cluster of immune system disorders (rheumatoid arthritis, lupus, Sjogren's syndrome) and associated varieties of blood vessel inflammation. Separately, both methods include several other disorders in the cluster that deviate from this theme. This is very likely due in part to the specific cuts used to obtain the partitions; a coarser complete-linkage partition might include a cluster that more closely matches the Walktrap partition cluster, for example. It also may reflect some advantage to pruning statistically weak associations before performing any clustering method (i.e. setting distances between statistically uncorrelated disorders to infinity). But it also suggests that there may be some benefit to using these methods in tandem.
-->


### Centrality rankings of disorders

<!--
Figure \ref{fig:centrality-heatmap-construction} presents Kendall correlation heatmaps for each centrality measure on a sample of four data sets, determining links using a range of evidential cutoffs and BAM weights.
Rankings based on networks constructed from the same dataset tend to concord; that is, for most data sets, certain disorders are clearly more centrally located. In a few cases, though, the choice of association weight can radically impact the rankings: the betweenness and closeness rankings on the $OR$- and $F$-weighted networks for the MedPAR(3), Columbia, and MIMIC-III data sets, were negatively correlated with the $\phi$-, $r_t$-, and unit-weighted rankings.
These were also negatively correlated with the rankings obtained from the same dataset using different centrality measures (not shown).
-->

From correlation biplots, we observed great variation in the disorder rankings within each data source and centrality measure (Figure \ref{fig:centrality-biplots}).
Note that, for these comparisons, the p-value cutoff determined the discrete network structure while the BAM determined the weighting scheme upon this structure.
While rankings were rarely discordant (the relative positions of more pairs of disorders reversed than preserved), frequently two different constructions yielded weak correlations ($r<.5$).
The rankings were sensitive to both construction parameters, and no single choice of p-value correction or BAM consistently produced rankings that were robust to the other parameter.

\begin{figure}[h!]
\includegraphics[width=\textwidth]{../../../fig/centrality-biplots-Michigan.pdf}
\caption{
Eigendecomposition biplots for the Kendall correlations among (left to right) degree, betweenness, and closeness centrality rankings of disorders in networks constructed from the Michigan data, using a 5\% TWER with each error rate correction and each BAM.
The linetype of each arrow indicates the correction (solid for none, dotted for FWER, dashed for FDR) and its color and label indicate the BAM.
\label{fig:centrality-biplots-Michigan}
}
\end{figure}

The data sets also range widely in terms of which construction parameter explains more of the variation in rankings.
For an example taken at random, rankings of full ICD9 codes based on Michigan data were sensitive to the BAM though robust to the correction (Figure \ref{fig:centrality-biplots-Michigan}). In other cases, rankings were variably more sensitive to the BAM (MedPAR(3), Columbia\*) or to the correction (Sct. Hans, MIMIC).
This is quantified in terms of the decomposition of inertia (Table \ref{tab:centrality-concordance-inertia}). Comorbidity networks based on Sct. Hans and MIMIC produced centrality rankings that were highly sensitive to the p-value correction but robust to the BAM, while rankings of those based on MedPAR(3) and Columbia\* were more sensitive to the BAM. The underlying ontologies do not explain these differences. However, the results reveal clear differences between the sensitivities of the rankings based on different centrality measures, with degree centrality most sensitive to the p-value correction and closeness centrality most sensitive to the BAM (though only slightly more than betweenness). This is consistent with the theoretical distinctions between the measures, with betweenness and closeness reliant on geodesic paths that vary more with differences in link weights, and may help guide the choice of measure best suited to a particular application.

\input{../../../tab/centrality-concordance-inertia.tex}

The betweenness centrality scores, like those for degree but unlike those for closeness, yield clear hubs, dominated by non-specific symptoms and disorders. For the regional EHR data sets, these include epilepsy (Columbia), limb pain, unclassified respiratory problems (Michigan), vitamin deficiency (Stanford), benign neoplasms, and tuberculosis (Columbia\*).
While the rankings differ, most of the same disorders fill the top slots according to degree centrality. These include gram-negative bacteria, carcinoma in situ, lipid metabolism disorders (Columbia), unclassified respiratory disorders, unspecified chest pain, unspecified pneumonia (Michigan), acidosis, mineral metabolism of calcium and magnesium (Stanford), and Hepititis C (Columbia\*).

The more specific populations covered by the other data sets yield their own characteristic hubs: non-specific diagnoses of fluid and electrolyte imbalances, urinary tract disorders, and bacterial infections (MedPAR), which may be associated with increased hospital and nursing home care as well as with aging itself; gait and mobility disorders, which are strongly associated with nervous disorders (Sct. Hans); and acute posthemorrhagic anemia (APHA), a common symptom of injury-induced blood loss (MIMIC).
The high betweenness of these disorders can be largely explained by their high degree; they are associated with a variety of comorbidities each.
Indeed, in the first two cases (Medicare and psychiatric patients), the same disorders are identified as hubs according to degree centrality. In the third case (intensive care patients), though, despite its more than two-fold lead in betweenness centrality, APHA has lower degree centrality than unspecified acute kidney failure, acute respiratory failure, severe sepsis, unspecified urinary tract infection, acidosis, and unspecified congestive heart failure. This discrepancy may reflect that APHA is a common result of several otherwise ontologically distinct types of injury and trauma.

\input{../../../tab/centrality-betweenness.tex}

Notably, centrality rankings of the disorders in a common ontology, but based on node versus group centralities calculated in different network models, were generally not concordant: in some case the concordance was weak, in others negative.
The limitations of our data and methodology prevented us from determining how much of this discordance was attributable to the data (i.e. the patient populations and collection protocols) versus to the effect of using group centralities versus crosswalking the incidence data before constructing the network.
See the SI Text for more details.


## Multiple and multivariate analysis

### Correlation structure and link determination for NAMCS encounters

The four correlation matrices $R_t,{R_t}',\hat\Rho_0,\hat\Rho_1$ exhibited both clear similarities and clear differences (Figure \ref{fig:namcs-biplots}).
Most notably, in the pairwise and full partial correlation matrices $R_t$ and ${R_t}'$, all 13 disorders loaded positively onto the first eigenvector, consistent with most comorbidity (controlling for prevalence) being explained by a one-dimensional spectrum between good and poor health.
This spectrum was most closely aligned, under both models, with hypertension (HT), hyperlipidemia (HLD), and ischemic heart disease (IHD).
The matrices differed starkly, though, in the proportion of variance captured by this first eigenvector---64% (pairwise) versus 22% (partial).
The joint distribution estimates $\hat\Rho_0$ and $\hat\Rho_1$ attributed only about 30--40% of variance to the first eigenvector, and this dimension was again most aligned with HT, HLD, and IHD.
In contrast, though, these models more clearly oriented some disorders, particularly depression and cancer, in opposition to the majority of others, including diabetes and obesity in addition to the aforementioned cardiac disorders.

Figure \ref{fig:namcs-networks} presents four networks on the node set of NAMCS chronic diseases, using the conventional, partial correlation, and two JDM constructions.
Most of the residual interactions in JDM1 were weaker than in JDM0---for example, the associations of HT with arthritis and with cerebrovascular disease (CVD). From the conventional to the multivariate models, some pairs switched from positive to indiscernible (chronic obstructive pulmonary disease (COPD) with osteoporosis (OP), arthritis with IHD), negative to indiscernible (IHD with obesity), or indiscernible to negative (CVD with obesity).
Notably, all three negative associations observed in the pairwise analysis were negative in every analysis, and every positive association observed under the strictest model (JDM1) was positive under the pairwise model.
See the SI Text for a quantitative comparison of the correlation estimates from each of the models.

\begin{figure}[h!]
\includegraphics[width=\textwidth]{../../../fig/namcs-networks.pdf}
\caption{
Four comorbidity networks constructed from the NAMCS chronic disease incidence data.
From left to right, then top to bottom:
conventional comorbidity network with links determined from a 5% TWER and weighted by $r_t$;
partial correlation comorbidity network adapted from the conventional network;
JDM network controlling only for disease prevalence, with links weighted by $\hat\Rho$;
JDM network also controlling for patient-level demographics.
Black (respectively, grey) links indicate positive (negative) associations.
\label{fig:namcs-networks}
}
\end{figure}

<!--
The triad of obesity, hypertension, and diabetes illustrate some of the discrepancies between these constructions: Each pair is strongly associated when other factors are ignored (the conventional pairwise network), and the associations are weakened or even flipped after controlling for confounding correlations (the partial correlation network). When the correlations are modeled simultaneously (the uncontrolled JDM network), the association between obesity and diabetes is weakened, and that between obesity and hypertension is rendered indiscernible. Then, after controlling for patient-level predictors (the controlled JDM network), all three associations are again discerned, with similar strengths as when estimated separately and corrected (the partial correlation network).
-->


### Correlation structure and link determination for MIMIC-III units

Results for the care unit populations from MIMIC-III were in several respects dissimilar to those for NAMCS.
Network diagrams showed similar behavior across the units but were less informative due to the number of nodes in each; they are included for each unit and each model as supporting figures.

Table \ref{tab:mimic-links} reports, for each care unit and each network model, the proportions of positive, negative, and indiscernible links, based on whether the confidence (resp. credible) interval of radius two standard errors (resp. standard deviations) about each correlation estimate contains zero.
The clear patterns across most units are that (a) in the pairwise network, vastly more links are positive than negative, and more negative than missing; (b) in the partial correlation network, links are positive, negative, and missing in roughly equal proportions; and (c) in the joint distribution network, vastly more links are missing than positive, and more positive than negative.
For the neonatal units (NWARD and NICU), which used smaller ontologies, the majority types (the positive links in the pairwise network and the missing links in the joint distribution network) were still more dominant, and much more of the links in the pairwise correlation network were missing.

\input{../../../tab/mimic-links.tex}

Scatterplots of correlation estimates from the different models are included as supporting figures.
In contrast to NAMCS, in this case the partial estimates were more correlated than the JDM estimates with the pairwise estimates.
Though the correlation was strong between estimates in the conventional model and the JDM among the most prevalent disorders, those for the least prevalent were essentially uncorrelated.
Meanwhile, the relationship between the pairwise and partial correlations was roughly a scaling one: For this and other non-neonatal units, the relationship was noisy but compatible with the constraint of passing through the origin.


### Centrality rankings of disorders

As in our comparisons of pairwise constructions, centrality rankings of disorders within each MIMIC unit varied greatly by network model, and overall patterns were not evident (Figure \ref{fig:mimic-centrality-biplots}).
Pairwise network rankings consistently stood apart from those of the multivariate model--based networks based on the medical intensive care unit (MICU) data; joint distribution network rankings tended to be more distinctive for the neonatal ward (NWARD), full partial network rankings were more distinctive for the neonatal intensive care unit (NICU), and for other units no two models consistently produced similar rankings.


# Acknowledgments \label{sec:acknowledgments}

Beverly Setzer and Lauren Geiser contributed code for processing data and constructing graphs as well as helpful methodological conversations.
David Hanauer kindly shared the pre-processed contingency table data from the University of Michigan Health System.
JCB was supported in part by an NIH T90 training grant (5T90DE021989-07).
<!--
[Blinded] contributed code for processing data and constructing graphs as well as helpful methodological conversations.
David Hanauer kindly shared the pre-processed contingency table data from the University of Michigan Health System.
[Blinded] was supported in part by [blinded].
-->


# Supporting tables and figures

Most supporting figures and one supporting table are made available as separate files. The remainder appear in the SI Text.
\LaTeX labels correspond to file paths.

\renewcommand\thefigure{S\arabic{figure}}
\setcounter{figure}{0}
\renewcommand\thetable{S\arabic{table}}
\setcounter{table}{0}

<!--graphs/pairs-summary.r-->

\begin{table}[h!]
\caption{Quintiles of binary association measures for pairs in each dataset for different evidential thresholds. ``B'' indicates Bonferroni correction.}
\label{tab:weights-1}
\end{table}

\begin{table}[h!]
\caption{Quintiles of binary association measures for pairs in each dataset for different evidential thresholds. ``B'' indicates Bonferroni correction.}
\label{tab:weights-2}
\end{table}

<!--graphs/quantiles.r-->

\begin{figure}[h!]
\caption{
For each data source (row), evaluative cutoff (column), and BAM (abscissa), a box-and-whisker plot of the quantiles at which the links of each comorbidity network were trimmed by the cutoff. The networks were constructed over the range of evidential (p-value) cutoffs $10^{-i},\ i=1,\ldots,6$ and for each p-value correction (none, FWER, FDR).
\label{fig:quantile-boxplot}
}
\end{figure}

<!--graphs/visualizations.r-->

\begin{figure}[h!]
\caption{
Network diagram (hairball plot) of the comorbidity network constructed from the Columbia data, using the evidential cutoff $\alpha<5\%$ with Bonferroni correction and the evaluative cutoff $OR\geq 6$. Clusters identified using the Walktrap algorithm are color-coded.
\label{fig:network-ggplot-columbia}
}
\end{figure}

\begin{figure}[h!]
\caption{
Network diagram (hairball plot) of the comorbidity network constructed from the MedPAR data on level-3 ICD9 codes, using the evidential cutoff $\alpha<5\%$ with Bonferroni correction and the evaluative cutoff $OR\geq 6$. Clusters identified using the Walktrap algorithm are color-coded.
\label{fig:network-ggplot-medicare1993-3}
}
\end{figure}

\begin{figure}[h!]
\caption{
Network diagram (hairball plot) of the comorbidity network constructed from the Sct. Hans data, using the evidential cutoff $\alpha<5\%$ with Bonferroni correction and the evaluative cutoff $OR\geq 6$. Clusters identified using the Walktrap algorithm are color-coded.
\label{fig:network-ggplot-scthans1998-2008}
}
\end{figure}

\begin{figure}[h!]
\caption{
Network diagram (hairball plot) of the comorbidity network constructed from the Stanford data, using the evidential cutoff $\alpha<5\%$ with Bonferroni correction and the evaluative cutoff $OR\geq 6$. Clusters identified using the Walktrap algorithm are color-coded.
\label{fig:network-ggplot-stanford2013}
}
\end{figure}

\begin{figure}[h!]
\caption{
Network diagram (hairball plot) of the comorbidity network constructed from the Columbia\* data, using the evidential cutoff $\alpha<5\%$ with Bonferroni correction and the evaluative cutoff $OR\geq 6$. Clusters identified using the Walktrap algorithm are color-coded.
\label{fig:network-ggplot-columbia2013}
}
\end{figure}

<!--graphs/statistics-degseq-summary.r-->

\begin{table}[h!]
\caption{Log-likelihood ratios $R$ and two-sided p-values $p$ from likelihood-ratio tests between four families of models to degree sequence tails. LRTs were performed for graphs constructed from each dataset using two p-value significance thresholds and corrections for family-wise error rate and for false discovery rate. Family 1 is preferred when $R>0$, Family 2 when $R<0$.}
\label{tab:degseq-compare}
\end{table}

\begin{figure}[h!]
\caption{
Diverging-color tilings of log-likelihood ratios $R$ from likelihood-ratio tests (LRT) between fits of different families of models to degree sequence tails. LRTs were performed for graphs constructed from each dataset using evidential cutoff $\alpha<0.05$ and both corrections. Family 1 is preferred when $R>0$, Family 2 when $R<0$. The boundary color of each tile indicates whether $R$ is positive or negative. The p-value from the LRT is printed on each tile.
\label{fig:degseq-compare}
}
\end{figure}

\begin{table}[h!]
\caption{Tail index (\(\xi\)) and tail exponent (\(\gamma\)) estimators that were successfully estimated on comorbidity networks.}
\label{tab:tail-estimation}
\end{table}

<!--graphs/statistics-summary.r-->

\begin{table}[h!]
\caption{
Values of several global statistics calculated on comorbidity networks constructed over a range of data sets and parameter settings. See Section~\ref{sec:global} in the main text.
\label{tab:summ}
}
\end{table}

\begin{figure}[h!]
\caption{
Mean values of several global statistics calculated on comorbidity networks constructed over a range of data sets and parameter settings, plotted against several measures of network size. See Section~\ref{sec:global} in the main text.
\label{fig:summ-pval}
}
\end{figure}

\begin{table}[h!]
\caption{LMs of network statistics on data source and test-wise error rate.}
\label{tab:lm1}
\end{table}

\begin{table}[h!]
\caption{Hierarchical models of network statistics on test-wise error rate, grouped by data source.}
\label{tab:hlm1-stargazer}
\end{table}

\begin{table}[h!]
\caption{Hierarchical models of network statistics on test-wise error rate and binary association measure, grouped by data source.}
\label{tab:hlm2-stargazer}
\end{table}

\begin{table}[h!]
\caption{Variance decomposition for hierarchical models of network statistics on test-wise error rate, grouped by data source.}
\label{tab:hlm1-xtable}
\end{table}

\begin{table}[h!]
\caption{Variance decomposition for hierarchical models of network statistics on test-wise error rate and binary association measure, grouped by data source.}
\label{tab:hlm2-xtable}
\end{table}

\begin{table}[h!]
\caption{Akaike information criteria for each model fitted to the values taken by each network statistic.}
\label{tab:aic}
\end{table}

<!--<community detection>-->

<!--
\begin{figure}[h!]
\caption{
Overlapping clusters obtained from the Columbia dataset, using complete-linkage hierarchical clustering on the $1/OR$ distance matrix (right) and fast--greedy community detection on the comorbidity network constructed using a Bonferroni-corrected $0.05$ TWER (left).
The hierarchical clustering tree was cut so as to maximize the adjusted Rand index of the two partitions of the concepts.
Concepts shared by both clusters are rendered in boldface.
\label{fig:overlap-clusters-fg}
}
\end{figure}

\begin{figure}[h!]
\caption{
Overlapping clusters obtained from the Columbia dataset, using complete-linkage hierarchical clustering on the $1/OR$ distance matrix (right) and edge betweenness community detection on the comorbidity network constructed using a Bonferroni-corrected $0.05$ TWER (left).
The hierarchical clustering tree was cut so as to maximize the adjusted Rand index of the two partitions of the concepts.
Concepts shared by both clusters are rendered in boldface.
\label{fig:overlap-clusters-eb}
}
\end{figure}
-->

<!--graphs/centralities-kendall.r-->

\begin{figure}[h!]
\caption{
For each data source and centrality measure, a correlation biplot of Kendall correlations between centrality rankings of disorders based on comorbidity networks constructed using the evidential cutoff $\alpha<0.05$, each of three corrections (none, FWER, FDR), and each of five BAMs (unit, odds ratio, Pearson correlation, Forbes coefficient, and tetrachoric). In this and other biplots, first and second eigenvectors are reversed if necessary so that each centroid lies in the first quadrant.
\label{fig:centrality-biplots}
}
\end{figure}

<!--graphs/centralities-top.r-->

\begin{table}[h!]
\caption{Most degree-central disorders in each comorbidity network, subject to Benjamini-Hochberg-corrected 5\% FDR.}
\label{tab:centrality-degree}
\end{table}

\begin{table}[h!]
\caption{Most closeness-central disorders in each comorbidity network, subject to Benjamini-Hochberg-corrected 5\% FDR.}
\label{tab:centrality-closeness}
\end{table}

<!--graphs/prevalences.r-->

\begin{table}[h!]
\caption{Most prevalent disorders in each comorbidity network.}
\label{tab:prevalence}
\end{table}

\begin{figure}[h!]
\caption{
Scatterplots of prevalences of disorders in the ICD9 ontology in different data sets using this ontology. Note the effect of the restriction, in the Michigan dataset, to disorders that appeared on at least 30 patient records.
\label{fig:prevalences-ICD9-5}
}
\end{figure}

\begin{figure}[h!]
\caption{
Scatterplots of prevalences of disorders in the Rzhetsky ontology in different data sets using this ontology. Note that the disorders included in the Columbia\* dataset exactly match their prevalences in the Columbia dataset.
\label{fig:prevalences-Rzhetsky}
}
\end{figure}

<!--graphs/centralities-kendall.r-->

\begin{figure}[h!]
\caption{
For each p-value correction and centrality measure, a correlation biplot of Kendall correlations between centrality rankings of disorders based on comorbidity networks constructed from each data set that uses or could be crosswalked to the Rzhetsky ontology. Centrality measures are node-based for data encoded using this ontology and group-based for data crosswalked to this ontology.
\label{fig:centrality-biplots-dataset-Rzhetsky}
}
\end{figure}

\begin{figure}[h!]
\caption{
For each p-value correction and centrality measure, a correlation biplot of Kendall correlations between centrality rankings of disorders based on comorbidity networks constructed from each data set that uses or could be crosswalked to the level-3 ICD9 ontology. Centrality measures are node-based for data encoded using this ontology and group-based for data crosswalked to this ontology.
\label{fig:centrality-biplots-dataset-ICD9-3}
}
\end{figure}

\begin{figure}[h!]
\caption{
For each p-value correction and centrality measure, a correlation biplot of Kendall correlations between centrality rankings of disorders based on comorbidity networks constructed from each data set that uses the level-5 ICD9 ontology.
\label{fig:centrality-biplots-dataset-ICD9-5}
}
\end{figure}

<!--graphs/centralities-top.r-->

\begin{table}[h!]
\caption{Most degree-central disorders in comorbidity networks using the ICD9-3 ontology, subject to Benjamini-Hochberg-corrected 5\% FDR.}
\label{tab:centrality-degree-ICD9-3}
\end{table}

\begin{table}[h!]
\caption{Most degree-central disorders in comorbidity networks using the Rzhetsky ontology, subject to Benjamini-Hochberg-corrected 5\% FDR.}
\label{tab:centrality-degree-Rzhetsky}
\end{table}

\begin{table}[h!]
\caption{Most closeness-central disorders in comorbidity networks using the ICD9-3 ontology, subject to Benjamini-Hochberg-corrected 5\% FDR.}
\label{tab:centrality-closeness-ICD9-3}
\end{table}

\begin{table}[h!]
\caption{Most closeness-central disorders in comorbidity networks using the Rzhetsky ontology, subject to Benjamini-Hochberg-corrected 5\% FDR.}
\label{tab:centrality-closeness-Rzhetsky}
\end{table}

<!--multivar/summary-namcs.r-->

\begin{figure}[h!]
\caption{Frequency--rank plot for the 13 chronic disorders recorded in the NAMCS sample.}
\label{fig:namcs-freqrank}
\end{figure}

\begin{figure}[h!]
\caption{Alluvial diagram of discernible signs of the population-level associations between NAMCS chronic disorders, using each of 4 network models: pairwise correlation, full partial correlation, endogenous joint distribution, and joint distribution controlling for exogenous predictors.}
\label{fig:namcs-pairs}
\end{figure}

\begin{table}[h!]
\caption{Estimated effects of demographic predictors on the incidence of chronic disorders in Model 1. Each value indicates the effect of the predictor on the mean of the normal distribution from which the latent variable is sampled (see the text). Estimates whose 95\% credible intervals contain zero are excluded.}
\label{tab:jdm-coefs}
\end{table}

\begin{table}[h!]
\caption{Histograms of estimates from the joint distribution model with exogenous (patient-level) predictors.
Left: Exogenous effects on disorder prevalence.
Right: Correlation (epidemiological comorbidity) accounted for by exogenous effects.}
\label{fig:namcs-hists}
\end{table}

<!--multivar/pairs-namcs.r-->

\begin{figure}[h!]
\caption{Alluvial diagram of discernible signs of the population-level associations between NAMCS chronic disorders, using each of 4 network models: pairwise correlation, full partial correlation, endogenous joint distribution, and joint distribution controlling for exogenous predictors.}
\label{fig:namcs-pairs-alluvial-alluvium}
\end{figure}

<!--multivar/visualize-namcs.r-->

\begin{figure}[h!]
\caption{
For each of four models, a correlation biplot of estimated latent correlations between 13 chronic disorders in the NAMCS sample.
\label{fig:namcs-biplots}
}
\end{figure}

<!--multivar/summary-mimic.r-->

\begin{figure}[h!]
\caption{
For each critical care unit in the MIMIC-III database, a frequency--rank plot for the recorded diagnoses, crosswalked to CCS codes.
\label{fig:mimic-freqrank}
}
\end{figure}

<!--multivar/pairs-mimic.r-->

\begin{figure}[h!]
\caption{Scatterplots between the pairwise tetrachoric correlations $r_t$, the full partial correlations ${r_t}'$, and the correlation estimates $\hat\rho_0$ from the endogenous JDM for the CSRU population.}
\label{fig:mimic-csru-pairs}
\end{figure}

\begin{figure}[h!]
\caption{Alluvial diagrams of discernible signs of the population-level associations between MIMIC diagnoses within each admission unit cohort, using each of 3 network models: pairwise correlation, full partial correlation, and endogenous joint distribution.}
\label{fig:mimic-pairs-alluvial-alluvium}
\end{figure}

<!--multivar/visualize-mimic.r-->

\begin{figure}[h!]
\caption{
Three comorbidity networks constructed from the CCU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.
\label{fig:mimic-ccu-networks}
}
\end{figure}

\begin{figure}[h!]
\caption{
Three comorbidity networks constructed from the CSRU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.
\label{fig:mimic-csru-networks}
}
\end{figure}

\begin{figure}[h!]
\caption{
Three comorbidity networks constructed from the MICU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.
\label{fig:mimic-micu-networks}
}
\end{figure}

\begin{figure}[h!]
\caption{
Three comorbidity networks constructed from the NICU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.
\label{fig:mimic-nicu-networks}
}
\end{figure}

\begin{figure}[h!]
\caption{
Three comorbidity networks constructed from the NWARD population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.
\label{fig:mimic-nward-networks}
}
\end{figure}

\begin{figure}[h!]
\caption{
Three comorbidity networks constructed from the SICU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.
\label{fig:mimic-sicu-networks}
}
\end{figure}

\begin{figure}[h!]
\caption{
Three comorbidity networks constructed from the TSICU population.
From left to right: Sample tetrachoric correlations $r_t$, partial tetrachoric correlations ${r_t}'$, and correlation estimates $\hat\rho_0$ from the endogenous JDM.
\label{fig:mimic-tsicu-networks}
}
\end{figure}

<!--multivar/centrality-mimic.r-->

\begin{figure}[h!]
\caption{
For each care unit and centrality measure, a correlation biplot of Kendall correlations between centrality rankings of disorders based on each of four comorbidity network models: pairwise correlations, full partial correlations, and a joint distribution model without exogenous effects.
\label{fig:mimic-centrality-biplots}
}
\end{figure}

# References \label{sec:references}

<!--
# To generate the PDF, execute the following on the command line:
pandoc comorbidity.md \
-o comorbidity.pdf \
-s \
--number-sections \
--bibliography=../comorbidity.bib \
--csl=../network-modeling-analysis-in-health-informatics-and-bioinformatics.csl
-->
