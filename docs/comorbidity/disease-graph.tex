\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage{hyperref}
\hypersetup{unicode=true,
            pdftitle={Validity and reliability of the disease graph concept},
            pdfauthor={Cory Brunson},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi
\usepackage{dcolumn}
\usepackage{geometry}
\usepackage{pdflscape}

\title{Validity and reliability of the disease graph concept}
\author{Cory Brunson}
\date{2017-05-10}

\begin{document}
\maketitle

\subsection{Introduction}\label{introduction}

In the past decade, several research teams have mined patient-level
datasets for over- and under-represented pairs of clinical events. While
much of the emphasis has been on the discovery of heretofore
unrecognized comorbidities and the tracking of disease progression,
comorbid pairs are often represented as a mathematical graph, with nodes
representing diseases and links indicating comorbidity. While this
representation is conceptually and visually appealing, it obscures the
hierarchical structure and statistical uncertainty of the underlying
co-occurrence data. It also implicitly suggests the application of
graph-theoretic methods based on motifs, walks, and internode distances.
These properties are sensitive to link addition and deletion, and
therefore to the strength of evidence and the comorbidity measure used
to determine links. Our objective is to describe the dependencies of the
resulting graph structure on the source of data and graph construction
process.

\subsection{Background}\label{background}

``Network'' is one of many of qualifiers in circulation (see also
``systems'', ``precision'', and ``personalized medicine'') referring to
highly computational systems-level medical research, inspired and driven
in part by concurrent successes in bioinformatics. While any complex
system can be conceptualized as a network, network science rests most
fundamentally on the theory of mathematical graphs, and network medicine
is often presented within this framework. \textbf{Expand to incorporate
observations from review articles. Include common claims and
assumptions.}

\subsubsection{Clinical co-occurrence
networks}\label{clinical-co-occurrence-networks}

It stands to reason that the tools of graph theory would play a major
role in the network analysis of medical data, but at the
anatomical/physiological level, in which networks are used to model
clinical phenotypes (as opposed to individuals and institutions at the
social level or genes and molecules at the biological level), this is
the case: The most common uses of graph-theoretic methods are for
visualization and community detection, applications arguably
better-suited to other tools, while essential graph-theoretic concepts
like motifs, walks, and distances are rarely invoked.

An exception is the use of temporal graphs, constructed from directed
pairs or longer sequences of clinical events along individual patient
timelines, which relies on directed (temporal) paths to draw meaningful
insights {[}@jmoeesjjb2014{]} and to distinguish itself from non-network
models {[}@cbcbd2009{]}. The theory behind this approach is quite young
{[}@\ldots{}{]}, and only a handful of independent studies constitute it
{[}@bl2017{]}. We focus our attention on the more common static models
with the potential to draw upon a much larger body of mathematical
theory and software implementations.

\textbf{Expand:} case studies that used graph theory

\begin{itemize}
\tightlist
\item
  note any justifications or assumptions made
\item
  force-directed layout algorithms for visualization
\item
  community detection
\item
  motif mining
\item
  walks, paths, and distances
\end{itemize}

\subsubsection{Objectives}\label{objectives}

We aim to test the following properties of the disease graph as a
theoretical construct and as an empirical model:

\begin{itemize}
\tightlist
\item
  Utility: Has the construct yielded new insights and results that would
  be inaccessible or less straightforward under a different conception
  of disease space?
\item
  Endogenous reliability: How are the purely graph-theoretic properties
  of the model vary across datasets and constructions, and can any
  systematic differences be accounted for? This amounts to a structural
  sensitivity analysis of graphs constructed from different datasets
  using different comorbidity measures and thresholds to determine
  links.
\item
  Exogenous reliability: Do the graph models relate to ontological
  hierarchies, known molecular associations, and patient factors in a
  consistent way?
\end{itemize}

\subsection{Materials and Methods}\label{materials-and-methods}

For the endogenous reliability tests, we (re)construct several disease
graph models from existing literature based on publicly available or
graciously shared co-occurrence data obtained from a variety of clinical
datasets, using several binary association measures and cutoff values.
We then compute a panel of common statistics for each resulting graph
and examine how the values of these statistics vary by dataset, measure,
and cutoff.

For the exogenous reliability tests, we assess the relationship between
each graph and other knowledge sources, taking advantage of the
ontologies used to label the nodes and the crosswalks between them
developed for the original studies. \textbf{Talk with Tom about what
analyses to perform here.}

\subsubsection{Construct
conceptualization}\label{construct-conceptualization}

\subsubsection{Data}\label{data}

The following datasets included full (\(2\times 2\)) contingency tables
for each pair of conditions, from which most other included summary
statistics could be recovered:

\begin{itemize}
\tightlist
\item
  \(C\): 1.5 million patients from the Columbia University Medical
  Center clinical database {[}@rwpz2007{]}. The authors used a custom
  mapping from ICD9 codes to a phenomenological disease ontology.
  Frequency counts for every pair of diseases was included in the
  dataset. This is the only dataset we obtained that includes statistics
  for \emph{every} pair of conditions.
\item
  \(P\) and \(Q\): 32 million patients from the Medicare Provided
  Analysis and Review files over 1990--1993 {[}@hbbc2009{]}. The authors
  constructed two datasets, based on complete ICD9 codes and on their
  level-3 prefixes. Both datasets are limited to a subset of pairs,
  though the authors do not indicate how this subsetting was done.
\item
  \(H\): 5,543 patients from the Sct. Hans Hospital
  {[}@rjsdahsbjwjb2011{]}. The authors used level-3 ICD10 codes. This
  dataset carries a specific bias in having come from a psychiatric
  hospital. \textbf{Cutoff?}
\item
  \(M\): 1.62 million patients from the University of Michigan Health
  System {[}@hr2013{]}. The authors used full ICD9 codes.
  \textbf{Cutoff?}
\item
  \(S\): 277,290 patients from the Stanford Translational Research
  Integrated Database Environment (STRIDE) at Stanford University
  {[}@bscba2016{]}. The authors collected records in 2013 and excluded
  records before 2008, internally inconsistent records, patients older
  than 90, conditions with prevalence below 50, and pairs of conditions
  with co-occurrence below 5. The supporting information files included
  only pairs that were discernibly comorbid and that were grouped
  together in a cluster analysis.
\end{itemize}

In most cases, \(\chi^2\) tests on the contingency tables yielded
p-values ranging from \(0\) to \(1-\varepsilon\), \(\varepsilon<.001\).
The exception was the data from Sct. Hans, which only included highly
discernible comorbid pairs (\(p<10^{-4}\)). One additional dataset did
not include full contingency tables but did include relative rate
calculations for each pair of conditions {[}@rjsdahsbjwjb2011{]}.

Supplementing these patient-level datasets, we calculated pairwise
contingency tables for two publicly available datasets:

\begin{itemize}
\tightlist
\item
  10 years of data from the National Ambulatory Medical Care
  Survey,\footnote{https://www.cdc.gov/nchs/ahcd/ahcd\_questionnaires.htm}
  conducted by hundreds of physicians each year. Each entry describes a
  single encounter, and each physician collects data for only two weeks
  out of the year, \emph{without} linking records for encounters with
  the same patient. Thus, the data does not contain long-term data for
  any patient, and all multiple diagnoses are recorded at a single
  encounter, presumably (\textbf{?}) with a single physician. This is
  expected to introduce a powerful bias toward links between conditions
  relevant to common domains of specialty care.
\item
  \textbf{??} years of data from the Vaccine Adverse Event Reporting
  System,\footnote{https://vaers.hhs.gov/data/index} maintaned by the
  United States Department of Health and Human Services. Each entry
  contains information on from a single adverse event report, including
  about the patient, the vaccine(s) administered, and the symptom(s)
  observed. (See {[}@bb2011{]} and several follow-up studies for a novel
  network analysis of these data.) We construct contingency tables of
  symptom co-occurrence across the reports.
\end{itemize}

Both of these datasets are inappropriate candidates for constructing
comorbidity graphs, if the goal is to produce a meaningful model of
population-level comorbidities. We therefore expect to be able to
recognize systematic differences between the structure of these graphs
and that of the graphs constructed from more complete patient records.
Of course, many biases are at play in the construction process. For
example, comorbidity graphs constructed from Medicare data
{[}@hbbc2009{]} are likely to omit comorbidities expressed in infants
and adolescents {[}@ckt2014{]}. However, to the extent that population
comorbidity graphs exhibit a characteristic structure, they should be
distinguishable not only from random null models but from graphs
constructed using the same process but unsound data.

\subsubsection{Model construction}\label{model-construction}

We adopt four measures of binary association for the disease pairs in
each dataset, formulated here in terms of the entries \(a,b,c,d\) in a
\(2\times 2\) contingency table (since all measures are symmetric, the
roles of \(b\) and \(c\) are interchangeable):

\begin{itemize}
\tightlist
\item
  Pearson's tetrachoric correlation coefficient
  \(\phi=\frac{ad-bc}{\sqrt{(a+b)(a+c)(b+d)(c+d)}}\), sometimes referred
  to as ``the'' binary correlation coefficient, designated A30 in
  {[}@h1981{]}.
\item
  Forbes' coefficient of association
  \(F=\frac{a(a+b+c+d)}{(a+b)(a+c)}\), often misleadingly called
  ``relative risk'' in the comorbidity network literature, designated
  A40 in {[}@h1981{]}.
\item
  Jaccard's coefficient of community \(J=\frac{a}{a+b+c}\), designated
  A4 in {[}@h1981{]}.
\item
  The correlation coefficient \(T=\frac{a}{\sqrt{(a+b)(a+c)}}\) obtained
  by Thomson {[}@t1916{]} (possibly among others), designated A11 in
  {[}@h1981{]}.
\end{itemize}

For each dataset \(d\) and each measure \(m\), we construct a disease
graph by measuring the association between each pair of diseases and
linking each pair whose \(\chi^2\) statistic is statistically
discernible, i.e.~corresponds to a false discovery rate (FDR) below
\(\alpha\), \emph{and} whose association is greater than some threshold.
We use three choices of threshold, based on three increasingly strict
quantiles \(\theta\): 20\%, 50\%, and 90\%, in addition to the 0\%
quantile resulting in the graph of all statistically discernible links.
This obtains the collection of graphs \(G^d_{\alpha,m,\theta}\) with
\(\alpha=.05\) and ranging over \(d=C,P,Q,H,M,S\), \(m=\phi,F,J,T\), and
\(\theta=.2,.5,.9\). We also construct four graphs each using the
Bonferroni-corrected FDR \(\alpha=5\%/n\), where \(n\) is the sample
size.

Note that these quantile-based thresholds introduce a confounder on the
data source: The subset of pairs included in a dataset determine the
distribution of values attained by each measure. This implies that the
\(\{G^d_{\alpha,m,\theta}\}_d\) do not form an intrinsically meaningful
family. Instead, we can and do consider the effect
\(\Delta s(G^d_{\alpha,m,\theta})/\Delta\theta\) for a graph-theoretic
statistic \(s\) of interest. (Clearly an effect will be present when
\(s\) is the graph density or mean degree, and any other effects may be
partly or entirely due to this effect.)

\textbf{Note: Association measures involving the sample size \(n\) may
depend on whether \(n\) represents the number of patients for whom
records are available or the theoretical number of patients in the
population covered by the source of the records (Either Rzhetsky et al
or Blair et al discuss this.)}

We calculate the following single-value statistics on each graph:

\begin{itemize}
\tightlist
\item
  mean degree: the mean number of links incident to each disease
\item
  Gini index of the degree distribution {[}following @b2013{]}
\item
  classical clustering coefficient
\item
  mean distance
\item
  modularity
\end{itemize}

If the degree and distance distributions of the graphs appear to have
been drawn from a consistent statistical family (e.g.~power-law tails,
Poisson), then we also record the parameters for the MLE fit of the
family to each distribution.

Ideally, we would also calculate the following distance matrices for the
full collection of models:

\begin{itemize}
\tightlist
\item
  Kolmogorov--Smirnov distance between degree distributions
\item
  Spearman rank correlation between degree sequences
\item
  Kolmogorov--Smirnov distance between distance distributions
\item
  Spearman rank correlation between closeness, betweenness, and
  eigenvector centrality scores
\item
  adjusted Rand index\footnote{Additional tests of partition similarity
    are available and may be preferable to the Rand index.} between
  Newman--Girvan and Walktrap community assignments
\item
  Wallace coefficient between \textbf{{[}method{]}} core--periphery
  assignments
\end{itemize}

However, the datasets use different disease ontologies. This means that
we cannot make most of these comparisons directly based on graphs
constructed from different datasets, which therefore have different node
sets. (We could compare the distribution families of the node degrees or
internode distances.)

We can partially overcome this limitation by way of a mapping between
the ontologies on which two models were built. For example, for their
study, {[}@rwpz2007{]} introduced an ontology that combined subsets of
ICD9 terms into single phenotypes. Any partition of their graph can
therefore be unambiguously extended to a partition of a graph on the
ICD9 ontology, by assigning each term the community label its image
received under the original mapping. In general, two ontologies may have
one-to-many conversions in both directions, i.e.~one won't be a
refinement of the other. In this case, formal concept analysis (FCA)
allows us to combine both into a single ontology, whose formal concepts
constitute the new terms. We can then compare the partitions induced
onto the formal concepts from the community partitions obtained on the
two original graphs. (Discrepant assignments to the same formal concept
from constituent nodes originally assigned to different parts may need
to be reconciled.)

\subsection{Results}\label{results}

\subsubsection{Endogenous reliability}\label{endogenous-reliability}

We fit separate linear regression models to each graph statistic as a
function of the source of data (categorical), the comorbidity measure
(categorical), and the cutoff value of the measure (continuous). Table 1
presents the results, which take the DNPR dataset as the source baseline
\textbf{(Change this to the Medicare 3-character dataset.} and the
absence of a comorbidity measure as the source measure (i.e., the case
that no cutoff is imposed). \textbf{Currently, the model omits the
cutoff value as a predictor; need to fix this in the data generation
script.}

\begin{landscape}
\input{../../tab/lmfits1.tex}
\end{landscape}

The table indicates, for each graph statistic, a sharp divide between
the categorical predictors that influence it (\(p<.01\)) and those that
don't (\(p\geq .1\)). The choice of dataset matters strongly to each
graph statistic, though \emph{which} datasets have discernible effects
varies from statistic to statistic. Notably, only the mean internode
distance discriminates the NAMCS, VAERS, and Sct. Hans graphs from the
``unbiased'' comorbidity graphs, though it also discriminates most of
those from each other. That is to say, no single graph statistic
examined here distinguishes the comorbidity graphs with systematic
biases from the rest.

The comorbidity measures have discernible effects on some of the graph
statistics, namely the Gini index of the degree sequence, the degree
assortativity, and the triadic closure. Even in these cases, the effect
of the choice of measure is usually an order of magnitude less than that
of the choice of dataset, suggesting that the choice of dataset is of
primary importance to the graph structure.

A variance decomposition reinforces this suggestion. \textbf{Report the
results.} A principal components analysis (PCA) of the 10 graph
statistics is not very well clustered by dataset, but much better by
dataset than by comorbidity measure. In Figure 1, which projects the
10-dimensional point cloud to its first two principal components (PCs),
the point clouds corresponding to specific datasets are encircled by
standard error ellipses at a FDR of 5\%, and the thickness of each
ellipse reflects the number of diseases (nodes) used to construct the
disease graph. The first two PCs account for only 68\% of the variance,
and the first three account for 83\%, with the fourth PC containing only
6\%. We suggest that the graph statistics included here capture three
dimensions of variability in the structure of disease graphs, each of
them sensitive to the choice of dataset but only two to the choice of
comorbidity measure. \textbf{Explain this based on variance
decomposition.}

\begin{figure}[htbp]
\centering
\includegraphics{../../fig/pca.pdf}
\caption{PCA biplot}
\end{figure}

\end{document}
