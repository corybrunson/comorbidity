# run this script
# sh comorbidity-figs-tabs.sh

# confirm location
[ ! -f ../../comorbidity.Rproj ] && {
  echo "Run this script from 'comorbidity/docs/comorbidity/'."; exit 1;
}


## main text: figures

cp ../../fig/cna-sensitivity-objectives.pdf JAMIA-Open/Figure1.pdf

# summary statistics versus p-value cutoffs
#cp ../../fig/summ-pval.pdf

# global statistics PCA
cp ../../fig/pca-label.pdf bioarXiv/Figure1.pdf
cp ../../fig/pca-label.pdf JAMIA-Open/Figure2.pdf

# modularity and Dunn index of partitions
#cp ../../fig/community-dunn-modularity-n.pdf

# overlapping clusters (not saved as .tex files)

# centrality heatmap
#cp ../../fig/centrality-heatmap-construction.pdf

# sample of centrality biplots
cp ../../fig/centrality-biplots-Michigan.pdf JAMIA-Open/Figure3.pdf

# multivariate NAMCS networks
cp ../../fig/namcs-networks.pdf bioarXiv/Figure2.pdf
cp ../../fig/namcs-networks.pdf JAMIA-Open/Figure4.pdf


## main text: tables

# data sets (not saved as .tex files)

# density table
cp ../../tab/densities.tex bioarXiv/Table2.tex

# multiple regression model with BAMs
cp ../../tab/model2.tex bioarXiv/Table3.tex

# overlap matrix for overlapping clusters
#cp ../../tab/overlap-matrix-wt.tex

# inertia decomposition of centrality rankings
cp ../../tab/centrality-concordance-inertia.tex bioarXiv/Table4.tex

# most betweenness-central disorders
cp ../../tab/centrality-betweenness.tex bioarXiv/Table5.tex

# link determinations for MIMIC units
cp ../../tab/mimic-links.tex bioarXiv/Table6.tex


## supporting text: figures

# quantile boxplots
cp ../../fig/quantile-boxplot.pdf bioarXiv/FigureS1.pdf
cp ../../fig/quantile-boxplot.pdf JAMIA-Open/FigureS1.pdf

# network hairball plots for secondary use data sets
cp ../../fig/network-ggplot-columbia.pdf bioarXiv/FigureS2.pdf
cp ../../fig/network-ggplot-columbia.pdf JAMIA-Open/FigureS2.pdf
cp ../../fig/network-ggplot-medicare1993-3.pdf bioarXiv/FigureS3.pdf
cp ../../fig/network-ggplot-scthans1998-2008.pdf bioarXiv/FigureS4.pdf
cp ../../fig/network-ggplot-stanford2013.pdf bioarXiv/FigureS5.pdf
cp ../../fig/network-ggplot-columbia2013.pdf bioarXiv/FigureS6.pdf

# diverging-color tilings of likelihood-ratio tests
cp ../../fig/degseq-compare.pdf bioarXiv/FigureS7.pdf

# t-SNE projection
#cp ../../fig/tsne.pdf

# summary statistics versus p-value cutoffs
cp ../../fig/summ-pval.pdf bioarXiv/FigureS8.pdf
cp ../../fig/summ-pval.pdf JAMIA-Open/FigureS3.pdf

# overlapping clusters (not saved as .tex files)

# centrality biplots
cp ../../fig/centrality-biplots.pdf bioarXiv/FigureS9.pdf
cp ../../fig/centrality-biplots.pdf JAMIA-Open/FigureS4.pdf

# prevalence scatterplots
cp ../../fig/prevalences-ICD9-5.pdf bioarXiv/FigureS10.pdf
cp ../../fig/prevalences-ICD9-5.pdf JAMIA-Open/FigureS5.pdf
cp ../../fig/prevalences-Rzhetsky.pdf bioarXiv/FigureS11.pdf
cp ../../fig/prevalences-Rzhetsky.pdf JAMIA-Open/FigureS6.pdf

# centrality heatmaps
#cp ../../fig/centrality-heatmap-dataset-ICD9-5.pdf
#cp ../../fig/centrality-heatmap-dataset-ICD9-3.pdf
#cp ../../fig/centrality-heatmap-dataset-Rzhetsky.pdf

# centrality biplots by data set
cp ../../fig/centrality-biplots-dataset-Rzhetsky.pdf bioarXiv/FigureS12.pdf
cp ../../fig/centrality-biplots-dataset-Rzhetsky.pdf JAMIA-Open/FigureS7.pdf
cp ../../fig/centrality-biplots-dataset-ICD9-3.pdf bioarXiv/FigureS13.pdf
cp ../../fig/centrality-biplots-dataset-ICD9-3.pdf JAMIA-Open/FigureS8.pdf
cp ../../fig/centrality-biplots-dataset-ICD9-5.pdf bioarXiv/FigureS14.pdf
cp ../../fig/centrality-biplots-dataset-ICD9-5.pdf JAMIA-Open/FigureS9.pdf

# NAMCS frequency-rank plot
cp ../../fig/namcs-freqrank.pdf bioarXiv/FigureS15.pdf
cp ../../fig/namcs-freqrank.pdf JAMIA-Open/FigureS10.pdf

# scatterplots of NAMCS correlation estimates
cp ../../fig/namcs-pairs.pdf bioarXiv/FigureS16.pdf
cp ../../fig/namcs-pairs.pdf JAMIA-Open/FigureS11.pdf

# histograms of NAMCS effect estimates
cp ../../fig/namcs-hists.pdf bioarXiv/FigureS17.pdf
cp ../../fig/namcs-hists.pdf JAMIA-Open/FigureS12.pdf

# link determination NAMCS alluvial diagram
cp ../../fig/namcs-pairs-alluvial-alluvium.pdf bioarXiv/FigureS18.pdf
cp ../../fig/namcs-pairs-alluvial-alluvium.pdf JAMIA-Open/FigureS13.pdf

# NAMCS correlation biplot
cp ../../fig/namcs-biplots.pdf bioarXiv/FigureS19.pdf
cp ../../fig/namcs-biplots.pdf JAMIA-Open/FigureS14.pdf

# MIMIC frequency-rank plot
cp ../../fig/mimic-freqrank.pdf bioarXiv/FigureS20.pdf
cp ../../fig/mimic-freqrank.pdf JAMIA-Open/FigureS15.pdf

# scatterplots of MIMIC CSRU correlation estimates
cp ../../fig/mimic-csru-pairs.pdf bioarXiv/FigureS21.pdf
cp ../../fig/mimic-csru-pairs.pdf JAMIA-Open/FigureS16.pdf

# link determination MIMIC alluvial diagram
cp ../../fig/mimic-pairs-alluvial-alluvium.pdf bioarXiv/FigureS22.pdf
cp ../../fig/mimic-pairs-alluvial-alluvium.pdf JAMIA-Open/FigureS17.pdf

# network hairball plots for MIMIC units
cp ../../fig/mimic-ccu-networks.pdf bioarXiv/FigureS23.pdf
cp ../../fig/mimic-csru-networks.pdf bioarXiv/FigureS24.pdf
cp ../../fig/mimic-csru-networks.pdf JAMIA-Open/FigureS18.pdf
cp ../../fig/mimic-micu-networks.pdf bioarXiv/FigureS25.pdf
cp ../../fig/mimic-nicu-networks.pdf bioarXiv/FigureS26.pdf
cp ../../fig/mimic-nward-networks.pdf bioarXiv/FigureS27.pdf
cp ../../fig/mimic-sicu-networks.pdf bioarXiv/FigureS28.pdf
cp ../../fig/mimic-tsicu-networks.pdf bioarXiv/FigureS29.pdf

# centrality biplots for MIMIC units
cp ../../fig/mimic-centrality-biplots.pdf bioarXiv/FigureS30.pdf
cp ../../fig/mimic-centrality-biplots.pdf JAMIA-Open/FigureS19.pdf


## supporting text: tables

# density table
cp ../../tab/densities.tex JAMIA-Open/TableS1.tex

# binary association quintiles
cp ../../tab/weights-1.tex bioarXiv/TableS1.tex
cp ../../tab/weights-1.tex JAMIA-Open/TableS2.tex
cp ../../tab/weights-2.tex bioarXiv/TableS2.tex
cp ../../tab/weights-2.tex JAMIA-Open/TableS3.tex

# degree sequence comparisons table
cp ../../tab/degseq-compare.tex bioarXiv/TableS3.tex
cp ../../tab/degseq-compare.tex JAMIA-Open/TableS4.tex

# degree sequence tail estimations table
cp ../../tab/tail-estimation.tex bioarXiv/TableS4.tex
cp ../../tab/tail-estimation.tex JAMIA-Open/TableS5.tex

# multiple regression model without BAMs
cp ../../tab/model1.tex bioarXiv/TableS6.tex
cp ../../tab/model1.tex JAMIA-Open/TableS7.tex

# hierarchical regression models with and without BAMs
cp ../../tab/model3-stargazer.tex bioarXiv/TableS7.tex
cp ../../tab/model3-stargazer.tex JAMIA-Open/TableS8.tex
cp ../../tab/model4-stargazer.tex bioarXiv/TableS8.tex
cp ../../tab/model4-stargazer.tex JAMIA-Open/TableS9.tex

# hierarchical regression variance decomposition
cp ../../tab/model3-xtable.tex bioarXiv/TableS9.tex
cp ../../tab/model3-xtable.tex JAMIA-Open/TableS10.tex
cp ../../tab/model4-xtable.tex bioarXiv/TableS10.tex
cp ../../tab/model4-xtable.tex JAMIA-Open/TableS11.tex

# AIC for four regression models
cp ../../tab/aic.tex bioarXiv/TableS11.tex
cp ../../tab/aic.tex JAMIA-Open/TableS12.tex

# inertia decomposition of centrality rankings
cp ../../tab/centrality-concordance-inertia.tex JAMIA-Open/TableS13.tex

# most degree-central disorders
cp ../../tab/centrality-degree.tex bioarXiv/TableS12.tex
cp ../../tab/centrality-degree.tex JAMIA-Open/TableS14.tex
cp ../../tab/centrality-betweenness.tex JAMIA-Open/TableS15.tex
cp ../../tab/centrality-closeness.tex bioarXiv/TableS13.tex
cp ../../tab/centrality-closeness.tex JAMIA-Open/TableS16.tex

# prevalence tables
cp ../../tab/prevalence.tex bioarXiv/TableS14.tex
cp ../../tab/prevalence.tex JAMIA-Open/TableS17.tex

# most-central disorders for fixed ontologies
cp ../../tab/centrality-degree-Rzhetsky.tex bioarXiv/TableS15.tex
cp ../../tab/centrality-degree-Rzhetsky.tex JAMIA-Open/TableS18.tex
cp ../../tab/centrality-degree-ICD9-3.tex bioarXiv/TableS16.tex
cp ../../tab/centrality-degree-ICD9-3.tex JAMIA-Open/TableS19.tex
cp ../../tab/centrality-closeness-Rzhetsky.tex bioarXiv/TableS17.tex
cp ../../tab/centrality-closeness-Rzhetsky.tex JAMIA-Open/TableS20.tex
cp ../../tab/centrality-closeness-ICD9-3.tex bioarXiv/TableS18.tex
cp ../../tab/centrality-closeness-ICD9-3.tex JAMIA-Open/TableS21.tex

# link determinations for MIMIC units
cp ../../tab/mimic-links.tex JAMIA-Open/TableS22.tex

# NAMCS JDM coefficient estimates
cp ../../tab/jdm-coefs.tex bioarXiv/TableS19.tex
cp ../../tab/jdm-coefs.tex JAMIA-Open/TableS23.tex


## supporting tables

cp ../../tab/summ.csv bioarXiv/TableS5.csv
cp ../../tab/summ.csv JAMIA-Open/TableS6.csv
