---
title: "Reproducibility and sensitivity of comorbidity network analysis"
author:
  - J.C. Brunson (UConn Health)
  - T.P. Agresta (UConn Health)
  - R.C. Laubenbacher (UConn Health)
output:
  pdf_document:
    fig_caption: yes
    keep_tex: yes
    number_sections: no
header-includes:
  - \usepackage{dcolumn}
---

<!--
To generate the PDF, execute the following in bash:
```sh
pandoc comorbidity-title.md \
-o comorbidity-title.pdf \
-s \
-S \

```
-->

**Correspondence to:**\
Jason Cory Brunson\
Center for Quantitative Medicine\
UConn Health\
263 Farmington Avenue\
Farmington, CT 06030--6033\
Phone: (+1) 860 679 1354\
FAX: (+1) 860 679 7522\
E-mail: brunson@uchc.edu\
