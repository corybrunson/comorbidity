z_cutoff <- 2

# joint distribution model JAGS settings
n_chains <- 5
n_iter <- 100000
n_burnin <- 50000
