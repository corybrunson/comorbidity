
# cluster sample
# https://github.com/tidyverse/dplyr/issues/361#issuecomment-284305359
cluster_sample_n <- function(
  tbl, size, replace = FALSE, weight = NULL, average = FALSE
) {
  weight <- rlang::eval_tidy(enquo(weight), tbl)
  wt_fun <- if (average) mean else sum
  grps <- unlist(lapply(groups(tbl), as.character))
  summ <- summarize(tbl, .weight = if (is.null(weight)) 1 else wt_fun(weight))
  keep <- sample_n(summ, size = size, replace = replace, weight = .weight)
  group_by_at(left_join(select(keep, -.weight), tbl, by = grps), .vars = grps)
}
cluster_sample_frac <- function(
  tbl, size, replace = FALSE, weight = NULL, average = FALSE
) {
  weight <- rlang::eval_tidy(enquo(weight), tbl)
  wt_fun <- if (average) mean else sum
  grps <- unlist(lapply(groups(tbl), as.character))
  summ <- summarize(tbl, .weight = if (is.null(weight)) 1 else wt_fun(weight))
  keep <- sample_frac(summ, size = size, replace = replace, weight = .weight)
  group_by_at(left_join(select(keep, -.weight), tbl, by = grps), .vars = grps)
}
