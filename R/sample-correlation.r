# sample correlation matrix
# 1. Gaussian
# 2. Uniform positive semi-definite
# https://www.sciencedirect.com/science/article/pii/S0047259X09000876
# https://stats.stackexchange.com/a/125017/68743
# 3. "realistic"
# https://arxiv.org/pdf/1106.5834.pdf
# alternative: sample 0,1-matrices
# 4. Uniform (sequential information sampling)

sample_correlation <- function(n = 1, method = NULL, ...) {
  if (is.null(method)) method <- "uniform"
  fun <- get(paste0("sample_correlation_", method))
  fun(n = n, ...)
}

sample_correlation_onion <- function(n = 1, eta = 0) {
  if (eta != 0 & n > 1) {
    return(sample_correlation_onion_extended(n = n, eta = eta))
  }
  s <- matrix(1)
  k <- 1
  while (k < n) {
    k <- k + 1
    y <- rbeta(n = 1, shape1 = (k - 1) / 2, shape2 = (n - k) / 2)
    theta <- as.matrix(runif(n = k - 1))
    theta <- theta / norm(theta, "2")
    e <- eigen(s)
    r <- e$vectors %*% diag(sqrt(e$values)) %*% t(e$vectors)
    q <- r %*% (sqrt(y) * theta)
    s <- rbind(cbind(s, q), cbind(t(q), 1))
  }
  s
}

sample_correlation_onion_extended <- function(n = 1, eta = 0) {
  if (n == 1) {
    return(sample_correlation_onion(n = n))
  }
  beta <- eta + (n - 2) / 2
  r12 <- 2 * rbeta(n = 1, shape1 = beta, shape2 = beta) - 1
  s <- matrix(c(1, r12, r12, 1), nrow = 2)
  k <- 2
  while (k < n) {
    k <- k + 1
    beta <- beta - .5
    y <- rbeta(n = 1, shape1 = (k - 1) / 2, shape2 = beta)
    theta <- as.matrix(runif(n = k - 1))
    theta <- theta / norm(theta, "2")
    e <- eigen(s)
    r <- e$vectors %*% diag(sqrt(e$values)) %*% t(e$vectors)
    q <- r %*% (sqrt(y) * theta)
    s <- rbind(cbind(s, q), cbind(t(q), 1))
  }
  s
}

sample_correlation_uniform <- sample_correlation_onion

#sample_correlation_realistic

#sample_correlation_sis
