# names for data sources
source_info <- tibble(
  file = c(
    "columbia.rds",
    "medicare1993-3.rds",
    "medicare1993-5.rds",
    "scthans1998-2008.rds",
    "michigan.rds",
    "stanford2013.rds",
    "columbia2013.rds",
    "mimic-iii.rds",
    "namcs2011-2015-3.rds",
    "namcs2011-2015-5.rds",
    "vaers2012-2016.rds"
  ),
  source = c(
    "columbia",
    "medicare1993-3",
    "medicare1993-5", 
    "scthans1998-2008",
    "michigan",
    "stanford2013",
    "columbia2013",
    "mimic-iii",
    "namcs2011-2015-3",
    "namcs2011-2015-5", 
    "vaers2012-2016"
  ),
  abbr = c(
    "Columbia", "MedPAR(3)", "MedPAR(5)", "Sct.Hans",
    "Michigan", "Stanford", "Columbia*",
    "MIMIC", "NAMCS(3)", "NAMCS(5)", "VAERS"
  ),
  ontology = c(
    "Rzhetsky", "ICD9-3", "ICD9-5", "ICD10-3", "ICD9-5", "Rzhetsky", "Rzhetsky",
    "ICD9-5", "ICD9-3", "ICD9-5", "VAERS"
  )
) %>%
  mutate_all(forcats::fct_inorder)

# names and features of binary association measures
measure_info <- tibble(
  measure = c(
    "unit",
    "oddsratio",
    "oddsratiommle",
    "phi",
    "forbes",
    "rho_psych",
    "rho_bpe",
    "jaccard"
  ),
  abbr = c("unit", "OR", "OR-MMLE", "phi", "F", "r[t]", "r[t]-BP", "J"),
  abbr_tex = c(
    "$1$",
    "$OR$", "$\\widehat{OR}$",
    "$\\phi$",
    "$F$",
    "$r_{t}$", "$\\widehat{r_{t}}$",
    "$J$"
  ),
  zero = c(-Inf, 1, 1, 0, 1, 0, 0, NA_real_)
) %>%
  mutate_if(is.character, forcats::fct_inorder)

# list of node subsets under each ontology
map_info <- tribble(
  ~ontology, ~map
  , "ICD9-5", "ICD9-3"
  , "ICD9-5", "Rzhetsky"
  #, "ICD9-5", "CCS-single"
  #, "ICD9-5", "CCS-multi"
)

# disorders included in NAMCS to be used in JDMs
namcs_disorders <- tibble(
  namcs = c(
    "arthrtis", "asthma", "cancer", "cebvd", "chf", "copd", "deprn",
    "diabetes", "hyplipid", "htn", "ihd", "obesity", "ostprsis"
  ),
  disorder = c(
    "arthritis", "asthma", "cancer", "cerebrovascular disease",
    "congestive heart failure", "chronic obstructive pulmonary disease",
    "depression", "diabetes", "hyperlipidemia", "hypertension",
    "ischemic heart disease", "obesity", "osteoporosis"
  ),
  abbr = c(
    "arthritis", "asthma", "cancer", "CVD", "CHF", "COPD", "depression",
    "DM", "HLD", "HT", "IHD", "obesity", "OP"
  )
) %>%
  mutate_all(forcats::fct_inorder)

# global statistics
stat_info <- tribble(
  ~statistic, ~stat_abbr,
  "connected_prop", "LCP",
  "density", "$\\delta$",
  "degree_mean", "$\\overline{k}$",
  "degree_gini", "$G$",
  "degree_param1", "$\\hat{\\mu}$",
  "degree_param2", "$\\hat{\\sigma}$",
  "degree_assortativity", "$r$",
  "triad_closure", "$C$",
  "distance_mean", "$\\overline{\\ell}$",
  #"modularity_fg", "$Q_f$",
  "modularity_wt", "$Q$"
)
