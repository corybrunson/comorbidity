library(igraph)

# http://yaroslavvb.com/papers/bapat-simple.pdf
resistance_dist_some <- function(graph, from = V(graph), to = V(graph)) {
  from <- as.numeric(from)
  to <- as.numeric(to)
  L <- laplacian_matrix(graph)
  Omega <- matrix(NA, nrow = vcount(graph), ncol = vcount(graph))
  for (i in from) {
    Li <- L[-i, -i]
    for (j in to) {
      Lij <- L[-c(i, j), -c(i, j)]
      Omega[i, j] <- Matrix::det(Lij) / Matrix::det(Li)
    }
  }
  Omega
}

# http://mathworld.wolfram.com/ResistanceDistance.html
resistance_dist_all <- function(graph) {
  L <- laplacian_matrix(graph)
  Gamma <- L + 1 / vcount(graph)
  Gamma_inv <- solve(Gamma)
  Omega <- matrix(diag(Gamma_inv), nrow = vcount(graph), ncol = vcount(graph)) +
    t(matrix(diag(Gamma_inv), nrow = vcount(graph), ncol = vcount(graph))) -
    2 * Gamma_inv
  Omega
}

inverse_min_cut <- function(graph) {
  1 / sapply(V(graph), function(i) sapply(V(graph), function(j) {
    if (i == j) Inf else {
      igraph::min_cut(graph, source = i, target = j)
    }
  }))
}
