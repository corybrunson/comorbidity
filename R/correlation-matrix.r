# v1, v2, n, n1, n2, n12
contingency_data <- function(x) {
  if (is.matrix(x)) x <- as_tibble(x, rownames = NULL)
  vars <- if (is.null(names(x))) as.factor(1:ncol(x)) else fct_inorder(names(x))
  combn(ncol(x), 2) %>%
    apply(MARGIN = 2, FUN = function(ij) {
      c(
        ij,
        nrow(x), sum(x[, ij[1]]), sum(x[, ij[2]]),
        sum(apply(x[, ij], 1, all))
      )
    }) %>%
    t() %>% as_tibble() %>% setNames(c("v1", "v2", "n", "n1", "n2", "n12")) %>%
    mutate(v1 = vars[v1], v2 = vars[v2])
}

# https://onlinecourses.science.psu.edu/stat505/node/41
pairwise_data <- function(x, z = 2) {
  n <- nrow(x)
  if (is.matrix(x)) x <- as_tibble(x, rownames = NULL)
  vars <- if (is.null(names(x))) as.factor(1:ncol(x)) else fct_inorder(names(x))
  v <- length(vars)
  d <- contingency_data(x) %>%
    rowwise() %>%
    # p-values from Fisher's exact test
    mutate(pval = fisher.test(
      matrix(c(n12, n1 - n12, n2 - n12, n - n1 - n2 + n12), nrow = 2, ncol = 2)
    )$p.value) %>%
    # tetrachoric correlation coefficient via the 'psych' package
    mutate(rho = suppressMessages(psych::tetrachoric(matrix(
      c(n - n1 -  n2 + n12, n1 - n12, n2 - n12, n12),
      2, 2
    ))$rho)) %>%
    ungroup()
  # correlation matrx and confidence interval via Fisher's transformation
  # NO NEED TO BREAK THE PIPELINE HERE
  R_s <- d %>%
    select(v1, v2, rho) %>%
    bind_rows(select(., v1 = v2, v2 = v1, rho)) %>%
    arrange(v2) %>%
    spread(v2, rho) %>%
    arrange(v1) %>%
    select(-v1) %>%
    as.matrix() %>% unname()
  diag(R_s) <- 1
  Z_s <- .5 * log((1 + R_s) / (1 - R_s))
  dp <- tibble(
    v1 = factor(rep(vars, times = v), levels = vars),
    v2 = factor(rep(vars, each = v), levels = vars),
    #rho = as.vector(R_s),
    rho_Z = as.vector(Z_s)
  )
  # augment with confidence intervals
  d <- d %>%
    left_join(dp, by = c("v1", "v2")) %>%
    mutate(
      rho_Z_lower = rho_Z - z / sqrt(n - 3),
      rho_Z_upper = rho_Z + z / sqrt(n - 3),
      rho_lower = (exp(2 * rho_Z_lower) - 1) / (exp(2 * rho_Z_lower) + 1),
      rho_upper = (exp(2 * rho_Z_upper) - 1) / (exp(2 * rho_Z_upper) + 1)
    ) %>%
    select(-contains("_Z_"))
  # return data
  d
}

pairwise_data2 <- function(x, z = 2) {
  if (is.matrix(x)) x <- as_tibble(x, rownames = NULL)
  vars <- if (is.null(names(x))) as.factor(1:ncol(x)) else fct_inorder(names(x))
  d <- contingency_data(x) %>%
    rowwise() %>%
    # p-values from Fisher's exact test
    mutate(pval = fisher.test(
      matrix(c(n12, n1 - n12, n2 - n12, n - n1 - n2 + n12), nrow = 2, ncol = 2)
    )$p.value) %>%
    # Bonett-Price approximation and confidence interval for tetrachoric rho
    mutate(
      or_bp = n12 / pmax(n1 - n12, n2 - n12) *
        (n - n1 - n2 + n12) / pmin(n1 - n12, n2 - n12),
      c_bp = (
        1 - abs(n1 - n2) / n / 5 -
          (.5 - pmin(n1, n2, n - n1, n - n2) / n) ^ 2
      ) / 2,
      rho_bp = cos(pi / (1 + or_bp ^ c_bp)),
      rho_se_bp = sqrt(
        1 / n12 + 1 / (n1 - n12) + 1 / (n2 - n12) + 1 / (n - n1 - n2 + n12)
      ),
      or_lower_bp = exp(log(or_bp) - z * rho_se_bp),
      or_upper_bp = exp(log(or_bp) + z * rho_se_bp),
      rho_lower_bp = cos(pi / (1 + or_lower_bp ^ c_bp)),
      rho_upper_bp = cos(pi / (1 + or_upper_bp ^ c_bp))
    ) %>%
    ungroup()
}

pairwise_matrix <- function(d) {
  vars <- if (is.factor(d$v1)) {
    levels(d$v1)
  } else {
    d %>%
      select(v1, v2) %>%
      unlist() %>% unique()
  }
  m <- d %>%
    select(v1, v2, rho) %>%
    bind_rows(select(., v1 = v2, v2 = v1, rho)) %>%
    arrange(v2) %>%
    spread(v2, rho) %>%
    arrange(v1) %>%
    select(-v1) %>%
    as.matrix() %>% unname()
  diag(m) <- 1
  rownames(m) <- colnames(m) <- vars
  m
}

partial_data <- function(d, z = 2, use.shrinkage = FALSE) {
  n <- unique(d$n)
  vars <- if (is.factor(d$v1)) {
    levels(d$v1)
  } else {
    d %>%
      select(v1, v2) %>%
      unlist() %>% unique()
  }
  v <- length(vars)
  # (pairwise) correlation matrix
  R_s <- d %>%
    select(v1, v2, rho) %>%
    bind_rows(select(., v1 = v2, v2 = v1, rho)) %>%
    arrange(v2) %>%
    spread(v2, rho) %>%
    arrange(v1) %>%
    select(-v1) %>%
    as.matrix() %>% unname()
  diag(R_s) <- 1
  # warn if the sample correlation matrix is not positive-definite
  eigs <- eigen(R_s)
  if (min(eigs$values) < median(abs(R_s)) * 2^(-6)) {
    warning("Sample correlation matrix is far from positive definite.")
  }
  # partial correlation matrix
  if (use.shrinkage) {
    R_p <- corpcor::cor2pcor(corpcor::cor.shrink(R_s))
  } else {
    #R_p <- psych::partial.r(R_s)
    R_p <- -(solve(R_s))
    diag(R_p) <- 1 / (1 - psych::smc(R_s))
    R_p <- cov2cor(R_p)
  }
  Z_p <- .5 * log((1 + R_p) / (1 - R_p))
  dp <- tibble(
    v1 = factor(rep(vars, times = ncol(R_p)), levels = vars),
    v2 = factor(rep(vars, each = nrow(R_p)), levels = vars),
    rho_partial = as.vector(R_p),
    rho_partial_Z = as.vector(Z_p)
  )
  # augment with confidence intervals
  d <- d %>%
    left_join(dp, by = c("v1", "v2")) %>%
    mutate(
      rho_partial_Z_lower = rho_partial_Z - z / sqrt(n - 3 - (v - 2)),
      rho_partial_Z_upper = rho_partial_Z + z / sqrt(n - 3 - (v - 2)),
      rho_partial_lower = (exp(2 * rho_partial_Z_lower) - 1) /
        (exp(2 * rho_partial_Z_lower) + 1),
      rho_partial_upper = (exp(2 * rho_partial_Z_upper) - 1) /
        (exp(2 * rho_partial_Z_upper) + 1)
    ) %>%
    select(-contains("_Z_"))
  # return data
  d
}

partial_data_blocks <- function(d, blocks = NULL, z = 2) {
  n <- unique(d$n)
  vars <- if (is.factor(d$v1)) {
    levels(d$v1)
  } else {
    d %>%
      select(v1, v2) %>%
      unlist() %>% unique()
  }
  v <- length(vars)
  # partial correlation matrix
  R_s <- d %>%
    select(v1, v2, rho) %>%
    bind_rows(select(., v1 = v2, v2 = v1, rho)) %>%
    arrange(v2) %>%
    spread(v2, rho) %>%
    arrange(v1) %>%
    select(-v1) %>%
    as.matrix() %>% unname()
  diag(R_s) <- 1
  if (is.null(blocks)) {
    R_p <- psych::partial.r(R_s)
    Z_p <- .5 * log((1 + R_p) / (1 - R_p))
    se_p <- matrix(1 / sqrt(n - 3 - (v - 2)), nrow = v, ncol = v)
  } else {
    stopifnot(length(blocks) == v)
    R_p <- diag(v)
    Z_p <- .5 * log((1 + R_p) / (1 - R_p))
    se_p <- diag(Inf, nrow = v)
    for (b in unique(blocks)) {
      i <- which(blocks == b)
      if (length(i) == 1) next
      R_p[i, i] <- psych::partial.r(R_s[i, i])
      Z_p[i, i] <- .5 * log((1 + R_p[i, i]) / (1 - R_p[i, i]))
      se_p[i, i] <- 1 / sqrt(n - 3 - (length(i) - 2))
    }
    if (length(unique(blocks)) > 1) {
      R_bp <- diag(v)
      ijs <- combn(unique(blocks), 2)
      for (k in 1:ncol(ijs)) {
        i <- which(blocks == ijs[1, k])
        j <- which(blocks == ijs[2, k])
        R_bp[c(i, j), c(i, j)] <- psych::partial.r(R_s[c(i, j), c(i, j)])
        R_p[i, j] <- R_bp[i, j]
        Z_p[i, j] <- .5 * log((1 + R_p[i, j]) / (1 - R_p[i, j]))
        se_p[i, j] <- 1 / sqrt(n - 3 - (length(c(i, j)) - 2))
        R_p[j, i] <- R_bp[j, i]
        Z_p[j, i] <- .5 * log((1 + R_p[j, i]) / (1 - R_p[j, i]))
        se_p[j, i] <- 1 / sqrt(n - 3 - (length(c(i, j)) - 2))
      }
    }
  }
  # organize into a tibble
  dp <- tibble(
    v1 = factor(rep(vars, times = ncol(R_p)), levels = vars),
    v2 = factor(rep(vars, each = nrow(R_p)), levels = vars),
    rho_partial = as.vector(R_p),
    rho_partial_Z = as.vector(Z_p),
    rho_partial_se = as.vector(se_p)
  )
  # augment pairwise data with partial correlation data
  d <- d %>%
    left_join(dp, by = c("v1", "v2")) %>%
    mutate(
      rho_partial_Z_lower = rho_partial_Z - z * rho_partial_se,
      rho_partial_Z_upper = rho_partial_Z + z * rho_partial_se,
      rho_partial_lower = (exp(2 * rho_partial_Z_lower) - 1) /
        (exp(2 * rho_partial_Z_lower) + 1),
      rho_partial_upper = (exp(2 * rho_partial_Z_upper) - 1) /
        (exp(2 * rho_partial_Z_upper) + 1)
    ) %>%
    select(-contains("_Z_"), -rho_partial_se)
  # return data
  d
}

partial_matrix <- function(d) {
  vars <- if (is.factor(d$v1)) {
    levels(d$v1)
  } else {
    d %>%
      select(v1, v2) %>%
      unlist() %>% unique()
  }
  m <- d %>%
    select(v1, v2, rho_partial) %>%
    bind_rows(select(., v1 = v2, v2 = v1, rho_partial)) %>%
    arrange(v2) %>%
    spread(v2, rho_partial) %>%
    arrange(v1) %>%
    select(-v1) %>%
    as.matrix() %>% unname()
  diag(m) <- 1
  rownames(m) <- colnames(m) <- vars
  m
}

pairwise_network <- function(d) {
  v <- d %>%
    select(v1, v2) %>%
    unlist() %>% unique()
  d %>%
    dplyr::filter(rho_lower > 0 | rho_upper < 0) %>%
    select(1:2, weight = rho) %>%
    igraph::graph_from_data_frame(directed = FALSE, vertices = v) %>%
    tidygraph::as_tbl_graph()
}

partial_network <- function(d) {
  v <- d %>%
    select(v1, v2) %>%
    unlist() %>% unique()
  d %>%
    dplyr::filter(rho_partial_lower > 0 | rho_partial_upper < 0) %>%
    select(1:2, weight = rho_partial) %>%
    igraph::graph_from_data_frame(directed = FALSE, vertices = v) %>%
    tidygraph::as_tbl_graph()
}
