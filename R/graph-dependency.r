library(dplyr)
library(igraph)

adjacency_to_pairs <- function(x) {
  wh <- which(as.logical(x))
  cbind(
    (wh - 1L) %% nrow(x) + 1L,
    ceiling(wh / nrow(x)),
    x[wh]
  )
}

dependency_cube_matrix <- function(graph, verbose = TRUE) {
  # shortest path length matrix
  L <- distances(graph, weights = NA)
  suppressWarnings(storage.mode(L) <- "integer")
  d <- max(setdiff(as.vector(L), NA))
  # shortest path count matrix
  if (verbose) message("Calculating path count matrix...")
  A <- as_adj(graph)
  M <- matrix(NA_integer_, vcount(graph), vcount(graph))
  p <- 0
  P <- diag(vcount(graph))
  peb <- dplyr::progress_estimated(d + 1, 2)
  while (p <= d) {
    M[which(L == p)] <- as.integer(P[which(L == p)])
    p <- p + 1
    P <- P %*% A
    peb$tick()$print()
  }
  diag(M) <- 0L
  # brokerage data (increase efficiency by using 'apply' on two dimensions?)
  if (verbose) message("Generating brokerage dataset...")
  bs <- which(apply(A, 1, sum) > 1)
  peb <- dplyr::progress_estimated(length(bs), 2)
  B <- list()
  for (b in bs) {
    peb$tick()$print()
    S <- (log(exp(L[, b, drop = FALSE]) %*% exp(L[b, , drop = FALSE])) == L)
    if (!any(S[-b, -b], na.rm = TRUE)) next
    N <- (M[, b, drop = FALSE] %*% M[b, , drop = FALSE])
    B <- c(B, list(cbind(b, adjacency_to_pairs(N * S))))
  }
  B <- do.call(rbind, B)
  # format result
  B %>% as_tibble() %>%
    setNames(c("broker", "sender", "receiver", "broker_count")) %>%
    left_join(
      L %>% adjacency_to_pairs() %>% as_tibble() %>%
        setNames(c("sender", "receiver", "geodesic_length"))
    ) %>%
    left_join(
      M %>% adjacency_to_pairs() %>% as_tibble() %>%
        setNames(c("sender", "receiver", "geodesic_count"))
    ) %>%
    mutate(
      broker_prop = broker_count / geodesic_count,
      sender = vertex_attr(graph, "name", sender),
      receiver = vertex_attr(graph, "name", receiver),
      broker = vertex_attr(graph, "name", broker),
      geodesic_length = as.integer(geodesic_length),
      geodesic_count = as.integer(geodesic_count),
      broker_count = as.integer(broker_count)
    )
}

dependency_cube_matrix_write <- function(
  graph, file, incr = 10e6, verbose = TRUE
) {
  if (file.exists(file)) {
    stop("File ", file, " already exists.")
  }
  # shortest path length matrix
  L <- distances(graph, weights = NA)
  L_tibble <- L %>% adjacency_to_pairs() %>% as_tibble() %>%
    setNames(c("sender", "receiver", "geodesic_length"))
  suppressWarnings(storage.mode(L) <- "integer")
  d <- max(setdiff(as.vector(L), NA))
  # shortest path count matrix
  if (verbose) message("Calculating path count matrix...")
  A <- as_adj(graph)
  M <- matrix(NA_integer_, vcount(graph), vcount(graph))
  p <- 0
  P <- diag(vcount(graph))
  peb <- dplyr::progress_estimated(d + 1, 2)
  while (p <= d) {
    M[which(L == p)] <- as.integer(P[which(L == p)])
    p <- p + 1
    P <- P %*% A
    peb$tick()$print()
  }
  diag(M) <- 0L
  M_tibble <- M %>% adjacency_to_pairs() %>% as_tibble() %>%
    setNames(c("sender", "receiver", "geodesic_count"))
  # brokerage data (increase efficiency by using 'apply' on two dimensions?)
  if (verbose) message("Generating brokerage dataset...")
  bs <- which(apply(A, 1, sum) > 1)
  peb <- dplyr::progress_estimated(length(bs), 2)
  listB <- list()
  lenB <- 0
  for (b in bs) {
    peb$tick()$print()
    S <- (log(exp(L[, b, drop = FALSE]) %*% exp(L[b, , drop = FALSE])) == L)
    if (!any(S[-b, -b], na.rm = TRUE)) next
    N <- (M[, b, drop = FALSE] %*% M[b, , drop = FALSE])
    B <- setNames(data.frame(b, adjacency_to_pairs(N * S)),
                  c("broker", "sender", "receiver", "broker_count"))
    listB <- c(listB, list(B))
    lenB <- lenB + nrow(B)
    if (lenB >= incr) {
      listB %>% bind_rows() %>% as_tibble() %>%
        left_join(L_tibble) %>%
        left_join(M_tibble) %>%
        select(
          sender, broker, receiver,
          geodesic_length, geodesic_count, broker_count
        ) %>%
        mutate_all(as.integer) %>%
        write.table(
          file = file, append = file.exists(file), sep = ",",
          row.names = FALSE, col.names = !file.exists(file)
        )
      listB <- list()
      lenB <- 0
    }
  }
}

dependency_cube_crawl <- function(graph, weights = NA, verbose = FALSE) {
  l <- list()
  # by connected components
  comp <- components(graph)
  if (verbose) {
    message("Component sizes:")
    print(as.data.frame(table(Size = comp$csize)))
  }
  if (all(comp$csize == 1)) return(tibble(
    s = character(0), r = character(0), b = character(0),
    m = integer(0), l = integer(0), n = integer(0), delta = numeric(0)
  ))
  peb <- dplyr::progress_estimated(sum(sapply(comp$csize, choose, k = 2)), 2)
  for (i in which(comp$csize > 1)) {
    comp_vs <- which(comp$membership == i)
    for (j in 1:(length(comp_vs) - 1)) for (k in (j + 1):length(comp_vs)) {
      peb$tick()$print()
      s = comp_vs[j]
      r = comp_vs[k]
      if (verbose) message("Component ", i, ", nodes ", s, " and ", r, ".")
      if (are_adjacent(graph, s, r)) next
      asp <- all_shortest_paths(
        graph, from = s, to = r,
        weights = weights
      )$res %>%
        do.call(what = rbind) %>% unname() %>%
        .[, 2:(ncol(.) - 1), drop = FALSE] %>%
        # m = number of shortest paths from s to r
        # l = length of shortest paths from s to r
        # n = number of shortest paths from s to r via b
        (function(x) data.frame(b = as.vector(x), m = nrow(x), l = ncol(x))) %>%
        group_by(b, m, l) %>% summarize(n = n()) %>% ungroup() %>%
        cbind(
          s = vertex_attr(graph, "name")[s],
          r = vertex_attr(graph, "name")[r]
        )
      l <- c(l, list(asp))
    }
  }
  l %>% bind_rows() %>% as_tibble() %>%
    mutate(b = vertex_attr(graph, "name", b)) %>%
    select(s, r, b, m, l, n) %>%
    mutate(delta = n / m)
}

if (FALSE) {
  data(karate, package = "igraphdata")
  dc1 <- dependency_cube_matrix(karate)
  dc2 <- dependency_cube_crawl(karate)
  dc <- dplyr::full_join(
    dc1, dc2,
    by = c("sender" = "s", "receiver" = "r", "broker" = "b")
  )
  library(ggplot2)
  ggplot(dc, aes(x = delta, y = broker_prop)) + geom_point()
  rbenchmark::benchmark(
    dependency_cube_matrix(karate),
    dependency_cube_crawl(karate),
    replications = 10
  )
  rbenchmark::benchmark(
    suppressMessages(dependency_cube_matrix(karate)),
    suppressMessages(lapply(V(karate), all_shortest_paths, graph = karate)),
    replications = 10
  )
}
