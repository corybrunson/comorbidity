library(igraph)
library(dplyr)

graph_from_pairs_data <- function(
  data,
  measure = NULL, measure_cutoff = NULL, measure_quantile = NULL,
  pval_cutoff = NULL, pval_correction = "none",
  weighted = FALSE, transformation = identity, negatives = FALSE
) {
  
  if (nrow(unique(data[, 1:2])) < nrow(data)) {
    warning(
      "Link data with duplicates passed to `graph_from_pairs_data.",
      immediate. = TRUE
    )
  }
  
  pval_correction <- match.arg(pval_correction, c("none", "fwer", "fdr"))
  
  nodes <- as.character(unique(unlist(data[, 1:2])))
  n_pairs <- choose(length(nodes), 2)
  
  if (is.null(pval_cutoff)) pval_cutoff <- 1
  if (pval_correction == "fwer") {
    # Bonferroni correction for family-wise error rate
    pval_cutoff <- pval_cutoff / n_pairs
  } else if (pval_correction == "fdr") {
    # Benjamini-Hochberg correction for false discovery rate
    # assume missing pairs have larger p-values than included pairs
    # err on the side of including links; use maximum cutoff to resolve ties
    pval_rank <- rank(data$pval_fisher, ties.method = "max")
    pval_cutoff <- (1:nrow(data) / n_pairs * pval_cutoff)[pval_rank]
  }
  if (is.character(measure)) measure <- data[[measure]]
  if (weighted) data$weight <- transformation(measure)
  
  rows <- TRUE
  rows <- rows & (data$pval_fisher < pval_cutoff)
  if (!negatives) {
    rows <- rows & (data$n12 > data$n1 / data$n * data$n2)
  }
  if (!is.null(measure)) {
    if (!is.null(measure_quantile)) {
      mc <- quantile(measure[rows], measure_quantile)
      rows <- rows & (measure > mc)
    }
    if (!is.null(measure_cutoff)) {
      rows <- rows & (measure > measure_cutoff)
    }
  }
  
  cols <- c(
    1:2,
    if (weighted) grep("^weight$", names(data)) else NULL,
    grep("^pval_fisher$", names(data))
  )
  
  graph_from_data_frame(
    d = data[rows, cols],
    directed = FALSE,
    vertices = nodes
  )
}
