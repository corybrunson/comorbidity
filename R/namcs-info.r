# disorders included in NAMCS to be used in JDMs
namcs_disorders <- tibble(
  code = c(
    "arthrtis", "asthma", "cancer", "cebvd", "chf", "copd", "deprn",
    "diabetes", "hyplipid", "htn", "ihd", "obesity", "ostprsis"
  ),
  name = c(
    "arthritis", "asthma", "cancer", "cerebrovascular disease",
    "congestive heart failure", "chronic obstructive pulmonary disease",
    "depression", "diabetes", "hyperlipidemia", "hypertension",
    "ischemic heart disease", "obesity", "osteoporosis"
  ),
  abbr = c(
    "arthritis", "asthma", "cancer", "CVD", "CHF", "COPD", "depression",
    "DM", "HLD", "HT", "IHD", "obesity", "OP"
  )
) %>%
  mutate_all(forcats::fct_inorder)

namcs_demographics <- tibble(
  name = c(
    paste0("age_", c("0_14", "15_24", "25_44", "45_64", "65_74")),
    "male",
    "asian", "black", "native", "white", "hispanic",
    "private", "medicare", "medicaid",
    "midwest", "south", "west",
    "metro"
  ),
  abbr = c(
    c("0-14", "15-24", "25-44", "45-64", "65-74"),
    "M",
    "Asian", "Black", "Native", "White", "Hispanic",
    "Private", "Medicare", "Medicaid",
    "Midwest", "South", "West",
    "Metro"
  )
) %>%
  mutate_if(is.character, forcats::fct_inorder)
