
param_vals <- list(
  source = as.character(source_info$source)[1:8],
  pval = 10 ^ (-(1:6)),
  correction = c("none", "fwer", "fdr"),
  measure = c("oddsratiommle", "phi", "forbes", "rho_psych"),
  threshold = list(
    oddsratiommle = c(1, 2, 6, 60),
    phi = c(0, .005, .05, .2),
    forbes = c(1, 2, 6, 60),
    rho_psych = c(0, .1, .4, .6)
  )
)
