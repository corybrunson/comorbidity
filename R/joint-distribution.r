# y = binary matrix of endogenous variables
# x = binary matrix of exogenous variables
# z = number of standard deviations
# remaining variables: JDM
jdm_data <- function(
  y, x = NULL,
  z,
  # df = 0 for max variability; df++ to improve convergence
  df_extra = 1,
  n.chains = n_chains, n.iter = n_iter, n.burnin = floor(n.iter / 2),
  n.thin = max(1, floor((n.iter - n.burnin) / 1000)),
  DIC = FALSE, ...
) {
  
  # ensure that necessary packages are installed and attached
  stopifnot(require(tidyverse))
  stopifnot(file.exists(here::here("src", "jdm.txt")))
  
  # restrict to subjects with occurrences
  w <- which(rowSums(y) > 0)
  stopifnot(ncol(y) > 1 & length(w) > 1)
  Y <- as.matrix(y)[w, ]
  n <- nrow(Y); m <- ncol(Y)
  
  # subject-level intercepts (occurrence rates)
  vars <- if (is.null(names(y))) 1:ncol(y) else fct_inorder(names(y))
  X <- if (is.null(x)) {
    matrix(1, nrow = n, ncol = 1)
  } else {
    x <- as.matrix(x)
    cbind(constant = 1, x)[w, ]
  }
  # remove exogenous constants
  X <- X[, c(1, which(apply(X, 2, n_distinct) > 1)), drop = FALSE]
  q <- ncol(X)
  
  ID <- diag(m)
  df <- m + df_extra
  
  # initialization function
  inits <- function() {
    Tau <- MCMCpack::rwish(df, ID)
    Sigma <- solve(Tau)
    Z <- abs(t(replicate(n, MASS::mvrnorm(1, rep(0, m), Sigma))))
    Z <- ifelse(as.matrix(Y), Z, -1 * Z)
    Sigma <- mclust::mvnXXX(Z)$parameters$variance$sigma[, , 1]
    Beta <- t(sapply(seq_len(ncol(Y)), function(j) {
      unname(coef(glm(
        Y[, j] ~ X + 0,
        family = binomial(link = probit)
      )))
    }))
    if (ncol(X) == 1) Beta <- t(Beta)
    Beta.raw <- Beta * sqrt(diag(Sigma))
    mu.raw <- apply(Beta.raw, 2, mean)
    sigma.raw <- apply(Beta.raw, 2, sd)
    return(list(
      Tau = solve(Sigma),
      Z = Z,
      Beta.raw = Beta.raw,
      mu.raw = mu.raw,
      sigma.raw = sigma.raw
    ))
  }
  
  # call JAGS
  fits <- R2jags::jags(
    data = list(Y = Y, X = X, n = n, m = m, q = q, ID = ID, df = df),
    inits,
    parameters.to.save = c("Beta.raw", "Tau"),
    model.file = here::here("src", "jdm.txt"),
    n.chains = n.chains, n.iter = n.iter, n.burnin = n.burnin,
    n.thin = n.thin,
    DIC = DIC
  )
  
  # attach model fits
  R2jags::attach.jags(fits, overwrite = TRUE)
  # covariance matrices
  Sigma2 <- apply(Tau, 1, solve)
  dim(Sigma2) <- rev(dim(Tau))
  Sigma2 <- aperm(Sigma2, c(3, 2, 1))
  # correlation matrices
  Rho <- apply(Sigma2, 1, cov2cor)
  dim(Rho) <- rev(dim(Sigma2))
  Rho <- aperm(Rho, c(3, 2, 1))
  # normalized coefficients
  dim(Beta.raw) <- c(dim(Beta.raw)[1], dim(Beta.raw)[2], q)
  Beta <- apply(Beta.raw, 3, function(x) x / t(sqrt(apply(Sigma2, 1, diag))))
  dim(Beta) <- dim(Beta.raw)
  # mean occurrence rates
  Mu <- array(dim = c(n.sims, n, m))
  for (s in seq_len(n.sims)) {
    for (i in seq_len(n)) {
      for (j in seq_len(m)) {
        Mu[s, i, j] <- Beta.raw[s, j, ] %*% X[i, ] 
      }
    }
  }
  if (q > 1) {
    # correlation due to covariates
    EnvRho <- apply(Beta, 1, function(x) {
      matrix(rowSums(apply(cbind(x[, -1]), 2, function(y) outer(y, y))), m)
    })
    dim(EnvRho) <- rev(dim(Sigma2))
    EnvRho <- aperm(EnvRho, c(3, 2, 1))
    covX <- cov(X)
    for (s in seq_len(n.sims)) {
      for (i in seq_len(m)) {
        for (j in seq_len(m)) {
          EnvRho[s, i, j] <-
            (EnvRho[s, i, j] +
               sum(sapply(seq_len(q)[-1], function(k) {
                 sum(Beta[s, i, k] * 
                       Beta[s, j, seq_len(q)[-c(1, k)]] * 
                       covX[k, seq_len(q)[-c(1, k)]]
                 )
               }))
            )
        }
      }
    }
    EnvRho <- apply(EnvRho, 1, cov2cor)
    dim(EnvRho) <- rev(dim(Sigma2))
    EnvRho <- aperm(EnvRho, c(3, 2, 1))
  }
  # detach model fits
  R2jags::detach.jags()
  
  # posterior settings
  alpha <- (1 - pnorm(z)) * 2
  upper_tri <- upper.tri(ID)
  # posterior Rho data
  Rho_tbl <- tibble(
    v1 = vars[(which(upper_tri) - 1) %% m + 1],
    v2 = vars[rep(2:m, times = 2:m - 1)],
    rho = apply(Rho, 2:3, mean)[upper_tri],
    rho_sd = apply(Rho, 2:3, sd)[upper_tri],
    rho_lower = apply(Rho, 2:3, function(x) {
      quantile(x, probs = alpha / 2)
    })[upper_tri],
    rho_upper = apply(Rho, 2:3, function(x) {
      quantile(x, probs = 1 - alpha / 2)
    })[upper_tri]
  )
  # posterior Beta data
  make_Beta_tbl <- function(fun, name) {
    set_names(
      as_tibble(apply(Beta, 2:3, fun)[, , drop = TRUE]),
      str_c("beta", name, if (ncol(X) > 1) colnames(X), sep = "_")
    )
  }
  Beta_tbl <- bind_cols(
    var = vars,
    make_Beta_tbl(mean, NULL),
    make_Beta_tbl(sd, "sd"),
    make_Beta_tbl(function(x) quantile(x, probs = alpha / 2), "lower"),
    make_Beta_tbl(function(x) quantile(x, probs = 1 - alpha / 2), "upper")
  )
  # posterior Mu data
  make_Mu_tbl <- function(fun, name) {
    set_names(
      as_tibble(apply(Mu, 2:3, fun)[, , drop = TRUE]),
      str_c("mu", name, colnames(Y), sep = "_")
    )
  }
  Mu_tbl <- bind_cols(
    .id = w,
    make_Mu_tbl(mean, NULL),
    make_Mu_tbl(sd, "sd"),
    make_Mu_tbl(function(x) quantile(x, probs = alpha / 2), "lower"),
    make_Mu_tbl(function(x) quantile(x, probs = 1 - alpha / 2), "upper")
  )
  if (q > 1) {
    # posterior environmental Rho data
    EnvRho_tbl <- tibble(
      v1 = vars[(which(upper_tri) - 1) %% m + 1],
      v2 = vars[rep(2:m, times = 2:m - 1)],
      rho = apply(EnvRho, 2:3, mean)[upper_tri],
      rho_sd = apply(EnvRho, 2:3, sd)[upper_tri],
      rho_lower = apply(EnvRho, 2:3, function(x) {
        quantile(x, probs = alpha / 2)
      })[upper_tri],
      rho_upper = apply(EnvRho, 2:3, function(x) {
        quantile(x, probs = 1 - alpha / 2)
      })[upper_tri]
    )
  }
  
  # return tibbles
  res <- list(
    rho = Rho_tbl,
    beta = Beta_tbl,
    mu = Mu_tbl
  )
  if (q > 1) res$rho1 <- EnvRho_tbl
  res
}

jdm_matrix <- function(d) {
  vars <- if (is.factor(d$v1)) {
    levels(d$v1)
  } else {
    d %>%
      select(v1, v2) %>%
      unlist() %>% unique()
  }
  m <- d %>%
    select(v1, v2, rho_joint) %>%
    bind_rows(select(., v1 = v2, v2 = v1, rho_joint)) %>%
    arrange(v2) %>%
    spread(v2, rho_joint) %>%
    arrange(v1) %>%
    select(-v1) %>%
    as.matrix() %>% unname()
  diag(m) <- 1
  rownames(m) <- colnames(m) <- vars
  m
}

jdm_network <- function(d) {
  v <- d %>%
    select(v1, v2) %>%
    unlist() %>% unique()
  d %>%
    dplyr::filter(rho_joint_lower > 0 | rho_joint_upper < 0) %>%
    select(1:2, weight = rho_joint) %>%
    igraph::graph_from_data_frame(directed = FALSE, vertices = v) %>%
    as_tbl_graph()
}
