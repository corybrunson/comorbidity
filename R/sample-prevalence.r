sample_prevalence <- function(n = 1, method = NULL, ...) {
  if (is.null(method)) method <- "normal"
  fun <- get(paste0("sample_prevalence_", method))
  fun(n = n, ...)
}

sample_prevalence_normal <- function(n = 1, p = NULL, mu = NULL, sigma = 1) {
  if (is.null(mu)) {
    if (is.null(p)) stop("Provide either 'p' or 'mu', and not both.")
    mu <- qlogis(p)
  } else {
    if (!is.null(p)) stop("Provide either 'p' or 'mu', and not both.")
  }
  plogis(rnorm(n = n, mean = mu, sd = sigma))
}
