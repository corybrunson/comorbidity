# statistics and p-values from the chi-squared approximation
chisquare_from_ft <- function(data) data %>%
  mutate(
    # measured counts
    a = n12, b = n1 - n12, c = n2 - n12, d = n - n1 - n2 + n12,
    # expected counts
    a_ = n1 / n * n2, b_ = n1 / n * (n - n2),
    c_ = (n - n1) / n * n2, d_ = (n - n1) / n * (n - n2)
  ) %>%
  mutate(
    chisquare = ((a - a_) ^ 2) / a_ + ((b - b_) ^ 2) / b_ +
      ((c - c_) ^ 2) / c_ + ((d - d_) ^ 2) / d_
  ) %>%
  mutate(pval_chisquare = pchisq(chisquare, 1, lower.tail = FALSE)) %>%
  select(-a, -b, -c, -d, -a_, -b_, -c_, -d_, -chisquare)

# chi-squared approximation using the base R function
chisq_from_ft <- function(data) data %>%
  rowwise() %>%
  mutate(pval_chisq = chisq.test(
    matrix(c(n12, n1 - n12, n2 - n12, n - n1 - n2 + n12), 2, 2),
    correct = TRUE
  )$p.value)

# p-values from Fisher's exact test
fisherexact_from_ft <- function(data) data %>%
  rowwise() %>%
  mutate(pval_fisher = fisher.test(
    matrix(c(n12, n1 - n12, n2 - n12, n - n1 - n2 + n12), nrow = 2, ncol = 2)
  )$p.value)

# odds ratios (OR)
oddsratio_from_ft <- function(data) data %>%
  mutate(
    oddsratio = n12 / pmax(n1 - n12, n2 - n12) *
      (n - n1 - n2 + n12) / pmin(n1 - n12, n2 - n12),
    oddsratiommle = (n12 + .5) / (pmax(n1 - n12, n2 - n12) + .5) *
      (n - n1 - n2 + n12 + .5) / (pmin(n1 - n12, n2 - n12) + .5),
    logoddsratio = log(oddsratiommle)
  )

# Pearson's phi
phi_from_ft <- function(data) data %>%
  mutate(
    phi = n / sqrt(n1) / sqrt(n2) / sqrt(n - n1) / sqrt(n - n2) * n12 -
      (pmax(n1, n2) - n12) / sqrt(n1) / sqrt(n2) /
      sqrt(n - n1) / sqrt(n - n2) * (pmin(n1, n2) - n12)
  )

calculate_rho <- function(n, n1, n2, n12) {
  mat <- matrix(
    c(n12, n1 - n12, n2 - n12, n - n1 - n2 + n12),
    ncol = 2
  )
  rho_try <- try(polycor::polychor(mat, ML = FALSE, std.err = TRUE))
  if (class(rho_try) == "try-error") {
    rho_try <- try(polycor::polychor(mat, ML = TRUE, std.err = TRUE))
    if (class(rho_try) == "try-error") {
      rho_try <- list(
        rho = polycor::polychor(mat, ML = FALSE, std.err = FALSE),
        var = NA
      )
    }
  }
  rho <- rho_try$rho
  rho_se <- sqrt(rho_try$var[1])
  data.frame(rho_polycor = rho, rho_se_polycor = rho_se)
}
# Pearson's tetrachoric rho w/ two-sided CI
rho_from_ft <- function(data, z = NULL, alpha = NULL) {
  if (is.null(z)) {
    if (is.null(alpha)) stop("Need 'z' or 'alpha'.")
    z <- qnorm(1 - alpha / 2)
  }
  data %>%
    # 'polycor' package
    #group_by(n, n1, n2, n12) %>%
    #do({
    #  return(data.frame(rho_polycor = NA))
    #  mat <- matrix(
    #    c(.$n - .$n1 - .$n2 + .$n12, .$n1 - .$n12, .$n2 - .$n12, .$n12),
    #    2, 2
    #  )
    #  res <- try(polycor::polychor(mat, ML = FALSE, std.err = TRUE))
    #  if (class(res) == "try-error") {
    #    res <- list(
    #      rho = try(polycor::polychor(mat, ML = FALSE, std.err = FALSE)$rho),
    #      var = NA
    #    )
    #    if (class(res$rho) == "try-error") res$rho <- NA
    #  }
    #  data.frame(rho_polycor = res$rho, rho_se_polycor = sqrt(res$var[1]))
    #}) %>%
    #ungroup() %>%
    rowwise() %>%
    # 'psych' package
    mutate(rho_psych = psych::tetrachoric(matrix(
      c(n - n1 -  n2 + n12, n1 - n12, n2 - n12, n12),
      2, 2
    ))$rho) %>%
    # Bonett-Price approximation and confidence interval for tetrachoric rho
    mutate(
      or_bp = n12 / pmax(n1 - n12, n2 - n12) *
        (n - n1 - n2 + n12) / pmin(n1 - n12, n2 - n12),
      c_bp = (
        1 - abs(n1 - n2) / n / 5 -
          (.5 - pmin(n1, n2, n - n1, n - n2) / n) ^ 2
      ) / 2,
      rho_bp = cos(pi / (1 + or_bp ^ c_bp)),
      rho_se_bp = sqrt(
        1 / n12 + 1 / (n1 - n12) + 1 / (n2 - n12) + 1 / (n - n1 - n2 + n12)
      ),
      or_lower_bp = exp(log(or_bp) - z * rho_se_bp),
      or_upper_bp = exp(log(or_bp) + z * rho_se_bp),
      rho_lower_bp = cos(pi / (1 + or_lower_bp ^ c_bp)),
      rho_upper_bp = cos(pi / (1 + or_upper_bp ^ c_bp)),
      # correction for zero entries
      or_bpe = (n12 + .5) / (pmax(n1 - n12, n2 - n12) + .5) *
        (n - n1 - n2 + n12 + .5) / (pmin(n1 - n12, n2 - n12) + .5),
      c_bpe = (
        1 - abs(n1 - n2) / n / 5 -
          (.5 - pmin(n1, n2, n - n1, n - n2) / n) ^ 2
      ) / 2,
      rho_bpe = cos(pi / (1 + or_bpe ^ c_bpe)),
      rho_se_bpe = sqrt(
        1 / (n12 + .5) + 1 / (n1 - n12 + .5) +
          1 / (n2 - n12 + .5) + 1 / (n - n1 - n2 + n12 + .5)
      ),
      or_lower_bpe = exp(log(or_bpe) - z * rho_se_bpe),
      or_upper_bpe = exp(log(or_bpe) + z * rho_se_bpe),
      rho_lower_bpe = cos(pi / (1 + or_lower_bpe ^ c_bpe)),
      rho_upper_bpe = cos(pi / (1 + or_upper_bpe ^ c_bpe))
    )
}

# Forbes' coefficient of association
forbes_from_ft <- function(data) data %>%
  mutate(forbes = n / pmax(n1, n2) * n12 / pmin(n1, n2))

# Jaccard's coefficient of community
jaccard_from_ft <- function(data) data %>%
  mutate(p1 = n1 / n, p2 = n2 / n) %>%
  mutate(
    jaccard = n12 / (n1 + n2 - n12),
    jaccard_baseline = pmin(p1, p2) /
      (p1 * (1 - p2) + (1 - p1) * p2 + p1 * p2) *
      pmax(p1, p2)
  ) %>%
  select(-p1, -p2)
