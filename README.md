# A reproducibility and sensitivity analysis of comorbidity network analyses

In this project we conduct sensitivity and reproducibility analyses of prototypical results obtained from the study of comorbidity networks.

## Introduction

Since around 2007 several research teams at the intersection of data mining and healthcare have proposed network models of population comorbidity constructed by mining statistical evidence for two- or few-disorder dependencies from large patient-level datasets and aggregating these relations into a mathematical graph. These researchers drew upon their models as aides to manual exploration, for hypothesis generation, and, from a network perspective, to test for associations between the structural positions of disorders and other important features such as genetic overlap, lethality, sequentiality, and ontological position.

Many domains of scientific research, including healthcare, are in the midst of a methodological renaissance impelled by a series of alarming reproducibility studies. These have implicated, among other culprits, the failure to report the ways in which early data explorations induced formal methodological choices.
The numerous forks encountered in the construction of comorbidity networks, coupled with known biases in the collection of healthcare data and population heterogeneity, amplifies these concerns.
However, while the use of comorbidity networks continues to expand, very few studies have reported on the sensitivity of their network analytic results to adjustments in the many parameters governing model construction, and fewer still have evaluated the reproducibility of results across different datasets.

## Functionality

- `utils.r`: Summary information for several sets of objects or choices used in the analysis, including the data source, the ontology, and the association measures.
- `pairs-calculations.r`: Functions used to append comorbidity data frames with values of statistical tests and association measures.
- `graph-construction.r`: A function that takes a comorbidity data frame, which first two columns must contain the disease pairs and which must contain columns for each of the partial sums `n`, `n1`, `n2`, `n12` of a frequency table, and several construction parameters and returns the comorbidity network constructed using those data and parameters.
- `graph-distances.r`: Helper functions for calculating graph distances.

## Analysis sequence

Several analyses scripted in this repository were not ultimately included in the manuscript, though we discuss them in the Supporting Information.
The following sequence of scripts, located in the `scr` folder, perform the analyses reported in the paper:

- `namcs/namcs-scrape-stata.r`: Download the STATA files for the NAMCS data set and pre-process them into a data frame.
- `ccs.r`: Download Clinical Classifications Software ontology data and pre-process it into a data frame.
- `ontology-maps.r`: Assemble several ontologies used by the data sets into a set of crosswalks between them.

The following scripts constitute the sensitivity analysis of pairwise comorbidity network analysis, using different evidential and evaluative thresholds to construct networks:

- `graphs/pairs-extract.r`: Pre-process each provided comorbidity data set (except MIMIC-III) into a standardized comorbidity data frame.
- `graphs/pairs-extract-mimic.r`: Pre-process the MIMIC-III diagnosis data into a standardized comorbidity data frame.
- `graphs/pairs-augment.r`: Augment the comorbidity data frames with values of statistical tests and association measures.
- `graphs/pairs-summary.r`: Calculate density and weight distribution summaries, which don't require graph construction but only comorbidity (pairs) data.
    - `tab/densities.tex` (manuscript)
    - `tab/weights-*.tex`
- `graphs/quantiles.r`: Compare the ranges of quantiles obtained using different evaluative cutoffs for different association measures. (Experiment by changing the values in `measure_cutoff`.) We used the values obtained here in `R/params.r`.
    - `fig/quantile-boxplot.pdf` (SI)
- `graphs/visualizations.r`: Render hairball visualizations of each comorbidity network using a consistent, strict cutoff that produces more interpretable images.
    - `fig/network-viz-*.pdf`
    - `fig/network-ggplot-*.pdf` (SI)
- `graphs/statistics-degseq.r`: Fit several distribution families to the degree sequence data of each comorbidity network and perform likelihood-ratio tests.
    - `data/graphs/graphs-degseq.rds`
- `graphs/statistics-degseq-summary.r`: Render heatmap and write LaTeX table summarizing likelihood-ratio tests.
    - `tab/degseq-compare.tex` (SI text)
    - `fig/degseq-compare.pdf` (SI)
- `graphs/statistics-degseq-tail.r`: Fit three regularly varying (power-law) models to the degree sequence data of each comorbidity network.
- `graphs/statistics-degseq-tailstats.r`: Summarize the results of the tail estimation analysis.
    - `tab/tail-estimation.tex` (SI text)
- `graphs/statistics-calculate.r`: Calculate summary statistics for comorbidity networks constructed using each data set and parameter choices.
    - `fig/fits.pdf`
    - `data/graphs/graphs-summary.rds`
- `graphs/statistics-summary.r`: Analyze the summary statistics calculated above using visualization, multiple (hierarchical) regression, and principal components analysis.
    - `tab/summ.csv` (SI)
    - `fig/summ-versus.pdf` (SI)
    - `fig/summ-pval.pdf`
    - `tab/model1.tex` (SI text)
    - `tab/model2.tex` (manuscript)
    - `tab/model3-stargazer.tex` (SI text)
    - `tab/model3-xtable.tex` (SI text)
    - `tab/model4-stargazer.tex` (SI text)
    - `tab/model4-xtable.tex` (SI text)
    - `tab/aic.tex` (SI text)
    - `fig/pca.pdf`
    - `fig/pca-label.pdf`
    - `fig/pca-label-flip.pdf` (manuscript)
- `graphs/centralities-concordance.r`: Calculate concordances between three centrality rankings on the comorbidity networks constructed using each data set and parameter choices.
    - `data/sensitivity/concordance-data.RData`
- `graphs/centralities-prevalences.r`: Plot centrality versus prevalence for the disorders in the ontology of each data set.
    - `fig/centrality-prevalence.pdf`
- `graphs/centralities-node-group.r`: Compare the most-central disorders in unweighted comorbidity networks constructed using a fixed data set but varying evidential cutoffs.
    - `data/sensitivity/node-centrality.RData`
    - `data/sensitivity/group-centrality.RData`
- `graphs/centralities-kendall.r`: Render heatmaps and biplots of Kendall correlations between node centrality rankings for constructions within a dataset and for datasets within an ontology.
    - `fig/centrality-heatmap-construction.pdf`
    - `tab/centrality-concordance-inertia.tex` (manuscript)
    - `fig/centrality-decomps.pdf`
    - `fig/centrality-biplots.pdf` (SI)
    - `fig/ont-cent.pdf`
    - `fig/centrality-heatmap-dataset-*.pdf`
    - `fig/centrality-biplots-dataset-*.pdf` (SI)
- `graphs/centralities-top.r`: Write LaTeX tables of most-central disorders.
    - `tab/centrality-<centrality>.tex` (manuscript + SI text)
    - `tab/centrality-<centrality>-<ontology>.tex` (SI text)
- `graphs/prevalences.r`: Calculate prevalences of all disorders in the ontology for each data set.
    - `tab/prevalence.tex` (SI text)
    - `fig/prevalences-ICD9-5.pdf` (SI)
    - `fig/prevalences-Rzhetsky.pdf` (SI)

The following scripts constitute the comparative analysis between pairwise and more systems-style network constructions using partial correlations and joint distribution models:

- `multivar/data-css.r`: Download Clinical Classifications Software ontology data and pre-process it into a tibble.
    - `data/ccs-dat.rds`
- `multivar/data-namcs.r`: Format the NAMCS data as a tibble and summarize it.
    - `data/namcs/namcs.rds`
- `multivar/summary-namcs.r`: Provide a basic summary of the NAMCS data and network.
    - `fig/namcs-freqrank.pdf` (SI text)
    - `tab/jdm-coefs.tex` (SI text)
- `multivar/data-namcs-jd.r`: Fit the JDM to the NAMCS data and save the result.
    - `data/namcs/namcs-jd0.rds`
    - `data/namcs/namcs-jd1.rds`
- `multivar/pairs-namcs.r`:
    - `fig/namcs-pairs-alluvial-alluvium.pdf` (SI text)
    - `fig/namcs-pairs-alluvial-flow.pdf`
- `multivar/visualize-namcs.r`: Generate association strength scatterplots and hairball diagrams to compare the pairwise correlation, partial correlation, and joint distribution network models for the NAMCS data.
    - `fig/namcs-scatterplots.pdf`
    - `fig/namcs-pairs.pdf` (manuscript)
    - `fig/namcs-biplots.pdf` (SI)
    - `fig/namcs-circles.pdf` (manuscript)
- `multivar/data-mimic.r`: Read the `DIAGNOSES` tables from the demo and full MIMIC-III databases, partitioned by care unit, and write presence-absence tables for CCS diagnosis groups.
    - `data/mimic/mimic-ccs.rds`
- `multivar/data-mimic-pw-pt.r`: Summarize and analyze the positive, negative, and indiscernible associations produced by the pairwise correlation, partial correlation, and joint distribution network models for the NAMCS data.
    - `data/mimic/mimic-*-pw.rds`
    - `data/mimic/mimic-*-pt.rds`
- `multivar/data-mimic-jd.r`: Construct joint distribution networks for each care unit.
    - `data/mimic/mimic-*-<JDM parameters>-jd.rds`
- `multivar/summary-mimic.r`: Summarize the comorbidity network for each care unit, across the hierarchical interpolation between the pairwise and partial constructions.
    - `fig/mimic-freqrank.pdf` (SI)
    - `fig/mimic-*-freqrank.pdf`
- `multivar/pairs-mimic.r`: Summarize and analyze the positive, negative, and indiscernible associations produced by the pairwise correlation, partial correlation, and joint distribution network models for each unit.
    - `tab/mimic-links.tex` (manuscript)
    - `fig/mimic-pairs-alluvial-flow.pdf`
    - `fig/mimic-pairs-alluvial-alluvium.pdf` (SI text)
- `multivar/visualize-mimic.r`: Generate association strength scatterplots and hairball diagrams to compare the pairwise correlation, partial correlation, and joint distribution network models for each unit.
    - `fig/mimic-*-scatters.pdf`
    - `fig/mimic-*-scatters-jdm.pdf` (manuscript)
    - `fig/mimic-*-networks.pdf` (SI)
- `multivar/centrality-mimic.r`: Compare node centrality rankings between different network models on data from each unit included in MIMIC.
    - `fig/mimic-centrality-biplots.pdf` (SI)

This bash script copies figures and tables generated directly from the above scripts to their numbered names (e.g. "Figure 1") in the manuscript:

- `docs/comorbidity/comorbidity-figs-tabs.sh`: Copy figures and tables from the `fig` and `tab` folders in the root directory into `docs/comorbidity` for inclusion in the manuscript.
